//
// Created by mtaabodi on 7/13/2016.
//

#ifndef TargetGroupCreativeCacheService_H
#define TargetGroupCreativeCacheService_H



#include "TargetGroupCreative.h"
class MySqlDriver;
#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "TargetGroupCreative.h"
class TargetGroupCreativeCacheService;
#include "HttpUtilServiceMock.h"

class TargetGroupCreativeCacheServiceTest  : public ::testing::Test {

private:

public:

    TargetGroupCreativeCacheService* service;
    std::string allEntitiesInJsonReturned;

    std::string allEntitiesInJsonExpected;

    std::unordered_map<int, TargetGroupCreativeMapPtr> allEntitiesExpected;

    std::unordered_map<int, TargetGroupCreativeMapPtr> parseDtoReturnedToMap(std::string json) ;
    void assertBothMapsAreEqual(std::unordered_map<int, TargetGroupCreativeMapPtr> actualEntitiesMap,
                                std::unordered_map<int, TargetGroupCreativeMapPtr> allEntitiesExpected);

    HttpUtilServiceMockPtr httpUtilServiceMock;

    void SetUp();

    void TearDown();

    TargetGroupCreativeCacheServiceTest() ;

    TargetGroupCreativeMapPtr createSampleEntity();

    void givenHttpServiceReturnsJsonData();

    void whenTargetGroupCreativeReloaCachesViaHtppIsIsCalled();

    void thenAllTargetGroupCreativesAreAsExpected();

    void thenBothTargetGroupCreativesAreEqual(TargetGroupCreativeMapPtr actualTgCreativeMap, TargetGroupCreativeMapPtr expectedTgCreativeMap);

    std::string createArrayOfTargetGroupCreatives(TargetGroupCreativeMapPtr targetGroupExpected);

    void givenDataInCache();

    void whenGetAllAsJsonIsCalled();

    void thenAllEntitiesJosnIsCreatedAsExpected();

    virtual ~TargetGroupCreativeCacheServiceTest() ;

};
#endif //EXCHANGESIMULATOR_MYSQLCREATIVESERVICETEST_H
