//
// Created by mtaabodi on 7/13/2016.
//

#ifndef EXCHANGESIMULATOR_MYSQLTARGETGROUPSERVICETEST_H
#define EXCHANGESIMULATOR_MYSQLTARGETGROUPSERVICETEST_H

#include "TargetGroupTypeDefs.h"
class TargetGroup;
class MySqlDriver;
#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "CampaignTypeDefs.h"
#include "MySqlTargetGroupService.h"
class MySqlTargetGroupServiceTest  : public ::testing::Test {

private:

public:

    std::shared_ptr<MySqlTargetGroupService> service;

    void SetUp();

    void TearDown();

    MySqlTargetGroupServiceTest() ;

    std::shared_ptr<TargetGroup> givenTargetGroup();

    void whenTargetGroupIsInserted(std::shared_ptr<TargetGroup> cmp);

    std::shared_ptr<TargetGroup> thenTargetGroupIsReadById(int id);

    void thenTargetGroupsAreEqual(std::shared_ptr<TargetGroup> tgReadFromDb, std::shared_ptr<TargetGroup> tgInsretedInDb);

    virtual ~MySqlTargetGroupServiceTest() ;

};
#endif //EXCHANGESIMULATOR_MYSQLTARGETGROUPSERVICETEST_H
