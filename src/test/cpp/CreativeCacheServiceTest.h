// //
// // Created by mtaabodi on 7/13/2016.
// //
//
// #ifndef CreativeCacheService_H
// #define CreativeCacheService_H
//
//
//
// #include "CreativeTypeDefs.h"
// class Creative;
// class MySqlDriver;
// #include "CollectionUtil.h"
// #include <memory>
// #include <string>
// #include <gtest/gtest.h>
// #include "TestsCommon.h"
// #include "CreativeTypeDefs.h"
// class Creative;
// class CreativeCacheService;
// #include "HttpUtilServiceMock.h"
//
// class CreativeCacheServiceTest : public ::testing::Test {
//
// private:
//
// public:
//
// CreativeCacheService* creativeCacheService;
// std::string allEntitiesInJsonReturned;
//
// std::string allEntitiesInJsonExpected;
//
// std::unordered_map<int, std::shared_ptr<Creative>> allCreativesExpected;
//
// HttpUtilServiceMockPtr httpUtilServiceMock;
//
// void SetUp();
//
// void TearDown();
//
// CreativeCacheServiceTest();
//
// void givenHttpServiceReturnsJsonData();
//
// void whenCreativeReloaCachesViaHtppIsIsCalled();
//
// void thenAllCreativesAreAsExpected();
//
// std::string createArrayOfCreatives(std::shared_ptr<Creative> creativeExpected);
//
// void thenBothCreativesAreEqual(std::shared_ptr<Creative> actualCreative, std::shared_ptr<Creative> expectedCreative);
//
// void givenDataInCache();
//
// void whenGetAllAsJsonIsCalled();
//
// void thenAllEntitiesJosnIsCreatedAsExpected();
//
// virtual ~CreativeCacheServiceTest();
//
// };
// #endif //EXCHANGESIMULATOR_MYSQLCREATIVESERVICETEST_H
