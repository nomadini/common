//
// Created by mtaabodi on 7/13/2016.
//

#ifndef TargetGroupGeoSegmentCacheService_H
#define TargetGroupGeoSegmentCacheService_H



// #include "TargetGroupGeoSegment.h"
class MySqlDriver;
#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
// #include "TargetGroupGeoSegment.h"
class TargetGroupGeoSegmentCacheService;
#include "HttpUtilServiceMock.h"

class TargetGroupGeoSegmentCacheServiceTest  : public ::testing::Test {

private:

public:

    TargetGroupGeoSegmentCacheService* service;
    std::string allEntitiesInJsonReturned;

    std::string allEntitiesInJsonExpected;

    // std::unordered_map<int, TargetGroupstd::shared_ptr<GeoSegment>> allTargetGroupGeoSegmentsExpected;

    HttpUtilServiceMockPtr httpUtilServiceMock;

    void SetUp();

    void TearDown();

    TargetGroupGeoSegmentCacheServiceTest() ;

    void givenHttpServiceReturnsJsonData();

    void whenTargetGroupGeoSegmentReloaCachesViaHtppIsIsCalled();

    void thenAllTargetGroupGeoSegmentsAreAsExpected();

    // std::string createArrayOfTargetGroupGeoSegments(TargetGroupstd::shared_ptr<GeoSegment> targetGroupExpected);
    //
    // void thenBothTargetGroupGeoSegmentsAreEqual(TargetGroupstd::shared_ptr<GeoSegment> actualTargetGroupGeoSegment, TargetGroupstd::shared_ptr<GeoSegment> expectedTargetGroupGeoSegment);

    void givenDataInCache();

    void whenGetAllAsJsonIsCalled();

    void thenAllEntitiesJosnIsCreatedAsExpected();

    virtual ~TargetGroupGeoSegmentCacheServiceTest() ;

};
#endif //EXCHANGESIMULATOR_MYSQLCREATIVESERVICETEST_H
