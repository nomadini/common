// //
// // Created by mtaabodi on 7/13/2016.
// //
// #include "GUtil.h"
// #include "JsonUtil.h"
// #include "JsonArrayUtil.h"
// #include "JsonMapUtil.h"
// #include "StringUtil.h"
// #include <string>
// #include <memory>
// #include "DateTimeUtil.h"
// #include "TargetGroupCreativeCacheServiceTest.h"
// #include "TargetGroupCreativeCacheService.h"
// #include "RandomUtil.h"
// #include "BeanFactory.h"
// #include "CollectionUtil.h"
// #include "HttpUtilServiceMock.h"
//
// TargetGroupCreativeCacheServiceTest::TargetGroupCreativeCacheServiceTest() {
//   this->service = beanFactory->targetGroupCreativeCacheService;
// }
//
// TargetGroupCreativeCacheServiceTest::~TargetGroupCreativeCacheServiceTest() {
//
// }
//
// std::string TargetGroupCreativeCacheServiceTest::createArrayOfTargetGroupCreatives(TargetGroupCreativeMapPtr targetGroupCreativeMap) {
//     RapidJsonValueTypeNoRef crtObject(rapidjson::kObjectType);
//     auto doc = JsonUtil::createADcoument (rapidjson::kArrayType);
//     targetGroupCreativeMap->addPropertiesToJsonValue (crtObject, doc);
//
//     doc->PushBack (crtObject , doc->GetAllocator ());
//     return JsonUtil::docToString (doc.get());
// }
//
//
// TargetGroupCreativeMapPtr TargetGroupCreativeCacheServiceTest::createSampleEntity() {
//   auto entity = std::make_shared<TargetGroupCreativeMap> ();
//   entity->targetGroupId = RandomUtil::sudoRandomNumber(10000);
//   entity->creativeId = RandomUtil::sudoRandomNumber(10000);
//   entity->dateCreated = DateTimeUtil::getNowInMySqlFormat();
//   entity->dateModified =  DateTimeUtil::getNowInMySqlFormat();
//   return entity;
// }
//
// void TargetGroupCreativeCacheServiceTest::givenHttpServiceReturnsJsonData(){
//
//     auto targetGroupExpected = createSampleEntity();
//     allEntitiesExpected.insert(std::make_pair(targetGroupExpected->id , targetGroupExpected));
//     //
//     std::string sampleJsonOfListOfTargetGroupCreatives = createArrayOfTargetGroupCreatives(targetGroupExpected);
//     MLOG(3)<<"sampleJsonOfListOfTargetGroupCreatives : "<<sampleJsonOfListOfTargetGroupCreatives;
//     ON_CALL(*httpUtilServiceMock,
//             sendPostRequest (_,_,_))
//             .WillByDefault(Return(sampleJsonOfListOfTargetGroupCreatives));
//
// }
//
// void TargetGroupCreativeCacheServiceTest::whenTargetGroupCreativeReloaCachesViaHtppIsIsCalled() {
//     this->service->reloadCachesViaHttp();
// }
//
// void TargetGroupCreativeCacheServiceTest::SetUp() {
//   this->httpUtilServiceMock = std::make_shared<HttpUtilServiceMock>();
//   this->service->httpUtilService = httpUtilServiceMock;
//   this->allEntitiesExpected.clear();
//   this->service->getAllTargetGroupCreativesMap().clear();
//   this->service->clearCaches();
// }
//
// void TargetGroupCreativeCacheServiceTest::TearDown() {
//
// }
//
// void TargetGroupCreativeCacheServiceTest::thenBothTargetGroupCreativesAreEqual(TargetGroupCreativeMapPtr actualTgCreativeMap, TargetGroupCreativeMapPtr expectedTgCreativeMap) {
//   EXPECT_THAT( actualTgCreativeMap->id, testing::Eq(expectedTgCreativeMap->id));
//   EXPECT_THAT( actualTgCreativeMap->targetGroupId, testing::Eq(expectedTgCreativeMap->targetGroupId));
//   EXPECT_THAT( actualTgCreativeMap->creativeId, testing::Eq(expectedTgCreativeMap->creativeId));
//   EXPECT_THAT( actualTgCreativeMap->dateCreated, testing::Eq(expectedTgCreativeMap->dateCreated));
//   EXPECT_THAT( actualTgCreativeMap->dateModified, testing::Eq(expectedTgCreativeMap->dateModified));
// }
//
// void TargetGroupCreativeCacheServiceTest::thenAllTargetGroupCreativesAreAsExpected()  {
//   std::unordered_map<int, TargetGroupCreativeMapPtr> actualTargetGroupsMap;
//   for(auto entity :  this->service->allTargetGroupCreativesMap) {
//       actualTargetGroupsMap.insert(std::make_pair(entity->id, entity));
//   }
//   TargetGroupCreativeCacheServiceTest::assertBothMapsAreEqual(actualTargetGroupsMap, allEntitiesExpected );
// }
//
// void TargetGroupCreativeCacheServiceTest::givenDataInCache() {
//     auto targetGroup = createSampleEntity();
//
//     allEntitiesExpected.insert(std::make_pair(targetGroup->id, targetGroup));
//     this->service->allTargetGroupCreativesMap.push_back(targetGroup);
//
//     targetGroup = createSampleEntity();
//     allEntitiesExpected.insert(std::make_pair(targetGroup->id, targetGroup));
//     this->service->allTargetGroupCreativesMap.push_back(targetGroup);
// }
//
// void TargetGroupCreativeCacheServiceTest::whenGetAllAsJsonIsCalled() {
//     this->allEntitiesInJsonReturned = this->servicecacheServiceServer->getAllAsJson ();
//     MLOG(3)<<"allEntitiesInJsonReturned : "<<allEntitiesInJsonReturned<<std::endl;
// }
//
// std::unordered_map<int, TargetGroupCreativeMapPtr> TargetGroupCreativeCacheServiceTest::parseDtoReturnedToMap(std::string json) {
//
//   std::unordered_map<int, TargetGroupCreativeMapPtr> actualTargetGroupsMap;
//   auto docReturned = parseJsonSafely(json);
//   for (rapidjson::SizeType i = 0; i < docReturned->Size (); i++) {
//       auto& advValue = (*docReturned)[i];
//       TargetGroupCreativeMapPtr adv = TargetGroupCreativeMap::fromJsonValue(advValue);
//       MLOG(3) << "parsed this TargetGroupCreativeMap : "<<adv->toString();
//       actualTargetGroupsMap.insert(std::make_pair(adv->id, adv));
//   }
//   return actualTargetGroupsMap;
// }
// void TargetGroupCreativeCacheServiceTest::assertBothMapsAreEqual(std::unordered_map<int, TargetGroupCreativeMapPtr> actualTargetGroupsMap,
//                        std::unordered_map<int, TargetGroupCreativeMapPtr> allEntitiesExpected ) {
//                          EXPECT_THAT(actualTargetGroupsMap.size(),
//                                      testing::Eq(this->allEntitiesExpected.size()));
//

//
//                          //    BOOST_FOREACH_MAP
//                          for(auto const& entry  :  allEntitiesExpected) {
//                              auto expectedTgCreativeMap = entry.second;
//                              auto pair = actualTargetGroupsMap.find(expectedTgCreativeMap->id);
//                              if (pair == actualTargetGroupsMap.end()) {
//                                  LOG(WARNING)<<"couldnt find this creative id in actual map : " <<expectedTgCreativeMap->id;
//                                  EXPECT_TRUE (false);//we didn't the expected creative in actual creatives map, so we fail here
//                                  continue;
//                              }
//                              auto actualTgCreativeMap = pair->second;
//                              thenBothTargetGroupCreativesAreEqual(actualTgCreativeMap, expectedTgCreativeMap);
//                          }
// }
// void TargetGroupCreativeCacheServiceTest::thenAllEntitiesJosnIsCreatedAsExpected() {
//
//   std::unordered_map<int, TargetGroupCreativeMapPtr> actualTargetGroupsMap = parseDtoReturnedToMap(this->allEntitiesInJsonReturned);
//   assertBothMapsAreEqual(actualTargetGroupsMap, this->allEntitiesExpected );
// }
//
// TEST_F(TargetGroupCreativeCacheServiceTest, testParsingTargetGroupCreativeFromHttpUtilService) {
//
//     givenHttpServiceReturnsJsonData();
//
//     whenTargetGroupCreativeReloaCachesViaHtppIsIsCalled();
//
//     thenAllTargetGroupCreativesAreAsExpected();
// }
//
//
// TEST_F(TargetGroupCreativeCacheServiceTest, testGetAllEntitiesAsJson) {
//
//     givenDataInCache();
//
//     whenGetAllAsJsonIsCalled();
//
//     thenAllEntitiesJosnIsCreatedAsExpected();
// }
//
// //TODO :: add  a test to validate the populateMapsAndLists function in the
