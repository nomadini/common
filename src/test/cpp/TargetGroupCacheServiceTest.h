// //
// // Created by mtaabodi on 7/13/2016.
// //
//
// #ifndef TargetGroupCacheService_H
// #define TargetGroupCacheService_H
//
//
//
// #include "TargetGroupTypeDefs.h"
// class TargetGroup;
// class MySqlDriver;
// #include "CollectionUtil.h"
// #include <memory>
// #include <string>
// #include <gtest/gtest.h>
// #include "TestsCommon.h"
// #include "TargetGroupTypeDefs.h"
// class TargetGroup;
// class TargetGroupCacheService;
// #include "HttpUtilServiceMock.h"
//
// class TargetGroupCacheServiceTest  : public ::testing::Test {
//
// private:
//
// public:
//
//     TargetGroupCacheService* service;
//     std::string allEntitiesInJsonReturned;
//
//     std::string allEntitiesInJsonExpected;
//
//     std::unordered_map<int, std::shared_ptr<TargetGroup>> allTargetGroupsExpected;
//
//     HttpUtilServiceMockPtr httpUtilServiceMock;
//
//     void SetUp();
//
//     void TearDown();
//
//     TargetGroupCacheServiceTest() ;
//
//     void givenHttpServiceReturnsJsonData();
//
//     void whenTargetGroupReloaCachesViaHtppIsIsCalled();
//
//     void thenAllTargetGroupsAreAsExpected();
//
//     std::string createArrayOfTargetGroups(std::shared_ptr<TargetGroup> targetGroupExpected);
//
//     void thenBothTargetGroupsAreEqual(std::shared_ptr<TargetGroup> actualTargetGroup, std::shared_ptr<TargetGroup> expectedTargetGroup);
//
//     void givenDataInCache();
//
//     void whenGetAllAsJsonIsCalled();
//
//     void thenAllEntitiesJosnIsCreatedAsExpected();
//
//     virtual ~TargetGroupCacheServiceTest() ;
//
// };
// #endif //EXCHANGESIMULATOR_MYSQLCREATIVESERVICETEST_H
