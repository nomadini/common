// //
// // Created by mtaabodi on 7/13/2016.
// //
//
// #ifndef TargetGroupGeoLocationCacheService_H
// #define TargetGroupGeoLocationCacheService_H
//
//
//
// #include "TargetGroupGeoLocation.h"
// class MySqlDriver;
// #include "CollectionUtil.h"
// #include <memory>
// #include <string>
// #include <gtest/gtest.h>
// #include "TestsCommon.h"
// #include "TargetGroupGeoLocation.h"
// class TargetGroupGeoLocationCacheService;
// #include "HttpUtilServiceMock.h"
//
// class TargetGroupGeoLocationCacheServiceTest  : public ::testing::Test {
//
// private:
//
// public:
//
//     TargetGroupGeoLocationCacheService* service;
//     std::string allEntitiesInJsonReturned;
//
//     std::string allEntitiesInJsonExpected;
//
//     std::unordered_map<int, std::shared_ptr<TargetGroupGeoLocationList>> allTargetGroupGeoLocationsExpected;
//
//     HttpUtilServiceMockPtr httpUtilServiceMock;
//
//     void SetUp();
//
//     void TearDown();
//
//     std::unordered_map<int, std::shared_ptr<TargetGroupGeoLocationList>> parseDtoReturnedToMap(std::string json);
//
//     TargetGroupGeoLocationCacheServiceTest() ;
//
//     void givenHttpServiceReturnsJsonData();
//
//     void whenTargetGroupGeoLocationReloaCachesViaHtppIsIsCalled();
//
//     void thenAllTargetGroupGeoLocationsAreAsExpected();
//
//     std::shared_ptr<TargetGroupGeoLocationList> createSampleEntity();
//
//     std::string createArrayOfTargetGroupGeoLocations(std::shared_ptr<TargetGroupGeoLocationList> targetGroupExpected);
//
//     void assertBothMapsAreEqual(std::unordered_map<int, std::shared_ptr<TargetGroupGeoLocationList>> actualTargetGroupsMap,
//                                 std::unordered_map<int, std::shared_ptr<TargetGroupGeoLocationList>> allTargetGroupCreativesExpected);
//
//
//     void thenBothTargetGroupGeoLocationsAreEqual(std::shared_ptr<TargetGroupGeoLocationList> actualTargetGroupGeoLocation, std::shared_ptr<TargetGroupGeoLocationList> expectedTargetGroupGeoLocation);
//
//     void givenDataInCache();
//
//     void whenGetAllAsJsonIsCalled();
//
//     void thenAllEntitiesJosnIsCreatedAsExpected();
//
//     virtual ~TargetGroupGeoLocationCacheServiceTest() ;
//
// };
// #endif //EXCHANGESIMULATOR_MYSQLCREATIVESERVICETEST_H
