//
// Created by Mahmoud Taabodi on 7/12/16.
//

#ifndef AerospikeDriverAtomicWritesVisitHistoryTest_H
#define AerospikeDriverAtomicWritesVisitHistoryTest_H


#include "AerospikeDriver.h"
#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
class EntityToModuleStateStats;
class AerospikeDriverAtomicWritesVisitHistoryTest : public ::testing::Test {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;
AerospikeDriverAtomicWritesVisitHistoryTest();

virtual ~AerospikeDriverAtomicWritesVisitHistoryTest();

void SetUp();

void TearDown();
};

#endif //COMMON_AerospikeDriverAtomicWritesVisitHistoryTestS_H
