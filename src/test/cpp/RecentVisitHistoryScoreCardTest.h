//
// Created by Mahmoud Taabodi on 7/12/16.
//

#ifndef RecentVisitHistoryScoreCardTest_H
#define RecentVisitHistoryScoreCardTest_H


#include "AerospikeDriver.h"
#include "TgModelEligibilityRecord.h"
#include "TgScoreEligibilityRecord.h"
#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "RecentVisitHistoryScoreCard.h"
class EntityToModuleStateStats;
class RecentVisitHistoryScoreCardTest : public ::testing::Test {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;
RecentVisitHistoryScoreCardTest();

virtual ~RecentVisitHistoryScoreCardTest();

void SetUp();

void TearDown();
};

#endif //COMMON_RecentVisitHistoryScoreCardTestS_H
