//
// Created by Mahmoud Taabodi on 7/12/16.
//

#include "HistogramTest.h"
#include "GUtil.h"
#include <string>
#include <memory>
#include "Histogram.h"

void HistogramTest::testHistogram() {
        Histogram histo(0, 100, 10);
        histo.record(5);
        histo.record(11);
        histo.record(11);
        histo.record(24);
        histo.record(25);
        histo.record(26);
        histo.record(43);
        histo.record(47);
        histo.record(50);
        histo.record(50);
        histo.record(50);
        histo.record(50);
        histo.record(89);
        histo.record(89);
        histo.record(89);
        histo.record(91);
        histo.record(900);
        histo.record(900);
        histo.record(900);
        histo.record(-3);
        histo.record(-30);
        histo.record(-300);
        histo.record(-399);
        histo.record(-399);
        histo.record(-399);

        histo.printBinCounts();

        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(900, 0), false);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(900, 10), true);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(900, 17), false);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(50, 16), false);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(50, 100), true);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(50, 0), false);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(5, 16), false);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(6, 81), true);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(6, 76), true);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(6, 100), true);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(6, 0), false);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(89, 16), false);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(89, 20), true);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(89, 35), true);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(89, 26), true);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(92, 16), true);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(92, 100), true);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(92, 0), false);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(-3, 100), true);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(-3, 0), false);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(-3, 10), false);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(32, 54), true);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(32, 53), true);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(32, 47), false);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(-3, 20), false);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(-399, 82), true);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(399, 78), true);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(399, 79), true);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(399, 80), true);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(399, 100), true);
        EXPECT_THAT(histo.isAmongTopXPercentOfDataWithHighestValues(399, 0), false);
        histo.printBinPercentages();
}

HistogramTest::HistogramTest() {

}

HistogramTest::~HistogramTest() {

}

void HistogramTest::SetUp() {

}

void HistogramTest::TearDown() {

}

TEST_F(HistogramTest, testHistogram) {
        testHistogram();
}
