//
// Created by Mahmoud Taabodi on 7/12/16.
//

#include "AerospikeDriverAtomicWritesOneVisitHistoryTest.h"
#include "GUtil.h"

#include "EntityToModuleStateStats.h"
#include <string>
#include <memory>
#include "BeanFactory.h"
#include <aerospike/as_config.h>
#include "AerospikeUtils.h"
#include <thread>
#include <algorithm>
#include <string>
#include <iostream>
#include <cctype>



#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/geometries/polygon.hpp>

#include <boost/geometry/index/rtree.hpp>

#include <cmath>
#include <vector>
#include <iostream>
#include <boost/foreach.hpp>

#include "DateTimeUtil.h"
#include "FeatureHistory.h"
#include "DeviceFeatureHistory.h"
#include "Device.h"
#include "Feature.h"
#include "JsonArrayUtil.h"

#include "Poco/Runnable.h"
#include "Poco/ThreadPool.h"

AerospikeDriverAtomicWritesOneVisitHistoryTest::AerospikeDriverAtomicWritesOneVisitHistoryTest() {

}

AerospikeDriverAtomicWritesOneVisitHistoryTest::~AerospikeDriverAtomicWritesOneVisitHistoryTest() {

}

void AerospikeDriverAtomicWritesOneVisitHistoryTest::SetUp() {
        entityToModuleStateStats = new EntityToModuleStateStats();
}

void AerospikeDriverAtomicWritesOneVisitHistoryTest::TearDown() {

}

class Worker : public Poco::Runnable {
public:
std::shared_ptr<AerospikeDriver> driver;
std::shared_ptr<DeviceFeatureHistory> responseObject;

Worker() {
}
virtual void run() {
        std::string featureName = "abc.com";

        for (int i = 0; i < 10; i++) {

                //first we read whatever that is in the bin
                std::string valueWritten = driver->readTheBin("test",
                                                              "testVisitHistory",
                                                              "visithistoryKey",
                                                              "vstBinSt4");

                LOG(ERROR)<< "thread : read This raw : "<<valueWritten;
                if (valueWritten.empty()) {
                        LOG(ERROR)<< "thread : wiriting original object";
                        valueWritten = driver->setValueAndRead("test",
                                                               "testVisitHistory",
                                                               "visithistoryKey",
                                                               "vstBinSt4",
                                                               responseObject->toJson(),
                                                               10000);

                }

                auto newDeviceFeatureHistory = DeviceFeatureHistory::fromJson(valueWritten);
                LOG(ERROR)<< "thread : read This : "<<newDeviceFeatureHistory->toJson();

                //TODO fix this
                // auto pairPtr = newDeviceFeatureHistory->getFeatures()->find(featureName);
                // //we increase the time feature was visited by one
                // pairPtr->second = pairPtr->second + 1;

                //we write the feature back to aerospike
                LOG(ERROR)<< "thread : updated this : "<<newDeviceFeatureHistory->toJson();
                valueWritten = driver->setValueAndRead("test",
                                                       "testVisitHistory",
                                                       "visithistoryKey",
                                                       "vstBinSt4",
                                                       newDeviceFeatureHistory->toJson(),
                                                       10000);
                LOG(ERROR)<< "thread : final valueWritten : "<<valueWritten;

                gicapods::Util::sleepMillis(10);
        }
}
};

TEST_F(AerospikeDriverAtomicWritesOneVisitHistoryTest, testUpdatingOneVisitHistoryObjectInMultipleThreads) {
        std::string host="10.136.46.157";
        std::shared_ptr<AerospikeDriver> driver= std::make_shared<AerospikeDriver>(host, 3000, nullptr);
        driver->entityToModuleStateStats = entityToModuleStateStats;
        //we create a device with id nomadini0
        auto device = std::make_shared<Device>("nomadini0", "DESKTOP");
        auto responseObject = std::make_shared<DeviceFeatureHistory>(device);

        //we set timeOfVisit to 0
        TimeType timeOfVisit = 0;
        std::string featureName = "abc.com";

        std::string featureType = Feature::generalTopLevelDomain;
        auto feature = std::make_shared<Feature>(featureType, featureName, 0);
        auto featureHistory = std::make_shared<FeatureHistory>(feature, DateTimeUtil::getNowInMilliSecond());

        responseObject->getFeatures()->push_back(featureHistory);

        Poco::ThreadPool threadpool;

        //first we empty the bin name
        std::string valueWritten = driver->setValueAndRead("test",
                                                           "testVisitHistory",
                                                           "visithistoryKey",
                                                           "vstBinSt4",
                                                           "",
                                                           10000);

        int numberOfThreads = 10;
        for (int i = 0; i < numberOfThreads; i++) {
                Worker work;
                work.driver = driver;
                work.responseObject = responseObject;
                threadpool.start(work);
        }
        //we started 10 threads that write to a bin atomically
        threadpool.joinAll(); // we should always join the threads

        //we read the final object to see what we have at the end
        std::string resultOfAllUpdates = driver->addValueAndRead("test",
                                                                 "testVisitHistory",
                                                                 "visithistoryKey",
                                                                 "vstBinSt4",
                                                                 "",
                                                                 //  "responseObject",
                                                                 10000);
        LOG(ERROR) << "resultOfAllUpdates : "<< resultOfAllUpdates;

}
