
//
// Created by mtaabodi on 7/13/2016.
//

#ifndef EXCHANGESIMULATOR_MYSQLADVERTISERSERVICETEST_H
#define EXCHANGESIMULATOR_MYSQLADVERTISERSERVICETEST_H


#include "Advertiser.h"
class MySqlDriver;
#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "Advertiser.h"
#include "MySqlAdvertiserService.h"
class MySqlAdvertiserServiceTest  : public ::testing::Test {

private:

public:

    std::shared_ptr<MySqlAdvertiserService> service;

    void SetUp();

    void TearDown();

    MySqlAdvertiserServiceTest() ;

    std::shared_ptr<Advertiser> givenAdvertiser();

    void whenAdvertiserIsInserted(std::shared_ptr<Advertiser> ad);

    std::shared_ptr<Advertiser> thenAdvertiserIsReadById(int id);

    void thenAdvertisersAreEqual(std::shared_ptr<Advertiser> adReadFromDb, std::shared_ptr<Advertiser> adInsretedInDb);

    virtual ~MySqlAdvertiserServiceTest() ;

};

#endif //EXCHANGESIMULATOR_MYSQLADVERTISERSERVICETEST_H

