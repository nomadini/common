// //
// // Created by mtaabodi on 7/13/2016.
// //
// #include "GUtil.h"
// #include "JsonUtil.h"
// #include "JsonArrayUtil.h"
// #include "MySqlBWListServiceMock.h"
// #include "StringUtil.h"
// #include <string>
// #include <memory>
// #include "DateTimeUtil.h"
// #include "TargetGroupBWListCacheServiceTest.h"
// #include "TargetGroupBWListCacheService.h"
// #include "RandomUtil.h"
// #include "BeanFactory.h"
// #include "CollectionUtil.h"
// #include "HttpUtilServiceMock.h"
//
// TargetGroupBWListCacheServiceTest::TargetGroupBWListCacheServiceTest() {
//     this->service = beanFactory->targetGroupBWListCacheService;
// }
//
// TargetGroupBWListCacheServiceTest::~TargetGroupBWListCacheServiceTest() {
//
// }
//
// std::string TargetGroupBWListCacheServiceTest::createArrayOfTargetGroups(std::shared_ptr<TargetGroupBWListMap> entityExpected) {
//     RapidJsonValueTypeNoRef crtObject(rapidjson::kObjectType);
//     auto doc = JsonUtil::createADcoument (rapidjson::kArrayType);
//     entityExpected->addPropertiesToJsonValue (crtObject, doc);
//
//     doc->PushBack (crtObject , doc->GetAllocator ());
//     return JsonUtil::docToString (doc.get());
// }
//
// std::shared_ptr<TargetGroupBWListMap> TargetGroupBWListCacheServiceTest::createSampleEntity() {
//   std::shared_ptr<TargetGroupBWListMap> entityBWListMapExpected = std::make_shared<TargetGroupBWListMap> ();
//   entityBWListMapExpected->id = RandomUtil::sudoRandomNumber(10000);
//   entityBWListMapExpected->targetGroupId = RandomUtil::sudoRandomNumber(10000);
//   entityBWListMapExpected->bwListId = RandomUtil::sudoRandomNumber(10000);
//   entityBWListMapExpected->dateCreated = DateTimeUtil::getNowInMySqlFormat();
//   entityBWListMapExpected->dateModified =  DateTimeUtil::getNowInMySqlFormat();
//   return entityBWListMapExpected;
// }
//
//  void TargetGroupBWListCacheServiceTest::givenHttpServiceReturnsJsonData(){
//
//     std::shared_ptr<TargetGroupBWListMap> entityBWListMapExpected = createSampleEntity();
//
//     allEntitiesExpected.insert(std::make_pair(entityBWListMapExpected->id, entityBWListMapExpected));
//
//     std::string sample = createArrayOfTargetGroups(entityBWListMapExpected);
//     MLOG(3)<<"entityBWListMapExpected : "<<sample;
//     ON_CALL(*httpUtilServiceMock,
//             sendPostRequest (_,_,_))
//             .WillByDefault(Return(sample));
//
// }
//
// void TargetGroupBWListCacheServiceTest::whenTargetGroupReloaCachesViaHtppIsIsCalled() {
//     this->service->reloadCachesViaHttp();
// }
//
//
// void TargetGroupBWListCacheServiceTest::TearDown() {
//
// }
//
// void TargetGroupBWListCacheServiceTest::thenBothTargetGroupsAreEqual(std::shared_ptr<TargetGroupBWListMap> actualTargetGroup, std::shared_ptr<TargetGroupBWListMap> expectedEntity) {
//     EXPECT_TRUE(TargetGroupBWListMap::equals(actualTargetGroup, expectedEntity));
// }
//
// void TargetGroupBWListCacheServiceTest::thenAllTargetGroupsAreAsExpected()  {
//
//     std::unordered_map<int, std::shared_ptr<TargetGroupBWListMap>> actualEntitiesMap;
//
//     EXPECT_TRUE(!actualEntitiesMap.empty());
//     EXPECT_THAT(actualEntitiesMap.size(),
//                 testing::Eq(this->allEntitiesExpected.size()));
//
//

//
// //    BOOST_FOREACH_MAP
//     for(auto const& entry  :  allEntitiesExpected) {
//         auto expectedEntity = entry.second;
//         auto pair = actualEntitiesMap.find(expectedEntity->id);
//         if (pair == actualEntitiesMap.end()) {
//             LOG(WARNING)<<"couldnt find this id in actual map : " <<expectedEntity->id;
//             EXPECT_TRUE (false);//we didn't the expected entity in actual entitys map, so we fail here
//             continue;
//         }
//         auto actualTargetGroup = pair->second;
//         thenBothTargetGroupsAreEqual(actualTargetGroup, expectedEntity);
//     }
// }
//
//
// void TargetGroupBWListCacheServiceTest::whenGetAllAsJsonIsCalled() {
//     this->allEntitiesInJsonReturned = this->servicecacheServiceServer->getAllAsJson ();
//     MLOG(3)<<"allEntitiesInJsonReturned : "<<allEntitiesInJsonReturned<<std::endl;
// }
//
// std::unordered_map<int, std::shared_ptr<TargetGroupBWListMap>> TargetGroupBWListCacheServiceTest::parseDtoReturnedToMap(std::string json) {
//
//   std::unordered_map<int, std::shared_ptr<TargetGroupBWListMap>> actualEntitiesMap;
//   auto docReturned = parseJsonSafely(json);
//   for (rapidjson::SizeType i = 0; i < docReturned->Size (); i++) {
//       auto& advValue = (*docReturned)[i];
//       std::shared_ptr<TargetGroupBWListMap> adv = TargetGroupBWListMap::fromJsonValue(advValue);
//       MLOG(3) << "parsed this TargetGroupBWListMap : "<<adv->toString();
//       actualEntitiesMap.insert(std::make_pair(adv->id, adv));
//   }
//   return actualEntitiesMap;
// }
//
// void TargetGroupBWListCacheServiceTest::thenAllEntitiesJosnIsCreatedAsExpected() {
//   std::unordered_map<int, std::shared_ptr<TargetGroupBWListMap>> actualEntitiesMap = parseDtoReturnedToMap(this->allEntitiesInJsonReturned);
//   assertBothMapsAreEqual(actualEntitiesMap, this->allEntitiesExpected );
// }
//
// void TargetGroupBWListCacheServiceTest::assertBothMapsAreEqual(
//                        std::unordered_map<int, std::shared_ptr<TargetGroupBWListMap>> actualEntitiesMap,
//                        std::unordered_map<int, std::shared_ptr<TargetGroupBWListMap>> allEntitiesExpected ) {
//                          EXPECT_TRUE(!actualEntitiesMap.empty());
//                          EXPECT_THAT(actualEntitiesMap.size(),
//                                      testing::Eq(this->allEntitiesExpected.size()));
//

//
//                          //    BOOST_FOREACH_MAP
//                          for(auto const& entry  :  allEntitiesExpected) {
//                              auto expectedEntity = entry.second;
//                              auto pair = actualEntitiesMap.find(expectedEntity->id);
//                              if (pair == actualEntitiesMap.end()) {
//                                  LOG(WARNING)<<"couldnt find this creative id in actual map : " <<expectedEntity->id;
//                                  EXPECT_TRUE (false);//we didn't the expected creative in actual creatives map, so we fail here
//                                  continue;
//                              }
//                              auto actualEntity = pair->second;
//                              thenBothTargetGroupsAreEqual(actualEntity, expectedEntity);
//                          }
// }
// TEST_F(TargetGroupBWListCacheServiceTest, testParsingTargetGroupFromHttpUtilService) {
//
//     givenHttpServiceReturnsJsonData();
//
//     whenTargetGroupReloaCachesViaHtppIsIsCalled();
//
//     thenAllTargetGroupsAreAsExpected();
// }
//
//
// TEST_F(TargetGroupBWListCacheServiceTest, testGetAllEntitiesAsJson) {
//
//     givenDataInCache();
//
//     whenGetAllAsJsonIsCalled();
//
//     thenAllEntitiesJosnIsCreatedAsExpected();
// }
//
// //TODO :: add  a test to validate the populateMapsAndLists function in the
