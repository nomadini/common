//
// Created by Ms.Rahmani on 7/12/2016.
//
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "MySqlTargetGroupServiceTest.h"
#include "MySqlTargetGroupService.h"
#include "TargetGroup.h"
#include "MySqlDriver.h"
#include "EventLog.h"
#include "BeanFactory.h"
#include "CollectionUtil.h"
#include "RandomUtil.h"
MySqlTargetGroupServiceTest::MySqlTargetGroupServiceTest() {
        // this->service= beanFactory->mySqlTargetGroupService;
}

MySqlTargetGroupServiceTest::~MySqlTargetGroupServiceTest() {

}

void MySqlTargetGroupServiceTest::SetUp() {
        service->deleteAll ();
}

void MySqlTargetGroupServiceTest::TearDown() {

}

std::shared_ptr<TargetGroup> MySqlTargetGroupServiceTest::givenTargetGroup(){
        auto tg = std::make_shared<TargetGroup>();
        tg->setName("abc");
        tg->id = RandomUtil::sudoRandomNumber(1000);
        tg->idAsString = StringUtil::toStr(tg->getId());
        tg->setStatus("1a");
        tg->setCampaignId(21);

        tg->setMaxImpression(3121111111);
        tg->setDailyMaxImpression(1411111111);
        tg->setMaxBudget(17.7);
        tg->setDailyMaxBudget(7.7);
        tg->setFrequencyInSec(777.7);
        tg->setDescription("abc is 123");
        tg->setIabCategories(std::vector<std::string>{"aaa", "bbb", "ccc" });
        tg->setIabSubCategories(std::vector<std::string>{ "aa", "bb", "cc" });
        // tg->getAdPositions()->put(StringUtil::toStr("abc"), nullptr);
        return tg;
}

void MySqlTargetGroupServiceTest::whenTargetGroupIsInserted(std::shared_ptr<TargetGroup> tg){
        service->insert(tg);
}


std::shared_ptr<TargetGroup> MySqlTargetGroupServiceTest::thenTargetGroupIsReadById(int id) {

        std::shared_ptr<MySqlTargetGroupService> service;//= beanFactory->mySqlTargetGroupService;
        return service->readTargetGroupById(id);
}

void MySqlTargetGroupServiceTest::thenTargetGroupsAreEqual(std::shared_ptr<TargetGroup> tgReadFromDb, std::shared_ptr<TargetGroup> tgInsretedInDb)  {


        EXPECT_THAT( tgReadFromDb->getName(),
                     testing::Eq(tgInsretedInDb->getName()));
        EXPECT_THAT( tgReadFromDb->getStatus(),
                     testing::Eq(tgInsretedInDb->getStatus()));
        EXPECT_THAT( tgReadFromDb->getCampaignId(),
                     testing::Eq(tgInsretedInDb->getCampaignId()));
        EXPECT_THAT( tgReadFromDb->getMaxImpression(),
                     testing::Eq(tgInsretedInDb->getMaxImpression()));
        EXPECT_THAT( tgReadFromDb->getDailyMaxImpression(),
                     testing::Eq(tgInsretedInDb->getDailyMaxImpression()));
        EXPECT_THAT( tgReadFromDb->getMaxBudget(),
                     testing::Eq(tgInsretedInDb->getMaxBudget()));
        EXPECT_THAT( tgReadFromDb->getDailyMaxBudget(),
                     testing::Eq(tgInsretedInDb->getDailyMaxBudget()));
        EXPECT_THAT( tgReadFromDb->getFrequencyInSec(),
                     testing::Eq(tgInsretedInDb->getFrequencyInSec()));

        EXPECT_THAT( tgReadFromDb->getDescription(),
                     testing::Eq(tgInsretedInDb->getDescription()));

        EXPECT_TRUE( CollectionUtil::areListsEqual(*tgReadFromDb->getIabCategory(), *tgInsretedInDb->getIabCategory()));
        EXPECT_TRUE( CollectionUtil::areListsEqual(*tgReadFromDb->getIabSubCategory(), *tgInsretedInDb->getIabSubCategory()));
        // EXPECT_TRUE( CollectionUtil::areMapsEqual(*tgReadFromDb->getAdPositions(), *tgInsretedInDb->getAdPositions()));
}



TEST_F(MySqlTargetGroupServiceTest, testInsertingAndReadingTargetGroup) {

        auto tg = givenTargetGroup();
        whenTargetGroupIsInserted(tg);
        auto tgReadFromDb= thenTargetGroupIsReadById(tg->getId());
        thenTargetGroupsAreEqual(tgReadFromDb, tg);
}
