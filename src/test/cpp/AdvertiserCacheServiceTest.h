// //
// // Created by mtaabodi on 7/13/2016.
// //
//
// #ifndef AdvertiserCacheService_H
// #define AdvertiserCacheService_H
//
//
//
// #include "Advertiser.h"
// class MySqlDriver;
// #include "CollectionUtil.h"
// #include <memory>
// #include <string>
// #include <gtest/gtest.h>
// #include "TestsCommon.h"
// #include "Advertiser.h"
// 
// #include "HttpUtilServiceMock.h"
//
// class AdvertiserCacheServiceTest  : public ::testing::Test {
//
// private:
//
// public:
//
//     CacheService<Advertiser>* service;
//     std::string allEntitiesInJsonReturned;
//
//     std::string allEntitiesInJsonExpected;
//
//     std::unordered_map<int, std::shared_ptr<Advertiser>> allAdvertisersExpected;
//
//     HttpUtilServiceMockPtr httpUtilServiceMock;
//
//     void SetUp();
//
//     void TearDown();
//
//     AdvertiserCacheServiceTest() ;
//
//     void givenHttpServiceReturnsJsonData();
//
//     void whenAdvertiserReloaCachesViaHtppIsIsCalled();
//
//     void thenAllAdvertisersAreAsExpected();
//
//     std::string createArrayOfAdvertisers(std::shared_ptr<Advertiser> advertiserExpected);
//
//     void thenBothAdvertisersAreEqual(std::shared_ptr<Advertiser> actualAdvertiser, std::shared_ptr<Advertiser> expectedAdvertiser);
//
//     void givenDataInCache();
//
//     void whenGetAllAsJsonIsCalled();
//
//     void thenAllEntitiesJosnIsCreatedAsExpected();
//
//     virtual ~AdvertiserCacheServiceTest() ;
//
// };
// #endif //EXCHANGESIMULATOR_MYSQLCREATIVESERVICETEST_H
