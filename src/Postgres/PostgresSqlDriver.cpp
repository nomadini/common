

http://www.tutorialspoint.com/postgresql/postgresql_c_cpp.htm

PostgresSqlDriver::PostgresSqlDriver(const std::string& hostAddress,
                               const std::string& port,
                               const std::string& username,
                               const std::string& password,
                               const std::string& schema) {
            assertAndThrow (!schema.empty());
            assertAndThrow (!username.empty());
            assertAndThrow (!password.empty());
            assertAndThrow (!hostAddress.empty());
            assertAndThrow (!port.empty());
            this->hostAddress = hostAddress;
            this->port = port;
            this->username = username;
            this->password = password;
            this->schema = schema;
}

PostgresSqlDriver::connectToDb() {

    try{
        //"dbname=testdb user=postgres password=cohondob hostaddr=127.0.0.1 port=5432"
        std::string connectionUrl = "dbname=" + schema +" user="+ username +" "
                "password=" + password +" hostaddr=" + hostAddress + " port=" + port;

        connection C(connectionUrl.c_str ());
        if (C.is_open()) {
            cout << "Opened database successfully: " << C.dbname() << endl;
        } else {
            cout << "Can't open database" << endl;
            return 1;
        }
        C.disconnect ();
    } catch (std::exception const &e){
        cerr << e.what() << std::endl;
        return 1;
    }

}

PostgresSqlDriver::~PostgresSqlDriver() {

}

//
//PostgresSql::executeQuery(std::string query) {
//
//    try {
////        pqxx::connection c;
////        pqxx::work w(c);
////        pqxx::result r = w.exec(query.c_str());
////        w.commit();
////        std::cout << r[0][0].as<int>() << std::endl;
//    }
//    catch (std::exception const &e)
//    {
//        std::cerr << e.what() << std::endl;
//        return 1;
//    }
//}