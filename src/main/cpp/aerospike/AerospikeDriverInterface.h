//
// Created by Mahmoud Taabodi on 7/12/17.
//

#ifndef AerospikeDriverInterface_H
#define AerospikeDriverInterface_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string>
#include <memory>
#include <vector>

#include <aerospike/aerospike.h>
#include <aerospike/aerospike_key.h>
#include <aerospike/as_error.h>
#include <aerospike/as_record.h>
#include <aerospike/as_status.h>
#include "AtomicLong.h"
class EntityToModuleStateStats;
class AerospikeDriverInterface {
public:

EntityToModuleStateStats* entityToModuleStateStats;
std::string stringValueSeperator;
AerospikeDriverInterface();
virtual ~AerospikeDriverInterface();

virtual std::string get_cache(std::string snamespace,
                              std::string sset,
                              std::string skey,
                              std::string sbin,
                              int& cacheResultStatus) = 0;

virtual void set_cache(std::string snamespace,
                       std::string sset,
                       std::string skey,
                       std::string sbin,
                       std::string svalue,
                       int ttlInSeconds) = 0;

virtual void set_cache_async(std::string snamespace,
                             std::string sset,
                             std::string skey,
                             std::string sbin,
                             std::string svalue,
                             int ttlInSeconds) = 0;

virtual long addValueAndRead(std::string snamespace,
                             std::string sset,
                             std::string skey,
                             std::string sbin,
                             int valueToAdd,
                             int ttlInSeconds) = 0;

virtual std::string addValueAndRead(std::string snamespace,
                                    std::string sset,
                                    std::string skey,
                                    std::string sbin,
                                    std::string valueToAdd,
                                    int ttlInSeconds) = 0;

virtual std::string readTheBin(std::string snamespace,
                               std::string sset,
                               std::string skey,
                               std::string sbin) = 0;

virtual void removeRecordWithKey(std::string key,
                                 std::string namespaceName,
                                 std::string set) = 0;

virtual std::string setValueAndRead(std::string snamespace,
                                    std::string sset,
                                    std::string skey,
                                    std::string sbin,
                                    std::string valueToAdd,
                                    int ttlInSeconds) = 0;
};



#endif //COMMON_AEROSPIKECLIENT_H
