//
// Created by Mahmoud Taabodi on 7/12/16.
//

#ifndef COMMON_AEROSPIKEUTILS_H
#define COMMON_AEROSPIKEUTILS_H


/*******************************************************************************
 * Copyright 2008-2016 by Aerospike.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 ******************************************************************************/


//==========================================================
// Includes
//

#include <errno.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <aerospike/aerospike.h>
#include <aerospike/aerospike_index.h>
#include <aerospike/aerospike_key.h>
#include <aerospike/aerospike_udf.h>
#include <aerospike/as_bin.h>
#include <aerospike/as_bytes.h>
#include <aerospike/as_error.h>
#include <aerospike/as_config.h>
#include <aerospike/as_key.h>
#include <aerospike/as_operations.h>
#include <aerospike/as_password.h>
#include <aerospike/as_record.h>
#include <aerospike/as_record_iterator.h>
#include <aerospike/as_status.h>
#include <aerospike/as_string.h>
#include <aerospike/as_val.h>

#include "example_utils.h"


//==========================================================
// Constants
//

#define MAX_HOST_SIZE 1024
#define MAX_KEY_STR_SIZE 1024

#define OP_CASE_ASSIGN(__enum) \
	case __enum : \
		return #__enum; \


class AerospikeUtils {
    char DEFAULT_HOST[1];
    int DEFAULT_PORT;
    char DEFAULT_NAMESPACE[1];
    char DEFAULT_SET[1] ;
    char DEFAULT_KEY_STR[1] ;
    uint32_t DEFAULT_NUM_KEYS;

    char SHORT_OPTS_BASIC[8];
    struct option LONG_OPTS_BASIC[8];
    char SHORT_OPTS_MULTI_KEY[8];
    struct option LONG_OPTS_MULTI_KEY[8];

//==========================================================
// Globals
//

//------------------------------------------------
// The namespace and set used by all examples.
// Created using command line options:
// -n <namespace>
// -s <set name>
//
    char g_namespace[MAX_NAMESPACE_SIZE];
    char g_set[MAX_SET_SIZE];

//------------------------------------------------
// The test key used by all basic examples.
// Created using command line options:
// -n <namespace>
// -s <set name>
// -k <key string>
//
    as_key g_key;

//------------------------------------------------
// For examples that use many keys.
// Obtained using command line option:
// -K <number of keys>
//
    uint32_t g_n_keys;

//------------------------------------------------
// The host info used by all basic examples.
// Obtained using command line options:
// -h <host name>
// -p <port>
//
    static char g_host[MAX_HOST_SIZE];
    static int g_port;

//------------------------------------------------
// The optional user/password.
// Obtained using command line options:
// -U <user name>
// -P [<password>]
//
    static char g_user[AS_USER_SIZE];
    static char g_password[AS_PASSWORD_HASH_SIZE];

//------------------------------------------------
// The (string) value of the test key used by all
// basic examples. From command line option:
// -k <key string>
//
    static char g_key_str[MAX_KEY_STR_SIZE];

//==========================================================
// Command Line Options
//

//------------------------------------------------
// Parse command line options.
//
    bool example_get_opts(int argc, char* argv[], int which_opts);

    AerospikeUtils();
//------------------------------------------------
// Display supported command line options.
//
    static void
    usage(const char* short_opts);

//==========================================================
// Initialize asynchronous event loop
//
    bool
    example_create_event_loop();

//==========================================================
// Logging
//
    static bool
    example_log_callback(as_log_level level, const char * func, const char * file, uint32_t line, const char * fmt, ...);
//==========================================================
// Connect/Disconnect
//

//------------------------------------------------
// Connect to database cluster.
//
    void
    example_connect_to_aerospike(aerospike* p_as);

//------------------------------------------------
// Connect to database cluster, setting UDF
// configuration.
//
    void
    example_connect_to_aerospike_with_udf_config(aerospike* p_as,
                                                 const char* lua_user_path);

//------------------------------------------------
// Remove the test record from database, and
// disconnect from cluster.
//
    void
    example_cleanup(aerospike* p_as);

//==========================================================
// Database Operation Helpers
//

//------------------------------------------------
// Read the whole test record from the database.
//
    bool
    example_read_test_record(aerospike* p_as);

//------------------------------------------------
// Remove the test record from the database.
//
    void
    example_remove_test_record(aerospike* p_as);

//------------------------------------------------
// Read multiple-record examples' test records
// from the database.
//
    bool
    example_read_test_records(aerospike* p_as);
//------------------------------------------------
// Remove multiple-record examples' test records
// from the database.
//
    void
    example_remove_test_records(aerospike* p_as);

//==========================================================
// UDF Function (Script) Registration
//

//------------------------------------------------
// Register a UDF function in the database.
//
    bool
    example_register_udf(aerospike* p_as, const char* udf_file_path);
//------------------------------------------------
// Remove a UDF function from the database.
//
    bool
    example_remove_udf(aerospike* p_as, const char* udf_file_path);


//==========================================================
// Secondary Index Registration
//

//------------------------------------------------
// Create a numeric secondary index for a
// specified bin in the database.
//
    bool
    example_create_integer_index(aerospike* p_as, const char* bin,
                                 const char* index);
//------------------------------------------------
// Create a geospatial secondary index for a
// specified bin in the database.
//
    bool
    example_create_2dsphere_index(aerospike* p_as, const char* bin,
                                  const char* index);

//------------------------------------------------
// Remove a secondary index from the database.
//
    void
    example_remove_index(aerospike* p_as, const char* index);

//==========================================================
// Logging Helpers
// TODO - put (something like) these in aerospike library?
//

    static void
    example_dump_bin(const as_bin* p_bin);
    void example_dump_record(const as_record* p_rec);

    static const char*
    operator_to_string(as_operator op);

    static void example_dump_op(const as_binop* p_binop);
    void example_dump_operations(const as_operations* p_ops);

    int example_handle_udf_error(as_error* err, const char* prefix);

};


#endif //COMMON_AEROSPIKEUTILS_H
