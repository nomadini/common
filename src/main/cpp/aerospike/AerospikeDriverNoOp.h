//
// Created by Mahmoud Taabodi on 7/12/16.
//

#ifndef AerospikeDriverNoOp_H
#define AerospikeDriverNoOp_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string>
#include <memory>
#include <vector>

#include <aerospike/aerospike.h>
#include <aerospike/aerospike_key.h>
#include <aerospike/as_error.h>
#include <aerospike/as_record.h>
#include <aerospike/as_status.h>
#include "AtomicLong.h"
#include "AerospikeDriverInterface.h"

class EntityToModuleStateStats;

/*
   nice multi op examples
   http://www.aerospike.com/docs/client/c/usage/kvs/multiops.html

 */
class AerospikeDriverNoOp : public AerospikeDriverInterface {
public:
// Connect to the aerospike database cluster.
aerospike as;
std::string stringValueSeperator;
EntityToModuleStateStats* entityToModuleStateStats;
std::shared_ptr<gicapods::AtomicLong> numberOfCurrentReads;
std::shared_ptr<gicapods::AtomicLong> numberOfCurrentWrites;
AerospikeDriverNoOp();

std::string get_cache(std::string snamespace,
                      std::string sset,
                      std::string skey,
                      std::string sbin,
                      int& cacheResultStatus);

/**
 *	ttlInSeconds : The time-to-live (expiration) of the record in seconds.
 *	There are two special values that can be set in the record TTL:
 *	(*) ZERO (defined as AS_RECORD_DEFAULT_TTL), which means that the
 *	    record will adopt the default TTL value from the namespace.
 *	(*) 0xFFFFFFFF (also, -1 in a signed 32 bit int)
 *	    (defined as AS_RECORD_NO_EXPIRE_TTL), which means that the record
 *	    will get an internal "void_time" of zero, and thus will never expire.
 *
 *	Note that the TTL value will be employed ONLY on write/update calls.
 */
void set_cache(std::string snamespace,
               std::string sset,
               std::string skey,
               std::string sbin,
               std::string svalue,
               int ttlInSeconds);

void set_cache_async(std::string snamespace,
                     std::string sset,
                     std::string skey,
                     std::string sbin,
                     std::string svalue,
                     int ttlInSeconds);

long addValueAndRead(std::string snamespace,
                     std::string sset,
                     std::string skey,
                     std::string sbin,
                     int valueToAdd,
                     int ttlInSeconds);

std::string addValueAndRead(std::string snamespace,
                            std::string sset,
                            std::string skey,
                            std::string sbin,
                            std::string valueToAdd,
                            int ttlInSeconds);

std::string readTheBin(std::string snamespace,
                       std::string sset,
                       std::string skey,
                       std::string sbin);

void removeRecordWithKey(std::string key,
                         std::string namespaceName,
                         std::string set);

std::string setValueAndRead(std::string snamespace,
                            std::string sset,
                            std::string skey,
                            std::string sbin,
                            std::string valueToAdd,
                            int ttlInSeconds);
virtual ~AerospikeDriverNoOp();

};



#endif //COMMON_AEROSPIKECLIENT_H
