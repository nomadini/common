#ifndef COMMON_AeroRecordHolder_H
#define COMMON_AeroRecordHolder_H

#include "Object.h"
#include <aerospike/as_record.h>
class AeroRecordHolder : public Object {
public:
as_record* record;
AeroRecordHolder();
virtual ~AeroRecordHolder();

std::string getName();
};

#endif
