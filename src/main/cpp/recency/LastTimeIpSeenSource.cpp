//
// Created by Mahmoud Taabodi on 7/14/16.
//

#include "LastTimeIpSeenSource.h"
#include "AerospikeDriver.h"
#include "GUtil.h"

LastTimeIpSeenSource::LastTimeIpSeenSource(
        AerospikeDriverInterface* aeroSpikeDriver,
        std::string namespaceName,
        std::string setName,
        std::string binName) {

        this->aeroSpikeDriver = aeroSpikeDriver;
        this->namespaceName = namespaceName;
        this->setName = setName;
        this->binName = binName;

        assertAndThrow(!this->namespaceName.empty());
        assertAndThrow(!this->setName.empty());
        assertAndThrow(!this->binName.empty());
}

long LastTimeIpSeenSource::countAndGetTimesSeen(std::string& deviceIp, int ttlInSeconds) {
        return aeroSpikeDriver->addValueAndRead(namespaceName,
                                                setName,
                                                deviceIp,
                                                "timesSeen",
                                                1,
                                                ttlInSeconds);
}

bool LastTimeIpSeenSource::hasSeenInLastXSeconds(std::string& deviceIp) {
        NULL_CHECK(aeroSpikeDriver);
        int cacheResultStatus = 0;
        std::string value = aeroSpikeDriver->get_cache(namespaceName,
                                                       setName,
                                                       deviceIp,
                                                       binName,
                                                       cacheResultStatus);
        MLOG(3)<< "device Ip value seen : "<< value<<", cacheResultStatus : "<<cacheResultStatus;
        return !value.empty ();
}


void LastTimeIpSeenSource::markAsSeenInLastXSeconds(std::string& deviceIp,
                                                    int ttlInSeconds) {
        assertAndThrow (ttlInSeconds != AS_RECORD_DEFAULT_TTL &&
                        ttlInSeconds != AS_RECORD_NO_EXPIRE_TTL);

        aeroSpikeDriver->set_cache(namespaceName,
                                   setName,
                                   deviceIp,
                                   binName,
                                   "yes",
                                   ttlInSeconds);
}

bool LastTimeIpSeenSource::isBadIpByCount(std::string& deviceIp) {
        NULL_CHECK(aeroSpikeDriver);
        int cacheResultStatus = 0;
        std::string value = aeroSpikeDriver->get_cache(namespaceName,
                                                       setName,
                                                       deviceIp,
                                                       "badIpByCount",
                                                       cacheResultStatus);

        return !value.empty ();
}


void LastTimeIpSeenSource::markAsBadIpByCount(std::string& deviceIp,
                                              int ttlInSeconds) {
        assertAndThrow (ttlInSeconds != AS_RECORD_DEFAULT_TTL &&
                        ttlInSeconds != AS_RECORD_NO_EXPIRE_TTL);

        aeroSpikeDriver->set_cache(namespaceName,
                                   setName,
                                   deviceIp,
                                   "badIpByCount",
                                   "yes",
                                   ttlInSeconds);
}

LastTimeIpSeenSource::~LastTimeIpSeenSource() {

}
