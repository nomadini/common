
#include "ConcurrentQueueFolly.h"

template <class T>
ConcurrentQueueFolly<T>::ConcurrentQueueFolly()   : Object(__FILE__) {
        this->entityQueue =
                std::make_shared<tbb::concurrent_queue<std::shared_ptr<T> > >();

}

template <class T>
std::shared_ptr<tbb::concurrent_queue<std::shared_ptr<T> > >
ConcurrentQueueFolly<T>::ConcurrentQueueFolly::getEntityQueue() {
        return entityQueue;
}

template <class T>
long ConcurrentQueueFolly<T>::size() {
        return entityQueue->unsafe_size();
}

template <class T>
void ConcurrentQueueFolly<T>::enqueueEntity(std::shared_ptr<T> segment) {
        getEntityQueue()->push(segment);
}

template <class T>
void ConcurrentQueueFolly<T>::clear() {
        getEntityQueue()->clear();
}

template <class T>
bool ConcurrentQueueFolly<T>::empty() {
        return getEntityQueue()->empty();
}

template <class T>
boost::optional<std::shared_ptr<T> > ConcurrentQueueFolly<T>::dequeueEntity() {
        std::shared_ptr<T> message;
        if (getEntityQueue()->try_pop(message)) {
                boost::optional<std::shared_ptr<T> > optioanlValue =  message;
                return optioanlValue;
        }
        return boost::none;
}

template <class T>
ConcurrentQueueFolly<T>::~ConcurrentQueueFolly() {

}

#include "Entity.h"
#include "Segment.h"
#include "Feature.h"

template class ConcurrentQueueFolly<Entity>;
template class ConcurrentQueueFolly<Segment>;
template class ConcurrentQueueFolly<Feature>;
template class ConcurrentQueueFolly<std::string>;
