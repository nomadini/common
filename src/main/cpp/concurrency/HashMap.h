#ifndef Nomadini_HashMap_h
#define Nomadini_HashMap_h

#include <atomic>
#include <memory>
#include <string>
#include <vector>
#include <unordered_map>
#include "AtomicLong.h"
#include "HashMapCacheTraits.h"
#include "Object.h"
class EntityToModuleStateStats;
class IntWrapper;
#include <boost/thread.hpp>
namespace gicapods {

template<class K, class V>
class HashMap : public Object {
private:

boost::shared_mutex _access;

std::shared_ptr<gicapods::AtomicLong> numberOfPutCalls;

public:
std::shared_ptr<std::unordered_map<K, std::shared_ptr<V> > > map;

typedef HashMapCacheTraits<K,V> CacheTraits;
EntityToModuleStateStats* entityToModuleStateStats;
HashMap<K,V>();
virtual ~HashMap<K,V>();

void put(K key, std::shared_ptr<V> value);

void putIfNotExisting(K key, std::shared_ptr<V> value);

std::shared_ptr<V> getOrCreate(K key);

std::shared_ptr<V> get(K key);

std::shared_ptr<V> getOptional(K key);

void checkMapSize();

bool exists(K key);

long size();

void clear();
};

}

#endif
