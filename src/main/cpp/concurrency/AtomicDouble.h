//
//  AtomicDouble.h
//  PicoDB
//
//  Created by Mahmoud Taabodi on 5/31/14.
//  Copyright (c) 2014 Mahmoud Taabodi. All rights reserved.
//

#ifndef GICO_AtomicDouble_h
#define GICO_AtomicDouble_h

#include <atomic>
#include <memory>
#include "Object.h"

#include <boost/thread.hpp>
namespace gicapods {

class AtomicDouble : public Object {

private:

boost::shared_mutex _access;
std::atomic<double> doubleValue;

public:
AtomicDouble();

AtomicDouble(double val);

double getValue();
double getValueRound(int percision = 2);

void setValue(double val);

void addValue(double val);

void decrement(double val);
};

} //end of namespace

#endif
