#ifndef gicapods_ConcurrentHashMapContainer_h
#define gicapods_ConcurrentHashMapContainer_h

#include <atomic>
#include <memory>
#include <tbb/concurrent_hash_map.h>
class EntityToModuleStateStats;
#include <boost/thread.hpp>
#include "ConcurrentHashMap.h"
#include "Object.h"
namespace gicapods {

template<class K, class V>
class ConcurrentHashMapContainer : public Object {
public:
ConcurrentHashMapContainer<K,V>();
virtual ~ConcurrentHashMapContainer<K,V>();
std::shared_ptr<ConcurrentHashMap<K,V> > map;
};

}

#endif
