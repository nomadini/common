#include "ConcurrentHashSet.h"
#include "GUtil.h"
#include "EntityToModuleStateStats.h"
#include "AtomicLong.h"


template <class V>
gicapods::ConcurrentHashSet<V>::ConcurrentHashSet()   : Object(__FILE__) {
        set = std::make_shared<tbb::concurrent_unordered_set<V> > ();
        numberOfPutCalls = std::make_shared<gicapods::AtomicLong>();
}

template <class V>
gicapods::ConcurrentHashSet<V>::~ConcurrentHashSet() {

}


template <class V>
void gicapods::ConcurrentHashSet<V>::put(V value) {
        // get shared access
        boost::shared_lock<boost::shared_mutex> lock(_access);

        set->insert(value);
        checkHashSetSize();
}

template <class V>
void gicapods::ConcurrentHashSet<V>::putIfNotExisting(V value) {
        // get shared access
        boost::shared_lock<boost::shared_mutex> lock(_access);

        if (set->find(value) != set->end()) {
                set->insert(value);
                checkHashSetSize();

        }

}


template <class V>
bool gicapods::ConcurrentHashSet<V>::exists(V value) {
        // get shared access
        boost::shared_lock<boost::shared_mutex> lock(_access);

        if (set->find(value) != set->end()) {
                return true;
        }
        return false;
}

template <class V>
long gicapods::ConcurrentHashSet<V>::size() {
        // get shared access
        boost::shared_lock<boost::shared_mutex> lock(_access);

        return (long) set->size();
}
template <class V>
void gicapods::ConcurrentHashSet<V>::checkHashSetSize() {

        // get shared access
        boost::shared_lock<boost::shared_mutex> lock(_access);

        //we perform this check every 100 calls
        numberOfPutCalls->increment();
        if (numberOfPutCalls->getValue() > 100) {
                if (size() > 10000) {
                        entityToModuleStateStats->addStateModuleForEntity(
                                "ERROR_SET_TOO_BIG",
                                "ConcurrentHashSet"+ CacheTraits::getValueName(),
                                EntityToModuleStateStats::all,
                                EntityToModuleStateStats::exception
                                );
                }

                numberOfPutCalls->setValue(0);
        }
}
template <class V>
void gicapods::ConcurrentHashSet<V>::clear() {

        //clear method for noraml tbb set is not thread safe,
        //here we get a write lock to clear the method,
        //in all other methods, we are getting shared access lock


        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(_access);

        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access


        set->clear();
}

template <class V>
std::shared_ptr<tbb::concurrent_unordered_set<V> > gicapods::ConcurrentHashSet<V>::getCopyOfSet() {

        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(_access);

        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access
        auto copyOfSet = std::make_shared<tbb::concurrent_unordered_set<V> > ();

        typename tbb::concurrent_unordered_set<V>::iterator iter;
        for (iter = set->begin ();
             iter != set->end ();
             iter++) {
                copyOfSet->insert(*iter);
        }

        return copyOfSet;
}

#include "TargetGroup.h"
template class gicapods::ConcurrentHashSet<std::string>;
template class gicapods::ConcurrentHashSet<int>;
