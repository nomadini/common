/*
 * AtomicBoolean.h
 *
 *  Created on: Sep 6, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_CONCURRENCY_ATOMICBOOLEAN_H_
#define GICAPODS_GICAPODSSERVER_SRC_CONCURRENCY_ATOMICBOOLEAN_H_

#include <atomic>
#include <memory>
#include "Object.h"
namespace gicapods {

class AtomicBoolean : public Object {

private:

std::shared_ptr<std::atomic<bool> > booleanValue;

public:
AtomicBoolean();

AtomicBoolean(bool value);

bool getValue();

void setValue(bool val);

void alternate();
};

} //end of namespace




#endif /* GICAPODS_GICAPODSSERVER_SRC_CONCURRENCY_ATOMICBOOLEAN_H_ */
