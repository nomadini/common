
#include "ConcurrentHashMapContainer.h"
#include "ConcurrentHashMap.h"

template <class K, class V>
gicapods::ConcurrentHashMapContainer<K, V>::ConcurrentHashMapContainer()   : Object(__FILE__) {
        map = std::make_shared<gicapods::ConcurrentHashMap<K, V> > ();
}

template <class K, class V>
gicapods::ConcurrentHashMapContainer<K, V>::~ConcurrentHashMapContainer() {

}

#include "IntWrapper.h"
#include "BooleanObject.h"
template class gicapods::ConcurrentHashMapContainer<std::string, IntWrapper>;
template class gicapods::ConcurrentHashMapContainer<int, BooleanObject>;
