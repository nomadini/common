/*
 * QueueExample.h
 *
 *  Created on: Mar 15, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_CONCURRENCY_QUEUEEXAMPLE_H_
#define GICAPODS_GICAPODSSERVER_SRC_CONCURRENCY_QUEUEEXAMPLE_H_


#include <iostream>

#include "string.h"

using namespace std;

boost::atomic_int producer_count(0);
boost::atomic_int consumer_count(0);

boost::lockfree::queue<int> queue(128);
boost::lockfree::queue<char*> browserIdQueue(1000);

const int iterations = 10000000;
const int producer_thread_count = 4;
const int consumer_thread_count = 4;

void producer(void) {
								for (int i = 0; i != iterations; ++i) {
																int value = ++producer_count;
																while (!queue.push(value))
																								;
								}
}

boost::atomic<bool> done(false);
void consumer(void) {
								int value;
								while (!done) {
																while (queue.pop(value))
																								++consumer_count;
								}

								while (queue.pop(value))
																++consumer_count;
}

int producerConsumerExample() {
								using namespace std;
								cout << "boost::lockfree::queue is ";
								if (!queue.is_lock_free())
																cout << "not ";
								cout << "lockfree" << endl;

								boost::thread_group producer_threads, consumer_threads;

								for (int i = 0; i != producer_thread_count; ++i)
																producer_threads.create_thread(producer);

								for (int i = 0; i != consumer_thread_count; ++i)
																consumer_threads.create_thread(consumer);

								producer_threads.join_all();
								done = true;

								consumer_threads.join_all();

								cout << "produced " << producer_count << " objects." << endl;
								cout << "consumed " << consumer_count << " objects." << endl;

}

void readBrowserIdFromQueueJob() {

								int k = 0;
								while (true) {
																char* deviceExampleId;
																browserIdQueue.pop(deviceExampleId);
																std::string bId(deviceExampleId);
																//cout << " " <<k<<"  : deviceExampleId popped from queue : " << bId << std::endl;
																printf("2 string1: %s", deviceExampleId);
																k++;
																if (k == 10)
																								break;
//		if(deviceExampleId !=NULL) delete deviceExampleId;

								}
}
//hello
int readBrowserIdFromQueue() {

								boost::thread_group consumer_threads;

								for (int i = 0; i != 1; ++i)
																consumer_threads.create_thread(readBrowserIdFromQueueJob);

								consumer_threads.join_all();

								cout << "consumed " << consumer_count << " objects." << endl;

}

void putBrowserIdsInQueueJob() {
								int j = 0;
								while (j < 100) {
																int i = RandomUtil::sudoRandomNumber(3);
																std::string deviceExampleId("browser");
																deviceExampleId += StringUtil::toStr(i);
																char* bId = (char*) deviceExampleId.c_str();
																cout << j << " put " << deviceExampleId << "in queue\n";
																browserIdQueue.push(bId);
																j++;
								}
}
void putBrowserIdsInQueue() {

								boost::thread_group producer_threads;
								int numberOfBrowserIds = 1;
								for (int i = 0; i != numberOfBrowserIds; ++i)
																producer_threads.create_thread(putBrowserIdsInQueueJob);

								producer_threads.join_all();
//	done = true;
								cout << "put " << numberOfBrowserIds << " browserIds in the queue" << endl;

}

int main(int argc, char* argv[]) {

								putBrowserIdsInQueue();
								readBrowserIdFromQueue();
								producerConsumerExample();


}

#endif /* GICAPODS_GICAPODSSERVER_SRC_CONCURRENCY_QUEUEEXAMPLE_H_ */
