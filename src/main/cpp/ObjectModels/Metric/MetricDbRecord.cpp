//
// Created by Mahmoud Taabodi on 7/3/16.
//

#include "MetricDbRecord.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "DateTimeUtil.h"


std::string MetricDbRecord::getName() {
        return "MetricDbRecord";
}

MetricDbRecord::MetricDbRecord() : Object(__FILE__) {
        id = 0;
        value = 0;
}

void MetricDbRecord::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "hostName", hostName, value);
        JsonUtil::addMemberToValue_FromPair (doc, "appName", appName, value);
        JsonUtil::addMemberToValue_FromPair (doc, "appVersion", appVersion, value);
        JsonUtil::addMemberToValue_FromPair (doc, "domain", domain, value);

        JsonUtil::addMemberToValue_FromPair (doc, "module", module, value);
        JsonUtil::addMemberToValue_FromPair (doc, "state", state, value);
        JsonUtil::addMemberToValue_FromPair (doc, "entity", entity, value);

        JsonUtil::addMemberToValue_FromPair (doc, "value", this->value, value);
        JsonUtil::addMemberToValue_FromPair (doc, "timeOfRecord", DateTimeUtil::dateTimeToStr(timeOfRecord),value);
        JsonUtil::addMemberToValue_FromPair (doc, "createdAt", DateTimeUtil::dateTimeToStr(createdAt),value);
        JsonUtil::addMemberToValue_FromPair (doc, "updatedAt", DateTimeUtil::dateTimeToStr(updatedAt),value);

        JsonUtil::addMemberToValue_FromPair (doc, "level", level,value);

}

std::string MetricDbRecord::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}
