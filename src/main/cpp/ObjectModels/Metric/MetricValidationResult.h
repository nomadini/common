//
// Created by Mahmoud Taabodi on 7/3/16.
//

#ifndef MetricValidationResult_H
#define MetricValidationResult_H
#include "Object.h"
#include "AtomicDouble.h"
#include <tbb/concurrent_hash_map.h>
#include <memory>
#include <string>
class MetricValidationResult;


class MetricValidationResult : public Object {
public:
std::string metricUniqueName;
std::string result;
//like success count or percentage, or failure count or percentage or etc
std::string resultMeasure;
std::string failureType;

MetricValidationResult();
std::string toJson();

virtual ~MetricValidationResult();
};


#endif //COMMON_METRICDBRECORD_H
