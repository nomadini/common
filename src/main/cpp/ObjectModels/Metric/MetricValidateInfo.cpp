//
// Created by Mahmoud Taabodi on 7/3/16.
//

#include "MetricValidateInfo.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "CollectionUtil.h"
#include "MetricDbRecord.h"
#include "MetricValidationResult.h"
MetricValidateInfo::MetricValidateInfo() : Object(__FILE__) {

}

std::string MetricValidateInfo::toJson() {
        auto doc = JsonUtil::createDcoumentAsObjectDoc ();
        RapidJsonValueTypeNoRef valueObject(rapidjson::kObjectType);

        JsonUtil::addMemberToValue_FromPair (doc.get(), "metricUniqueName", metricUniqueName, valueObject);


        return JsonUtil::valueToString (valueObject);
}

MetricValidateInfo::~MetricValidateInfo() {

}
