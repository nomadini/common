//
// Created by Mahmoud Taabodi on 7/3/16.
//

#ifndef PercentageMetricValidateInfo_H
#define PercentageMetricValidateInfo_H
#include "Object.h"
#include "AtomicDouble.h"
#include "ConcurrentHashMap.h"
#include "MetricValidateInfo.h"
class DateTimeUtil;


#include <tbb/concurrent_hash_map.h>
#include <memory>
class PercentageMetricValidateInfo;
class MetricValidationResult;



class PercentageMetricValidateInfo : public MetricValidateInfo, public Object {
public:

int minPercentageThreshold;
int maxPercentageThreshold;
std::string totalMetricSourceName;

PercentageMetricValidateInfo(
        int minPercentageThreshold,
        int maxPercentageThreshold,
        std::string metricUniqueName,
        std::string totalMetricSourceName
        );
virtual std::shared_ptr<MetricValidationResult> validate(std::shared_ptr<gicapods::ConcurrentHashMap<std::string, MetricDbRecord>  >
                                                         mapOfValidateInfoNameToMetricDbRecord);
std::string toJson();
virtual ~PercentageMetricValidateInfo();
};


#endif //COMMON_METRICDBRECORD_H
