//
// Created by Mahmoud Taabodi on 3/6/16.
//

#ifndef COMMON_STATEPERCENTAGEStateToStateMetricMap_H
#define COMMON_STATEPERCENTAGEStateToStateMetricMap_H

#include "AtomicDouble.h"
#include "ConcurrentHashMap.h"
class DateTimeUtil;
class StateMetric;
#include "Object.h"

#include <tbb/concurrent_hash_map.h>

class StateToStateMetricMap;



/**
 * this class has bunch of states in a map, you can provide a value for
 * each state and get the percentage of each state
 *
 * you can also get a 1 minute, 15 minute , 1 hour percentage of each state in that period
 */
class StateToStateMetricMap : public Object {
public:

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, StateMetric > > statesToStateMetrics;

StateToStateMetricMap();

void clearRates();

void update(
        std::string state,
        std::string metricLevel,
        double value);

/**
 * percentages
 */
double getOneMinuteStatePerc(std::string state);

bool hasState(std::string state);

/**
 * values AKA counts
 */
double getOneMinuteStateValue(std::string state);


std::vector<std::string> getAllPossibleStatesInLastOneMinue();

std::vector<std::string> getAllPossibleStates();
private:
void addStateToMap(std::string state,
                   std::string metricLevel,
                   double value,
                   std::shared_ptr<gicapods::ConcurrentHashMap<std::string,
                                                               StateMetric > > map);
};
#endif //COMMON_STATEPERCENTAGEStateToStateMetricMap_H
