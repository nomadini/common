//
// Created by Mahmoud Taabodi on 3/7/16.
//

#include "EntityToModuleStateStats.h"
#include "CollectionUtil.h"
#include <set>
#include "JsonArrayUtil.h"
#include "GUtil.h"
#include <boost/foreach.hpp>
#include "ModulesToStatePercentages.h"
#include "StateToStateMetricMap.h"
#include "StateMetric.h"
#include "ConcurrentHashMap.h"
#include "ConfigService.h"
#include "EMSVRecord.h"

std::shared_ptr<EntityToModuleStateStats> EntityToModuleStateStats::globalEntityToModuleStateStats;
void EntityToModuleStateStats::clearAllMetrics() {
        entityToFilterStatePercentages->clear();
}
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ModulesToStatePercentages > >  EntityToModuleStateStats::getEntityToFilterStatePercentages() {
        assertAndThrow (entityToFilterStatePercentages != NULL);
        return entityToFilterStatePercentages;
}

EntityToModuleStateStats::EntityToModuleStateStats() {
        entityToFilterStatePercentages = std::make_shared<gicapods::ConcurrentHashMap<std::string, ModulesToStatePercentages > > ();
        isStateRecordingDisabled = std::make_shared<gicapods::AtomicBoolean>(false);
        stateQueue = std::make_shared<tbb::concurrent_queue<std::shared_ptr<EMSVRecord> > > ();
        comboKeyToLastInsertedInSecondMap = std::make_shared<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong > > ();
}

void EntityToModuleStateStats::addStateModuleForEntity(std::string state,
                                                       std::string module,
                                                       std::string entity) {
        addStateModuleForEntityWithValue(state, 1, module, entity, "DEBUG");
}

void EntityToModuleStateStats::addStateModuleForEntity(std::string state,
                                                       std::string module,
                                                       std::string entity,
                                                       std::string metricLevel) {
        addStateModuleForEntityWithValue(state, 1, module, entity, metricLevel);
}

void EntityToModuleStateStats::addStateModuleForEntityWithValue(std::string state, double value, std::string module, std::string entity) {
        addStateModuleForEntityWithValue(state, value, module, entity, "DEBUG");
}

void EntityToModuleStateStats::addStateModuleForEntityWithValue(std::string state,
                                                                double value,
                                                                std::string module,
                                                                std::string entity,
                                                                std::string metricLevel) {
        if (isStateRecordingDisabled->getValue() == true) {
                //this option can be set to true, to check the heap allocations for example
                //or speed up the app
                LOG_EVERY_N(ERROR, 10000)<< "entity recording is disabled";
                return;
        }

        if (entity.empty()) {
                EXIT(" entity is empty for module " +  module  +  ", state : " +state);
        }
        if (module.empty()) {
                EXIT(" module is empty for entity " +  entity  +  ", state : " +state);
        }
        if (state.empty()) {
                EXIT(" state is empty for entity " +  entity  +  ", module : " +module);
        }

        auto msg = std::make_shared<EMSVRecord>();
        msg->state = state;
        msg->value = value;
        msg->module = module;
        msg->entity = entity;
        msg->metricLevel = metricLevel;
        stateQueue->push (msg);
}

EntityToModuleStateStats::~EntityToModuleStateStats() {
        while(stateDequeueThreadIsRunning->getValue() ==  true)  {
                //try stopping the thread before exiting
                stopStateConsumingThread->setValue(true);
                gicapods::Util::sleepViaBoost(_L_, 2);  //until consumer thread goes out
        };
}

void EntityToModuleStateStats::dequeueStates() {
        while(stopStateConsumingThread->getValue() == false) {
                stateDequeueThreadIsRunning->setValue(true);
                try {
                        while(stateQueue->unsafe_size() > 0) {
                                consumeFromQueue();
                        }

                } catch(...) {
                        gicapods::Util::showStackTrace();
                }

                //very important to sleep for a second
                //or the cpu usage goes high
                gicapods::Util::sleepMiliSecond (2000);
        }

        stateDequeueThreadIsRunning->setValue(false);
}

void EntityToModuleStateStats::consumeFromQueue() {
        std::shared_ptr<EMSVRecord> msg;
        if (!stateQueue->try_pop(msg)) {
                return;
        }

        std::string state = msg->state;
        double value = msg->value;
        std::string module = msg->module;
        std::string entity = msg->entity;
        std::string metricLevel = msg->metricLevel;
        //this line is very important for debugging
        // MLOG(3)<<" addStateModuleForEntity : state : " <<  state << " module : "<< module << " entity : "<< entity;


        auto moduleStatePercentages = entityToFilterStatePercentages->getOptional (entity);
        if (moduleStatePercentages == nullptr) {
                //MLOG(3)<<" new entity  : state : " <<  state << " module : "<< module << " entity : "<< entity;
                //this is a new entity
                auto value = std::make_shared<ModulesToStatePercentages> ();
                getEntityToFilterStatePercentages ()->put (entity, value);
                moduleStatePercentages = value;
        }

        //MLOG(3)<<" we found the entity , state : " <<  state << " module : "<< module << " entity : "<< entity;
        //we found the entity
        auto stateToStateMetricMap = moduleStatePercentages->moduleToStatePercentage->getOptional(module);
        if (stateToStateMetricMap == nullptr) {
                //MLOG(3)<<" a new module for entity found, module : " << module;
                auto value = std::make_shared<StateToStateMetricMap> ();
                moduleStatePercentages->moduleToStatePercentage->put(module, value);
                stateToStateMetricMap = value;
        }

        stateToStateMetricMap->update(state, metricLevel, value);
}
double EntityToModuleStateStats::getValueOfStateModuleForEntity(std::string state,
                                                                std::string module,
                                                                std::string entity) {
        auto moduleStatePercentages = getEntityToFilterStatePercentages ()->getOptional (entity);
        if (moduleStatePercentages != nullptr) {
                return moduleStatePercentages->getValueOfStateModule (state, module);
        } else {
                return 0;
        }
}

bool EntityToModuleStateStats::hasStateModuleEntity(std::string state,
                                                    std::string module,
                                                    std::string entity) {
        auto moduleStatePercentages = getEntityToFilterStatePercentages ()->getOptional (entity);
        if (moduleStatePercentages != nullptr) {
                return moduleStatePercentages->hasStateModule (state, module);
        } else {
                return false;
        }
}


std::vector<std::string> EntityToModuleStateStats::getAllPossibleStatesForModuleAndEntity(std::string module, std::string entity) {

        auto moduleStatePercentages = getEntityToFilterStatePercentages ()->getOptional (entity);
        if (moduleStatePercentages != nullptr) {
                return moduleStatePercentages->getAllPossibleStatesForModule (module);
        } else {
                std::vector<std::string> empty;
                return empty;
        }
}

std::vector<std::string> EntityToModuleStateStats::getAllPossibleStatesForModule(std::string module) {
        std::set<std::string> allStates;
        auto map = getEntityToFilterStatePercentages()->getCopyOfMap();
        tbb::concurrent_hash_map<std::string, std::shared_ptr<ModulesToStatePercentages> >::iterator iter;
        for (iter = map->begin ();
             iter != map->end ();
             iter++) {
                auto moduleStatePercentages = iter->second;
                std::vector<std::string> possibleStates = moduleStatePercentages->getAllPossibleStatesForModule (module);
                for(std::string pState :  possibleStates) {
                        allStates.insert(pState);
                }
        }
        return CollectionUtil::convertSetToList(allStates);
}


std::vector<std::string>  EntityToModuleStateStats::getAllPossibleStates() {
        std::set<std::string> allStates;

        auto map = getEntityToFilterStatePercentages()->getCopyOfMap();
        tbb::concurrent_hash_map<std::string, std::shared_ptr<ModulesToStatePercentages> >::iterator iter;
        for (iter = map->begin ();
             iter != map->end ();
             iter++) {
                auto moduleStatePercentages = iter->second;
                std::vector<std::string> possibleStates = moduleStatePercentages->getAllPossibleStates ();
                for(std::string pState :  possibleStates) {
                        allStates.insert(pState);
                }
        }
        return CollectionUtil::convertSetToList(allStates);
}

std::vector<std::string> EntityToModuleStateStats::getAllEntities(){
        std::vector<std::string> entities;

        auto map = getEntityToFilterStatePercentages()->getCopyOfMap();
        tbb::concurrent_hash_map<std::string, std::shared_ptr<ModulesToStatePercentages> >::iterator iter;
        for (iter = map->begin ();
             iter != map->end ();
             iter++) {
                entities.push_back(iter->first);
        }
        return entities;
}
std::vector<std::string> EntityToModuleStateStats::getAllModulesForEntity(std::string entity){
        std::vector<std::string> modules;

        auto map = getEntityToFilterStatePercentages()->getCopyOfMap();
        tbb::concurrent_hash_map<std::string, std::shared_ptr<ModulesToStatePercentages> >::iterator iter;
        for (iter = map->begin ();
             iter != map->end ();
             iter++) {

                auto moduleToStates =  iter->second->moduleToStatePercentage->getCopyOfMap();
                for (tbb::concurrent_hash_map<std::string, std::shared_ptr<StateToStateMetricMap> >::iterator iterMod = moduleToStates->begin ();
                     iterMod != moduleToStates->end ();
                     iterMod++) {
                        modules.push_back(iterMod->first);
                }
        }

        return modules;
}
std::vector<std::string> EntityToModuleStateStats::getAllStatesForModule(std::string module){
        std::set<std::string> statesSet;
        assertAndThrow(getEntityToFilterStatePercentages() != nullptr);

        auto map = getEntityToFilterStatePercentages()->getCopyOfMap();
        tbb::concurrent_hash_map<std::string, std::shared_ptr<ModulesToStatePercentages> >::iterator iter;
        for (iter = map->begin ();
             iter != map->end ();
             iter++) {
                std::shared_ptr<ModulesToStatePercentages> moduleState = iter->second;
                auto moduleStateMap = moduleState->moduleToStatePercentage->getCopyOfMap();
                for (auto iterMod = moduleStateMap->begin();
                     iterMod != moduleStateMap->end();
                     iterMod++) {
                        std::shared_ptr<StateToStateMetricMap> stateMetric = iterMod->second;
                        auto stateMetricMap = stateMetric->statesToStateMetrics->getCopyOfMap();
                        for (auto iterMod2 = stateMetricMap->begin();
                             iterMod2 != stateMetricMap->end();
                             iterMod2++) {
                                statesSet.insert(iterMod2->first);
                        }
                }
        }
        return CollectionUtil::convertSetToList(statesSet);
}

std::vector<std::string> EntityToModuleStateStats::getAllModulesInLastOneMintue() {
        std::set<std::string> statesSet;
        auto map = getEntityToFilterStatePercentages()->getCopyOfMap();
        tbb::concurrent_hash_map<std::string, std::shared_ptr<ModulesToStatePercentages> >::iterator iter;
        for (iter = map->begin ();
             iter != map->end ();
             iter++) {
                std::shared_ptr<ModulesToStatePercentages> moduleState = iter->second;
                statesSet.insert(iter->first);
        }

        return CollectionUtil::convertSetToList(statesSet);

}

std::vector<std::string> EntityToModuleStateStats::getAllModules() {
        std::set<std::string> statesSet;
        auto map = getEntityToFilterStatePercentages()->getCopyOfMap();
        tbb::concurrent_hash_map<std::string, std::shared_ptr<ModulesToStatePercentages> >::iterator iter;
        for (iter = map->begin ();
             iter != map->end ();
             iter++) {
                std::shared_ptr<ModulesToStatePercentages> moduleState = iter->second;
                statesSet.insert(iter->first);
        }

        return CollectionUtil::convertSetToList(statesSet);
}

void EntityToModuleStateStats::addStateModuleForEntityPerMinute(
        std::string state, std::string module, std::string entityId) {
        addStateModuleForEntityWithValuePerMinute(state, 1, module, entityId);
}

void EntityToModuleStateStats::addStateModuleForEntityWithValuePerMinute(
        std::string state,
        double value,
        std::string module,
        std::string entityId) {

        std::string comboKey = state+"_module_"+module+"_entityId_"+entityId;
        auto lastTimeInserted = comboKeyToLastInsertedInSecondMap->getOptional(comboKey);
        if (lastTimeInserted == nullptr) {
                lastTimeInserted = std::make_shared<gicapods::AtomicLong>(DateTimeUtil::getNowInSecond());
                addStateModuleForEntityWithValue(state, value, module, entityId);
                comboKeyToLastInsertedInSecondMap->put(comboKey, lastTimeInserted);

        } else if (DateTimeUtil::getNowInSecond() - lastTimeInserted->getValue() <= 60) {
                //we cannot insert data, data is already in the map
        } else {
                //we must insert now
                lastTimeInserted->setValue(DateTimeUtil::getNowInSecond());
                addStateModuleForEntityWithValue(state, value, module, entityId);
                comboKeyToLastInsertedInSecondMap->put(comboKey, lastTimeInserted);
        }
}

std::shared_ptr<EntityToModuleStateStats>
EntityToModuleStateStats::getInstance(gicapods::ConfigService* configService) {
        static auto ins = std::make_shared<EntityToModuleStateStats> ();
        return ins;
}
