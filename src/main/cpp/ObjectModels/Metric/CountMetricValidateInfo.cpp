//
// Created by Mahmoud Taabodi on 7/3/16.
//

#include "CountMetricValidateInfo.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "MetricDbRecord.h"
#include "MetricValidationResult.h"

CountMetricValidateInfo::CountMetricValidateInfo(
        std::string metricUniqueName,
        int minThreshold,
        int maxThreshold) : Object(__FILE__) {
        this->metricUniqueName = metricUniqueName;
        this->minThreshold = minThreshold;
        this->maxThreshold = maxThreshold;
}

std::string CountMetricValidateInfo::toJson() {
        auto doc = JsonUtil::createDcoumentAsObjectDoc ();
        RapidJsonValueTypeNoRef valueObject(rapidjson::kObjectType);

        JsonUtil::addMemberToValue_FromPair (doc.get(), "metricUniqueName", metricUniqueName, valueObject);


        return JsonUtil::valueToString (valueObject);
}


std::shared_ptr<MetricValidationResult> CountMetricValidateInfo::validate(std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<MetricDbRecord>> >
                                                                          mapOfValidateInfoNameToMetricDbRecord) {
        auto validationResult = std::make_shared<MetricValidationResult>();
        tbb::concurrent_hash_map<std::string, std::shared_ptr<MetricDbRecord>>::accessor accessor;
        if (mapOfValidateInfoNameToMetricDbRecord->find(accessor, metricUniqueName)) {
                if (accessor->second->value >= minThreshold &&
                    accessor->second->value <= maxThreshold) {
                        validationResult->result = _toStr("success");
                        validationResult->resultMeasure = _toStr(accessor->second->value);
                } else {
                        LOG(ERROR) << "metricUniqueName : "<<metricUniqueName <<" is in red state, value : "<<accessor->second->value;
                        validationResult->result = _toStr("failure");
                        validationResult->failureType = _toStr("outside-threshold");
                        validationResult->resultMeasure = _toStr(accessor->second->value);
                }
        } else {
                LOG(ERROR) << "metricUniqueName : "<<metricUniqueName <<" is in red state, value not found";
                validationResult->result = _toStr("failure");
                validationResult->failureType = _toStr("no-value-recorded");
                validationResult->resultMeasure = _toStr("NO-VALUE");
        }

        return validationResult;

}

CountMetricValidateInfo::~CountMetricValidateInfo() {

}
