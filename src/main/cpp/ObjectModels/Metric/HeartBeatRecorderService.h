//
// Created by Mahmoud Taabodi on 7/15/16.
//

#ifndef HeartBeatRecorderService_H
#define HeartBeatRecorderService_H



#include "Object.h"
#include <memory>
#include <string>
#include <vector>
#include <set>
#include <boost/thread.hpp>
class EntityToModuleStateStats;
class MySqlMetricService;
class MetricDbRecord;
#include "AtomicBoolean.h"
#include "PocoSession.h"
#include "AtomicDouble.h"
#include "ConfigService.h"
class HeartBeatRecorderService;


class HeartBeatRecorderService : public Object {

public:

EntityToModuleStateStats* entityToModuleStateStats;
std::string appName;
long appStartedInSecond;
std::shared_ptr<gicapods::AtomicBoolean> stopConsumingThread;
std::shared_ptr<gicapods::AtomicBoolean> dequeueThreadIsRunning;

HeartBeatRecorderService(EntityToModuleStateStats* entityToModuleStateStats,
                         std::string appName);

void thread();
void updateUpTimeThread();
void startThread();
virtual ~HeartBeatRecorderService();
};

#endif //BIDDER_ENTITYTOMODULESTATESTATSPERSISTENCESERVICE_H
