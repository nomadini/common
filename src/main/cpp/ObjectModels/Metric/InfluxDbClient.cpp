//
// Created by Mahmoud Taabodi on 7/15/16.
//

#include "InfluxDbClient.h"
#include "EntityToModuleStateStats.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "StateToStateMetricMap.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "MySqlMetricService.h"
#include "MySqlMetricService.h"
#include "MetricDbRecord.h"
#include "CollectionUtil.h"
#include "MetricDbRecord.h"
#include "NetworkUtil.h"
#include <boost/foreach.hpp>
#include "DateTimeUtil.h"
#include <thread>
#include "MetricDbRecord.h"
#include "StateMetric.h"
#include "HttpUtil.h"
#include "ModulesToStatePercentages.h"
#include "ConfigService.h"

InfluxDbClient::InfluxDbClient(
        EntityToModuleStateStats* entityToModuleStateStats,
        gicapods::ConfigService* configService) : Object(__FILE__) {
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->influxHost = configService->get("influxHost") + ":8086/write?db=";
        session = std::make_shared<PocoSession>(
                "InfluxDbClient",
                configService->getAsBooleanFromString("influxdbCallKeepAliveMode"),
                configService->getAsInt("influxdbKeepAliveTimeoutInMillis"),
                configService->getAsInt("influxdbCallTimeoutInMillis"),
                configService->get("influxdbHostUrl"),
                entityToModuleStateStats);
}

InfluxDbClient::~InfluxDbClient() {

}

void InfluxDbClient::recordMetricsInInfluxDb(std::string allInfluxdbRecords, std::string db) {

        std::string url =  influxHost + db;

        MLOG(20)<<"allInfluxdbRecords :\n"<< allInfluxdbRecords;

        auto request = PocoHttpRequest::createSimplePostRequest(
                url,
                allInfluxdbRecords);
        for (int i=0; i< 5; i++) {
                auto response = session->sendRequest(
                        *request);

                if (response->statusCode != 204) {
                        LOG(INFO)<<" request body failed"<<std::endl<<"startOfRequest_"<<request->body<<"_endOfRequest";
                        LOG(ERROR)<<"response->statusCode " << response->statusCode <<
                                "request with error : "<<
                                allInfluxdbRecords << std::endl;
                        entityToModuleStateStats->addStateModuleForEntity("ERROR_WRITING_DATA_INFLUX_"+
                                                                          _toStr(response->statusCode),
                                                                          "InfluxDbClient",
                                                                          EntityToModuleStateStats::all,
                                                                          EntityToModuleStateStats::exception);
                        if (i == 4) {
                                //this was last time ...exiting now
                                EXIT("exiting because of error in saving metrics");
                        }
                        //sleeping before next try
                        gicapods::Util::sleepViaBoost(_L_, 3);
                } else {
                        //response is Ok, breaking out of loop
                        break;
                }


        }

}

std::string InfluxDbClient::replaceIllegalCharacters(std::string& candidate) {
        candidate.erase(std::remove(candidate.begin(), candidate.end(), '\t'), candidate.end());
        candidate.erase(std::remove(candidate.begin(), candidate.end(), '\n'), candidate.end());

        // std::vector<std::string> badCharacters = {"'", ",", ")", "(", ":", ";", "_"," "};
        //https://docs.influxdata.com/influxdb/v1.3/write_protocols/line_protocol_tutorial/#special-characters-and-keywords
        //more relaxed
        std::vector<std::string> badCharacters = {"'", ",", " ", "=", "\"", "-"};
        for (auto badCharacter : badCharacters) {
                candidate = StringUtil::replaceStringCaseInsensitive(candidate, badCharacter, "");
        }

        return candidate;
}
