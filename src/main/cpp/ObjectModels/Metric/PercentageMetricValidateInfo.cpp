//
// Created by Mahmoud Taabodi on 7/3/16.
//

#include "PercentageMetricValidateInfo.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "MetricDbRecord.h"
#include "MetricValidationResult.h"

PercentageMetricValidateInfo::PercentageMetricValidateInfo(
        int minPercentageThreshold,
        int maxPercentageThreshold,
        std::string metricUniqueName,
        std::string totalMetricSourceName) : Object(__FILE__) {
        this->minPercentageThreshold = minPercentageThreshold;
        this->maxPercentageThreshold = maxPercentageThreshold;
        this->metricUniqueName = metricUniqueName;
        this->totalMetricSourceName = totalMetricSourceName;
}

std::string PercentageMetricValidateInfo::toJson() {
        auto doc = JsonUtil::createDcoumentAsObjectDoc ();
        RapidJsonValueTypeNoRef valueObject(rapidjson::kObjectType);

        JsonUtil::addMemberToValue_FromPair (doc.get(), "metricUniqueName", metricUniqueName, valueObject);


        return JsonUtil::valueToString (valueObject);
}

std::shared_ptr<MetricValidationResult> PercentageMetricValidateInfo::validate(std::shared_ptr<gicapods::ConcurrentHashMap<std::string, MetricDbRecord> >
                                                                               mapOfValidateInfoNameToMetricDbRecord)  {
        auto validationResult = std::make_shared<MetricValidationResult>();


        auto metricPercentageValue = mapOfValidateInfoNameToMetricDbRecord->getOptional(metricUniqueName);
        if (metricPercentageValue != nullptr) {

                auto totalMetricNameValue = mapOfValidateInfoNameToMetricDbRecord->getOptional(totalMetricSourceName);
                if (totalMetricNameValue != nullptr) {
                        if (totalMetricNameValue->value == 0) {
                                LOG(ERROR) << "metricUniqueName : "<<metricUniqueName <<" is in red state, total value is 0 : "<<totalMetricSourceName;
                                validationResult->result = _toStr("failure");
                                validationResult->failureType =_toStr("outside-threshold");
                                validationResult->resultMeasure = _toStr("TOTAL_VALUE=0");
                        }
                        double metricPercentage = metricPercentageValue->value /
                                                  totalMetricNameValue->value;

                        if (metricPercentage >= minPercentageThreshold &&
                            metricPercentage <= maxPercentageThreshold) {
                                validationResult->result = _toStr("success");
                        } else {
                                LOG(ERROR) << "metricUniqueName : "<<metricUniqueName <<" is in red state, metricPercentage : "<<metricPercentage;
                                validationResult->result = _toStr("failure");
                                validationResult->failureType = _toStr("outside-threshold");
                        }
                        validationResult->resultMeasure = _toStr(metricPercentage) + "%";
                } else {
                        LOG(ERROR) << "metricUniqueName : "<<metricUniqueName <<" is in red state, total value not found :  "<<totalMetricSourceName;
                        validationResult->result = _toStr("failure");
                        validationResult->failureType = _toStr("no-value-recorded");
                        validationResult->resultMeasure = _toStr("NO-VALUE");
                }

        } else {
                LOG(ERROR) << "metricUniqueName : "<<metricUniqueName <<" is in red state, value not found";
                validationResult->result = _toStr("failure");
                validationResult->failureType = _toStr("no-value-recorded");
                validationResult->resultMeasure = _toStr("NO-VALUE");
        }

        return validationResult;

}
PercentageMetricValidateInfo::~PercentageMetricValidateInfo() {

}
