//
// Created by Mahmoud Taabodi on 7/3/16.
//

#ifndef CountMetricValidateInfo_H
#define CountMetricValidateInfo_H
#include "Object.h"
#include "AtomicDouble.h"
#include <tbb/concurrent_hash_map.h>
#include <memory>
class MetricValidationResult;
class DateTimeUtil;


#include "MetricValidateInfo.h"
class CountMetricValidateInfo;



class CountMetricValidateInfo : public MetricValidateInfo, public Object {
public:

int minThreshold;
int maxThreshold;

CountMetricValidateInfo(std::string metricUniqueName,
                        int minThreshold,
                        int maxThreshold);
virtual std::shared_ptr<MetricValidationResult> validate(std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<MetricDbRecord> > >
                                                         mapOfValidateInfoNameToMetricDbRecord);
std::string toJson();

virtual ~CountMetricValidateInfo();
};


#endif //COMMON_METRICDBRECORD_H
