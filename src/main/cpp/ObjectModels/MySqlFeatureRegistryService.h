//
// Created by Mahmoud Taabodi on 2/15/16.
//

#ifndef MySqlFeatureRegistryService_h
#define MySqlFeatureRegistryService_h




#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "MySqlService.h"
#include "DateTimeUtil.h"
class Feature;
#include "Object.h"
class MySqlFeatureRegistryService : public Object {

public:

MySqlDriver* driver;

MySqlFeatureRegistryService(MySqlDriver* driver,
                            EntityToModuleStateStats* entityToModuleStateStats);

virtual ~MySqlFeatureRegistryService();

void writeFeatures(std::vector<std::shared_ptr<Feature> > allFeaturesToWrite);

std::string convertRecordToValueString(std::shared_ptr<Feature> feature);

std::vector<std::shared_ptr<Feature> > readAllFeatures(std::string timeUpdated);
};

#endif
