#include "Entity.h"
#include "GUtil.h"


std::map<Entity::Type, std::string> Entity::typeToEntity;

void Entity::initEntityMaps() {
								typeToEntity.insert(std::pair<Entity::Type, std::string>(Entity::Type::CLIENT, "CLIENT"));
								typeToEntity.insert(std::pair<Entity::Type, std::string>(Entity::Type::ADVERTISER, "ADVERTISER"));
								typeToEntity.insert(std::pair<Entity::Type, std::string>(Entity::Type::CAMPAIGN, "CAMPAIGN"));
								typeToEntity.insert(std::pair<Entity::Type, std::string>(Entity::Type::TARGET_GROUP, "TARGET_GROUP"));
								typeToEntity.insert(std::pair<Entity::Type, std::string>(Entity::Type::CREATIVE, "CREATIVE"));
}

std::string Entity::getValueOfType(Entity::Type type) {
								if (typeToEntity.size() == 0) {
																Entity::initEntityMaps();
								}
								if (typeToEntity.size() == 0) {
																throwEx("typeToEntityMap has not been initialized....");
								}
								auto typePtr = typeToEntity.find(type);
								if (typePtr != typeToEntity.end()) {
																return typePtr->second;
								}else {
																throwEx("the type was not found in typeToEntityMap ");
								}

}
