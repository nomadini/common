#ifndef ServiceFactory_H
#define ServiceFactory_H


#include <memory>
#include <string>
#include <vector>
#include <set>
#include <tbb/concurrent_hash_map.h>
#include <unordered_set>
#include <unordered_map>


namespace gicapods {
class ConfigService;
class BootableConfigService;
};
class EntityToModuleStateStats;
class DataReloadService;
class CommonRequestHandlerFactory;
class EntityProviderServiceInterface;
class CacheServiceInterface;
class BeanFactory;
class DataMasterRequestHandlerFactory;
/*
   holds the beans and services that are depndent on beanFactory beans'
   BeanFactory is holder of more basic beans
 */
class ServiceFactory {

public:

std::vector<CacheServiceInterface*> allCacheServices;
std::vector<EntityProviderServiceInterface*> allEntityProviderService;
EntityToModuleStateStats* entityToModuleStateStats;
gicapods::BootableConfigService* bootableConfigService;
gicapods::ConfigService* configService;
std::shared_ptr<CommonRequestHandlerFactory> commonRequestHandlerFactory;
std::shared_ptr<DataReloadService> dataReloadService;
std::unique_ptr<DataMasterRequestHandlerFactory> dataMasterRequestHandlerFactory;
ServiceFactory(EntityToModuleStateStats* entityToModuleStateStats,
               gicapods::ConfigService* configService,
               gicapods::BootableConfigService* bootableConfigService,
               std::vector<CacheServiceInterface*> allCacheServices,
               std::vector<EntityProviderServiceInterface*> allEntityProviderService
               );
void initializeModules();
virtual ~ServiceFactory();

};

#endif
