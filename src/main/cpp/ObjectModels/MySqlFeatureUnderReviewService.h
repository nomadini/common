//
// Created by Mahmoud Taabodi on 2/15/16.
//

#ifndef MySqlFeatureUnderReviewService_h
#define MySqlFeatureUnderReviewService_h

#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "MySqlService.h"
class ResultSetHolder;
class Feature;
#include "Object.h"
class MySqlFeatureUnderReviewService : public MySqlService<std::string, Feature>, public Object{

public:

MySqlDriver* driver;

MySqlFeatureUnderReviewService(MySqlDriver* driver,
                               EntityToModuleStateStats* entityToModuleStateStats);

virtual ~MySqlFeatureUnderReviewService();

static std::shared_ptr<Feature> mapResultSetToObjectStatic(std::shared_ptr<ResultSetHolder> res);

std::vector<std::shared_ptr<Feature> > readAllFeaturesUnderReview();

void deleteFromReview(std::vector<std::shared_ptr<Feature> > allFeaturesToWrite);

std::string convertRecordToValueString(std::shared_ptr<Feature> feature);

std::string getReadByIdSqlStatement(std::string id);

std::string getSelectAllQueryStatement();

void activateGeoFeatures();

std::shared_ptr<Feature> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);

std::string getInsertObjectSqlStatement(std::shared_ptr<Feature> object);
};

#endif
