#include "PixelCacheService.h"
#include "CollectionUtil.h"
#include "StringUtil.h"
#include "MySqlOfferService.h"
#include <boost/foreach.hpp>
#include "JsonArrayUtil.h"
#include "Pixel.h"
#include "HttpUtilService.h"
#include "CacheService.h"
#include "DataProvider.h"
#include "MySqlPixelService.h"

PixelCacheService::PixelCacheService(MySqlPixelService* mySqlPixelService,
                                     HttpUtilService* httpUtilService,
                                     std::string dataMasterUrl,
                                     EntityToModuleStateStats* entityToModuleStateStats,
                                     std::string appName) :
        CacheService<Pixel>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName),
        Object(__FILE__) {

        keyToPixelMap = std::make_shared<tbb::concurrent_hash_map<std::string, std::shared_ptr<Pixel> > >();
        idToPixelMap = std::make_shared<tbb::concurrent_hash_map<int, std::shared_ptr<Pixel> > > ();

}

void PixelCacheService::clearOtherCaches() {
        keyToPixelMap->clear();
        idToPixelMap->clear();

}

std::shared_ptr<Pixel> PixelCacheService::getPixelByKey(std::string pixelKey) {
        tbb::concurrent_hash_map<std::string, std::shared_ptr<Pixel> >::accessor accessor;
        if(this->keyToPixelMap->find(accessor, pixelKey)) {
                auto pixel  = accessor->second;
                return pixel;
        }
        throwEx("pixekey doesn't have any pixel : " + pixelKey + ", size of keyToPixelMap : " + _toStr(this->keyToPixelMap->size()));
}

void PixelCacheService::populateOtherMapsAndLists(std::vector<std::shared_ptr<Pixel> > allEntities) {
        for(auto pixel : allEntities) {
                tbb::concurrent_hash_map<std::string, std::shared_ptr<Pixel> >::accessor accessor;

                keyToPixelMap->insert(accessor, pixel->uniqueKey);
                accessor->second = pixel;

                tbb::concurrent_hash_map<int, std::shared_ptr<Pixel> >::accessor accessorForId;

                idToPixelMap->insert(accessorForId, pixel->id);
                accessorForId->second = pixel;

        }
}
