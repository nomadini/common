#ifndef Offer_H
#define Offer_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include <unordered_map>
#include "Poco/LRUCache.h"
#include "JsonTypeDefs.h"

class Offer;
#include "Object.h"

/**
   offer is a collection of pixelIds
 */
class Offer : public Object {

public:

static std::string AUDIENCE_TYPE_ACTION_TAKER;
static std::string AUDIENCE_TYPE_SITE_VISITOR;

int id;
std::string name;
int advertiserId;

std::string version;
std::string description;

std::string dateCreated;
std::string dateModified;

//this can be ActionTaker or SiteVisitor
std::string audienceType;

Offer();

std::string toString();

std::string toJson();
static std::string getEntityName();
virtual ~Offer();
static std::shared_ptr<Offer> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};




#endif
