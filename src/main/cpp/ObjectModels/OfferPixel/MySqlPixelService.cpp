#include "GUtil.h"


#include "Offer.h"
#include "MySqlDriver.h"
#include "ConverterUtil.h"
#include "MySqlPixelService.h"
#include "MySqlDriver.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>
#include "DateTimeUtil.h"
#include "Pixel.h"


MySqlPixelService::MySqlPixelService(MySqlDriver* driver) : Object(__FILE__) {
        this->driver = driver;
}

std::shared_ptr<Pixel> MySqlPixelService::getPixelFromResultSet(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<Pixel> pixel = std::make_shared<Pixel>();
        pixel->id = res->getInt ("id");
        pixel->name = MySqlDriver::getString( res, "name");
        pixel->advertiserId = res->getInt ("advertiser_id");
        pixel->dateCreated = MySqlDriver::getString( res, "created_at");
        pixel->dateModified = MySqlDriver::getString( res, "updated_at");
        pixel->uniqueKey = MySqlDriver::getString( res, "unique_key");

        return pixel;
}

std::vector<std::shared_ptr<Pixel> >  MySqlPixelService::readAllEntities() {
        std::vector<std::shared_ptr<Pixel> > allPixels;
        std::string query = "SELECT id,"
                            " name,"
                            " advertiser_id,"
                            " unique_key, "
                            " version, "
                            " description, "
                            " created_at, "
                            " updated_at"
                            " FROM pixel ";


        auto res = driver->executeQuery (query);

        while (res->next ()) {
                allPixels.push_back(getPixelFromResultSet(res));
        }

        return allPixels;
}

MySqlPixelService::~MySqlPixelService() {

}
