#include "OfferCacheService.h"
#include "CollectionUtil.h"
#include "StringUtil.h"
#include <boost/foreach.hpp>
#include "JsonArrayUtil.h"
#include "Offer.h"
#include "HttpUtilService.h"
#include "CacheService.h"
#include "DataProvider.h"

OfferCacheService::OfferCacheService(HttpUtilService* httpUtilService,
                                     std::string dataMasterUrl,
                                     EntityToModuleStateStats* entityToModuleStateStats,
                                     std::string appName) :
        CacheService<Offer>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName),
        Object(__FILE__) {

        idToOfferMap = std::make_shared<tbb::concurrent_hash_map<int, std::shared_ptr<Offer> > > ();
}

void OfferCacheService::clearOtherCaches() {

        idToOfferMap->clear();

}

void OfferCacheService::populateOtherMapsAndLists(std::vector<std::shared_ptr<Offer> > allEntities) {
        for(auto pixel : allEntities) {

                tbb::concurrent_hash_map<int, std::shared_ptr<Offer> >::accessor accessorForId;

                idToOfferMap->insert(accessorForId, pixel->id);
                accessorForId->second = pixel;

        }
}
