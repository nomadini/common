#include "GUtil.h"


#include "Device.h"
#include "PixelDeviceHistory.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>
#include "DateTimeUtil.h"
#include "CassandraService.h"
#include "JsonMapUtil.h"

PixelDeviceHistory::PixelDeviceHistory() : Object(__FILE__) {
        devicesHittingPixel = std::make_shared<std::unordered_map<std::string, std::shared_ptr<Device> > >();

}

PixelDeviceHistory::~PixelDeviceHistory() {

}


std::string PixelDeviceHistory::toString() {
        return this->toJson();
}

std::string PixelDeviceHistory::toJson() {
        auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString(value);
}

void PixelDeviceHistory::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair(doc, "pixelUniqueKey",  pixelUniqueKey, value);

        JsonMapUtil::addMemberToValue_From_NameMapPair(
                doc,
                "devicesHittingPixel",
                *devicesHittingPixel,
                value);
}
std::shared_ptr<PixelDeviceHistory> PixelDeviceHistory::fromJsonValue(RapidJsonValueType value) {
        auto pixelDeviceHistory = std::make_shared<PixelDeviceHistory>();
        pixelDeviceHistory->pixelUniqueKey =
                JsonUtil::GetStringSafely (value, "pixelUniqueKey");

        std::unordered_map<std::string, Device> simpleMap;
        JsonMapUtil::read_Map_From_Value_Member(
                value,
                "devicesHittingPixel",
                simpleMap);

        for(auto& pair : simpleMap) {
                pixelDeviceHistory->devicesHittingPixel->insert(std::make_pair(pair.first, std::make_shared<Device>(pair.second)));
        }

        return pixelDeviceHistory;
}
std::shared_ptr<PixelDeviceHistory> PixelDeviceHistory::fromJson(std::string json) {
        auto document = parseJsonSafely(json);
        return PixelDeviceHistory::fromJsonValue(*document);
}
