#include "GUtil.h"
#include "Offer.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>

std::string Offer::AUDIENCE_TYPE_ACTION_TAKER = "Action Taker";
std::string Offer::AUDIENCE_TYPE_SITE_VISITOR = "Site Visitor";

Offer::Offer() : Object(__FILE__) {
        id = 0;
}

std::string Offer::toString() {
        return toJson ();

}
std::string Offer::getEntityName() {
        return "Offer";
}
std::shared_ptr<Offer> Offer::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<Offer> offer = std::make_shared<Offer>();

        offer->id = JsonUtil::GetIntSafely(value, "id");
        offer->name =JsonUtil::GetStringSafely(value, "name");
        offer->description =JsonUtil::GetStringSafely(value, "description");
        offer->audienceType = JsonUtil::GetStringSafely(value,"audienceType");
        offer->advertiserId = JsonUtil::GetIntSafely(value,"advertiserId");
        offer->dateCreated  = JsonUtil::GetStringSafely(value,"dateCreated");
        offer->dateModified = JsonUtil::GetStringSafely(value,"dateModified");

        return offer;
}


void Offer::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "name", name, value);
        JsonUtil::addMemberToValue_FromPair (doc, "audienceType", audienceType, value);
        JsonUtil::addMemberToValue_FromPair (doc, "description", description, value);
        JsonUtil::addMemberToValue_FromPair (doc, "advertiserId", advertiserId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateCreated", dateCreated, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateModified", dateModified, value);
}

std::string Offer::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}

Offer::~Offer() {

}
