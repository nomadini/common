#include "OfferPixelMapCacheService.h"
#include "CollectionUtil.h"
#include "StringUtil.h"
#include "MySqlOfferService.h"
#include <boost/foreach.hpp>
#include "JsonArrayUtil.h"
#include "OfferPixelMap.h"
#include "HttpUtilService.h"
#include "CacheService.h"
#include "Offer.h"
#include "DataProvider.h"
#include "MySqlOfferPixelMapService.h"

OfferPixelMapCacheService::OfferPixelMapCacheService(MySqlOfferPixelMapService* mySqlOfferPixelMapService,
                                                     HttpUtilService* httpUtilService,
                                                     std::string dataMasterUrl,
                                                     EntityToModuleStateStats* entityToModuleStateStats,
                                                     std::string appName) :
        CacheService<OfferPixelMap>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName),
        Object(__FILE__) {
        pixelIdToOfferIds = std::make_shared<std::unordered_map<int, std::shared_ptr<std::vector<int> > > >();
        offerIdToPixelIds = std::make_shared<std::unordered_map<int, std::shared_ptr<std::vector<int> > > >();
}

void OfferPixelMapCacheService::clearOtherCaches() {
        pixelIdToOfferIds->clear();
        offerIdToPixelIds->clear();
}

std::vector<int> OfferPixelMapCacheService::findPixelIdsMappedToOfferId(int offerId) {
        auto pair = offerIdToPixelIds->find(offerId);
        if (pair == offerIdToPixelIds->end()) {
                std::vector<int>  empty;
                return empty;
        } else {
                return *pair->second;
        }
}
std::vector<int> OfferPixelMapCacheService::findOfferIdsMappedToPixelId(int pixelId) {
        auto pair = pixelIdToOfferIds->find(pixelId);
        if (pair == pixelIdToOfferIds->end()) {
                std::vector<int>  empty;
                return empty;
        } else {
                return *pair->second;
        }
}

void OfferPixelMapCacheService::populateOtherMapsAndLists(std::vector<std::shared_ptr<OfferPixelMap> > allEntities) {
        for (auto entity : allEntities) {
                auto pair = pixelIdToOfferIds->find(entity->pixelId);
                if (pair == pixelIdToOfferIds->end()) {
                        auto offerIds = std::make_shared<std::vector<int> >();
                        pixelIdToOfferIds->insert(std::make_pair(entity->pixelId, offerIds));
                        auto pair = pixelIdToOfferIds->find(entity->pixelId);
                        pair->second->push_back(entity->offerId);
                } else {
                        pair->second->push_back(entity->offerId);
                }
        }

        for (auto entity : allEntities) {
                auto pair = offerIdToPixelIds->find(entity->offerId);
                if (pair == offerIdToPixelIds->end()) {
                        auto pixelIds = std::make_shared<std::vector<int> >();
                        offerIdToPixelIds->insert(std::make_pair(entity->offerId, pixelIds));
                        auto pair = offerIdToPixelIds->find(entity->offerId);
                        pair->second->push_back(entity->pixelId);
                } else {
                        pair->second->push_back(entity->pixelId);
                }
        }
}
