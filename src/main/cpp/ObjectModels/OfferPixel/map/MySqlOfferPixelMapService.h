#ifndef MySqlOfferPixelMapService_h
#define MySqlOfferPixelMapService_h



#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "DataProvider.h"
#include "OfferPixelMap.h"

#include <cppconn/resultset.h>
class MySqlOfferPixelMapService;
#include "Object.h"


class MySqlOfferPixelMapService : public DataProvider<OfferPixelMap>,Object {

public:

MySqlDriver* driver;

MySqlOfferPixelMapService(MySqlDriver* driver);
std::vector<std::shared_ptr<OfferPixelMap> > readAllEntities();
std::shared_ptr<OfferPixelMap> getOfferPixelMapFromResultSet(std::shared_ptr<ResultSetHolder> res);
std::set<int> readPixelIdsForOffer(int id);

void persistOfferPixelMappings(int offerId, int pixelId);

virtual ~MySqlOfferPixelMapService();

};

#endif
