#include "GUtil.h"
#include "OfferPixelMap.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>

OfferPixelMap::OfferPixelMap()  : Object(__FILE__) {
        id = 0;
}

std::string OfferPixelMap::toString() {
        return toJson ();

}
std::string OfferPixelMap::getEntityName() {
        return "OfferPixelMap";
}
std::shared_ptr<OfferPixelMap> OfferPixelMap::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<OfferPixelMap> offer = std::make_shared<OfferPixelMap>();

        offer->id = JsonUtil::GetIntSafely(value, "id");
        offer->offerId =JsonUtil::GetIntSafely(value, "offerId");
        offer->pixelId =JsonUtil::GetIntSafely(value, "pixelId");

        offer->dateCreated  = JsonUtil::GetStringSafely(value,"dateCreated");
        offer->dateModified = JsonUtil::GetStringSafely(value,"dateModified");

        return offer;
}


void OfferPixelMap::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "offerId", offerId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "pixelId", pixelId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateCreated", dateCreated, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateModified", dateModified, value);
}

std::string OfferPixelMap::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}

OfferPixelMap::~OfferPixelMap() {

}
