#include "GUtil.h"
#include "AdInteractionRecordingModule.h"
#include "StringUtil.h"
#include "DateTimeUtil.h"
#include "CassandraServiceInterface.h"
#include "Device.h"
#include "EventLog.h"
#include "EntityToModuleStateStats.h"
#include "CassandraManagedType.h"
#include "InfluxDbClient.h"

AdInteractionRecordingModule::AdInteractionRecordingModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        CassandraServiceInterface* eventLogCassandraService,
        std::string adserverHostname,
        std::string adserverVersion) : Object(__FILE__){
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->eventLogCassandraService = eventLogCassandraService;
        queueOfEventLogs = std::make_shared<tbb::concurrent_queue<std::shared_ptr<EventLog> > > ();
        this->adserverHostname = adserverHostname;
        this->adserverVersion = adserverVersion;
}

std::string AdInteractionRecordingModule::getName() {
        return "AdInteractionRecordingModule";
}

void AdInteractionRecordingModule::enqueueEventForPersistence(std::shared_ptr<EventLog> eventLog) {
        assertAndThrow(!eventLog->eventType.empty());
        queueOfEventLogs->push(eventLog);
}

//this will be run every 10 seconds from AdServerAsyncJobService
void AdInteractionRecordingModule::recordEvents() {
        std::vector<std::shared_ptr<EventLog> > events;
        while(!queueOfEventLogs->empty()) {
                std::shared_ptr<EventLog> event;
                if (queueOfEventLogs->try_pop(event)) {
                        events.push_back(event);
                }

        }
        try {
                for(auto event : events) {
                        eventLogCassandraService->pushToWriteBatchQueue(std::static_pointer_cast<CassandraManagedType>(event));
                }
                writeEventsToInfluxDb(events);
                entityToModuleStateStats->addStateModuleForEntityWithValue ("EventsSavedInDb",
                                                                            events.size(),
                                                                            getName(),
                                                                            "ALL");
        } catch(...) {
                entityToModuleStateStats->
                addStateModuleForEntity("FAILED_TO_WRITE_EVENTS",
                                        getName(),
                                        "ALL",
                                        EntityToModuleStateStats::exception);
                gicapods::Util::showStackTrace();
        }
}

void AdInteractionRecordingModule::writeEventsToInfluxDb(std::vector<std::shared_ptr<EventLog> > events) {
        std::string batchInStr;

        for(int i=0; i < events.size(); i++) {
                batchInStr += toInfluxRecord(events.at(i), "impression_events") + "\n";

        }

        influxDbClient->recordMetricsInInfluxDb(batchInStr, "impression");


        //writeAlot of events for testing the load on reporting UI
        // for (int r = 0; r < 10; r++) {
        //         for(int i=0; i < events.size(); i++) {
        //                 batchInStr += toInfluxRecord(events.at(i), "impression_events") + "\n";
        //
        //         }
        //
        //         //TODO remove this later
        //         for(int test_z = 0; test_z < 1000; test_z++) {
        //                 influxDbClient->recordMetricsInInfluxDb(batchInStr, "impression");
        //                 gicapods::Util::sleepMillis(1);
        //         }
        // }

}


std::string AdInteractionRecordingModule::toInfluxRecord(
        std::shared_ptr<EventLog> record,
        std::string measurementName) {


        std::string recordStr =
                measurementName +
                ","+"event_type="+record->eventType
                +","+"targetgroup_id="+ _toStr(record->targetGroupId)
                +","+"creative_id="+ _toStr(record->creativeId)
                +","+"campaign_id="+ _toStr(record->campaignId)
                +","+"advertiser_id="+ _toStr(record->advertiserId)
                +","+"client_id="+ _toStr(record->clientId)
                +","+"geosegmentlist_id="+ _toStr(record->geoSegmentListId)
                +","+"deviceCountry="+record->deviceCountry
                +","+"deviceState="+InfluxDbClient::replaceIllegalCharacters(record->deviceState)
                +","+"deviceZipcode="+record->deviceZipcode
                +","+"adSize="+InfluxDbClient::replaceIllegalCharacters(record->adSize)
                +","+"siteDomain="+InfluxDbClient::replaceIllegalCharacters(record->siteDomain)
                +","+"siteCategory="+InfluxDbClient::replaceIllegalCharacters(record->siteCategory)
                +","+"impressionType="+record->impressionType
                +","+"deviceType="+record->device->getDeviceType()
                +","+"appHostname="+"emptyForNow"
                +","+"appVersion="+"SNAPSHOT"
                +" "//this space separates the tags from fields
                + "exchangeCostInCpm="+_toStr(record->winBidPrice)
                +",platformCostInCpm="+_toStr(record->platformCost)
                +",advertiserCostInCpm="+_toStr(record->advertiserCost)
                +",bidPriceInCpm="+_toStr(record->bidPrice)
                +","+"count="+_toStr(1);

        return recordStr;
}

AdInteractionRecordingModule::~AdInteractionRecordingModule() {

}
