#ifndef BooleanObject_h
#define BooleanObject_h


#include <atomic>
#include <memory>
#include <string>
#include "Object.h"
class BooleanObject : public Object {

public:

bool value;

BooleanObject ();
bool getValue();
void setValue(bool v);

virtual ~BooleanObject();
};
#endif
