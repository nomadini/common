#include "Advertiser.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "CollectionUtil.h"
#include "TestUtil.h"
#include <boost/foreach.hpp>
#include "DateTimeUtil.h"
Advertiser::Advertiser()   : Object(__FILE__) {
        domainNames = std::make_shared<tbb::concurrent_hash_map<std::string, int> >();

        id = 0;
        clientId = 0;
}


std::string Advertiser::getEntityName() {
        static std::string entityName = "advertiser";
        return entityName;
}

std::string Advertiser::toString() {
        return toJson ();
}

void Advertiser::validate() {
        assertAndThrow(id > 0);
        TestUtil::assertNotEmpty(name);
        TestUtil::assertNotEmpty(status);

        assertAndThrow(!domainNames->empty());

        assertAndThrow(clientId > 0);
}

std::shared_ptr<Advertiser> Advertiser::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<Advertiser> advertiser = std::make_shared<Advertiser>();

        advertiser->setId(JsonUtil::GetIntSafely(value, "id"));
        advertiser->setName(JsonUtil::GetStringSafely(value, "name"));

        advertiser->setDescription(JsonUtil::GetStringSafely(value,"description"));
        advertiser->setStatus(JsonUtil::GetStringSafely(value,"status"));

        advertiser->setUpdatedAt (DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"updatedAt")));
        advertiser->setCreatedAt (DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"createdAt")));
        advertiser->setClientId (JsonUtil::GetIntSafely(value, "clientId"));


        std::vector<std::string> domainNamesArray;
        JsonArrayUtil::getArrayFromValueMemeber(value, "domainNames", domainNamesArray);

        for(std::string domain :  domainNamesArray) {
                tbb::concurrent_hash_map<std::string, int>::accessor a;
                advertiser->domainNames->insert(a, domain);
        }

        return advertiser;
}

void Advertiser::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

        auto domainNamesArray = CollectionUtil::convertKeysOfConcurrentMapToList <std::string, int>(*domainNames);

        JsonUtil::addMemberToValue_FromPair ( doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (  doc, "name", name, value);
        JsonUtil::addMemberToValue_FromPair (  doc, "status", status, value);
        JsonUtil::addMemberToValue_FromPair (  doc, "description", description, value);
        JsonUtil::addMemberToValue_FromPair (  doc, "clientId", clientId, value);

        JsonArrayUtil::addMemberToValue_FromPair(doc, "domainNames", domainNamesArray, value);
        JsonUtil::addMemberToValue_FromPair (  doc, "createdAt", DateTimeUtil::dateTimeToStr(createdAt), value);
        JsonUtil::addMemberToValue_FromPair (  doc, "updatedAt", DateTimeUtil::dateTimeToStr(updatedAt), value);

}

std::string Advertiser::toJson() {
        auto doc = JsonUtil::createDcoumentAsObjectDoc();
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}

Advertiser::~Advertiser() {

}

int Advertiser::getId() {
        return id;
}

std::string Advertiser::getName() {
        return name;
}

std::string Advertiser::getStatus() {
        return status;
}

std::string Advertiser::getDescription() {
        return description;
}

int Advertiser::getClientId() {
        return clientId;
}


Poco::DateTime Advertiser::getUpdatedAt() {
        return updatedAt;
}
Poco::DateTime Advertiser::getCreatedAt() {
        return createdAt;
}
std::shared_ptr<tbb::concurrent_hash_map<std::string, int> > Advertiser::getDomainNames() {
        return domainNames;
}

void Advertiser::setId(int id) {
        this->id = id;
}

void Advertiser::setName(std::string name) {
        this->name = name;
}

void Advertiser::setStatus(std::string status) {
        this->status = status;
}

void Advertiser::setDescription(std::string description) {
        this->description = description;
}

void Advertiser::setClientId(int clientId) {
        this->clientId = clientId;
}

void Advertiser::setUpdatedAt(Poco::DateTime createdAt) {
        this->updatedAt = updatedAt;
}
void Advertiser::setCreatedAt(Poco::DateTime updatedAt) {
        this->createdAt = createdAt;
}

void Advertiser::setDomainNames(std::shared_ptr<tbb::concurrent_hash_map<std::string, int> > domainNames) {
        this->domainNames = domainNames;
}
