#include "MySqlAdvertiserService.h"
#include "MySqlDriver.h"

#include "JsonArrayUtil.h"
#include "CollectionUtil.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
#include <boost/foreach.hpp>
#include "MySqlDriver.h"

MySqlAdvertiserService::MySqlAdvertiserService(MySqlDriver* driver)   : Object(__FILE__) {
        this->driver = driver;
}

std::vector<std::shared_ptr<Advertiser>> MySqlAdvertiserService::readAllEntities() {

        auto all =  readAllAdvertisers();
        MLOG(3)<<"advertisers read from db are : "<< all.size();
        return all;
}

std::shared_ptr<Advertiser> MySqlAdvertiserService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<Advertiser> obj = std::make_shared<Advertiser> ();
        obj->id = res->getInt ("id");
        obj->name = MySqlDriver::getString(res, "name");
        obj->status = MySqlDriver::getString( res, "status");
        obj->description = MySqlDriver::getString( res, "description");
        obj->clientId = res->getInt ("client_id");

        //advertiser domain can be multiple for an advertiser, it's delimited by ; e.g
        //abc.com;bbc.com;cnn.com
        std::vector<std::string> allDomains;
        JsonArrayUtil::getArrayOfObjectsFromJsonString (StringUtil::toLowerCase (res->getString (6)),
                                                        allDomains);
        for(std::string domain :  allDomains) {
                tbb::concurrent_hash_map<std::string, int>::accessor accessor;
                if(!StringUtil::equalsIgnoreCase (domain, "")) {
                        obj->domainNames->insert(accessor, StringUtil::toLowerCase (domain));
                }

        }

        obj->createdAt = MySqlDriver::parseDateTime(res, "created_at");
        obj->updatedAt = MySqlDriver::parseDateTime(res, "updated_at");
        MLOG(3)<<"reading advertiser : " << obj->toJson();
        return obj;
}
std::vector<std::shared_ptr<Advertiser>> MySqlAdvertiserService::readAllAdvertisers() {


        std::vector<std::shared_ptr<Advertiser>> allObjects;
        std::string query  = "SELECT "
                             " `id`, "
                             " `name`,"
                             " `status`,"
                             " `description`,"
                             " `client_id`,"
                             " `domain_name`,"
                             " `created_at`,"
                             " `updated_at` "
                             " FROM `advertiser`";

        auto res = driver->executeQuery (query);

        while (res->next ()) {
                allObjects.push_back (mapResultSetToObject(res));
        }
        return allObjects;
}


void MySqlAdvertiserService::deleteAll() {
        driver->deleteAll("advertiser");
}


void MySqlAdvertiserService::insert(std::shared_ptr<Advertiser> advertiser) {
        std::string queryStr;

        MLOG(3) << "inserting new Advertiser in db : " << advertiser->toJson ();

        queryStr =
                " INSERT INTO `advertiser`"
                " ( "
                " `name`,"
                " `status`,"
                " `description`,"
                " `client_id`,"
                " `domain_name`,"
                " `created_at`,"
                " `updated_at`)"
                " VALUES"
                " ("
                " '__NAME__',"
                " '__STATUS__',"
                " '__DESCR__',"
                " __CLIENT_ID__,"
                " '__DOMAIN_NAME__',"
                " '__created_at__',"
                " '__updated_at__');";


        queryStr = StringUtil::replaceString (queryStr, "__NAME__", advertiser->name);
        queryStr = StringUtil::replaceString (queryStr, "__STATUS__", advertiser->status);
        queryStr = StringUtil::replaceString (queryStr, "__DESCR__", advertiser->description);
        queryStr = StringUtil::replaceString (queryStr, "__CLIENT_ID__", StringUtil::toStr (advertiser->clientId));

        //we save advertiser domain names as an json array e.g ["abc.com", "cnn.com"]
        std::vector<std::string> domains = CollectionUtil::convertKeysOfConcurrentMapToList<std::string, int> (*advertiser->domainNames);
        queryStr = StringUtil::replaceString (queryStr, "__DOMAIN_NAME__", JsonArrayUtil::convertArrayOfStringsToJsonArrayString(domains));

        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();
        MLOG(3) << "date to insert in mysql is : " << date;


        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);


        driver->executedUpdateStatement (queryStr);
        advertiser->id = driver->getLastInsertedId ();

        MLOG(3) << "advertiser id inserted in db is  : " << advertiser->id;

}


std::shared_ptr<Advertiser> MySqlAdvertiserService::readById(int id) {
        std::string query;

        query = "SELECT "
                " id,"
                " name,"
                " status,"
                " description,"
                " domain_name,"
                " client_id "

                " FROM advertiser where id = '__ID__'";
        query = StringUtil::replaceString (query, "__ID__", StringUtil::toStr(id));



        auto res = driver->executeQuery (query);


        while (res->next ()) {
                auto advertiser = mapResultSetToObject(res);
                MLOG(3) << "reading advertiser from db : " << advertiser->toJson ();
                return advertiser;
        }

        throwEx("no advertiser was found for id " + _toStr(id));
}

MySqlAdvertiserService::~MySqlAdvertiserService() {
}
