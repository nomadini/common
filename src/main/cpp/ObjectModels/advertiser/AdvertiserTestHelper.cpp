//
// Created by Mahmoud Taabodi on 4/15/16.
//

#include "AdvertiserTestHelper.h"
#include "Poco/DateTime.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTimeFormat.h"
#include "DateTimeUtil.h"
#include "StringUtil.h"
#include "GUtil.h"
#include "RandomUtil.h"
std::shared_ptr<Advertiser> AdvertiserTestHelper::createSampleAdvertiser() {
        std::shared_ptr<Advertiser> adv = std::make_shared<Advertiser>();

        adv->id = RandomUtil::sudoRandomNumber(1000);
        adv->name = "comcast-advertiser01";
        adv->status = "active";
        adv->description = "I am test advertiser";
        Poco::DateTime now;
        adv->createdAt = now;
        adv->updatedAt = now;
        adv->clientId = RandomUtil::sudoRandomNumber(1000);

        return adv;
}
