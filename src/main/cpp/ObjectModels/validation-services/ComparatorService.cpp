#include "GUtil.h"
#include "ComparatorService.h"
#include "ConfigService.h"
#include "ConverterUtil.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>
#include "EntityToModuleStateStats.h"
#include "DateTimeUtil.h"
#include <cmath>
#include "MySqlTgPerformanceTrackService.h"
#include "NumberUtil.h"
#include "EntityRealTimeDeliveryInfo.h"
#include "MySqlEntityRealTimeDeliveryInfoService.h"
#include "TargetGroupCacheService.h"
#include "TargetGroup.h"

ComparatorService::ComparatorService(
        int minuteAgoStartArg,
        int minuteAgoEndArg)  : Object(__FILE__) {
        errorInWinBidPercentageHighLimit = 30;
        errorInWinBidPercentageLowLimit = 10;
        this->minuteAgoStart = minuteAgoStartArg;
        this->minuteAgoEnd = minuteAgoEndArg;
        period = "[t " + _toStr(this->minuteAgoStart) + " _ t " +
                 _toStr(this->minuteAgoEnd) + "]";
}

ComparatorService::~ComparatorService() {

}

std::string ComparatorService::getName() {
        return "ComparatorService";
}

void ComparatorService::runComparisons() {
        try {
                NULL_CHECK(mySqlTgPerformanceTrackService);
                //so that we don't get affected by the race between the bidder and pacer in saving the latest measures

                MLOG(3) <<"minuteAgoStart : " << minuteAgoStart <<
                        "minuteAgoEnd : "<<minuteAgoEnd;
                Poco::DateTime nowDate = DateTimeUtil::getDateWithMinutePercision(0);
                Poco::DateTime startDate = DateTimeUtil::getDateWithMinutePercision(minuteAgoStart);
                Poco::DateTime endDate = DateTimeUtil::getDateWithMinutePercision(minuteAgoEnd);
                MLOG(3) <<"nowDate : " << DateTimeUtil::dateTimeToStr(nowDate) <<
                        "startDate : "<<DateTimeUtil::dateTimeToStr(startDate) <<
                        "endDate : "<<DateTimeUtil::dateTimeToStr(endDate);

                std::vector<std::shared_ptr<OverallBidWinContainerPerTargetGroup> > all = mySqlTgPerformanceTrackService->
                                                                                          readAllTargetGroupPerformanceTracksBetweenDates(startDate, endDate);
                for (auto overallBidWinContainerPerTargetGroup: all) {
                        MLOG(3) << "run comparison on this performanceTrackDto : " <<
                                overallBidWinContainerPerTargetGroup->toJson();

                        compareBidWinsForTg(overallBidWinContainerPerTargetGroup);
                }
        } catch (std::exception const &e) {
                gicapods::Util::showStackTrace();
                entityToModuleStateStats->addStateModuleForEntity ("EXCEPTION_IN_COMPARATOR_SERVICE",
                                                                   "ComparatorService" + period,
                                                                   EntityToModuleStateStats::all,
                                                                   EntityToModuleStateStats::exception);
        }
}

void ComparatorService::compareBidWinsForTg(std::shared_ptr<OverallBidWinContainerPerTargetGroup> overallBidWinContainerPerTargetGroup) {
        auto bids = overallBidWinContainerPerTargetGroup->numberOfBids->getValue();
        auto wins = overallBidWinContainerPerTargetGroup->numberOfWins->getValue();
        if (bids <=0) {
                return;
        }
        auto winBidCountDiff = wins - bids;
        double winBidPercentage = (winBidCountDiff / bids) * 100;
        winBidPercentage = std::abs(winBidPercentage);
        if (winBidPercentage > errorInWinBidPercentageHighLimit) {
                entityToModuleStateStats->addStateModuleForEntity (
                        "TOO_HIGH_WIN_BID_PERCENTAGE_DIFF : " + _toStr(winBidCountDiff)
                        + "_" +
                        "BID:" + _toStr(bids) + "_"
                        "WIN:" + _toStr(wins)
                        + _toStr("_For_tg") + _toStr(overallBidWinContainerPerTargetGroup->targetGroupId),
                        "ComparatorService" + period,
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception);
        } else if (bids > 100 && winBidPercentage <= errorInWinBidPercentageLowLimit)   {
                entityToModuleStateStats->addStateModuleForEntity (
                        "TOO_LOW_WIN_BID_PERCENTAGE_DIFF : " + _toStr(winBidCountDiff)
                        + "_" +
                        "BID:" + _toStr(bids) + "_"
                        "WIN:" + _toStr(wins)
                        + _toStr("_For_tg") + _toStr(overallBidWinContainerPerTargetGroup->targetGroupId),
                        "ComparatorService" + period,
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception);
        } else {
                entityToModuleStateStats->addStateModuleForEntity (
                        "WIN_BID_PERCENTAGE_DIFF_IS_GOOD_VALUE:" +
                        _toStr("_") + _toStr(winBidPercentage)
                        + "_" +
                        "BID:" + _toStr(bids) + "_"
                        "WIN:" + _toStr(wins)
                        +_toStr("_For_tg") + _toStr(overallBidWinContainerPerTargetGroup->targetGroupId),
                        "ComparatorService" + period,
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::correct);
        }
}

std::string ComparatorService::getSpentAmountListOfTargetGroups(
        std::vector<std::string>& seriousExceptionMetrics) {
        //disable this for now!!!! it doesnt look good in the email
        return "";


        std::vector<std::shared_ptr<EntityRealTimeDeliveryInfo> > allDeliveryInfos =
                mySqlEntityRealTimeDeliveryInfoService->
                getTgRealTimeDeliveryInfos(TargetGroup::getEntityName(),
                targetGroupCacheService->getAllEntityIds());

        std::string message = "";
        message += "# tgId ,  Hour( imp , plt C, adv C) _  #Day  (imp, plt C, adv C)   #Total (imp, plt C, adv C)";
        message += "\n";
        for (auto deliveryInfo : allDeliveryInfos) {
                if (deliveryInfo->numOfImpressionsServedInCurrentHourUpToNow->getValue() > 1000) {
                        std::string exceptionMessage;
                        exceptionMessage = "targetGroupId : " + _toStr(deliveryInfo->entityId) + " hour Imp is too High(above : 1000)";
                        seriousExceptionMetrics.push_back(exceptionMessage);
                }
                if (deliveryInfo->numOfImpressionsServedInCurrentDateUpToNow->getValue() > 10000) {
                        std::string exceptionMessage;
                        exceptionMessage = "targetGroupId : " + _toStr(deliveryInfo->entityId) + " day Imp is too High(above : 10000)";
                        seriousExceptionMetrics.push_back(exceptionMessage);
                }
                if (deliveryInfo->numOfImpressionsServedOverallUpToNow->getValue() > 100000) {
                        std::string exceptionMessage;
                        exceptionMessage = "targetGroupId : " + _toStr(deliveryInfo->entityId) + " total Imp is too High(above : 100000)";
                        seriousExceptionMetrics.push_back(exceptionMessage);
                }
        }

        message += createHtmlTable(allDeliveryInfos);
        return message;
}

std::string ComparatorService::createHtmlTable(std::vector<std::shared_ptr<EntityRealTimeDeliveryInfo> > allDeliveryInfos) {
        std::string tableStructure = "<html> \
    <table width=\"600\" style=\"border:1px solid #333\"> \
  <tr>                                                    \
      <th> tgId </th>                                     \
      <th> Hour Imp #</th>                                \
      <th> Hout platform Cost</th>                        \
      <th> Hout adv Cost</th>                             \
      <th> Day Imp #</th>                                 \
      <th> Day platform Cost</th>                         \
      <th> Day adv Cost</th>                              \
      <th> Total Imp #</th>                               \
      <th> Total platform Cost</th>                       \
      <th> Total adv Cost</th>                            \
  </tr>";

        for (auto deliveryInfo : allDeliveryInfos) {
                tableStructure += "<tr>";
                tableStructure += "<td>" + _toStr(deliveryInfo->entityId) + "</td>";
                tableStructure += "<td>" + _toStr(deliveryInfo->numOfImpressionsServedInCurrentHourUpToNow->getValue()) + "</td>";
                tableStructure += "<td>" + _toStr(deliveryInfo->numOfImpressionsServedInCurrentDateUpToNow->getValue()) + "</td>";
                tableStructure += "<td>" + _toStr(deliveryInfo->numOfImpressionsServedOverallUpToNow->getValue()) + "</td>";
                tableStructure += "<td>" +  _toStr(deliveryInfo->platformCostSpentInCurrentHourUpToNow->getValueRound()) + "</td>";
                tableStructure += "<td>" +  _toStr(deliveryInfo->platformCostSpentInCurrentDateUpToNow->getValueRound()) + "</td>";
                tableStructure += "<td>" +  _toStr(deliveryInfo->platformCostSpentOverallUpToNow->getValueRound()) + "</td>";
                tableStructure += "<td>" +  _toStr(deliveryInfo->advertiserCostSpentInCurrentHourUpToNow->getValueRound()) + "</td>";
                tableStructure += "<td>" + _toStr(deliveryInfo->advertiserCostSpentInCurrentDateUpToNow->getValueRound()) + "</td>";
                tableStructure += "<td>" +  _toStr(deliveryInfo->advertiserCostSpentOverallUpToNow->getValueRound())  + "</td>";
                tableStructure += "</tr>";
        }

        tableStructure += "</table> </html>";
        LOG(ERROR)<<"tableStructure : "<< tableStructure;
        return tableStructure;
}
