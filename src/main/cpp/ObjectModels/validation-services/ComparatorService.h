#ifndef ComparatorService_H_
#define ComparatorService_H_

#include <memory>
#include <string>
#include "Poco/Util/Application.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/ServerApplication.h"
class EntityToModuleStateStats;
class MySqlTgPerformanceTrackService;
class MySqlEntityRealTimeDeliveryInfoService;
class OverallBidWinContainerPerTargetGroup;
class TargetGroupCacheService;
class EntityRealTimeDeliveryInfo;
namespace gicapods { class ConfigService; }
#include "Object.h"
#include <boost/thread.hpp>
#include <thread>
#include "AtomicLong.h"

class ComparatorService;



class ComparatorService : public std::enable_shared_from_this<ComparatorService>, public Object {

public:
int errorInWinBidPercentageHighLimit;
int errorInWinBidPercentageLowLimit;
EntityToModuleStateStats* entityToModuleStateStats;
MySqlTgPerformanceTrackService* mySqlTgPerformanceTrackService;
TargetGroupCacheService* targetGroupCacheService;
MySqlEntityRealTimeDeliveryInfoService* mySqlEntityRealTimeDeliveryInfoService;

int minuteAgoStart;
int minuteAgoEnd;
std::string period;

ComparatorService(
        int minuteAgoStartArg,
        int minuteAgoEndArg);

virtual ~ComparatorService();

void runComparisons();

void compareBidWinsForTg(std::shared_ptr<OverallBidWinContainerPerTargetGroup> overallBidWinContainerPerTargetGroup);

std::string getSpentAmountListOfTargetGroups(std::vector<std::string>& seriousExceptionMetrics);
std::string getName();

void startAsyncThreads();
std::string createHtmlTable(std::vector<std::shared_ptr<EntityRealTimeDeliveryInfo> > allDeliveryInfos);

private:
bool _helpRequested;
};

#endif
