
#include "StringUtil.h"
#include "GUtil.h"
#include "GicapodsIdToExchangeIdsMap.h"
#include <memory>
#include <string>
#include <vector>
#include <set>
#include "JsonMapUtil.h"
#include "DateTimeUtil.h"
#include "cassandra.h"
#include "JsonUtil.h"

GicapodsIdToExchangeIdsMap::GicapodsIdToExchangeIdsMap()  : Object(__FILE__) {

}

GicapodsIdToExchangeIdsMap::~GicapodsIdToExchangeIdsMap() {
}

std::string GicapodsIdToExchangeIdsMap::toString() {
								return this->toJson();

}

std::string GicapodsIdToExchangeIdsMap::toJson() {
								auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString(value);
}

void GicapodsIdToExchangeIdsMap::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
								JsonUtil::addMemberToValue_FromPair(doc, "nomadiniDeviceId", nomadiniDeviceId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "exchangeNameToBatchWrite", exchangeNameToBatchWrite, value);

								JsonMapUtil::addMemberToValue_From_NameMapPair(
																doc,
																"exchangeNameToExchangeId",
																exchangeNameToExchangeId,
																value);

}

std::shared_ptr<GicapodsIdToExchangeIdsMap> GicapodsIdToExchangeIdsMap::fromJsonValue(RapidJsonValueType value) {
								auto gicapodsIdToExchangeIdsMap =
																std::make_shared<GicapodsIdToExchangeIdsMap>();

								gicapodsIdToExchangeIdsMap->nomadiniDeviceId = JsonUtil::GetStringSafely (value, "nomadiniDeviceId");
								gicapodsIdToExchangeIdsMap->exchangeNameToBatchWrite =  JsonUtil::GetStringSafely (value, "exchangeNameToBatchWrite");

								JsonMapUtil::read_Map_From_Value_Member(
																value,
																"exchangeNameToExchangeId",
																gicapodsIdToExchangeIdsMap->exchangeNameToExchangeId);

								return gicapodsIdToExchangeIdsMap;
}

std::shared_ptr<GicapodsIdToExchangeIdsMap> GicapodsIdToExchangeIdsMap::fromJson(std::string json) {
								auto document = parseJsonSafely(json);
								return GicapodsIdToExchangeIdsMap::fromJsonValue(*document);

}
