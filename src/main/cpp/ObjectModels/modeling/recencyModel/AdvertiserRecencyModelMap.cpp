
#include "AdvertiserRecencyModelMap.h"

#include "JsonUtil.h"
#include "DateTimeUtil.h"
AdvertiserRecencyModelMap::AdvertiserRecencyModelMap() : Object(__FILE__) {
        id = 0;
        advertiserId= 0;
        modelId= 0;
}

AdvertiserRecencyModelMap::~AdvertiserRecencyModelMap() {

}

std::string AdvertiserRecencyModelMap::toJson() {
        auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "id", id, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "advertiserId", advertiserId, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "modelId", modelId, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "status", status, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "createdAt", DateTimeUtil::dateTimeToStr(createdAt), value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "updatedAt", DateTimeUtil::dateTimeToStr(updatedAt), value);
        return JsonUtil::valueToString(value);
}
