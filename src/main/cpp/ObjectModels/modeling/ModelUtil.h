#ifndef MODEL_UTILS_H
#define MODEL_UTILS_H




#include "Object.h"
class DeviceFeatureHistory;
#include <memory>
#include <string>
#include <vector>
#include <unordered_map>
#include <set>
#include <shogun/lib/common.h>
#include "RandomUtil.h"
#include "GUtil.h"

#include "FileUtil.h"
#include "DeviceFeatureHistory.h"
#include "NumberUtil.h"
#include "Feature.h"
#include "Device.h"
#include "ModelUtil.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "CollectionUtil.h"
#include "SignalHandler.h"

class ModelUtil : public Object {

public:



static std::unordered_map<std::string, double> getHighestFeatureScoreMap(std::unordered_map<std::string, double> featureScoreMap, int limit, double cutOffScore) {
        std::unordered_map<std::string, double> topFeatureScoreMap;
        for (auto kv : featureScoreMap) {
                if (kv.second >= cutOffScore) {
                        topFeatureScoreMap.insert(
                                std::pair<std::string, double>(kv.first, NumberUtil::roundDouble(kv.second, 2)));
                }
        }
        return topFeatureScoreMap;
}

static std::string convertFeatureIndexMapToJson(
        std::unordered_map<std::string,int> map) {
        rapidjson::StringBuffer mapStringBuffer;
        rapidjson::Writer <rapidjson::StringBuffer> mapWriter (mapStringBuffer);
        mapWriter.StartArray ();

        for (auto &kv : map) {
                rapidjson::StringBuffer pairStringBuffer;
                rapidjson::Writer <rapidjson::StringBuffer> pairWriter (pairStringBuffer);
                pairWriter.StartObject ();
                pairWriter.String ("feature");
                pairWriter.String (StringUtil::toStr(kv.first).c_str ());
                pairWriter.String ("index");
                pairWriter.Uint (kv.second);
                pairWriter.EndObject ();
                std::string pairInJson = pairStringBuffer.GetString ();
                mapWriter.String (pairInJson.c_str ());
        }
        mapWriter.EndArray ();
        std::string mapi_gicapodsnJson = mapStringBuffer.GetString ();
        MLOG(3)<<"created featureIndexMap in json ";
        return mapi_gicapodsnJson;
}

static void printFeatureVector(std::vector <float64_t> featureWeights,
                               std::unordered_map<int, std::string> indexFeatureMap) {
        int i = 0;
        for (auto &kv : featureWeights) {
                auto ptr = indexFeatureMap.find (i);
                i++;
                MLOG(3)<<"feature  "<<ptr->second<<" : weight  "<<kv;
        }
}

static std::unordered_map<int, std::string> getIndexFeatureMap(
        std::unordered_map<std::string, int> &featureIndexMap) {
        std::unordered_map<int, std::string> indexFeatureMap;

        for (std::unordered_map<std::string, int>::iterator it =
                     featureIndexMap.begin (); it != featureIndexMap.end (); ++it) {
                std::string feature = it->first;
                int index = it->second;
                indexFeatureMap.insert (std::pair<int, std::string> (index, feature));
        }

        return indexFeatureMap;
}

static int getHighetIndexInFeatureUniqueMap(
        std::unordered_map<std::string, int> &featureIndexMap) {
        int maxIndex = 1;
        for (std::unordered_map<std::string, int>::iterator it =
                     featureIndexMap.begin (); it != featureIndexMap.end (); ++it) {
                std::string feature = it->first;
                int index = it->second;
                if (index > maxIndex) {
                        maxIndex = index;
                }
        }

        return maxIndex;
}

static void dumpAllRecordsToModelInputFile(
        std::vector <std::string> allRecords, std::string inputFileName) {
        /*
         * putting all the records , action takers and non action takers
         * in the input file , to create the model from
         */
        std::vector <std::string> allRecords2 = CollectionUtil::shuffleTheList<
                std::string> (allRecords);
        while (!allRecords2.empty ()) {
                FileUtil::appendALineToFile (inputFileName, allRecords2.back ());
                allRecords2.pop_back ();
        }
}

/**
 * assigns new indexes for new features in featureIndexMap
 */
static void updateFeatureIndexMapWithNewFeatures(
        std::set <std::string> allFeatureSet,
        std::unordered_map<std::string, int> &featureIndexMap) {
        if (allFeatureSet.empty ()) {
                return;
        }
        int sizeBefore = featureIndexMap.size ();
        for (std::set<std::string>::iterator it = allFeatureSet.begin ();
             it != allFeatureSet.end (); ++it) {
                std::string feature = *it;
                int index = getIndexFromFeatureIndexMap (featureIndexMap, feature);
                if (index == -1) {
                        index = featureIndexMap.size () + 1;
                }
                featureIndexMap.insert (std::pair<std::string, int> (feature, index));

        }
        MLOG(3)<<"added  "<<featureIndexMap.size () - sizeBefore<<" features to  featureIndexMap ";
}

/**
 * returns the index of a feature in the featureIndexMap
 * returns -1 if the index is not found
 */
static int getIndexFromFeatureIndexMap(
        std::unordered_map<std::string, int> &map, std::string feature) {
        std::unordered_map<std::string, int>::iterator it;

        auto feaureIndexPair = map.find (feature);
        if (feaureIndexPair != map.end ()) {
                MLOG(2)<<"Found " << feaureIndexPair->first <<" "<<
                        StringUtil::toStr(feaureIndexPair->second);
                MLOG(2)<<"index of " << feature << " in featureIndexMap is "
                       <<StringUtil::toStr(feaureIndexPair->second);
                return feaureIndexPair->second;

        } else {
                MLOG(2)<<"feature not found  in feaureIndexMap" << feaureIndexPair->first;
                return -1;
        }
}

/**
 * builds feature score map from a list of weights
 */
static std::unordered_map<std::string, double> buildFeatureScoreMap(
        std::unordered_map<int, std::string> indexFeatureMap,
        std::vector <float64_t> featureWeights) {

        std::unordered_map<std::string, double> featureScoreMap;

        if (indexFeatureMap.size () != featureWeights.size ()) {
                throwEx ("featureWeights and indexFeatureMap should have equal size");
        }

        for (int index = 0; index < featureWeights.size (); index++) {
                float64_t weight = featureWeights.at (index);
                double score = weight;

                auto featurePair = indexFeatureMap.find (index);
                if (featurePair == indexFeatureMap.end ()) {
                        LOG(INFO)<<"index :  "<<index<<" was not found in indexFeatureMap. ";
                        continue;
                }
                std::string feature = featurePair->second;
                MLOG(3)<<"feature :  "<<feature<<", weight :  "<<weight<<", score :  "<<score;
                featureScoreMap.insert (
                        std::pair<std::string, double> (feature, score));
        }

        MLOG(3)<<"size of featureScoreMap :  "<<featureScoreMap.size ();
        return featureScoreMap;
}

static std::string getRandomRecord() {
        std::string record;

        if (RandomUtil::sudoRandomNumber (1) % 9 == 0) {
                record.append ("+1 ");
        } else {
                record.append ("-1 ");
        }
        int num = 1;
        do {
                num = RandomUtil::sudoRandomNumber (2);
                for (int i = 0; i < 99; i++) {
                        int feature;
                        do {
                                feature = RandomUtil::sudoRandomNumber (2);
                        } while (feature <= 0);
                        record.append (StringUtil::toStr(feature));
                        record.append (":");
                        record.append ("1 ");
                }

        } while (num <= 0);

        MLOG(3)<<"record :  "<<record;
        return record;

}

/**
 * returns features set that are in a list of device histories
 */
template<typename T>
static std::set<std::string> getFeatureSetFromDeviceHistories(
        std::shared_ptr<T> deviceFeatureHistory,
        std::set<std::string> featureTypesRequested) {

        std::set <std::string> featureSet;
        if (deviceFeatureHistory->getFeatures()->empty()) {
                LOG(WARNING) << "no feature history for this device to extract : " << deviceFeatureHistory->device->getDeviceId();
                return featureSet;
        }
        int numberOfDevicesWithFeatures = 0;
        int numberOfDevicesWithMatchingTypeFeatures = 0;
        for (auto feature : *deviceFeatureHistory->getFeatures()) {
                numberOfDevicesWithFeatures++;
                if (CollectionUtil::valueExistInSet(featureTypesRequested,
                                                    feature->feature->getType())) {
                        numberOfDevicesWithMatchingTypeFeatures++;
                        featureSet.insert (feature->feature->getName());
                } else {
                        MLOG(2)<< "not containing features wanted : "
                               <<" featureTypesRequested : " << JsonArrayUtil::convertListToJson(featureTypesRequested)
                               <<", feature type : "<<feature->feature->getType();

                }
        }

        if(featureSet.empty()) {
                LOG(WARNING) << "emptyFeature set for device : "
                             << deviceFeatureHistory->device->getDeviceId()<<
                        ", numberOfDevicesWithFeatures : "<< numberOfDevicesWithFeatures<<
                        ", numberOfDevicesWithMatchingTypeFeatures : "<< numberOfDevicesWithMatchingTypeFeatures;
        } else {
                MLOG(2)<< "extracted "<< featureSet.size()<< " features from device"
                       << deviceFeatureHistory->device->getDeviceId()<<
                        ", numberOfDevicesWithFeatures : "<< numberOfDevicesWithFeatures<<
                        ", numberOfDevicesWithMatchingTypeFeatures : "<< numberOfDevicesWithMatchingTypeFeatures;
        }

        return featureSet;
}

template<typename T>
static std::vector<int> getAllFeatureIndexesInDeviceFeatureHistory(
        std::shared_ptr<T> deviceFeatureHistory,
        std::unordered_map<std::string, int> &featureIndexMap) {
        std::vector<int> allIndexes;
        for (auto feature : *deviceFeatureHistory->getFeatures()) {
                auto featIndexPtr = featureIndexMap.find (feature->feature->getName());
                if (featIndexPtr != featureIndexMap.end()) {
                        allIndexes.push_back (featIndexPtr->second);
                }
        }
        return allIndexes;
}

template<typename T>
static std::string vectorToJson(
        shogun::SGVector<T> vector) {
        std::vector<T> vectorStd;
        for (int i = 0; i< vector.size(); i++) {
                T var = vector.get_element(i);
                vectorStd.push_back(var);
        }
        return JsonArrayUtil::convertListToJson(vectorStd);
}

};

#endif
