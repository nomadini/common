#ifndef AdvertiserModelMapping_H
#define AdvertiserModelMapping_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "Poco/DateTime.h"
#include "Object.h"
class DateTimeUtil;
/*
   we don't use advertiser model mapping now, because
   every model has a advertiserId, we might later, use
   this mapping, if we need ot come up with a different
   set of models that can be assigned to multiple advertisers
 */
class AdvertiserModelMapping;

class AdvertiserModelMapping : public Object {

public:

int id;
int advertiserId;
int modelId;
std::string status;
Poco::DateTime createdAt;
Poco::DateTime updatedAt;
AdvertiserModelMapping();
virtual ~AdvertiserModelMapping();
std::string toJson();
};

#endif
