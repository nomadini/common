#ifndef ModelCacheService_h
#define ModelCacheService_h


#include "ModelRequest.h"
#include <string>
#include <memory>
#include <unordered_map>

#include "MySqlModelService.h"
#include "HttpUtilService.h"
#include "EntityProviderService.h"
#include "ConcurrentHashMap.h"
#include "CacheService.h"
class EntityToModuleStateStats;
#include "Object.h"
class ModelCacheService;



class ModelCacheService : public CacheService<ModelRequest>, public Object {

public:
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ModelRequest > >  uniqueKeyToModelMap;
ModelCacheService(MySqlModelService* mySqlModelService,
                  HttpUtilService* httpUtilService,
                  std::string dataMasterUrl,
                  EntityToModuleStateStats* entityToModuleStateStats,std::string appName);

virtual ~ModelCacheService();


void clearOtherCaches();
void populateOtherMapsAndLists(std::vector<std::shared_ptr<ModelRequest> > allEntities);

};


#endif
