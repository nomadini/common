#include "GUtil.h"
#include "MySqlDriver.h"
#include "CollectionUtil.h"
#include "MySqlAdvertiserModelMappingService.h"
#include "MySqlDriver.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"

MySqlAdvertiserModelMappingService::MySqlAdvertiserModelMappingService(MySqlDriver* driver) : Object(__FILE__) {
        this->driver = driver;
}

MySqlAdvertiserModelMappingService::~MySqlAdvertiserModelMappingService() {

}

std::vector<std::shared_ptr<AdvertiserModelMapping>> MySqlAdvertiserModelMappingService::readAll() {

}

std::vector<std::shared_ptr<AdvertiserModelMapping>> MySqlAdvertiserModelMappingService::readAllActiveOnes() {
        std::vector<std::shared_ptr<AdvertiserModelMapping>> allModels;
        std::string query = "SELECT `ID`," //1
                            " `advertiser_id`," //2
                            " `status`," //3
                            " `model_id`," //4
                            " `created_at`," //5
                            " `updated_at`" //6
                            " FROM `advertiser_model_map` where status = 'active'";


        auto res = driver->executeQuery (query);
        while (res->next ()) {
                std::shared_ptr<AdvertiserModelMapping> advertiserModelMapping (new AdvertiserModelMapping ());
                advertiserModelMapping->id = res->getInt ("id");
                advertiserModelMapping->advertiserId = res->getInt ("advertiser_id");

                advertiserModelMapping->status = MySqlDriver::getString( res, "status");
                if (advertiserModelMapping->status.compare ("active") != 0 &&
                    advertiserModelMapping->status.compare ("inactive") != 0) {
                        throwEx("status is not correct " + advertiserModelMapping->status);
                }
                advertiserModelMapping->modelId = res->getInt ("model_id");
                advertiserModelMapping->createdAt = MySqlDriver::parseDateTime(res, "created_at");
                advertiserModelMapping->updatedAt = MySqlDriver::parseDateTime(res, "updated_at");
                allModels.push_back (advertiserModelMapping);
        }


        return allModels;
}

std::shared_ptr<AdvertiserModelMapping> MySqlAdvertiserModelMappingService::read(int id) {
        throwEx("error in mysql");
}

void MySqlAdvertiserModelMappingService::update(std::shared_ptr<AdvertiserModelMapping> obj) {

}

void MySqlAdvertiserModelMappingService::insert(std::shared_ptr<AdvertiserModelMapping> advMap) {

        MLOG(3) << "inserting new AdvertiserModelMapping in db : " << advMap->toJson ();
        std::string queryStr =
                "INSERT INTO advertiser_model_map"
                " ("
                " advertiser_id,"
                " model_id,"
                " status,"
                " created_at,"
                " updated_at)  "
                " VALUES"
                " ("
                " '__advertiser_id__',"
                " '__model_id__',"
                " '__status__',"
                " '__created_at__',"
                " '__updated_at__'"
                " );";

        Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "__model_id__", StringUtil::toStr (advMap->modelId));
        queryStr = StringUtil::replaceString (queryStr, "__advertiser_id__",
                                              StringUtil::toStr (advMap->advertiserId));
        queryStr = StringUtil::replaceString (queryStr, "__status__", advMap->status);

        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);

        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);

        //MLOG(3) << "queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);
        advMap->id = driver->getLastInsertedId ();

}

void MySqlAdvertiserModelMappingService::deleteAll() {
        driver->deleteAll("advertiser_model_map");
}
