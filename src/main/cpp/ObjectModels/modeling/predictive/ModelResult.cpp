#include "ModelResult.h"
#include "JsonUtil.h"
#include <string>
#include <memory>
#include "GUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "ModelUtil.h"
#include "CollectionUtil.h"
#include "DateTimeUtil.h"
#include "Feature.h"
#include <boost/foreach.hpp>
void ModelResult::validate() {

}

ModelResult::ModelResult() : Object(__FILE__) {
        id = -1;
        modelRequestId = -1;
        createdAt = DateTimeUtil::parseDateTime("2017-01-01 00:00:00");
        updatedAt = DateTimeUtil::parseDateTime("2017-01-01 00:00:00");

        numberOfNegativeDevicesUsed = 0;
        numberOfPositiveDevicesUsed = 0;
}

std::string ModelResult::toshortString() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "id", id, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "modelRequestId", modelRequestId, value);

        return JsonUtil::valueToString (value);
}

std::string ModelResult::toString() {
        return toJson ();
}

std::string ModelResult::toJson() {
        validate ();
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}

std::shared_ptr<ModelResult> ModelResult::fromJson(std::string json) {
        MLOG (3) << "json " << json;
        auto document = parseJsonSafely (json);
        return fromJsonValue(*document);
}


std::string ModelResult::getEntityName() {
        return "ModelResult";
}

std::string ModelResult::convertFeatureScoreMapToJsonDocument(std::shared_ptr<ModelResult> model, std::string nameOfMap) {
        auto doc = std::make_shared<DocumentType>();
        doc->SetObject ();
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

        if (nameOfMap.compare ("featureScoreMap") == 0) {

                JsonMapUtil::addMapAsMemberToValue<std::string, double> (doc.get(), model->featureScoreMap, value);
        } else if (nameOfMap.compare ("topFeatureScoreMap") == 0) {
                JsonMapUtil::addMapAsMemberToValue<std::string, double> (doc.get(), model->topFeatureScoreMap, value);

        } else {
                //name of map passed to function is not correct!
                LOG (WARNING) << "nameOfMap passed is : " << nameOfMap;
                assertAndThrow (false);
        }

        return JsonUtil::valueToString (value);
}

double ModelResult::getScoreOfTopFeatures(std::string feature) {
        auto pairPtr = topFeatureScoreMap.find (feature);
        if (pairPtr == topFeatureScoreMap.end ()) {
                return 0;
        } else {
                return pairPtr->second;
        }

}

ModelResult::~ModelResult() {

}

int ModelResult::getId() {
        return id;
}


std::shared_ptr<ModelResult> ModelResult::fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<ModelResult> model = std::make_shared<ModelResult>();

        //common fieds
        model->createdAt = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"createdAt"));
        model->updatedAt = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"updatedAt"));

        model->id = JsonUtil::GetIntSafely (value, "id");
        model->modelRequestId = JsonUtil::GetIntSafely (value, "modelRequestId");
        model->numberOfNegativeDevicesUsed = JsonUtil::GetIntSafely (value, "numberOfNegativeDevicesUsed");
        model->numberOfPositiveDevicesUsed = JsonUtil::GetIntSafely (value, "numberOfPositiveDevicesUsed");
        model->processResult = JsonUtil::GetStringSafely (value, "processResult");
        JsonArrayUtil::getArrayFromValueMemeber(value, "redFlags", model->redFlags);

        std::vector<std::string> negativeFeaturesUsed;
        JsonArrayUtil::getArrayFromValueMemeber(value, "negativeFeaturesUsed", negativeFeaturesUsed);

        model->negativeFeaturesUsed = CollectionUtil::convertListToSet<std::string>(negativeFeaturesUsed);

        std::vector<std::string> positiveFeaturesUsed;

        JsonArrayUtil::getArrayFromValueMemeber(
                value,
                "positiveFeaturesUsed",
                positiveFeaturesUsed);

        model->positiveFeaturesUsed = CollectionUtil::convertListToSet<std::string>(positiveFeaturesUsed);

        JsonMapUtil::read_Map_From_Value_Member<std::string, int>(value,
                                                                  "featureToAvgNumberOfHistoryUsed",
                                                                  model->featureToAvgNumberOfHistoryUsed);

        JsonMapUtil::read_Map_From_Value_Member<std::string, double> (value, "featureScoreMap", model->featureScoreMap);
        JsonMapUtil::read_Map_From_Value_Member<std::string, double> (value, "topFeatureScoreMap",model->topFeatureScoreMap);

        model->validate ();

        return model;
}

void ModelResult::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        /** adding all the common fields to json **/
        JsonUtil::addMemberToValue_FromPair (doc, "createdAt", DateTimeUtil::dateTimeToStr(createdAt), value);
        JsonUtil::addMemberToValue_FromPair (doc, "updatedAt", DateTimeUtil::dateTimeToStr(updatedAt), value);

        JsonUtil::addMemberToValue_FromPair(doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair(doc, "modelRequestId", modelRequestId, value);
        JsonUtil::addMemberToValue_FromPair(doc, "processResult", processResult, value);
        JsonArrayUtil::addMemberToValue_FromPair(doc,"negativeFeaturesUsed",
                                                 CollectionUtil::convertSetToList<std::string>(negativeFeaturesUsed),
                                                 value);

        JsonArrayUtil::addMemberToValue_FromPair(doc,"redFlags",
                                                 redFlags,
                                                 value);

        JsonArrayUtil::addMemberToValue_FromPair(doc,"positiveFeaturesUsed",
                                                 CollectionUtil::convertSetToList<std::string>(positiveFeaturesUsed),
                                                 value);

        JsonMapUtil::addMemberToValue_From_NameMapPair<std::string, int>(doc,
                                                                         "featureToAvgNumberOfHistoryUsed",
                                                                         featureToAvgNumberOfHistoryUsed,
                                                                         value);


        JsonUtil::addMemberToValue_FromPair(doc, "numberOfNegativeDevicesUsed",
                                            numberOfNegativeDevicesUsed, value);

        JsonUtil::addMemberToValue_FromPair(doc, "numberOfPositiveDevicesUsed",
                                            numberOfPositiveDevicesUsed, value);



        JsonMapUtil::addMemberToValue_From_NameMapPair<std::string, double> (doc,
                                                                             "featureScoreMap",
                                                                             featureScoreMap, value);
        JsonMapUtil::addMemberToValue_From_NameMapPair<std::string, double> (doc,
                                                                             "topFeatureScoreMap",
                                                                             topFeatureScoreMap, value);

}
