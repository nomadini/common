#include "ModelRequest.h"
#include "StringUtil.h"
#include "MySqlDriver.h"
#include <boost/foreach.hpp>
#include "ModelCacheService.h"
#include "MySqlModelService.h"
#include "JsonArrayUtil.h"

ModelCacheService::ModelCacheService(
        MySqlModelService* mySqlModelServiceArg,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,std::string appName) :
        CacheService<ModelRequest>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName),
        Object(__FILE__) {
        uniqueKeyToModelMap = std::make_shared<gicapods::ConcurrentHashMap<std::string, ModelRequest > > ();
}

ModelCacheService::~ModelCacheService() {

}

void ModelCacheService::clearOtherCaches() {
        uniqueKeyToModelMap->clear();
}

void ModelCacheService::populateOtherMapsAndLists(std::vector<std::shared_ptr<ModelRequest> > allEntities) {
        for(auto request : allEntities) {
                MLOG(2)<<" popupating uniqueKeyToModelMap with "<< request->toJson();
                uniqueKeyToModelMap->put(request->getUniqueKey(), request);
        }
}
