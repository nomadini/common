#ifndef ModelResultCacheService_h
#define ModelResultCacheService_h


#include "ModelResult.h"
#include <string>
#include <memory>
#include <unordered_map>

#include "MySqlModelResultService.h"
#include "HttpUtilService.h"
#include "EntityProviderService.h"
#include "ConcurrentHashMap.h"
#include "CacheService.h"
class EntityToModuleStateStats;
#include "Object.h"
class ModelResultCacheService;



class ModelResultCacheService : public CacheService<ModelResult>, public Object {

public:
std::shared_ptr<gicapods::ConcurrentHashMap<int, ModelResult > >  modelRequestIdToResultMap;
ModelResultCacheService(MySqlModelResultService* mySqlModelResultService,
                        HttpUtilService* httpUtilService,
                        std::string dataMasterUrl,
                        EntityToModuleStateStats* entityToModuleStateStats,std::string appName);


void clearOtherCaches();
void populateOtherMapsAndLists(std::vector<std::shared_ptr<ModelResult> > allEntities);

virtual ~ModelResultCacheService();
};


#endif
