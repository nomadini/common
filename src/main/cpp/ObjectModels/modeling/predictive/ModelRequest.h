//
// Created by Mahmoud Taabodi on 11/19/15.
//

#ifndef ModelRequest_H
#define ModelRequest_H


#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <set>
#include "JsonTypeDefs.h"
#include "Object.h"
#include "ModelResult.h"
/**
   a ModelRequest contains a modelResult class that has the built model in it.
 */

class ModelRequest : public std::enable_shared_from_this<ModelRequest>, public Object {

private:

public:

std::shared_ptr<ModelResult> modelResult;

static constexpr const char *pixelModel = "pixelModel";
static constexpr const char *seedModel = "seedModel";
static constexpr const char *actionTakerCatchAllModel = "actionTakerCatchAllModel";

bool rebuildModelOnNextRun;
bool approvedForScoring;
bool isModelRequestValid;
int id;

std::string name;

std::string modelType;
std::string algorithmToModelBasedOn;


std::string segmentSeedName;
std::string dateOfRequest;

int numberOfNegativeDevicesToBeUsed;
int numberOfPositiveDevicesToBeUsed;
int featureRecencyInSecond;

int maxNumberOfHistoryPerDevice;

double cutOffScore;
int numberOfTopFeatures;
int parentModelRequestId;
int advertiserId;
std::string createdAt;
std::string dateOfRquestCompletion;
std::string description;
std::string cutOffType;

/*** Seed Model Fields ***/

std::set<std::string> seedWebsites;     //all the websites that we use to create positive samples from.
std::set<std::string> featureTypesRequested;

std::set<std::string> negativeFeaturesRequested;    //list of negative features
//that users want to create negative devices from.

int maxNumberOfDeviceHistoryPerFeature;

int maxNumberOfNegativeFeaturesToPick;


/** Pixel Model Fields **/

int pixelHitRecencyInSecond;    //validate this
/**
 * these are the offers that provide the positive source for our pixelModeler
 */
std::set<int> positiveOfferIds;

/**
 * these are the offers that provide the negative source for our pixelModeler
 * it might be an empty list, if its empty, we should use random deviceIds
 * to create negative samples
 */
std::set<int> negativeOfferIds;


virtual void validate();

ModelRequest();

static std::shared_ptr<ModelRequest> clone(std::shared_ptr<ModelRequest> original);

virtual std::string toString();

virtual std::string toJson();
std::string toshortString();

static std::shared_ptr<ModelRequest> fromJson(std::string json);

static void verifyModelHasRequiredFields(std::shared_ptr<ModelRequest> model, bool fullValidations);

static std::string getEntityName();
virtual ~ModelRequest();

/**
 * getters and setters
 */

int getId();

static std::shared_ptr<ModelRequest> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
std::string getUniqueKey();
};

#endif
