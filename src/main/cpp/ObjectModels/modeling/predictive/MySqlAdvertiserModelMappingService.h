#ifndef MySqlAdvertiserModelMappingService_h
#define MySqlAdvertiserModelMappingService_h




#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "AdvertiserModelMapping.h"
#include "Object.h"
class MySqlAdvertiserModelMappingService;




class MySqlAdvertiserModelMappingService : public Object {

public:

MySqlDriver* driver;

MySqlAdvertiserModelMappingService(MySqlDriver* driver);

std::vector<std::shared_ptr<AdvertiserModelMapping>> readAll();

std::vector<std::shared_ptr<AdvertiserModelMapping>> readAllActiveOnes();

std::shared_ptr<AdvertiserModelMapping> read(int id);

void update(std::shared_ptr<AdvertiserModelMapping> obj);

void insert(std::shared_ptr<AdvertiserModelMapping> obj);

virtual void deleteAll();

virtual ~MySqlAdvertiserModelMappingService();

};


#endif
