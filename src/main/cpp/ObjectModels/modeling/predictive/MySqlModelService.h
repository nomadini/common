#ifndef MySqlModelService_h
#define MySqlModelService_h


#include "ModelRequest.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "DataProvider.h"

class MySqlModelService;
#include "Object.h"



class MySqlModelService : public DataProvider<ModelRequest>, public Object {

public:

static std::string commonReadModelQuery;

MySqlDriver* driver;

MySqlModelService(MySqlDriver* driver);

std::shared_ptr<ModelRequest> readModelByNameAndAdvertiserId(std::string name, int advertiserId);
std::shared_ptr<ModelRequest> readModelById(int modelId);


std::shared_ptr<ModelRequest> fetchModelBySegmentSeedNameAndAdvertiserIdOptional(std::string name, int advertiserId);

std::vector<std::shared_ptr<ModelRequest> > readAllEntities();

std::vector<std::string> parseJsonArrayFromString(std::string line, std::string arrayInString);

std::unordered_map<std::string, double> getMapOfStringDoubleFromString(std::string mapDoubleString);

std::unordered_map<std::string, int> getMapOfStringIntFromString(std::string mapInString);


std::shared_ptr<ModelRequest> getModelFromResultSet(std::shared_ptr<ResultSetHolder> res);

std::shared_ptr<ModelRequest> upsertModel(std::shared_ptr<ModelRequest> model);

std::shared_ptr<ModelRequest> updateTheModel(std::shared_ptr<ModelRequest> model);

std::shared_ptr<ModelRequest> insert(std::shared_ptr<ModelRequest> model);

void deleteAll();

std::string strictReplace(std::string main, std::string old, std::string newStr);

std::string replaceStringsInModelQuery(std::string queryStr, std::shared_ptr<ModelRequest> model);

std::vector<int> parseJsonArrayOfIntegersFromString(std::string arrayInString);

virtual ~MySqlModelService();

virtual std::vector<std::shared_ptr<ModelRequest> > readModelRequestsOlderThan(int hours);

static std::shared_ptr<MySqlModelService> getInstance(gicapods::ConfigService* configService);
};


#endif
