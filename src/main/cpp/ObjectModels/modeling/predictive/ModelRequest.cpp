#include "ModelRequest.h"
#include "JsonUtil.h"
#include <string>
#include <memory>
#include "GUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "ModelUtil.h"
#include "CollectionUtil.h"
#include "Feature.h"
#include "DateTimeUtil.h"
#include <boost/foreach.hpp>
void ModelRequest::validate() {
        bool valid = true;

        if (StringUtil::equalsIgnoreCase(ModelRequest::actionTakerCatchAllModel, modelType)) {
                //no validation is needed for actionTakerCatchAllModel
                return;

        } else if (StringUtil::equalsIgnoreCase(ModelRequest::seedModel, modelType)) {
                if (seedWebsites.empty ()) {
                        LOG(ERROR) << "seedWebsites.empty ()";
                        valid = false;
                }
                if (modelType.compare (ModelRequest::seedModel) != 0) {
                        LOG(ERROR) << "modelType.compare (ModelRequest::seedModel) != 0";
                        valid = false;
                }
                if (maxNumberOfDeviceHistoryPerFeature <= 0) {
                        LOG(ERROR) << "maxNumberOfDeviceHistoryPerFeature <= 0";
                        valid = false;
                }
                if (maxNumberOfNegativeFeaturesToPick <= 0) {
                        LOG(ERROR) << "maxNumberOfNegativeFeaturesToPick <= 0";
                        valid = false;
                }
        } else if (StringUtil::equalsIgnoreCase(ModelRequest::pixelModel, modelType)) {
                if (pixelHitRecencyInSecond <= 100) {
                        LOG(ERROR) << "pixelHitRecencyInSecond <= 100";
                        valid = false;
                }
                if (positiveOfferIds.empty ()) {
                        LOG(ERROR) << "positiveOfferIds.empty ()";
                        valid = false;
                }
        } else {
                throwEx ("wrong type of model : " + modelType);
        }


        if (numberOfNegativeDevicesToBeUsed <= 0) {
                LOG(WARNING) << "setting numberOfNegativeDevicesToBeUsed to 10000";
                numberOfNegativeDevicesToBeUsed = 10000;
        }
        if (numberOfPositiveDevicesToBeUsed <= 0) {
                LOG(WARNING) << "setting numberOfPositiveDevicesToBeUsed to 1000";
                numberOfPositiveDevicesToBeUsed = 1000;
        }
        if (maxNumberOfHistoryPerDevice <= 0) {
                LOG(WARNING) << "setting maxNumberOfHistoryPerDevice to 100";
                maxNumberOfHistoryPerDevice = 100;
        }

        if (advertiserId  <= 0) {
                LOG(ERROR) << "advertiserId <= 0";
                valid = false;
        }
        // if (numberOfTopFeatures <= 0) {
        // valid = false;
        // }
        if (cutOffScore <= 0) {
                LOG(ERROR) << "cutOffScore <= 0";
                valid = false;
        }

        if (name.empty ()) {
                LOG(ERROR) << "name.empty ()";
                valid = false;
        }

        if (modelType.empty ()) {
                LOG(ERROR) << "modelType.empty ()";
                valid = false;
        }

        if (algorithmToModelBasedOn.empty ()) {
                LOG(ERROR) << "algorithmToModelBasedOn.empty ()";
                valid = false;
        }

        if (segmentSeedName.empty ()) {
                LOG(ERROR) << "segmentSeedName.empty ()";
                valid = false;
        }
        if (dateOfRequest.empty ()) {
                LOG(ERROR) << "dateOfRequest.empty ()";
                valid = false;
        }

        if (featureRecencyInSecond <= 10) {
                LOG(ERROR) << "featureRecencyInSecond <= 10";
                valid = false;
        }

        if (!valid) {
                LOG(ERROR) << "model with id : "<<id << " is invalid";
        }
        assertAndThrow(valid);
        this->isModelRequestValid = !valid;

}

ModelRequest::ModelRequest() : Object(__FILE__) {
        modelResult = std::make_shared<ModelResult>();

        maxNumberOfDeviceHistoryPerFeature = 0;
        maxNumberOfNegativeFeaturesToPick = 0;
        pixelHitRecencyInSecond = 0;
        parentModelRequestId = -1;
        isModelRequestValid = false;
        rebuildModelOnNextRun = false;
        approvedForScoring = false;
        numberOfNegativeDevicesToBeUsed = -2;
        numberOfPositiveDevicesToBeUsed = -2;
        maxNumberOfHistoryPerDevice = 100;

        featureRecencyInSecond = -2;
        numberOfTopFeatures = -1;
        cutOffScore = -1;
        advertiserId = -1;

        featureTypesRequested = Feature::getAllFeatureTypes();

        featureRecencyInSecond = 3600 * 24 * 7;


        id = -1;
}

std::string ModelRequest::toshortString() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "id", id, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "parentModelRequestId", parentModelRequestId, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "name", name, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "modelType", modelType, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "segmentSeedName", segmentSeedName, value);

        return JsonUtil::valueToString (value);
}

std::string ModelRequest::toString() {
        return toJson ();
}

std::string ModelRequest::toJson() {
        validate ();
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}

std::shared_ptr<ModelRequest> ModelRequest::fromJson(std::string json) {
        MLOG (3) << "json " << json;
        auto document = parseJsonSafely (json);
        return fromJsonValue(*document);
}


std::string ModelRequest::getEntityName() {
        return "ModelRequest";
}

void ModelRequest::verifyModelHasRequiredFields(std::shared_ptr<ModelRequest> model, bool fullValidations) {

        MLOG (3) << "model : " << model->toJson ();
        if (StringUtil::equalsIgnoreCase(ModelRequest::actionTakerCatchAllModel, model->modelType)) {
                //no validation is needed for actionTakerCatchAllModel
                return;

        }

        assertAndThrow (!model->segmentSeedName.empty ());
        assertAndThrow (!model->dateOfRequest.empty ());
        assertAndThrow (model->featureRecencyInSecond > 0);
        assertAndThrow (model->advertiserId > 0);
        assertAndThrow (model->maxNumberOfDeviceHistoryPerFeature > 0);
        assertAndThrow (!model->modelType.empty ());

        if (fullValidations) {

                assertAndThrow (!model->algorithmToModelBasedOn.empty ());
                assertAndThrow (!model->negativeFeaturesRequested.empty ());

        }

}

ModelRequest::~ModelRequest() {

}

int ModelRequest::getId() {
        return id;
}


std::shared_ptr<ModelRequest> ModelRequest::fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<ModelRequest> model = std::make_shared<ModelRequest>();
        JsonArrayUtil::getArrayFromValueMemeber (value,"seedWebsites", model->seedWebsites);
        JsonArrayUtil::getArrayFromValueMemeber (value,"featureTypesRequested", model->featureTypesRequested);

        model->maxNumberOfNegativeFeaturesToPick =  JsonUtil::GetIntSafely (value, "maxNumberOfNegativeFeaturesToPick");

        JsonArrayUtil::getArrayFromValueMemeber (value, "negativeFeaturesRequested", model->negativeFeaturesRequested );

        model->maxNumberOfDeviceHistoryPerFeature = JsonUtil::GetIntSafely (value, "maxNumberOfDeviceHistoryPerFeature");

        JsonArrayUtil::getArrayFromValueMemeber (value, "positiveOfferIds", model->positiveOfferIds);

        JsonArrayUtil::getArrayFromValueMemeber
                (value,
                "negativeOfferIds",
                model->negativeOfferIds
                );
        model->pixelHitRecencyInSecond = JsonUtil::GetIntSafely (value, "pixelHitRecencyInSecond");



        //common fieds
        model->id = JsonUtil::GetIntSafely (value, "id");
        model->parentModelRequestId = JsonUtil::GetIntSafely (value, "parentModelRequestId");
        model->name = JsonUtil::GetStringSafely (value, "name");
        model->modelType = JsonUtil::GetStringSafely (value, "modelType");
        model->segmentSeedName = JsonUtil::GetStringSafely (value, "segmentSeedName");
        model->createdAt = JsonUtil::GetStringSafely (value, "createdAt");
        model->dateOfRquestCompletion = JsonUtil::GetStringSafely (value, "dateOfRquestCompletion");
        model->description = JsonUtil::GetStringSafely (value, "description");
        model->cutOffType = JsonUtil::GetStringSafely (value, "cutOffType");
        model->dateOfRequest = JsonUtil::GetStringSafely (value, "dateOfRequest");

        model->algorithmToModelBasedOn = JsonUtil::GetStringSafely (value, "algorithmToModelBasedOn");

        model->numberOfNegativeDevicesToBeUsed = JsonUtil::GetIntSafely(value,
                                                                        "numberOfNegativeDevicesToBeUsed");

        model->numberOfPositiveDevicesToBeUsed = JsonUtil::GetIntSafely(value,
                                                                        "numberOfPositiveDevicesToBeUsed");
        model->maxNumberOfHistoryPerDevice = JsonUtil::GetIntSafely(value,
                                                                    "maxNumberOfHistoryPerDevice");

        model->featureRecencyInSecond = JsonUtil::GetIntSafely(value, "featureRecencyInSecond");

        model->cutOffScore = JsonUtil::GetDoubleSafely(value, "cutOffScore");
        model->advertiserId = JsonUtil::GetIntSafely(value, "advertiserId");

        model->numberOfTopFeatures = JsonUtil::GetIntSafely(value, "numberOfTopFeatures");

        model->maxNumberOfNegativeFeaturesToPick =
                JsonUtil::GetIntSafely(value, "maxNumberOfNegativeFeaturesToPick");



        model->maxNumberOfDeviceHistoryPerFeature =
                JsonUtil::GetIntSafely(value, "maxNumberOfDeviceHistoryPerFeature");

        JsonArrayUtil::getArrayFromValueMemeber(
                value,
                "positiveOfferIds",
                model->positiveOfferIds);

        JsonArrayUtil::getArrayFromValueMemeber(
                value,
                "negativeOfferIds",
                model->negativeOfferIds);

        model->pixelHitRecencyInSecond =
                JsonUtil::GetIntSafely(value, "pixelHitRecencyInSecond");

        model->validate ();

        return model;
}

void ModelRequest::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        /** adding all the common fields to json **/

        JsonUtil::addMemberToValue_FromPair(doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair(doc, "parentModelRequestId", parentModelRequestId, value);
        JsonUtil::addMemberToValue_FromPair(doc, "name", name, value);
        JsonUtil::addMemberToValue_FromPair(doc, "modelType", modelType, value);
        JsonUtil::addMemberToValue_FromPair(doc, "segmentSeedName", segmentSeedName, value);
        JsonUtil::addMemberToValue_FromPair(doc, "createdAt", createdAt, value);
        JsonUtil::addMemberToValue_FromPair(doc, "dateOfRquestCompletion", dateOfRquestCompletion, value);
        JsonUtil::addMemberToValue_FromPair(doc, "description", description, value);
        JsonUtil::addMemberToValue_FromPair(doc, "cutOffType", cutOffType, value);


        JsonArrayUtil::addMemberToValue_FromPair(doc,"seedWebsites",
                                                 CollectionUtil::convertSetToList<std::string>(seedWebsites),
                                                 value);

        JsonArrayUtil::addMemberToValue_FromPair(doc,"featureTypesRequested",
                                                 CollectionUtil::convertSetToList<std::string>(featureTypesRequested),
                                                 value);
        JsonUtil::addMemberToValue_FromPair(doc, "dateOfRequest",
                                            dateOfRequest, value);

        JsonUtil::addMemberToValue_FromPair(doc, "algorithmToModelBasedOn",
                                            algorithmToModelBasedOn, value);



        JsonUtil::addMemberToValue_FromPair(doc, "featureRecencyInSecond",
                                            featureRecencyInSecond, value);


        JsonUtil::addMemberToValue_FromPair(doc, "cutOffScore",
                                            cutOffScore, value);

        JsonUtil::addMemberToValue_FromPair(doc, "advertiserId",
                                            advertiserId, value);

        JsonUtil::addMemberToValue_FromPair(doc, "numberOfTopFeatures",
                                            numberOfTopFeatures, value);

        JsonUtil::addMemberToValue_FromPair(doc, "maxNumberOfNegativeFeaturesToPick",
                                            maxNumberOfNegativeFeaturesToPick, value);

        JsonArrayUtil::addMemberToValue_FromPair (doc, "negativeFeaturesRequested",
                                                  CollectionUtil::convertSetToList<std::string>(negativeFeaturesRequested),  value);

        JsonUtil::addMemberToValue_FromPair(doc, "maxNumberOfDeviceHistoryPerFeature",
                                            maxNumberOfDeviceHistoryPerFeature, value);


        JsonArrayUtil::addMemberToValue_FromPair (doc,"positiveOfferIds",
                                                  positiveOfferIds,  value);

        JsonArrayUtil::addMemberToValue_FromPair (doc, "negativeOfferIds",
                                                  negativeOfferIds,  value);

        JsonUtil::addMemberToValue_FromPair(doc, "pixelHitRecencyInSecond", pixelHitRecencyInSecond, value);

        JsonUtil::addMemberToValue_FromPair(doc, "numberOfNegativeDevicesToBeUsed", numberOfNegativeDevicesToBeUsed, value);

        JsonUtil::addMemberToValue_FromPair(doc, "numberOfPositiveDevicesToBeUsed", numberOfPositiveDevicesToBeUsed, value);
        JsonUtil::addMemberToValue_FromPair(doc, "maxNumberOfHistoryPerDevice", maxNumberOfHistoryPerDevice, value);

        JsonUtil::addBooleanMemberToValue_FromPair(doc, "rebuildModelOnNextRun", rebuildModelOnNextRun, value);
        JsonUtil::addBooleanMemberToValue_FromPair(doc, "approvedForScoring", approvedForScoring, value);
        JsonUtil::addBooleanMemberToValue_FromPair(doc, "isModelRequestValid", isModelRequestValid, value);
}

std::shared_ptr<ModelRequest> ModelRequest::clone(std::shared_ptr<ModelRequest> original) {
        auto clone = std::make_shared<ModelRequest>();
        clone->isModelRequestValid = original->isModelRequestValid;
        clone->rebuildModelOnNextRun = original->rebuildModelOnNextRun;
        clone->approvedForScoring = original->approvedForScoring;
        clone->id = original->id;

        clone->name = original->name;
        clone->modelType = original->modelType;
        clone->algorithmToModelBasedOn = original->algorithmToModelBasedOn;


        clone->segmentSeedName = original->segmentSeedName;
        clone->dateOfRequest = original->dateOfRequest;

        clone->numberOfNegativeDevicesToBeUsed = original->numberOfNegativeDevicesToBeUsed;
        clone->numberOfPositiveDevicesToBeUsed = original->numberOfPositiveDevicesToBeUsed;
        clone->maxNumberOfHistoryPerDevice = original->maxNumberOfHistoryPerDevice;

        clone->featureRecencyInSecond = original->featureRecencyInSecond;


        clone->cutOffScore = original->cutOffScore;
        clone->numberOfTopFeatures = original->numberOfTopFeatures;
        clone->parentModelRequestId = original->parentModelRequestId;
        clone->advertiserId = original->advertiserId;
        clone->createdAt = original->createdAt;
        clone->dateOfRquestCompletion = original->dateOfRquestCompletion;
        clone->description = original->description;
        clone->cutOffType = original->cutOffType;
        clone->seedWebsites = original->seedWebsites;
        clone->featureTypesRequested = original->featureTypesRequested;

        clone->negativeFeaturesRequested = original->negativeFeaturesRequested;
        clone->maxNumberOfDeviceHistoryPerFeature = original->maxNumberOfDeviceHistoryPerFeature;

        clone->maxNumberOfNegativeFeaturesToPick = original->maxNumberOfNegativeFeaturesToPick;



        clone->pixelHitRecencyInSecond = original->pixelHitRecencyInSecond;

        clone->positiveOfferIds = original->positiveOfferIds;

        clone->negativeOfferIds = original->negativeOfferIds;

        return clone;
}

std::string ModelRequest::getUniqueKey() {
        return name+ "_" + _toStr (advertiserId);
}
