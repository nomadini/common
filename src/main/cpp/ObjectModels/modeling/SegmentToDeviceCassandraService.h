/*
 * SegmentToDeviceCassandraService.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mtaabodi
 */

#ifndef SegmentToDeviceCassandraService_H_
#define SegmentToDeviceCassandraService_H_

class CassandraDriverInterface;
namespace gicapods { class ConfigService; }
#include "CassandraService.h"
#include "SegmentDevices.h"
#include "HttpUtilService.h"
class EntityToModuleStateStats;
class SegmentToDeviceCassandraService;
#include "Object.h"


class SegmentToDeviceCassandraService : public CassandraService<SegmentDevices>, public Object {

private:
EntityToModuleStateStats* entityToModuleStateStats;
public:
SegmentToDeviceCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,
        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService);

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_CASSANDRA_SegmentToDeviceCassandraService_H_ */
