/*
 * DeviceFeatureHistoryTestHelper.h
 *
 *  Created on: Sep 7, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_DEVICEFEATUREHISTORYTESTHELPER_H_
#define GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_DEVICEFEATUREHISTORYTESTHELPER_H_
#include "Object.h"
#include <memory>
class DeviceFeatureHistory;

class DeviceFeatureHistoryTestHelper {
public:

static std::shared_ptr<DeviceFeatureHistory> createNewDeviceFeatureHistoryFromActionTakersPool(std::string seedWebsite);
static std::shared_ptr<DeviceFeatureHistory> createNewDeviceFeatureHistory();
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_DEVICEFEATUREHISTORYTESTHELPER_H_ */
