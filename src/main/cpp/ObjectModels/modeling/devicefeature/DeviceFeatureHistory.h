#ifndef DeviceFeatureHistory_H
#define DeviceFeatureHistory_H





#include "JsonTypeDefs.h"


#include "cassandra.h"
#include "AtomicLong.h"
#include "FeatureHistory.h"
#include "DateTimeMacro.h"
#include "CassandraManagedType.h"
#include <unordered_map>
#include <unordered_set>

class Device;
class Feature;
#include "Object.h"
class DeviceFeatureHistory : public CassandraManagedType, public Object {

private:

std::shared_ptr<std::vector<std::shared_ptr<FeatureHistory> > > features;
public:
std::shared_ptr<std::unordered_map<std::string, int> > featureToVisitCounts;


std::shared_ptr<Device> device;

//this variable is used only when reading features, it its set,
//we don't pick up the features that are older that this recency value
TimeType featureRecencyInSecondToPickUp;

//indicates if this device history is a positive or negative
bool positiveHistoryValue;

DeviceFeatureHistory(std::shared_ptr<Device> device);
virtual ~DeviceFeatureHistory();

std::shared_ptr<DeviceFeatureHistory> getCleanHistoryClone(int minFeatureTime);

std::string toString();
std::string toJson();
static std::shared_ptr<DeviceFeatureHistory> fromJson(std::string json);
// static void addFeature(TimeType timeOfVisit);
static std::shared_ptr<DeviceFeatureHistory> fromJsonValue(RapidJsonValueType value);

std::shared_ptr<std::vector<std::shared_ptr<FeatureHistory> > >  getFeatures();
std::shared_ptr<std::vector<std::shared_ptr<FeatureHistory> > >  getFeaturesHappenedMoreThanNTimes(int n);

void removeFeaturesFromHistory(std::unordered_set<std::string> featureName);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

std::shared_ptr<std::unordered_map<std::string, int> > consutructFeatureVisitCountsMap();
};


#endif
