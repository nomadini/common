
#include "GUtil.h"
#include "CollectionUtil.h"

#include "JsonMapUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonArrayUtil.h"
#include "DeviceRecentVisitHistory.h"
#include "RecentVisitHistory.h"

#include "GUtil.h"
#include "Device.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"

#include "ConverterUtil.h"
#include "CassandraService.h"

DeviceRecentVisitHistory::DeviceRecentVisitHistory(std::shared_ptr<Device> device)  : Object(__FILE__) {
								this->device = device;
}

DeviceRecentVisitHistory::~DeviceRecentVisitHistory() {
}

std::string DeviceRecentVisitHistory::toJson() {
								auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

								addPropertiesToJsonValue(value, doc.get());

								return JsonUtil::valueToString(value);
}



void DeviceRecentVisitHistory::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

								JsonUtil::addMemberToValue_FromPair(
																doc,
																"deviceId",
																device->getDeviceId(), value);
								JsonUtil::addMemberToValue_FromPair(
																doc,
																"deviceType",
																device->getDeviceType(),
																value);
								JsonArrayUtil::addMemberToValue_FromPair(
																doc,
																"allRecentHistories",
																allRecentHistories,
																value);


}


std::shared_ptr<DeviceRecentVisitHistory> DeviceRecentVisitHistory::fromJson(std::string json) {
								auto document = parseJsonSafely(json);
								return fromJsonValue(*document);
}


std::shared_ptr<DeviceRecentVisitHistory> DeviceRecentVisitHistory::fromJsonValue(RapidJsonValueType value) {
								auto deviceRecentVisitHistory =
																std::make_shared<DeviceRecentVisitHistory>(
																								std::make_shared<Device>(
																																JsonUtil::GetStringSafely (value, "deviceId"),
																																JsonUtil::GetStringSafely (value, "deviceType")));


								deviceRecentVisitHistory->allRecentHistories = JsonArrayUtil::getArrayOfObjectsFromMemberInValue<RecentVisitHistory>(value, "allRecentHistories");

								return deviceRecentVisitHistory;
}
