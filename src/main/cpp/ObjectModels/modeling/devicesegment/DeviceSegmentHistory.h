#ifndef DeviceSegmentHistory_H
#define DeviceSegmentHistory_H


#include "JsonTypeDefs.h"

class StringUtil;
#include <string>
#include <memory>

#include <gtest/gtest.h>
class Device;

#include "cassandra.h"
#include "CassandraManagedType.h"
class Segment;
class DeviceSegmentPair;
class DeviceSegmentHistory;

#include "Object.h"
class DeviceSegmentHistory : public CassandraManagedType, public Object {

public:

std::shared_ptr<Device> device;

std::shared_ptr<std::vector<std::shared_ptr<DeviceSegmentPair> > > deviceSegmentAssociations;



DeviceSegmentHistory(
        std::shared_ptr<Device> device);

std::string toString();

std::string toJson();
static std::string getName();

static std::shared_ptr<DeviceSegmentHistory> fromJson(std::string jsonModel);
static std::shared_ptr<DeviceSegmentHistory> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
virtual ~DeviceSegmentHistory();
};

#endif
