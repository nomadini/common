/*
 * DeviceSegmentHistoryCassandraService.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mtaabodi
 */

#ifndef DeviceSegmentHistoryCassandraService_H_
#define DeviceSegmentHistoryCassandraService_H_

class CassandraDriverInterface;
namespace gicapods { class ConfigService; }
#include "DeviceSegmentHistory.h"
#include "HttpUtilService.h"
class EntityToModuleStateStats;

class AsyncThreadPoolService;
#include "CassandraService.h"
#include "Object.h"
class DeviceSegmentHistoryCassandraService : public CassandraService<DeviceSegmentHistory>, public Object {

private:
CassandraDriverInterface* cassandraDriver;
HttpUtilService* httpUtilService;
EntityToModuleStateStats* entityToModuleStateStats;

public:

DeviceSegmentHistoryCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,
        
        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService);

void deleteAll();



virtual ~DeviceSegmentHistoryCassandraService();
};

#endif
