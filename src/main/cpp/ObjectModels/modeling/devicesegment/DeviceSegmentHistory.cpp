
#include "JsonUtil.h"
#include "StringUtil.h"
#include "JsonArrayUtil.h"
#include "DeviceSegmentHistory.h"
#include "CassandraService.h"

#include "Segment.h"
#include "DeviceSegmentPair.h"
#include "Device.h"
DeviceSegmentHistory::DeviceSegmentHistory(std::shared_ptr<Device> device)  : Object(__FILE__) {
        this->device = device;
        this->deviceSegmentAssociations = std::make_shared<std::vector<std::shared_ptr<DeviceSegmentPair> > > ();

}

DeviceSegmentHistory::~DeviceSegmentHistory() {

}

std::string DeviceSegmentHistory::getName() {
        return "DeviceSegmentHistory";
}

std::string DeviceSegmentHistory::toString() {
        return toJson();
}

std::string DeviceSegmentHistory::toJson() {
        auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString(value);
}

void DeviceSegmentHistory::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

        JsonUtil::addMemberToValue_FromPair(doc, "deviceId", device->getDeviceId(), value);
        JsonUtil::addMemberToValue_FromPair(doc, "deviceType", device->getDeviceType(), value);
        JsonUtil::assertJsonTypeAndThrow(value, "object");
        JsonArrayUtil::addMemberToValue_FromPair(
                doc,
                "deviceSegmentAssociations",
                *deviceSegmentAssociations,
                value
                );
}

std::shared_ptr<DeviceSegmentHistory> DeviceSegmentHistory::fromJsonValue(RapidJsonValueType value) {
        auto deviceSegmentHistory =
                std::make_shared<DeviceSegmentHistory>(
                        std::make_shared<Device>(
                                JsonUtil::GetStringSafely (value, "deviceId"),
                                JsonUtil::GetStringSafely (value, "deviceType")));

        JsonArrayUtil::getArrayFromValueMemeber(value,
                                                "deviceSegmentAssociations",
                                                *(deviceSegmentHistory->deviceSegmentAssociations));
        return deviceSegmentHistory;
}

std::shared_ptr<DeviceSegmentHistory> DeviceSegmentHistory::fromJson(std::string jsonModel) {
        auto document = parseJsonSafely(jsonModel);
        return fromJsonValue(*document);

}
