
#include "GUtil.h"
#include "CollectionUtil.h"

#include "JsonMapUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonArrayUtil.h"
#include "DeviceRecentVisitHistory.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"

#include "ConverterUtil.h"
#include "RecentVisitHistory.h"

RecentVisitHistory::RecentVisitHistory()  : Object(__FILE__) {
}
std::string RecentVisitHistory::getName() {
								return "RecentVisitHistory";
}

std::string RecentVisitHistory::toJson() {
								auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString(value);
}

std::shared_ptr<RecentVisitHistory> RecentVisitHistory::fromJson(std::string json) {
								auto document = parseJsonSafely(json);
								return fromJsonValue(*document);
}

std::shared_ptr<RecentVisitHistory> RecentVisitHistory::fromJsonValue(RapidJsonValueType value) {
								auto obj = std::make_shared<RecentVisitHistory>();
								obj->featureName = JsonUtil::GetStringSafely (value, "featureName");
								obj->featureType = JsonUtil::GetStringSafely (value, "featureType");
								obj->timeSeenInMillis = JsonUtil::GetLongSafely (value, "timeSeenInMillis");
								return obj;
}

void RecentVisitHistory::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
								JsonUtil::addMemberToValue_FromPair(doc, "featureName", featureName, value);
								JsonUtil::addMemberToValue_FromPair(doc, "featureType", featureType, value);
								JsonUtil::addMemberToValue_FromPair(doc, "timeSeenInMillis", timeSeenInMillis, value);
}

RecentVisitHistory::~RecentVisitHistory() {

}
