/*
 * FeatureDeviceHistoryCassandraService.h
 *
 *  Created on: Sep 8, 2015
 *      Author: mtaabodi
 */

#ifndef FeatureDeviceHistoryCassandraService_H
#define FeatureDeviceHistoryCassandraService_H
#include "Object.h"
#include "FeatureDeviceHistory.h"
#include "gmock/gmock.h"
class CassandraDriverInterface;
namespace gicapods {class ConfigService; }
#include <memory>
#include <string>
#include <vector>
#include <set>
#include "Device.h"
#include "Feature.h"
#include "DateTimeMacro.h"
#include "HttpUtilService.h"
class EntityToModuleStateStats;
#include "CassandraService.h"
class AsyncThreadPoolService;

class FeatureDeviceHistoryCassandraService : public CassandraService<FeatureDeviceHistory>, public Object {

private:
CassandraDriverInterface* cassandraDriver;
HttpUtilService* httpUtilService;
EntityToModuleStateStats* entityToModuleStateStats;


AsyncThreadPoolService* asyncThreadPoolService;

virtual void fetchDeviceHistory(CassIterator* rowIterator,
                                std::shared_ptr<FeatureDeviceHistory> featureDeviceHistories,
                                TimeType timeOfVisitFromDB);

public:
FeatureDeviceHistoryCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,

        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService);

virtual void deleteAll();

virtual std::vector<std::shared_ptr<FeatureDeviceHistory> > readDistinctFeaturesExcluding(
        std::set<std::string> featuresToExclude,
        int maxNumberOfFeaturesToPick,
        int featureRecencyInSecond,
        std::set<std::string> featureTypesRequested);

virtual std::vector<std::shared_ptr<FeatureDeviceHistory> > readDistinctFeaturesInThisSet(
        std::set<std::string> featureSet,
        int featureRecencyInSecond,
        std::set<std::string> featureTypesRequested);

virtual std::vector<std::shared_ptr<FeatureDeviceHistory> > readDeviceHistoryOfFeatures(
        int limit,
        TimeType time,
        std::set<std::string> historyWanted,
        std::string featureType);


virtual std::shared_ptr<FeatureDeviceHistory> readDeviceHistoryOfFeature(std::string feature,
                                                                         std::string featureType,
                                                                         TimeType time,
                                                                         int limit);



};

#endif
