#ifndef FeatureHistory_H
#define FeatureHistory_H



#include <string>
#include <memory>
#include "JsonTypeDefs.h"
#include "DateTimeMacro.h"
class Device;
#include "Feature.h"
#include "Object.h"

class FeatureHistory : public Object {

public:

std::shared_ptr<Feature> feature;
long featureSeenInMillis;

FeatureHistory(std::shared_ptr<Feature> feature, long featureSeenInMillis);
virtual ~FeatureHistory();

static std::shared_ptr<FeatureHistory> fromJsonValue(RapidJsonValueType value);
std::string toJson();
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

};


#endif
