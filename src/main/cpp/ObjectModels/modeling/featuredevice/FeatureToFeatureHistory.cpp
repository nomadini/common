

#include "GUtil.h"


#include "FeatureHistory.h"
#include "FeatureToFeatureHistory.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "GUtil.h"
#include "FeatureHistory.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "Feature.h"
#include "Device.h"
#include "DateTimeUtil.h"
#include "CassandraService.h"

FeatureToFeatureHistory::FeatureToFeatureHistory(
								std::unique_ptr<Feature> feature) : Object(__FILE__) {
								this->feature = std::move(feature);

}

FeatureToFeatureHistory::~FeatureToFeatureHistory() {

}

std::string FeatureToFeatureHistory::toString() {
								return this->toJson();
}

std::string FeatureToFeatureHistory::toJson() {
								auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

								addPropertiesToJsonValue(value, doc.get());

								return JsonUtil::valueToString(value);
}

std::shared_ptr<FeatureToFeatureHistory> FeatureToFeatureHistory::fromJsonValue(RapidJsonValueType value) {
								auto object = std::make_shared<FeatureToFeatureHistory>(
																std::make_unique<Feature>(
																								JsonUtil::GetStringSafely (value, "type"),
																								JsonUtil::GetStringSafely (value, "feature"),
																								0)
																);
								JsonArrayUtil::getArrayFromValueMemeber(
																value,
																"featureHistories",
																object->featureHistories);
								return object;
}

std::shared_ptr<FeatureToFeatureHistory> FeatureToFeatureHistory::fromJson(std::string jsonModel) {

								auto document = parseJsonSafely(jsonModel);
								return fromJsonValue(*document);
}

void FeatureToFeatureHistory::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

								RapidJsonValueTypeNoRef featureValue(rapidjson::kObjectType);
								feature->addPropertiesToJsonValue(featureValue, doc);
								JsonUtil::addMemberToValue_FromPair(doc, "feature", featureValue, value);

								JsonArrayUtil::addMemberToValue_FromPair(
																doc,
																"featureHistories",
																featureHistories,
																value
																);
}
