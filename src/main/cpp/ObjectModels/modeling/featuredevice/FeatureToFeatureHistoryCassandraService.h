/*
 * FeatureToFeatureHistoryCassandraService.h
 *
 *  Created on: Sep 8, 2015
 *      Author: mtaabodi
 */

#ifndef FeatureToFeatureHistoryCassandraService_H
#define FeatureToFeatureHistoryCassandraService_H
#include "Object.h"
#include "FeatureToFeatureHistory.h"
#include "gmock/gmock.h"
class CassandraDriverInterface;
namespace gicapods {class ConfigService; }
#include <memory>
#include <string>
#include <vector>
#include <set>
#include "Device.h"
#include "Feature.h"
#include "DateTimeMacro.h"
#include "HttpUtilService.h"
class EntityToModuleStateStats;
#include "CassandraService.h"
class AsyncThreadPoolService;

class FeatureToFeatureHistoryCassandraService : public CassandraService<FeatureToFeatureHistory>, public Object {

private:
CassandraDriverInterface* cassandraDriver;
HttpUtilService* httpUtilService;
EntityToModuleStateStats* entityToModuleStateStats;


AsyncThreadPoolService* asyncThreadPoolService;

public:
FeatureToFeatureHistoryCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,

        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService);

virtual void deleteAll();


virtual std::vector<std::shared_ptr<FeatureToFeatureHistory> > readFeatureHistoryOfFeatures(
        int limit,
        TimeType time,
        std::set<std::string> historyWanted,
        std::string featureType);

virtual void fetchFeatureHistory(CassIterator* rowIterator,
                                 std::shared_ptr<FeatureToFeatureHistory> featureDeviceHistories,
                                 TimeType timeOfVisitFromDB);

virtual std::shared_ptr<FeatureToFeatureHistory> readFeatureHistoryOfFeature(std::string feature,
                                                                             std::string featureType,
                                                                             TimeType time,
                                                                             int limit);



};


#endif
