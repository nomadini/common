
#include "FeatureDeviceHistory.h"
#include "CassandraDriverInterface.h"
#include "gmock/gmock.h"
#include "FeatureDeviceHistoryCassandraService.h"

#include "CollectionUtil.h"
#include "StringUtil.h"
#include "DateTimeUtil.h"
#include "JsonArrayUtil.h"
#include "StatisticsUtil.h"
#include "ConfigService.h"
#include "CassandraDriver.h"
#include "DeviceHistory.h"


FeatureDeviceHistoryCassandraService::FeatureDeviceHistoryCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,

        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService)  :
        CassandraService<FeatureDeviceHistory>(cassandraDriver,
                                               entityToModuleStateStats,

                                               asyncThreadPoolService,
                                               configService), Object(__FILE__) {
        this->cassandraDriver = cassandraDriver;
        this->httpUtilService = httpUtilService;
        this->entityToModuleStateStats = entityToModuleStateStats;
}


void FeatureDeviceHistoryCassandraService::deleteAll() {
        cassandraDriver->deleteAll (
                "gicapods_mdl.featuredevicehistory"
                );
}


void FeatureDeviceHistoryCassandraService::fetchDeviceHistory(CassIterator* rowIterator,
                                                              std::shared_ptr<FeatureDeviceHistory> featureDeviceHistories,
                                                              TimeType timeOfVisitFromDB) {
        std::shared_ptr<DeviceHistory> devHistory = std::make_shared<DeviceHistory>(
                std::make_shared<Device>(
                        CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "deviceId"),
                        CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "deviceType"))
                );
        devHistory->timeOfVisit = timeOfVisitFromDB;

        featureDeviceHistories->devicehistories.push_back(devHistory);
}


std::shared_ptr<FeatureDeviceHistory> FeatureDeviceHistoryCassandraService::
readDeviceHistoryOfFeature(
        std::string feature,
        std::string featureType,
        TimeType time,
        int limit) {

        int numberOfDeviceHistoriesFetchedForThisFeature = 0;
        std::string queryStr =
                "SELECT deviceId, deviceType, timeofvisit from "
                " gicapods_mdl.featuredevicehistory "
                " where feature = '__FEATURE__'";

        queryStr = StringUtil::replaceString(queryStr, "__FEATURE__", feature);
        MLOG(3)<<"queryStr to read feature device history : "<<queryStr;
        const char* query = queryStr.c_str();

        auto statement = cass_statement_new(query, 0);

        auto future = cass_session_execute(cassandraDriver->getSession(), statement);
        cass_future_wait_timed (future, 100000);

        auto rc = cass_future_error_code(future);
        std::shared_ptr<FeatureDeviceHistory> featureDeviceHistories =
                std::make_shared<FeatureDeviceHistory>(
                        std::make_unique<Feature>(featureType, feature, 0));

        if (rc != CASS_OK) {
                print_error(future, queryStr, entityToModuleStateStats);
        } else {
                const CassResult* result = cass_future_get_result(future);

                CassIterator* rowIterator = cass_iterator_from_result(result);

                while (cass_iterator_next(rowIterator)) {

                        long timeOfVisitFromDB =
                                CassandraDriverInterface::getLongValueFromRowByName (rowIterator, "timeofvisit");
                        if (timeOfVisitFromDB > time) {
                                fetchDeviceHistory(rowIterator,
                                                   featureDeviceHistories,
                                                   timeOfVisitFromDB);
                                numberOfDeviceHistoriesFetchedForThisFeature++;
                                if (numberOfDeviceHistoriesFetchedForThisFeature >= limit) {
                                        break;
                                }
                        } else {
                                MLOG(3)<<"feature device history is too old to be fetched, timeOfVisitFromDB "
                                        " :  "<<timeOfVisitFromDB
                                       <<" , time : "<<time;
                        }

                }

                cass_iterator_free(rowIterator);
                cass_result_free(result);
        }

        cass_future_free(future);
        cass_statement_free(statement);
        MLOG(3)<<"  "<<featureDeviceHistories->devicehistories.size()
               <<" devices visited this feature  "<<featureDeviceHistories->feature->getName();
        return featureDeviceHistories;
}


std::vector<std::shared_ptr<FeatureDeviceHistory> >
FeatureDeviceHistoryCassandraService::readDistinctFeaturesInThisSet(
        std::set<std::string> featureSet,
        int featureRecencyInSecond,
        std::set<std::string> featureTypesRequested) {
        int numberOfDeviceHistoriesFetchedForThisFeature = 0;
        std::string queryStr =
                " select feature, featureType from "
                " gicapods_mdl.featuredevicehistory "
                " where "
                "  timeOfVisit >=  "
                + _toStr(DateTimeUtil::getNowInMilliSecond() - (featureRecencyInSecond * 1000)) +
                " feature in ( __FEATURE__ ) ALLOW FILTERING ";

        for (std::set<std::string>::iterator iter = featureSet.begin();
             iter != featureSet.end(); ) {

                queryStr = StringUtil::replaceString(queryStr, "__FEATURE__",
                                                     StringUtil::toStr("'") + StringUtil::toStr(*iter) + StringUtil::toStr("'") + StringUtil::toStr(" ,__FEATURE__ "));

                if (++iter == featureSet.end()) {
                        queryStr = StringUtil::replaceString(queryStr, ",__FEATURE__", "");
                }
        }

        MLOG(3)<<"queryStr to read distinct features in feature set : "<<queryStr;
        const char* query = queryStr.c_str();

        auto statement = cass_statement_new(query, 0);

        auto future = cass_session_execute(cassandraDriver->getSession(), statement);
        cass_future_wait_timed (future, 100000);

        auto rc = cass_future_error_code(future);
        std::vector<std::shared_ptr<FeatureDeviceHistory> > allFeatures;
        std::set<std::string> allFeatureNames;

        if (rc != CASS_OK) {
                print_error(future, queryStr, entityToModuleStateStats);
        } else {
                const CassResult* result = cass_future_get_result(future);

                CassIterator* rowIterator = cass_iterator_from_result(result);

                while (cass_iterator_next(rowIterator)) {

                        std::string featureNameFromDB =
                                CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "feature");

                        std::string featureType = CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "featureType");
                        if (!CollectionUtil::valueExistInSet(featureTypesRequested,
                                                             featureType)) {
                                continue;
                        }
                        std::shared_ptr<FeatureDeviceHistory> featureDeviceHistory =
                                std::make_shared<FeatureDeviceHistory>(std::make_unique<Feature>(featureType, featureNameFromDB, 0));
                        //TODO : persist and read the level too

                        allFeatureNames.insert(featureDeviceHistory->feature->getName());
                        allFeatures.push_back(featureDeviceHistory);
                        numberOfDeviceHistoriesFetchedForThisFeature++;

                }

                cass_iterator_free(rowIterator);
                cass_result_free(result);
        }

        cass_future_free(future);
        cass_statement_free(statement);
        MLOG(3)<<"distinct feature size :  "<<allFeatures.size()<<" , list of negative features "
                " is : "<< JsonArrayUtil::convertListToJson(
                CollectionUtil::convertSetToList<std::string>(allFeatureNames));

        return allFeatures;
}


std::vector<std::shared_ptr<FeatureDeviceHistory> > FeatureDeviceHistoryCassandraService::
readDistinctFeaturesExcluding(
        std::set<std::string> featuresToExclude,
        int maxNumberOfFeaturesToPick,
        int featureRecencyInSecond,
        std::set<std::string> featureTypesRequested) {
        int numberOfDeviceHistoriesFetchedForThisFeature = 0;
        std::unordered_map<std::string, std::shared_ptr<FeatureDeviceHistory> > allFeaturesPickedUp;

        MLOG(3)<< "reading features excluding "<<JsonArrayUtil::convertListToJson(featuresToExclude)<<
                " maxNumberOfFeaturesToPick : "<< maxNumberOfFeaturesToPick;

        std::string queryStr =
                "select feature, featuretype, deviceid, devicetype, timeofvisit from "
                " gicapods_mdl.featuredevicehistory where timeOfVisit >=  " +
                _toStr(DateTimeUtil::getNowInMilliSecond() - (featureRecencyInSecond * 1000)) +
                //we pick ten times more to make sure we have enough right type features
                //after filtering out the mismatched type features
                " limit " + _toStr(maxNumberOfFeaturesToPick * 100) + " ALLOW FILTERING";


        MLOG(3)<<"queryStr to read distinct features : "<<queryStr;
        const char* query = queryStr.c_str();
        cass_bool_t has_more_pages = cass_false;
        auto statement = cass_statement_new(query, 0);

        //set the paging to small number
        cass_statement_set_paging_size(statement, 10);
        std::vector<std::shared_ptr<FeatureDeviceHistory> > allFeatureDeviceHistories;
        std::set<std::string> allFeatureNames;

        do {
                auto future = cass_session_execute(cassandraDriver->getSession(), statement);
                cass_future_wait_timed (future, 100000);

                auto rc = cass_future_error_code(future);

                if (rc != CASS_OK) {

                        MLOG(3)<<"error in queryStr : "<<queryStr;
                        printErrorWithNoExit(future, queryStr, entityToModuleStateStats);
                        //when we don't have data for a range of time...
                        //cassandra throwes an error. this is acceptable.
                        //so we return empty data here.
                        return allFeatureDeviceHistories;
                } else {
                        MLOG(3)<<"Result is Ok : query : "<<queryStr;

                        const CassResult* result = cass_future_get_result(future);

                        CassIterator* rowIterator = cass_iterator_from_result(result);

                        while (cass_iterator_next(rowIterator)) {

                                std::string featureNameFromDB =
                                        CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "feature");
                                MLOG(3)<<"feature to consider is for picking : "<< featureNameFromDB;
                                std::string featureType =
                                        CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "featureType");

                                std::string deviceId =
                                        CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "deviceId");
                                std::string deviceType =
                                        CassandraDriverInterface::getStringValueFromRowByName (rowIterator, "deviceType");

                                long timeOfVisit =
                                        CassandraDriverInterface::getLongValueFromRowByName (rowIterator, "timeOfVisit");



                                if (!CollectionUtil::valueExistInSet(featureTypesRequested,
                                                                     featureType)) {
                                        MLOG(3)<<"feature to is ignored because bad type : "<< featureNameFromDB
                                               <<"featureType : "<< featureType
                                               <<" featureTypesRequested : " << JsonArrayUtil::convertListToJson(featureTypesRequested);

                                        continue;
                                }

                                if (!CollectionUtil::valueExistInSet(featuresToExclude,
                                                                     featureNameFromDB)) {
                                        try {

                                                auto device = std::make_shared<Device> (
                                                        deviceId,  deviceType
                                                        );
                                                auto devHistory = std::make_shared<DeviceHistory>(device);
                                                devHistory->timeOfVisit = timeOfVisit;

                                                auto comboKey = featureNameFromDB + featureType;
                                                auto featurePickedUpPair = allFeaturesPickedUp.find(comboKey);
                                                if (featurePickedUpPair == allFeaturesPickedUp.end()) {
                                                        auto featureDeviceHistory =
                                                                std::make_shared<FeatureDeviceHistory>(
                                                                        std::make_unique<Feature>(featureType, featureNameFromDB, 0));

                                                        featureDeviceHistory->devicehistories.push_back(devHistory);

                                                        allFeatureNames.insert(featureDeviceHistory->feature->getName());
                                                        if (allFeatureNames.size() > maxNumberOfFeaturesToPick) {
                                                                MLOG(3)<<"picked enough features breaking";
                                                                break;
                                                        }

                                                        allFeaturesPickedUp.insert(std::make_pair(comboKey, featureDeviceHistory));
                                                        numberOfDeviceHistoriesFetchedForThisFeature++;
                                                } else {
                                                        featurePickedUpPair->second->devicehistories.push_back(devHistory);
                                                }

                                        } catch(...) {
                                                LOG(ERROR) << " ignoring bad feature type ";
                                        }
                                } else {
                                        MLOG(1)<< " feature : "<< featureNameFromDB << " is ignored because it exists in the exclude set";
                                }
                        }
                        has_more_pages = cass_result_has_more_pages(result);

                        if (has_more_pages) {
                                cass_statement_set_paging_state(statement, result);
                        }

                        cass_iterator_free(rowIterator);
                        cass_result_free(result);
                }

        } while (has_more_pages);
        cass_statement_free(statement);

        MLOG(3)<<"distinct feature size :  "<<allFeaturesPickedUp.size()<<" , list of negative features "
                " for seedWebsites :  "<< JsonArrayUtil::convertListToJson(featuresToExclude)<<" is : "<<
                JsonArrayUtil::convertListToJson(
                CollectionUtil::convertSetToList<std::string>(allFeatureNames));

        for (auto pair : allFeaturesPickedUp) {
                allFeatureDeviceHistories.push_back(pair.second);
        }

        return allFeatureDeviceHistories;
}

/**
 * reads all devices that are the visitors of these features
 */
std::vector<std::shared_ptr<FeatureDeviceHistory> > FeatureDeviceHistoryCassandraService::readDeviceHistoryOfFeatures(
        int limit, TimeType time, std::set<std::string> featuresWanted, std::string featureType) {

        std::vector<std::shared_ptr<FeatureDeviceHistory> > allFeatureDeviceHistories;
        for (auto feature : featuresWanted) {
                std::shared_ptr<FeatureDeviceHistory> featureDeviceHistory = readDeviceHistoryOfFeature(feature,
                                                                                                        featureType,
                                                                                                        time,
                                                                                                        limit);
                MLOG(3)<<"featureDeviceHistory size :  "<<featureDeviceHistory->devicehistories.size()
                       <<" feature   "<<feature;
                if (!featureDeviceHistory->devicehistories.empty() &&
                    featureDeviceHistory->feature->isValid()) {
                        allFeatureDeviceHistories.push_back(featureDeviceHistory);
                } else {
                        LOG(ERROR)<<" featureDeviceHistory is invalid or empty ,feature : "<<feature;
                }


        }
        return allFeatureDeviceHistories;
}
