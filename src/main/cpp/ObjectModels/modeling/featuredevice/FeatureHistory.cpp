
#include "FeatureHistory.h"
#include "Device.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "ConverterUtil.h"
#include <string>
#include <memory>

FeatureHistory::FeatureHistory(std::shared_ptr<Feature> featureArg,
                               long featureSeenInMillis ) : Object(__FILE__) {
        NULL_CHECK(featureArg);
        this->feature = featureArg;
        this->featureSeenInMillis = featureSeenInMillis;
}
FeatureHistory::~FeatureHistory() {

}

std::shared_ptr<FeatureHistory> FeatureHistory::fromJsonValue(RapidJsonValueType value) {

        auto feature = Feature::fromJsonValue (value["feature"]);
        auto featureSeenInMillis = JsonUtil::GetLongSafely(value, "featureSeenInMillis");

        std::shared_ptr<FeatureHistory> featureHistory = std::make_shared<FeatureHistory>(feature, featureSeenInMillis);
        return featureHistory;
}

void FeatureHistory::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

        JsonUtil::addMemberToValue_FromPair(doc, "feature", feature, value);
        JsonUtil::addMemberToValue_FromPair (doc, "featureSeenInMillis", featureSeenInMillis, value);

}


std::string FeatureHistory::toJson() {

        auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString(value);

}
