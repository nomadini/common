#ifndef DeviceRecentVisitHistory_H
#define DeviceRecentVisitHistory_H





#include "JsonTypeDefs.h"


#include "cassandra.h"
#include "AtomicLong.h"
#include "DateTimeMacro.h"
#include <unordered_map>

class RecentVisitHistory;
class Device;
class Feature;
#include "Object.h"
class DeviceRecentVisitHistory : public Object {

private:

public:
std::shared_ptr<Device> device;
std::vector<std::shared_ptr<RecentVisitHistory> > allRecentHistories;
DeviceRecentVisitHistory(std::shared_ptr<Device> device);

std::string toJson();
static std::shared_ptr<DeviceRecentVisitHistory> fromJson(std::string json);
static std::shared_ptr<DeviceRecentVisitHistory> fromJsonValue(RapidJsonValueType value);
virtual ~DeviceRecentVisitHistory();

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

};


#endif
