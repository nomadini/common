#ifndef AdHistory_H
#define AdHistory_H


#include "AdEntry.h"
#include "JsonTypeDefs.h"

#include <set>
#include <unordered_map>
#include <memory>
#include <string>
#include <vector>
class Device;

#include "CassandraManagedType.h";
#include "Object.h"
class AdHistory;




class AdHistory : public CassandraManagedType, public Object {

private:

public:


std::shared_ptr<Device> device;
std::vector<std::shared_ptr<AdEntry> > listOfAdEntries;
std::unordered_map<int, std::vector<std::shared_ptr<AdEntry> > > targetGroupIdToAdEntriesMap;
AdHistory(std::shared_ptr<Device> device);
std::string toString();
static std::string getName();

void createMapOfTgIdToAdEntries(bool forceCreated = false);

std::vector<std::shared_ptr<AdEntry> > getAdEntriesByTgId(int targetGroupId);

static std::shared_ptr<AdHistory> fromJson(std::string jsonString);
static std::shared_ptr<AdHistory> fromJsonValue(RapidJsonValueType value);

std::vector<std::string> convertAdEntriesToJson();
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
std::string toJson();
virtual ~AdHistory();
};
/*

   std::shared_ptr<AdHistory>List AdHistoryList;
   for (std::shared_ptr<AdHistory>List::iterator it = AdHistoryList.begin();it != std::shared_ptr<AdHistory>List.end(); ++it)
   {

   }
 */

#endif
