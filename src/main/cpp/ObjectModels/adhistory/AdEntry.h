#ifndef AdEntry_H
#define AdEntry_H


#include "DateTimeMacro.h"
#include "JsonTypeDefs.h"

#include <string>
#include <memory>
#include "Object.h"
class AdEntry;


//this class represents an event that is interacted with user
//this can be impression, click, conversion, video_first_querter_seen, and others
class AdEntry : public Object {

private:

public:
//can be impression, conversion, click, video_first_quarter_click, video_first_querter_seen ....
std::string adInteractionType;
std::string transactionId; //everyAdEntry happens because of a transaction , this id referes to that
int targetGroupId;

int creativeId;
int publisherId;
int segmentId;
TimeType timeAdShownInMillis;

AdEntry(std::string transactionId, std::string adInteractionType);
std::string toString();

static std::shared_ptr<AdEntry> fromJson(std::string jsonString);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
std::string toJson();
static std::shared_ptr<AdEntry> fromJsonValue(RapidJsonValueType value);
virtual ~AdEntry();
};


#endif
