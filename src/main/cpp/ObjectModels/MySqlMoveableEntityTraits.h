/*
 * MySqlMoveableEntityTraits.h
 *
 *  Created on: Jan 15, 2017
 *      Author: mtaabodi
 */

#ifndef MySqlMoveableEntityTraits_H_
#define MySqlMoveableEntityTraits_H_

#include "GUtil.h"
#include "StringUtil.h"
#include "MetricDbRecord.h"

template <class T>
class MySqlMoveableEntityTraits
{

public:

std::string getMainTableName();
std::string getCoreFieldsToRead();
};

template <>
class MySqlMoveableEntityTraits<ImpressionEventDto> {
public:

std::string getMainTableName() {
        return "impression_compressed";
}

std::string getCoreFieldsToRead() {
        return "created_at,"
               "client_id,"
               "advertiser_id,"
               "campaign_id,"
               "targetgroup_id,"
               "creative_id,"
               "geosegmentlist_id,"
               "adSize,"
               "deviceCountry,"
               "deviceState,"
               "deviceZipCode,"
               "impressionType,"
               "deviceType,"
               "siteCategory,"
               "siteDomain,"
               "event_type,"
               "count,"
               "advertiserCostInCpm,"
               "bidPriceInCpm,"
               "exchangeCostInCpm,"
               "platformCostInCpm";
}
};

#endif
