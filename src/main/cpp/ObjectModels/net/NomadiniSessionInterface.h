#ifndef NomadiniSessionInterface_H
#define NomadiniSessionInterface_H

namespace gicapods { class ConfigService; }
#include <memory>
#include <unordered_map>
#include <string>
#include "AtomicLong.h"

class NomadiniSessionInterface : public std::enable_shared_from_this<NomadiniSessionInterface> {


public:

NomadiniSessionInterface();

virtual void  sendRequest(std::string url,
                          std::string methodType,
                          std::string requestBody,
                          int& responseStatusCode,
                          std::string& responseBody,
                          std::unordered_map<std::string, std::string> requestHeaders,
                          std::unordered_map<std::string, std::string>& responseHeaders) = 0;
virtual void initSession() = 0;

virtual std::vector<std::string> sendRequests(std::string url,
                                              std::string methodType,
                                              std::vector<std::string> requestBodies,
                                              std::shared_ptr<gicapods::AtomicLong> numberOfSuccessfulRequestsSent,
                                              int numberOfTries,
                                              int retryAfterNMillis) = 0;

virtual std::string getServerUrl() = 0;
virtual void close() = 0;
virtual ~NomadiniSessionInterface();

};

#endif
