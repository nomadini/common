#ifndef NomadiniHttpServerInterface_H
#define NomadiniHttpServerInterface_H

namespace gicapods { class ConfigService; }
#include <memory>
#include <unordered_map>
#include <string>
namespace nghttp2 {
namespace asio_http2 {
namespace server {
class request;
class response;
}
}
}
class NomadiniHttpServerInterface {

public:

NomadiniHttpServerInterface();

virtual int currentConnections() = 0;
virtual int currentThreads() = 0;
virtual int maxConcurrentConnections() = 0;
virtual int maxThreads() = 0;
virtual int queuedConnections() = 0;
virtual int refusedConnections() = 0;
virtual int totalConnections() = 0;
virtual void start() = 0;
virtual void stop() = 0;

virtual void registerHandler(std::string path,
                             std::function<void(const nghttp2::asio_http2::server::request &request,
                                                const nghttp2::asio_http2::server::response & response)> requestCallBack);
virtual ~NomadiniHttpServerInterface();

};

#endif
