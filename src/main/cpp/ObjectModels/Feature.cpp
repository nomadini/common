

#include "GUtil.h"
#include "Feature.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"

std::string Feature::UNSURE_TYPE = "UNSURE_TYPE";
std::string Feature::IAB_CAT = "IAB_CAT";
std::string Feature::PLACE_TAG = "PLACE_TAG";
std::string Feature::generalTopLevelDomain = "GTLD";
/*
   update this function in case you add a new Feature Type
 */
std::set<std::string> Feature::getAllFeatureTypes() {
								return {Feature::generalTopLevelDomain, Feature::MGRS100, Feature::PLACE_TAG, Feature::IAB_CAT};
}


std::string Feature::MGRS100 = "MGRS100";
std::string Feature::UNKNOWN_STATUS = "UNKNOWN";
std::string Feature::ACTIVE_STATUS = "ACTIVE";
std::string Feature::BLOCKED_STATUS = "BLOCKED";
std::string Feature::INACTIVE_STATUS = "INACTIVE";

Feature::Feature() : Object(__FILE__) {
								valid = false;
}


Feature::Feature(std::string type, std::string name, int level) : Object(__FILE__){
								valid = false;

								if (StringUtil::containsCaseInSensitive(name, ".")) {
																//having a .  means its GTLS
																//we are deriving the type for names that have .
																//only because of a bug. remove this if after a while
																type = Feature::generalTopLevelDomain;
								}

								if (type.empty()) {
																if (StringUtil::containsCaseInSensitive(name, ".")) {
																								//having a .  means its GTLS
																								//this is for backward compatibility
																								type = Feature::generalTopLevelDomain;
																} else if (StringUtil::containsCaseInSensitive(name, "iab")) {
																								type = Feature::IAB_CAT;
																} else {
																								type = Feature::MGRS100;
																}
																throwEx("type is empty for feature : " + name);

								}

								if (StringUtil::equalsIgnoreCase(type, Feature::generalTopLevelDomain) &&
												!StringUtil::containsCaseInSensitive(name, ".")) {
																throwEx("wrong type " + type + " for feature : " + name);
								}

								if (StringUtil::equalsIgnoreCase(type, Feature::IAB_CAT) &&
												!StringUtil::containsCaseInSensitive(name, "iab")) {
																throwEx("wrong type " + type + " for feature : " + name);
								}

								if (StringUtil::equalsIgnoreCase(type, Feature::MGRS100) &&
												StringUtil::containsCaseInSensitive(name, ".")) {
																throwEx("wrong type " + type + " for feature : " + name);
								}

								if (!StringUtil::equalsIgnoreCase(type, Feature::generalTopLevelDomain) &&
												!StringUtil::equalsIgnoreCase(type, Feature::MGRS100) &&
												!StringUtil::equalsIgnoreCase(type, Feature::IAB_CAT) &&
												!StringUtil::equalsIgnoreCase(type, Feature::PLACE_TAG)) {
																//	LOG(ERROR) << "feature : " << name << " has wrong type :" << type <<
																//								" type length is "<< _toStr(type.size());
																throwEx("feature : " + name + " has wrong type :" + type +
																								" type length is "+ _toStr(type.size()));
								}
								this->type = type;
								this->name = name;
								this->level = level;
								validate();
}

Feature::~Feature() {

}

bool Feature::equals(Feature* feature) {
								return StringUtil::equalsIgnoreCase(
																this->name,feature->name) &&
															feature->level == level;
}
void Feature::setNameAndLevel(std::string name, int level) {
								this->name = name;
								this->level = level;
								validate();
}

std::string Feature::getStatus() {
								return this->status;
}

std::string Feature::getName() {
								validate();
								return this->name;
}

std::string Feature::getType() {
								validate();
								return this->type;
}

int Feature::getLevel() {
								validate();
								return this->level;
}

void Feature::validate() {
								assertAndThrow(isValid());
}

bool Feature::isValid() {
								valid = true;
								if (level < 0) {
																valid = false;
																LOG(ERROR) << "level is less than 0 for feature : "<< this->name;
								}

								if (StringUtil::equalsIgnoreCase(name, "localhost")) {
																LOG(ERROR) << "bad name for feature : "<< this->name;
																valid = false;
								}
								if (name.empty()) {
																LOG(ERROR) << "bad name for feature : "<< this->name;
																valid = false;
								}

								return valid;
}

std::string Feature::toString() {
								return this->toJson();
}

void Feature::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
								JsonUtil::addMemberToValue_FromPair (doc, "level", level, value);
								JsonUtil::addMemberToValue_FromPair (doc, "name", name, value);
								JsonUtil::addMemberToValue_FromPair (doc, "type", type, value);
								JsonUtil::addMemberToValue_FromPair (doc, "status", status, value);

}

std::shared_ptr<Feature> Feature::fromJsonValue(RapidJsonValueType value) {
								std::shared_ptr<Feature> feature = std::make_shared<Feature>(
																JsonUtil::GetStringSafely(value, "type"),
																JsonUtil::GetStringSafely(value, "name"),
																JsonUtil::GetIntSafely(value, "level"));

								feature->status = JsonUtil::GetStringSafely(value, "status");
								return feature;
}

std::shared_ptr<Feature> Feature::fromJson(std::string jsonString) {
								auto document = parseJsonSafely(jsonString);
								return fromJsonValue(*document);
}

std::string Feature::toJson() {
								auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString (value);
}
