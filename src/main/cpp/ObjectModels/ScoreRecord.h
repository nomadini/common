#ifndef ScoreRecord_h
#define ScoreRecord_h


#include <atomic>
#include <memory>
#include <string>
#include <unordered_map>
#include "JsonTypeDefs.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "JsonArrayUtil.h"
#include "DateTimeUtil.h"
#include "Object.h"
class ScoreRecord : public Object {
public:
long timeScoredInUtc;
float scoreRecorded;
float scoreBase;
ScoreRecord() : Object(__FILE__) {
        this->timeScoredInUtc = 0;
        this->scoreRecorded= 0;
        this->scoreBase= 0;

}
virtual ~ScoreRecord() {
}

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "scoreRecorded", scoreRecorded, value);
        JsonUtil::addMemberToValue_FromPair (doc, "scoreBase", scoreBase, value);
        JsonUtil::addMemberToValue_FromPair(doc, "timeScoredInUtc", timeScoredInUtc, value);
}

static std::shared_ptr<ScoreRecord> fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<ScoreRecord> scoreRecord = std::make_shared<ScoreRecord>();

        scoreRecord->scoreRecorded = JsonUtil::GetDoubleSafely(value, "scoreRecorded");
        scoreRecord->scoreBase = JsonUtil::GetDoubleSafely(value, "scoreBase");
        scoreRecord->timeScoredInUtc = JsonUtil::GetLongSafely(value, "timeScoredInUtc");

        return scoreRecord;
}

};

#endif
