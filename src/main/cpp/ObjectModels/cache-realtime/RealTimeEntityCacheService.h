/*
 * RealTimeEntityCacheService.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mtaabodi
 */

#ifndef RealTimeEntityCacheService_H_
#define RealTimeEntityCacheService_H_

class CassandraDriverInterface;

class HttpUtilService;
class EntityToModuleStateStats;


#include "Object.h"
#include "SizeAndTimeBasedLRUCache.h"
#include "AeroCacheTraits.h"
#include <memory>
template <class T>
class CassandraService;

template <class T>
class AeroCacheService;

/*
   wraps a aerospike and a cassandra service for a type and
   uses the aerospike as a caching layer to deliver the requests as
   fast as possible
 */
template<class T>
class RealTimeEntityCacheService : public Object {

private:

public:

CassandraService<T>* realTimeEntityCassandraService;
AeroCacheService<T>* realTimeEntityAerospikeCacheService;
EntityToModuleStateStats* entityToModuleStateStats;
std::shared_ptr<gicapods::SizeAndTimeBasedLRUCache<std::string, std::shared_ptr<T> > > inMemoryCache;
RealTimeEntityCacheService<T>(int inMemoryCacheSize,
                              EntityToModuleStateStats* entityToModuleStateStats);
typedef AeroCacheTraits<T> aeroTraits;
std::shared_ptr<T> readDataOptional(std::shared_ptr<T> keyObject);
std::shared_ptr<T> readFromMainSource(std::shared_ptr<T> keyObject);

void writeKeyValueInCache(std::shared_ptr<T> keyObject,std::shared_ptr<T> finalValue);

std::shared_ptr<T> readAndPopulateFromRemoteCache(std::shared_ptr<T> keyObject);

bool isPartOfCacheSample(std::shared_ptr<T> keyObject);

virtual ~RealTimeEntityCacheService<T>();

};

#endif
