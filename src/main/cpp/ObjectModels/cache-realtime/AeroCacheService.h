/*
 * AeroCacheService.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mtaabodi
 */

#ifndef AeroCacheService_H_
#define AeroCacheService_H_

class CassandraDriverInterface;
#include "Object.h"
class EntityToModuleStateStats;

class AsyncThreadPoolService;
class AerospikeDriverInterface;
#include "CassandraService.h"
#include "AeroCacheTraits.h"

template<class T>
class AeroCacheService : public Object {

private:
public:

EntityToModuleStateStats* entityToModuleStateStats;
AerospikeDriverInterface* aeroSpikeDriver;

typedef AeroCacheTraits<T> aeroTraits;
std::shared_ptr<T> readDataOptional(std::shared_ptr<T> keyObject, int& cachedResult);
void putDataInCache(std::shared_ptr<T> value);
void putDataInCache(std::shared_ptr<T> keyObject, std::shared_ptr<T> valueObject);

std::string getEntityName();

AeroCacheService<T>();

virtual ~AeroCacheService<T>();

};



#endif
