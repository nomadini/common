//
// Created by Mahmoud Taabodi on 3/6/16.
//

#include "CallStats.h"
#include "EntityToModuleStateStats.h"
#include "Object.h"
#include "GUtil.h"
CallStats::CallStats()
        : Object(__FILE__) {

        totalCalls = std::make_shared<gicapods::AtomicLong>(1); // just for avoiding division/0
        totalMicroseconds = std::make_shared<gicapods::AtomicLong>();
}

CallStats::~CallStats() {
}
