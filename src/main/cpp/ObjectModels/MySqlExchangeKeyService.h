//
// Created by User on 6/12/2016.
//

#ifndef EXCHANGESIMULATOR_MYSQLMETRICSERVICE_H
#define EXCHANGESIMULATOR_MYSQLMETRICSERVICE_H

#include "AtomicDouble.h"
class DateTimeUtil;


#include <tbb/concurrent_hash_map.h>
class EntityToModuleStateStats;
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include <set>
#include "MetricDbRecord.h"
#include "Object.h"
class MySqlExchangeKeyService;



/**
 * this class has writes metrics to mysql table called module_states;
 */
class MySqlExchangeKeyService : public Object {

public:

MySqlDriver* driver;

MySqlExchangeKeyService(MySqlDriver* driver);

std::string convertRecordToValueString(std::shared_ptr<MetricDbRecord> metricDbRecord);

void insertExchangeKey(std::vector<std::pair<std::string, std::string> > allPairs);

void readAllSyncedExchangeKyes(
        std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<std::set<std::string> > > > exchangeToSyncedIds,
        int limit);

void deleteAll();

private:

};

#endif //EXCHANGESIMULATOR_MYSQLMETRICSERVICE_H
