#include "GUtil.h"


#include "Deal.h"
#include "MySqlDealService.h"
#include "MySqlDriver.h"
#include "DateTimeUtil.h"

MySqlDealService::MySqlDealService(MySqlDriver* driver)
        : MySqlService(driver, nullptr),Object(__FILE__) {
        this->driver = driver;
}

std::vector<std::shared_ptr<Deal>> MySqlDealService::readAllEntities() {
        return this->readAll();
}
std::string MySqlDealService::getSelectAllQueryStatement() {
        static std::string selectAllQueryStatement = " SELECT "
                                                     " deal.`id`, "
                                                     " deal.`exchange_id`, "
                                                     " deal.`tp_deal_id`, "
                                                     " deal.`created_at`"
                                                     " FROM `deal`";
        return selectAllQueryStatement;
}

std::string MySqlDealService::getInsertObjectSqlStatement(std::shared_ptr<Deal> deal) {
        std::string queryStr =
                " INSERT INTO `deal`"
                " ("
                " `exchange_id`,"
                " `tp_deal_id`,"
                " `created_at`)"
                " VALUES"
                " ( "
                " __exchange_id__,"
                " '__tp_deal_id__',"
                " '__created_at__'"
                ");";

        queryStr = StringUtil::replaceString (queryStr, "__exchange_id__",
                                              StringUtil::toStr(deal->exchangeId));
        queryStr = StringUtil::replaceString (queryStr, "__tp_deal_id__",
                                              deal->tpDealId);

        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();
        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        return queryStr;
}

std::shared_ptr<Deal> MySqlDealService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<Deal> obj = std::make_shared<Deal> ();
        obj->id = res->getInt ("id");
        obj->exchangeId = res->getInt ("exchange_id");
        obj->tpDealId = MySqlDriver::getString( res, "tp_deal_id");
        obj->createdAt = MySqlDriver::parseDateTime(res, "created_at");
        MLOG(3) << "reading deal from mysql " << obj->toJson ();
        return obj;
}

std::string MySqlDealService::getReadByIdSqlStatement(int id) {
        std::string query =
                " SELECT "
                " deal.`id`, "
                " deal.`exchange_id`, "
                " deal.`tp_deal_id`, "
                " deal.`created_at`"
                " FROM `deal`"
                " WHERE id = '__id__'";


        query = StringUtil::replaceString (query, "__id__", StringUtil::toStr(id));
        return query;
}

void MySqlDealService::deleteAll() {
        driver->deleteAll("deal");
}

MySqlDealService::~MySqlDealService() {
}
