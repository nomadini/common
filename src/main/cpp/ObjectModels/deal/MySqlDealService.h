//
// Created by Mahmoud Taabodi on 2/15/16.
//

#ifndef MySqlDealService_h
#define MySqlDealService_h



#include "Object.h"
#include "Deal.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "DataProvider.h"
#include "MySqlService.h"

class MySqlDealService;


class MySqlDealService : public DataProvider<Deal>, public MySqlService<int, Deal>, public Object {

public:

MySqlDriver* driver;

MySqlDealService(MySqlDriver* driver);

virtual ~MySqlDealService();

std::string getSelectAllQueryStatement();
std::shared_ptr<Deal> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);
std::string getInsertObjectSqlStatement(std::shared_ptr<Deal> campaign);
std::string getReadByIdSqlStatement(int id);

//delete this later after you have merged MySqlService and DataProvider
std::vector<std::shared_ptr<Deal>> readAllEntities();
virtual void deleteAll();
};

#endif
