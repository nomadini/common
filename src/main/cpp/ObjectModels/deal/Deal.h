/*
 * Deal.h
 *
 *  Created on: Aug 27, 2015
 *      Author: mtaabodi
 */

#ifndef Deal_H_
#define Deal_H_


#include <memory>
#include <string>
#include <vector>
#include <unordered_map>

#include "JsonTypeDefs.h"


#include "Poco/DateTime.h"
#include "Object.h"

class Deal;


class Deal : public Object {

public:


int id;
int exchangeId;
std::string tpDealId;
std::string description;
Poco::DateTime createdAt;

Deal();

std::string toString();

std::string toJson();

static std::string getEntityName();
static std::shared_ptr<Deal> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
static std::shared_ptr<Deal> fromJson(std::string jsonString);
virtual ~Deal();

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_EVENTLOG_H_ */
