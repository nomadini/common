#include "GUtil.h"


#include "ClientDeal.h"
#include "MySqlClientDealService.h"
#include "MySqlDriver.h"
#include "DateTimeUtil.h"

MySqlClientDealService::MySqlClientDealService(MySqlDriver* driver)
        : MySqlService(driver, nullptr), Object(__FILE__) {
        this->driver = driver;
}

std::vector<std::shared_ptr<ClientDeal>> MySqlClientDealService::readAllEntities() {
        return this->readAll();
}
std::string MySqlClientDealService::getSelectAllQueryStatement() {
        static std::string selectAllQueryStatement = " SELECT "
                                                     " client_deal_map.`id`, "
                                                     " client_deal_map.`deal_id`, "
                                                     " client_deal_map.`client_id`, "
                                                     " client_deal_map.`status`, "
                                                     " client_deal_map.`descr`, "
                                                     " client_deal_map.`created_at`,"
                                                     " client_deal_map.`updated_at`"
                                                     " FROM `client_deal_map`";
        return selectAllQueryStatement;
}

std::string MySqlClientDealService::getInsertObjectSqlStatement(std::shared_ptr<ClientDeal> clientDeal) {
        std::string queryStr =
                " INSERT INTO `clientDeal`"
                " ("
                " `deal_id`, "
                " `client_id`, "
                " `status`, "
                " `descr`, "
                " `created_at`,"
                " `updated_at`"
                " )"
                " VALUES"
                " ( "
                " __deal_id__, "
                " __client_id__, "
                " '__status__', "
                " '__descr__', "
                " '__created_at__',"
                " '__updated_at__'"
                ");";

        queryStr = StringUtil::replaceString (queryStr, "__deal_id__",
                                              StringUtil::toStr(clientDeal->dealId));
        queryStr = StringUtil::replaceString (queryStr, "__client_id__",
                                              StringUtil::toStr(clientDeal->clientId));
        queryStr = StringUtil::replaceString (queryStr, "__status__",
                                              clientDeal->status);
        queryStr = StringUtil::replaceString (queryStr, "__descr__",
                                              clientDeal->description);
        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();
        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);
        return queryStr;
}

std::shared_ptr<ClientDeal> MySqlClientDealService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<ClientDeal> obj = std::make_shared<ClientDeal> ();
        obj->id = res->getInt ("id");
        obj->dealId = res->getInt ("deal_id");
        obj->clientId = res->getInt ("client_id");
        obj->status = MySqlDriver::getString( res, "status");
        obj->description = MySqlDriver::getString( res, "descr");
        obj->createdAt = MySqlDriver::parseDateTime(res, "created_at");
        obj->lastModified = MySqlDriver::parseDateTime(res, "updated_at");
        MLOG(3) << "reading clientDeal from mysql " << obj->toJson ();
        return obj;
}

std::string MySqlClientDealService::getReadByIdSqlStatement(int id) {
        std::string query =
                " SELECT "
                " `id`, "
                " `deal_id`, "
                " `client_id`, "
                " `status`, "
                " `descr`, "
                " `created_at`,"
                " `updated_at`"
                " FROM `client_deal_map`"
                " WHERE id = '__id__'";


        query = StringUtil::replaceString (query, "__id__", StringUtil::toStr(id));
        return query;
}

void MySqlClientDealService::deleteAll() {
        driver->deleteAll("clientDeal");
}

MySqlClientDealService::~MySqlClientDealService() {
}
