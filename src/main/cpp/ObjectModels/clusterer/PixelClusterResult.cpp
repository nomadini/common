#include "PixelClusterResult.h"
#include "ModelUtil.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
#include "StringUtil.h"

PixelClusterResult::PixelClusterResult() : Object(__FILE__) {

        pixelId = 0;
        id = 0;
        score = 0;
        hiteDate = DateTimeUtil::getCurrentDateWithHourPercision();
}

PixelClusterResult::~PixelClusterResult() {
}

std::shared_ptr<PixelClusterResult> PixelClusterResult::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<PixelClusterResult> obj = std::make_shared<PixelClusterResult>();
        obj->id = JsonUtil::GetIntSafely(value, "id");
        obj->pixelId = JsonUtil::GetIntSafely(value, "pixelId");
        obj->cluster = JsonUtil::GetStringSafely(value, "cluster");
        obj->clusterType = JsonUtil::GetStringSafely(value, "clusterType");
        obj->scoreType = JsonUtil::GetStringSafely(value, "scoreType");
        obj->score = JsonUtil::GetDoubleSafely(value, "score");
        obj->hiteDate  = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"hiteDate"));
        obj->createdAt  = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"createdAt"));
        obj->updatedAt  = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"updatedAt"));

        return obj;
}

void PixelClusterResult::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "pixelId", pixelId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "clusterType", clusterType, value);
        JsonUtil::addMemberToValue_FromPair (doc, "cluster", cluster, value);
        JsonUtil::addMemberToValue_FromPair (doc, "scoreType", scoreType, value);
        JsonUtil::addMemberToValue_FromPair (doc, "score", score, value);
        JsonUtil::addMemberToValue_FromPair (doc, "hiteDate", DateTimeUtil::dateTimeToStr(hiteDate), value);
        JsonUtil::addMemberToValue_FromPair (doc, "createdAt", DateTimeUtil::dateTimeToStr(createdAt), value);
        JsonUtil::addMemberToValue_FromPair (doc, "updatedAt", DateTimeUtil::dateTimeToStr(updatedAt), value);
}

std::string PixelClusterResult::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);

}
