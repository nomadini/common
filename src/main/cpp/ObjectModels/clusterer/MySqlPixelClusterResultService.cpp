#include "GUtil.h"


#include "PixelClusterResult.h"
#include "MySqlPixelClusterResultService.h"
#include "MySqlDriver.h"
#include "DateTimeUtil.h"
#include "AdvertiserCostModel.h"
MySqlPixelClusterResultService::MySqlPixelClusterResultService(MySqlDriver* driver) :
        MySqlService(driver, nullptr),  Object(__FILE__) {
        this->driver = driver;
        allFields =
                " `pixel_id`, "
                " `cluster`, "
                " `cluster_type`, "
                " `score`, "
                " `score_type`, "
                " `created_at`,"
                " `updated_at` ";
}

std::vector<std::shared_ptr<PixelClusterResult> > MySqlPixelClusterResultService::readAllEntities() {
        return this->readAll();
}

std::string MySqlPixelClusterResultService::getSelectAllQueryStatement() {
        static std::string selectAllQueryStatement = " SELECT "
                                                     " `id`, "
                                                     + allFields +
                                                     " FROM `pixel_cluster_result`";
        return selectAllQueryStatement;
}

std::string MySqlPixelClusterResultService::getInsertObjectSqlStatement(
        std::shared_ptr<PixelClusterResult> result) {
        std::string queryStr =
                " INSERT INTO `pixel_cluster_result`"
                " ("
                + allFields +
                ")"
                " VALUES"
                " ( "
                " __pixelId__,"
                " '__cluster__',"
                " '__clusterType__',"
                " __score__,"
                " '__scoreType__',"
                " '__created_at__',"
                " '__updated_at__');";

        queryStr = StringUtil::replaceString (queryStr, "__pixelId__",
                                              StringUtil::toStr (result->pixelId));
        queryStr = StringUtil::replaceString (queryStr, "__score__",
                                              StringUtil::toStr (result->score));

        queryStr = StringUtil::replaceString (queryStr, "__cluster__",
                                              result->cluster);

        queryStr = StringUtil::replaceString (queryStr, "__clusterType__",
                                              result->clusterType);

        queryStr = StringUtil::replaceString (queryStr, "__scoreType__",
                                              result->scoreType);

        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();
        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);
        return queryStr;
}

std::shared_ptr<PixelClusterResult> MySqlPixelClusterResultService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<PixelClusterResult> obj = std::make_shared<PixelClusterResult> ();

        obj->id = res->getInt ("id");
        obj->pixelId = res->getInt ("pixel_id");
        obj->cluster = res->getString ("cluster");
        obj->clusterType = res->getString ("cluster_type");
        obj->scoreType = res->getString ("score_type");
        obj->score = res->getDouble ("score");
        obj->createdAt = DateTimeUtil::parseDateTime(res->getString ("created_at"));
        obj->updatedAt = DateTimeUtil::parseDateTime(res->getString ("updated_at"));

        MLOG(3) << "reading pixel_cluster_result from mysql " << obj->toJson ();
        return obj;
}

std::string MySqlPixelClusterResultService::getReadByIdSqlStatement(int id) {
        std::string query = "SELECT "
                            " `id`,"
                            + allFields +
                            " FROM pixel_cluster_result where id = '__id__'";


        query = StringUtil::replaceString (query, "__id__", StringUtil::toStr(id));
        return query;
}

void MySqlPixelClusterResultService::deleteAll() {
        driver->deleteAll("pixel_cluster_result");
}

MySqlPixelClusterResultService::~MySqlPixelClusterResultService() {
}
