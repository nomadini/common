//
// Created by Mahmoud Taabodi on 2/15/16.
//

#ifndef MySqlPixelClusterResultService_h
#define MySqlPixelClusterResultService_h


#include "Object.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "PixelClusterResult.h"
#include "DataProvider.h"
#include "MySqlService.h"

class MySqlPixelClusterResultService;


class MySqlPixelClusterResultService : public DataProvider<PixelClusterResult>, public MySqlService<int, PixelClusterResult>, public Object {

public:

MySqlDriver* driver;

MySqlPixelClusterResultService(MySqlDriver* driver);

virtual ~MySqlPixelClusterResultService();

std::string getSelectAllQueryStatement();
std::string allFields;
std::shared_ptr<PixelClusterResult> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);
std::string getInsertObjectSqlStatement(std::shared_ptr<PixelClusterResult> campaign);
std::string getReadByIdSqlStatement(int id);

//delete this later after you have merged MySqlService and DataProvider
std::vector<std::shared_ptr<PixelClusterResult> > readAllEntities();
virtual void deleteAll();
};

#endif
