//
// Created by Mahmoud Taabodi on 2/14/16.
//

#ifndef COMMON_CREATIVECACHESERVICE_H
#define COMMON_CREATIVECACHESERVICE_H
#include "Object.h"
#include <string>
#include <memory>
#include <unordered_map>

#include "CreativeTypeDefs.h"
class Creative;
class MySqlDriver;
class MySqlCreativeService;
class HttpUtilService;
#include "CacheService.h"
#include "EntityProviderService.h"
class EntityToModuleStateStats;

class CreativeCacheService;
class CreativeCacheService;



class CreativeCacheService : public CacheService<Creative>, public Object {

private:

public:

std::shared_ptr<std::vector<std::shared_ptr<Creative> > > allCreatives;

MySqlCreativeService* mySqlCreativeService;

EntityToModuleStateStats* entityToModuleStateStats;
CreativeCacheService(MySqlCreativeService* mySqlCreativeService,
                     HttpUtilService* httpUtilService,
                     std::string dataMasterUrl,
                     EntityToModuleStateStats* entityToModuleStateStats,std::string appName);

std::vector<std::string> getListOfSizes(std::vector<std::shared_ptr<Creative> > list);

std::shared_ptr<std::vector<std::shared_ptr<Creative> > > getAllCreatives();

void clearOtherCaches();

void populateOtherMapsAndLists(std::vector<std::shared_ptr<Creative> > allCreatives);

};

#endif //COMMON_CREATIVECACHESERVICE_H
