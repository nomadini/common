//
// Created by Mahmoud Taabodi on 2/14/16.
//


#include "MySqlCreativeService.h"
#include <boost/foreach.hpp>
#include "HttpUtilService.h"
#include "JsonUtil.h"
#include <vector>
#include "JsonArrayUtil.h"
#include "CreativeCacheServiceMock.h"
#include "Creative.h"
CreativeCacheServiceMock::CreativeCacheServiceMock(
        MySqlCreativeService* mySqlCreativeService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,std::string appName) :
        CreativeCacheService(mySqlCreativeService,
                             httpUtilService,
                             dataMasterUrl,
                             entityToModuleStateStats, appName) {
}
