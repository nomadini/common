#include "CreativeTestHelper.h"
#include "RandomUtil.h"
#include "StringUtil.h"
#include "Creative.h"
std::shared_ptr<Creative> CreativeTestHelper::createSampleCreative() {
        std::shared_ptr<Creative> crt = std::make_shared<Creative> ();
        crt->setId (RandomUtil::sudoRandomNumber (1000));
        crt->setSize ("300x250");
        std::string uniqueName = StringUtil::toStr (RandomUtil::sudoRandomNumber (100000));

        crt->setName (StringUtil::toStr ("creative__").append (uniqueName));
        crt->setStatus ("active");
        crt->setPreviewUrl (StringUtil::toStr ("previewUrl__").append (uniqueName));

        auto apis = std::make_shared<std::vector<std::string> >();
        apis->push_back ("MRAID-1");
        apis->push_back ("MRAID-1");
        apis->push_back ("ORMMA");

        // crt->setApis (apis);
        crt->setLandingPageUrl (StringUtil::toStr ("creative01name__").append (uniqueName));

        crt->attributes->insert(std::make_pair("Audio_Ad_Auto_Play", true));
        crt->setAdvertiserId (RandomUtil::sudoRandomNumber (1000));


        std::shared_ptr<std::vector<std::string> > apisOfCreatives = std::make_shared<std::vector<std::string> >();
        apisOfCreatives->push_back("MRAID-1");

        // crt->setApis(apisOfCreatives);
        crt->setAdTag (
                "<iframe id='a62ae7d3' name='a62ae7d3'\
	 src='http://delivery_server_domain/w/1.0/afr?auid=\
	8635&cb=INSERT_RANDOM_NUMBER_HERE&rd=5&rm=3\"\
	 frameborder='0' scrolling='no' width='728'\
	 height='90'>\
	\
	<a href='http://delivery_server_domain/w/1.0/rc?cs=\
	acd22faf&cb=INSERT_RANDOM_NUMBER_HERE\"\
	 target='_blank'>\
	<img src='http://delivery_server_domain/w/1.0\
	/ai?uid=8635&cs=acd22faf&cb=INSERT_RANDOM_NUMBER_HERE\"\
	 border='0' alt='' /></a></iframe> ");
        crt->setAdType ("IFRAME");
        return crt;
}
