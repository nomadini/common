//
// Created by Mahmoud Taabodi on 5
//
//
//
//
//
// /8/16.
//

#include "MockHttpRequest.h"
#include <sstream>

//std::istream& MockHttpRequest::stream() const { }
///// Returns the input stream for reading
///// the request body.
/////
///// The stream must be valid until the HTTPServerRequest
///// object is destroyed.
//
//bool MockHttpRequest::expectContinue() const {  }
///// Returns true if the client expects a
///// 100 Continue response.
//
//const Poco::Net::SocketAddress& MockHttpRequest::clientAddress() const {  }
///// Returns the client's address.
//
//const Poco::Net::SocketAddress& MockHttpRequest::serverAddress() const {  }
///// Returns the server's address.
//
//const Poco::Net::HTTPServerParams& MockHttpRequest::serverParams() const {  }
///// Returns a reference to the server parameters.
//
//Poco::Net::HTTPServerResponse& MockHttpRequest::response() const {  }
///// Returns a reference to the associated response.

MockHttpRequest::MockHttpRequest(std::string requestBody) {
    requestStream = std::make_shared<std::istringstream>(requestBody);
}
/// Creates the HTTPServerRequest

MockHttpRequest::~MockHttpRequest() {}

inline std::istream& MockHttpRequest::stream()
{
    return *requestStream;
}


inline const Poco::Net::SocketAddress& MockHttpRequest::clientAddress() const
{
    return _clientAddress;
}


inline const Poco::Net::SocketAddress& MockHttpRequest::serverAddress() const
{
//    return _serverAddress;
}


inline const Poco::Net::HTTPServerParams& MockHttpRequest::serverParams() const
{
//    return *_pParams;
}


inline Poco::Net::HTTPServerResponse& MockHttpRequest::response() const
{
//    return _response;
}

bool MockHttpRequest::expectContinue() const
{
    return 1;
}