/*
 * Entity.h
 *
 *  Created on: Aug 24, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_ENTITY_H_
#define GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_ENTITY_H_




#include <map>
#include <string>
#include "Object.h"
class Entity : public Object {
public:
enum class Type {
								CLIENT = 1, ADVERTISER = 2, CAMPAIGN = 3, TARGET_GROUP = 4, CREATIVE = 5,

};
static std::map<Entity::Type, std::string> typeToEntity;

static void initEntityMaps();

static std::string getValueOfType(Entity::Type type);

};
#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_ENTITY_H_ */
