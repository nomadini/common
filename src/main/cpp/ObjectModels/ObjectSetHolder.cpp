#include "ObjectSetHolder.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"

#include "GUtil.h"
#include <string>
#include <memory>
template <class T>
ObjectSetHolder<T>::ObjectSetHolder() : Object(__FILE__) {
        values = std::make_shared<std::set<std::shared_ptr<T> > > ();
}

template <class T>
ObjectSetHolder<T>::~ObjectSetHolder() {

}
