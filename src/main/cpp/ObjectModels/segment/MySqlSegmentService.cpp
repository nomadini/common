#include "GUtil.h"
#include "Segment.h"
#include "MySqlSegmentService.h"
#include "MySqlDriver.h"
#include "StringUtil.h"
#include "DateTimeUtil.h"
#include <boost/foreach.hpp>
#include "ConcurrentQueueFolly.h"
#include "EntityToModuleStateStats.h"
#include "ConcurrentHashMap.h"
#include "ConfigService.h"
#include "MySqlDriver.h"
#include "MySqlModelService.h"

std::string MySqlSegmentService::coreSelectSql =
        "SELECT `segment`.`ID`,"
        "`segment`.`name`,"
        "`segment`.`advertiser_id`,"
        "`segment`.`model_id`,"
        "`segment`.`created_at`,"
        "`segment`.`type`";


MySqlSegmentService::MySqlSegmentService(MySqlDriver* driver,
                                         EntityToModuleStateStats* entityToModuleStateStatsArg) : Object(__FILE__) {
        this->entityToModuleStateStats = entityToModuleStateStatsArg;
        this->driver = driver;
        segmentCreatedQueueManager = std::make_shared<ConcurrentQueueFolly<Segment> >();
        segmentCreatedMapWrapper = std::make_shared<gicapods::ConcurrentHashMap<std::string, Segment> >(this->entityToModuleStateStats);
}


void MySqlSegmentService::insertIfNotExisting(std::shared_ptr<Segment> segment) {
        if (!segmentExists(segment->getUniqueName(),
                           segment->advertiserId,
                           segment->modelId)) {
                insert(segment);
        } else {
                entityToModuleStateStats->addStateModuleForEntity("SEGMENT_ALREADY_EXISTS_IN_MYSQL",
                                                                  "MySqlSegmentService",
                                                                  segment->getUniqueName());
        }
}

std::shared_ptr<Segment> MySqlSegmentService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<Segment> segment = std::make_shared<Segment>(DateTimeUtil::getNowInMilliSecond());
        int id = res->getInt("ID");
        std::string segmentName = MySqlDriver::getString( res, "NAME");
        int advertiserId = res->getInt("ADVERTISER_ID");
        int modelId = res->getInt("MODEL_ID");
        std::string dateCreated = MySqlDriver::getString( res, "created_at");
        std::string type = MySqlDriver::getString( res, "TYPE");

        MLOG(3) << "values loaded for segment id :  " << id << ","
                " segmentName :  " << segmentName << ", "
                " advertiserId :  " << advertiserId << ","
                " modelId :  " << modelId << ","
                " dateCreated :  " << dateCreated << ","
                " type :  " << type;

        segment->id = id;
        segment->setUniqueName(segmentName);
        segment->advertiserId = advertiserId;
        segment->modelId = modelId;

        segment->setType(type);

        return segment;
}

std::vector<std::shared_ptr<Segment> > MySqlSegmentService::readAllEntities() {
        return this->readAll();
}

bool MySqlSegmentService::segmentExists(std::string uniqueSegmentName,
                                        int advertiserId,
                                        int modelId) {
        return readSegment(uniqueSegmentName, advertiserId, modelId) != nullptr;
}
//a segment has a unique name
std::shared_ptr<Segment> MySqlSegmentService::readSegment(
        std::string uniqueSegmentName,
        int advertiserId,
        int modelId) {
        std::string query = coreSelectSql +
                            " FROM `segment` where "
                            " name = '__NAME__' and "
                            " advertiser_id = __ADVERTISER_ID__ and "
                            " model_id = __MODEL_ID__ ";

        query = StringUtil::replaceString(query, "__NAME__", uniqueSegmentName);
        query = StringUtil::replaceString(query, "__ADVERTISER_ID__", _toStr(advertiserId));
        query = StringUtil::replaceString(query, "__MODEL_ID__", _toStr(modelId));
        MLOG(3) << "query : " << query;
        auto res = driver->executeQuery(query);
        while (res->next()) {
                auto segment = mapResultSetToObject(res);
                return segment;
        }
        return nullptr;
}

void MySqlSegmentService::update(std::shared_ptr<Segment> segment) {
        std::string query(
                " UPDATE segment "
                " SET MODEL_ID = __MODEL_ID__ ,"
                "     ADVERTISER_ID = __ADVERTISER_ID__ "
                "     WHERE NAME = '__NAME__';");

        MLOG(3) << "updating segment " << segment->toJson();
        query = StringUtil::replaceString(query, "__MODEL_ID__", StringUtil::toStr(segment->modelId));
        query = StringUtil::replaceString(query, "__ADVERTISER_ID__", StringUtil::toStr(segment->advertiserId));
        query = StringUtil::replaceString(query, "__NAME__", segment->getUniqueName());

        driver->executedUpdateStatement (query);
}

std::vector <std::shared_ptr<Segment> > MySqlSegmentService::readAll() {

        std::vector <std::shared_ptr<Segment> > segments;
        std::string query = coreSelectSql +
                            "FROM `segment`";


        auto res = driver->executeQuery(query);

        while (res->next()) {

                auto segment = mapResultSetToObject(res);
                segments.push_back(segment);
        }



        return segments;
}

std::vector <std::shared_ptr<Segment> > MySqlSegmentService::readAllSegmentForAdvertiserAndModel(int advertiserId, int modelId) {

        std::vector <std::shared_ptr<Segment> > segments;
        std::string query = coreSelectSql +
                            "FROM `segment` where "
                            " ADVERTISER_ID = __ADVERTISER_ID__ AND "
                            " MODEL_ID = __MODEL_ID__ ";

        MLOG(3) << "readAllSegmentForAdvertiserAndModel";
        //each target group has a vector which has 168 numbers which represents each hour of the week

        query = StringUtil::replaceString(query, "__ADVERTISER_ID__", StringUtil::toStr(advertiserId));
        query = StringUtil::replaceString(query, "__MODEL_ID__", StringUtil::toStr(modelId));
        MLOG(3) << "query : " << query;


        auto res = driver->executeQuery(query);

        while (res->next()) {

                auto segment = mapResultSetToObject(res);
                segments.push_back(segment);
        }




        MLOG(3) << " " << segments.size() <<
                " segments read for model id :  "
                << modelId << " and advertiser id : "
                << advertiserId;
        return segments;

}

std::vector <std::shared_ptr<Segment> > MySqlSegmentService::readSegmentsOfAdvertiserId(int advertiserId) {

        throwEx("error in mysql, finish this later");
}

void MySqlSegmentService::insert(std::shared_ptr<Segment> segment) {
        std::string queryStr =
                "INSERT INTO segment"
                "( "
                " NAME,"
                " ADVERTISER_ID,"
                " MODEL_ID,"
                " created_at,"
                " TYPE"
                " )  "
                "VALUES"
                "("
                "'__NAME__',"
                " __ADVERTISER_ID__,"
                " __MODEL_ID__,"
                "'__created_at__',"
                "'__TYPE__'"
                ");";
        segment->validate();
        MLOG(3) << "inserting new segment in db : " << segment->toJson();
        queryStr = StringUtil::replaceString(queryStr, "__NAME__",
                                             segment->getUniqueName());
        queryStr = StringUtil::replaceString(queryStr, "__ADVERTISER_ID__",
                                             StringUtil::toStr(segment->advertiserId));
        queryStr = StringUtil::replaceString(queryStr, "__MODEL_ID__",
                                             StringUtil::toStr(segment->modelId));

        queryStr = StringUtil::replaceString(queryStr, "__TYPE__", segment->getType());

        std::string dateCreated;

        Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat();

        queryStr = StringUtil::replaceString(queryStr, "__created_at__", date);



        MLOG(3) << "queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);
        segment->id = driver->getLastInsertedId ();

}

void MySqlSegmentService::queueSegmentForPersistence(std::shared_ptr<Segment> segment) {
        if(!segmentCreatedMapWrapper->exists(segment->getUniqueName())) {
                if (segmentCreatedMapWrapper->size() > 1000) {
                        MLOG(3)<<"clearing the segment map ";
                        segmentCreatedMapWrapper->clear();
                        entityToModuleStateStats->addStateModuleForEntity(
                                "CLEAR_FULL_SEGMENT_MAP",
                                "MySqlSegmentService",
                                "ALL"
                                );
                }
                MLOG(3)<<"inserting a segment in map and queue , "<<
                        segment->toString();
                entityToModuleStateStats->addStateModuleForEntity(
                        "INSERT_SEGMENT_IN_QUEUE",
                        "MySqlSegmentService",
                        segment->getUniqueName()
                        );

                segmentCreatedMapWrapper->put(segment->getUniqueName(), segment);
                segmentCreatedQueueManager->enqueueEntity(segment);

                entityToModuleStateStats->addStateModuleForEntity(
                        "SEGMENT_QUEUED_FOR_MYSQL_INSERTION",
                        "MySqlSegmentService",
                        segment->getUniqueName());

                if (segmentCreatedQueueManager->getEntityQueue()->unsafe_size() > 100) {
                        //if queue size is more than 100, we invoke the insert manually
                        entityToModuleStateStats->addStateModuleForEntity(
                                "SEGMENT_QUEUED_OVER_LIMIT_INSERTION_MANUALLY",
                                "MySqlSegmentService",
                                "ALL");

                        insertSegmentsFromQueue();
                }

        } else {
                entityToModuleStateStats->addStateModuleForEntity(
                        "SEGMENT_ALREADY_IN_MAP",
                        "MySqlSegmentService",
                        segment->getUniqueName()
                        );
                MLOG(3)<<"segment is already in map , "<<segment->getUniqueName();
        }

}
void MySqlSegmentService::makeSureActionTakerSegmentsHaveModelIds(std::shared_ptr<Segment> segment) {
        if (!StringUtil::equalsIgnoreCase(segment->getType(), Segment::ActionTakerSegmentName)) {
                return;
        }
        std::string segmentSeedName =
                StringUtil::toStr(Segment::ActionTakerSegmentName) + "advertiser-catchAll";
        auto model = mySqlModelService->fetchModelBySegmentSeedNameAndAdvertiserIdOptional(
                segmentSeedName,
                segment->advertiserId);

        if (model == nullptr) {
                model = std::make_shared<ModelRequest>();
                model->name = segmentSeedName;
                model->segmentSeedName = segmentSeedName;
                model->advertiserId = segment->advertiserId;
                model->modelType = ModelRequest::actionTakerCatchAllModel;
                model = mySqlModelService->upsertModel(model);
        }
        segment->modelId = model->id;
        assertAndThrow(segment->modelId > 0);

}
//we should call this method from a thread that runs every minute....
void MySqlSegmentService::insertSegmentsFromQueue() {
        while (!segmentCreatedQueueManager->getEntityQueue()->empty()) {
                try {
                        auto segment = segmentCreatedQueueManager->dequeueEntity ();
                        if (segment == boost::none) {
                                continue;
                        }
                        MLOG(3) << "inserting segment : " << *segment;
                        entityToModuleStateStats->
                        addStateModuleForEntity(
                                "INSERT_SEGMENT_IN_MYSQL",
                                "MySqlSegmentService",
                                (*segment)->getUniqueName());

                        makeSureActionTakerSegmentsHaveModelIds(*segment);
                        insertIfNotExisting (*segment);

                        entityToModuleStateStats->
                        addStateModuleForEntity(
                                "INSERT_SEGMENT_IN_MYSQL",
                                "MySqlSegmentService",
                                "ALL",
                                EntityToModuleStateStats::important);

                } catch (std::exception const &e) {
                        LOG(ERROR) << "error happening building models  " << boost::diagnostic_information (e);
                        entityToModuleStateStats->
                        addStateModuleForEntity(
                                "EXCEPTION_IN_INSERT_SEGMENT_IN_MYSQL",
                                "MySqlSegmentService",
                                "ALL",
                                EntityToModuleStateStats::exception);


                } catch (...) {
                        LOG(ERROR) << "unknown error happening putting msg in kafka queue ";
                        entityToModuleStateStats->
                        addStateModuleForEntity(
                                "UNKNOWN_EXCEPTION_IN_INSERT_SEGMENT_IN_MYSQL",
                                "MySqlSegmentService",
                                "ALL",
                                EntityToModuleStateStats::exception);
                }
        }
}


void MySqlSegmentService::deleteAll() {
        driver->deleteAll("segment");
}

std::shared_ptr<MySqlSegmentService> MySqlSegmentService::getInstance(
        gicapods::ConfigService* configService
        ) {

        std::shared_ptr<MySqlSegmentService> ins =
                std::make_shared<MySqlSegmentService>(
                        MySqlDriver::getInstance(configService).get(),
                        EntityToModuleStateStats::getInstance(configService).get()
                        );
        ins->mySqlModelService = MySqlModelService::getInstance(configService).get();

        return ins;
}

MySqlSegmentService::~MySqlSegmentService() {

}
