#ifndef OfferSegmentCacheService_h
#define OfferSegmentCacheService_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include <boost/foreach.hpp>
#include "HttpUtilService.h"
#include "EntityProviderService.h"
#include "SegmentCacheService.h"
#include "CacheService.h"
#include "OfferSegment.h"
#include "Object.h"
class EntityToModuleStateStats;

class OfferSegmentCacheService : public CacheService<OfferSegment>, public Object {

private:

public:

std::shared_ptr <std::unordered_map<int,
                                    std::shared_ptr<std::vector<std::shared_ptr<Segment> > > > > offerIdToSegments;

SegmentCacheService* segmentCacheService;

EntityToModuleStateStats* entityToModuleStateStats;

OfferSegmentCacheService(HttpUtilService* httpUtilService,
                         std::string dataMasterUrl,
                         EntityToModuleStateStats* entityToModuleStateStats,
                         std::string appName);

void clearOtherCaches();

void populateOtherMapsAndLists(std::vector<std::shared_ptr<OfferSegment> > allEntities);

};


#endif
