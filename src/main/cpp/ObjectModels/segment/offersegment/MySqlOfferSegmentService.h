#ifndef MySqlOfferSegmentService_h
#define MySqlOfferSegmentService_h

#include "OfferSegment.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include <tbb/concurrent_hash_map.h>
#include "DataProvider.h"
#include "MySqlService.h"
#include "ConcurrentQueueFolly.h"
#include "Object.h"
class MySqlModelService;
namespace gicapods { template<class K, class V> class ConcurrentHashMap; }

class MySqlOfferSegmentService;




class MySqlOfferSegmentService : public DataProvider<OfferSegment>, public Object {

public:

static std::string coreSelectSql;

MySqlDriver* driver;
MySqlOfferSegmentService(MySqlDriver* driver);

MySqlModelService* mySqlModelService;

std::vector<std::shared_ptr<OfferSegment> > readAll();

virtual ~MySqlOfferSegmentService();

std::shared_ptr<OfferSegment> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);

std::vector<std::shared_ptr<OfferSegment> > readAllEntities();
};


#endif
