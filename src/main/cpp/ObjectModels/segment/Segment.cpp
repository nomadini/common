#include "Segment.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>


void Segment::validate() {

        assertAndThrow(!this->uniqueName.empty ());
        // assertAndThrow(this->modelId > 0);
        // assertAndThrow(this->advertiserId > 0);

        valid = true;
}

Segment::Segment(long dateCreatedInMillis)  : Object(__FILE__) {
        valid = false;
        id = 0;
        modelId = 0;
        advertiserId = 0;
        this->dateCreatedInMillis = dateCreatedInMillis;
}

void Segment::setUniqueName(std::string uniqueName) {
        this->uniqueName = uniqueName;
}

void Segment::setType(std::string type) {
        this->type = type;
}

std::string Segment::toString() {
        return toJson ();
}

void Segment::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "advertiserId", advertiserId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "uniqueName", uniqueName, value);
        JsonUtil::addMemberToValue_FromPair (doc, "type", type, value);
        JsonUtil::addMemberToValue_FromPair (doc, "modelId", modelId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateCreatedInMillis", dateCreatedInMillis, value);
}

std::shared_ptr<Segment> Segment::fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<Segment> geoSegmentList = std::make_shared<Segment>(
                JsonUtil::GetLongSafely(value, "dateCreatedInMillis")
                );
        geoSegmentList->id = JsonUtil::GetIntSafely(value, "id");
        geoSegmentList->advertiserId  =JsonUtil::GetIntSafely(value,"advertiserId");
        geoSegmentList->uniqueName = JsonUtil::GetStringSafely(value,"uniqueName");
        geoSegmentList->type = JsonUtil::GetStringSafely(value,"type");
        geoSegmentList->modelId = JsonUtil::GetIntSafely(value,"modelId");

        return geoSegmentList;
}

std::string Segment::getEntityName() {
        return "Segment";
}
std::string Segment::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);

}

std::string Segment::getUniqueName() {
        validate();
        return uniqueName;
}

std::string Segment::getType() {
        validate();
        return type;
}

Segment::~Segment() {

}
