#ifndef SegmentGroupSegmentMap_H
#define SegmentGroupSegmentMap_H


#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include "JsonTypeDefs.h"

#include "Poco/DateTime.h"
#include "Object.h"
class SegmentGroupSegmentMap : public Object {

public:

int id;
int segmentId;
int segmentGroupId;

SegmentGroupSegmentMap();

std::string toString();

std::string toJson();

static std::string getEntityName();

virtual ~SegmentGroupSegmentMap();

static std::shared_ptr<SegmentGroupSegmentMap> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};



#endif
