#include "SegmentGroupSegmentMap.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>

SegmentGroupSegmentMap::SegmentGroupSegmentMap(
        )  : Object(__FILE__) {
        id = 0;
        segmentId = 0;
        segmentGroupId = 0;
}

std::string SegmentGroupSegmentMap::toString() {
        return toJson ();
}

void SegmentGroupSegmentMap::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "segmentId", segmentId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "segmentGroupId", segmentGroupId, value);
}

std::shared_ptr<SegmentGroupSegmentMap> SegmentGroupSegmentMap::fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<SegmentGroupSegmentMap> sg = std::make_shared<SegmentGroupSegmentMap>();
        sg->id = JsonUtil::GetIntSafely(value, "id");
        sg->segmentId  =JsonUtil::GetIntSafely(value,"segmentId");
        sg->segmentGroupId  =JsonUtil::GetIntSafely(value,"segmentGroupId");

        return sg;
}

std::string SegmentGroupSegmentMap::getEntityName() {
        return "SegmentGroupSegmentMap";
}
std::string SegmentGroupSegmentMap::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);

}

SegmentGroupSegmentMap::~SegmentGroupSegmentMap() {

}
