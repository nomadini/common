#include "GUtil.h"
#include "SegmentGroup.h"
#include "MySqlSegmentGroupService.h"
#include "MySqlDriver.h"
#include "StringUtil.h"
#include "DateTimeUtil.h"
#include <boost/foreach.hpp>
#include "EntityToModuleStateStats.h"
#include "ConcurrentHashMap.h"
#include "ConfigService.h"
#include "MySqlDriver.h"
#include "MySqlModelService.h"

std::string MySqlSegmentGroupService::coreSelectSql =
        "SELECT `id`,"
        "`name`,"
        "`descr`,"
        "`advertiser_id`,"
        "`created_at`";


MySqlSegmentGroupService::MySqlSegmentGroupService(MySqlDriver* driver,
                                                   EntityToModuleStateStats* entityToModuleStateStatsArg) : Object(__FILE__) {
        this->entityToModuleStateStats = entityToModuleStateStatsArg;
        this->driver = driver;
}


void MySqlSegmentGroupService::insertIfNotExisting(std::shared_ptr<SegmentGroup> segment) {
        if (!segmentExists(segment->name,
                           segment->advertiserId)) {
                insert(segment);
        } else {
                entityToModuleStateStats->addStateModuleForEntity("SEGMENT_ALREADY_EXISTS_IN_MYSQL",
                                                                  "MySqlSegmentGroupService",
                                                                  segment->name);
        }
}

std::shared_ptr<SegmentGroup> MySqlSegmentGroupService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<SegmentGroup> segment =
                std::make_shared<SegmentGroup>();
        int id = res->getInt("id");
        std::string name = MySqlDriver::getString( res, "name");
        int advertiserId = res->getInt("advertiser_id");
        std::string descr = MySqlDriver::getString( res, "descr");

        MLOG(3) << "values loaded for segment id :  " << id << ","
                " name :  " << name << ", "
                " advertiserId :  " << advertiserId << ","
                " descr :  " << descr;

        segment->id = id;
        segment->name = name;
        segment->descr = descr;
        segment->advertiserId = advertiserId;

        return segment;
}

std::vector<std::shared_ptr<SegmentGroup> > MySqlSegmentGroupService::readAllEntities() {
        return this->readAll();
}

bool MySqlSegmentGroupService::segmentExists(std::string name,
                                             int advertiserId) {
        return readSegmentGroup(name, advertiserId) != nullptr;
}
//a segment has a unique name
std::shared_ptr<SegmentGroup> MySqlSegmentGroupService::readSegmentGroup(
        std::string name,
        int advertiserId) {
        std::string query = coreSelectSql +
                            " FROM `segment_group` where "
                            " name = '__NAME__' and "
                            " advertiser_id = __ADVERTISER_ID__";

        query = StringUtil::replaceString(query, "__NAME__", name);
        query = StringUtil::replaceString(query, "__ADVERTISER_ID__", _toStr(advertiserId));
        MLOG(3) << "query : " << query;
        auto res = driver->executeQuery(query);
        while (res->next()) {
                auto segment = mapResultSetToObject(res);
                return segment;
        }
        return nullptr;
}

void MySqlSegmentGroupService::update(std::shared_ptr<SegmentGroup> segment) {
        std::string query(
                " UPDATE segment_group "
                " SET "
                "     ADVERTISER_ID = __ADVERTISER_ID__ "
                "     WHERE NAME = '__NAME__';");

        MLOG(3) << "updating segment " << segment->toJson();
        query = StringUtil::replaceString(query, "__ADVERTISER_ID__",
                                          StringUtil::toStr(segment->advertiserId));
        query = StringUtil::replaceString(query, "__NAME__", segment->name);

        driver->executedUpdateStatement (query);
}

std::vector <std::shared_ptr<SegmentGroup> > MySqlSegmentGroupService::readAll() {

        std::vector <std::shared_ptr<SegmentGroup> > segments;
        std::string query = coreSelectSql +
                            "FROM `segment_group`";


        auto res = driver->executeQuery(query);

        while (res->next()) {

                auto segment = mapResultSetToObject(res);
                segments.push_back(segment);
        }



        return segments;
}

void MySqlSegmentGroupService::insert(std::shared_ptr<SegmentGroup> segment) {
        std::string queryStr =
                "INSERT INTO segment_group"
                "( "
                " NAME,"
                " ADVERTISER_ID,"
                " descr"
                " )  "
                "VALUES"
                "("
                "'__NAME__',"
                " __ADVERTISER_ID__,"
                "'__descr__');";

        MLOG(3) << "inserting new segment in db : " << segment->toJson();
        queryStr = StringUtil::replaceString(queryStr, "__NAME__",
                                             segment->name);
        queryStr = StringUtil::replaceString(queryStr, "__descr__",
                                             segment->descr);
        queryStr = StringUtil::replaceString(queryStr, "__ADVERTISER_ID__",
                                             StringUtil::toStr(segment->advertiserId));


        MLOG(3) << "queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);
        segment->id = driver->getLastInsertedId ();

}

void MySqlSegmentGroupService::deleteAll() {
        driver->deleteAll("segment_group");
}

std::shared_ptr<MySqlSegmentGroupService> MySqlSegmentGroupService::getInstance(
        gicapods::ConfigService* configService
        ) {

        static std::shared_ptr<MySqlSegmentGroupService> ins =
                std::make_shared<MySqlSegmentGroupService>(
                        MySqlDriver::getInstance(configService).get(),
                        EntityToModuleStateStats::getInstance(configService).get()
                        );
        return ins;
}

MySqlSegmentGroupService::~MySqlSegmentGroupService() {

}
