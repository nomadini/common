
#include "StringUtil.h"
#include "MySqlDriver.h"
#include <boost/foreach.hpp>
#include "CollectionUtil.h"
#include "SegmentGroupCacheService.h"
#include "ConfigService.h"
#include "JsonArrayUtil.h"

SegmentGroupCacheService::SegmentGroupCacheService(HttpUtilService* httpUtilService,
                                                   std::string dataMasterUrl,
                                                   EntityToModuleStateStats* entityToModuleStateStats,
                                                   std::string appName)
        : CacheService<SegmentGroup>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName), Object(__FILE__) {
        this->allNameToSegmentGroupsMap= std::make_shared<std::unordered_map<std::string, std::shared_ptr<SegmentGroup> > >();
        this->entityToModuleStateStats = entityToModuleStateStats;
}

void SegmentGroupCacheService::populateOtherMapsAndLists(std::vector<std::shared_ptr<SegmentGroup> > segments) {

        for(std::shared_ptr<SegmentGroup> segment : segments) {
                // allNameToSegmentGroupsMap->insert (
                //   std::make_pair (segment->getUniqueName(), segment));

        }
}

void SegmentGroupCacheService::clearOtherCaches() {
        MLOG(10) << "clearing SegmentGroupCacheService ";
        this->allNameToSegmentGroupsMap->clear ();

}

std::shared_ptr<SegmentGroupCacheService>
SegmentGroupCacheService::getInstance(gicapods::ConfigService* configService) {
        static auto instance = std::make_shared<SegmentGroupCacheService>(
                HttpUtilService::getInstance(configService).get(),
                configService->get("dataMasterUrl"),
                EntityToModuleStateStats::getInstance(configService).get(),
                configService->get("appName")
                );

        return instance;
}
