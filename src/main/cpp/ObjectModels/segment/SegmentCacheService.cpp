
#include "StringUtil.h"
#include "MySqlDriver.h"
#include <boost/foreach.hpp>
#include "CollectionUtil.h"
#include "SegmentCacheService.h"
#include "ConfigService.h"
#include "JsonArrayUtil.h"

SegmentCacheService::SegmentCacheService(HttpUtilService* httpUtilService,
                                         std::string dataMasterUrl,
                                         EntityToModuleStateStats* entityToModuleStateStats,
                                         std::string appName)
        : CacheService<Segment>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName), Object(__FILE__) {
        this->allNameToSegmentsMap= std::make_shared<std::unordered_map<std::string, std::shared_ptr<Segment> > >();
        this->entityToModuleStateStats = entityToModuleStateStats;
}

void SegmentCacheService::populateOtherMapsAndLists(std::vector<std::shared_ptr<Segment> > segments) {

        for(std::shared_ptr<Segment> segment : segments) {
                allNameToSegmentsMap->insert (std::make_pair (segment->getUniqueName(), segment));

        }
}

void SegmentCacheService::clearOtherCaches() {
        MLOG(10) << "clearing SegmentCacheService ";
        this->allNameToSegmentsMap->clear ();

}

std::shared_ptr<SegmentCacheService>
SegmentCacheService::getInstance(gicapods::ConfigService* configService) {
        static auto instance =
                std::make_shared<SegmentCacheService>(
                        HttpUtilService::getInstance(configService).get(),
                        configService->get("dataMasterUrl"),
                        EntityToModuleStateStats::getInstance(configService).get(),
                        configService->get("appName"));
        return instance;
}
