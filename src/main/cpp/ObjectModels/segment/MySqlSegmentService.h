#ifndef MySqlSegmentService_h
#define MySqlSegmentService_h




#include "Segment.h"
#include <cppconn/resultset.h>
#include <tbb/concurrent_hash_map.h>
#include "DataProvider.h"
#include "MySqlService.h"
class EntityToModuleStateStats;
class MySqlDriver;
#include "ConcurrentQueueFolly.h"
#include "Object.h"
#include "MySqlModelService.h"
namespace gicapods {
template<class K, class V> class ConcurrentHashMap;
class ConfigService;
}

class MySqlSegmentService : public DataProvider<Segment>, public Object {

public:

static std::string coreSelectSql;

MySqlDriver* driver;
EntityToModuleStateStats* entityToModuleStateStats;
std::shared_ptr<ConcurrentQueueFolly<Segment> > segmentCreatedQueueManager;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, Segment> > segmentCreatedMapWrapper;

MySqlSegmentService(MySqlDriver* driver,
                    EntityToModuleStateStats* entityToModuleStateStatsArg);

MySqlModelService* mySqlModelService;

std::vector<std::shared_ptr<Segment> > readSegmentsOfAdvertiserId(int advertiserId);

std::shared_ptr<Segment> readSegment(std::string name,
                                     int advertiserId,
                                     int modelId);

void makeSureActionTakerSegmentsHaveModelIds(std::shared_ptr<Segment> segment);

std::vector<std::shared_ptr<Segment> > readAllSegmentForAdvertiserAndModel(int advertiserId, int modelId);

std::vector<std::shared_ptr<Segment> > readAll();

virtual ~MySqlSegmentService();

void insertIfNotExisting(std::shared_ptr<Segment> segment);

bool segmentExists(std::string uniqueSegmentName,
                   int advertiserId,
                   int modelId);


void update(std::shared_ptr<Segment> segment);

void insert(std::shared_ptr<Segment> segment);

void deleteAll();

void insertSegmentsFromQueue();

void queueSegmentForPersistence(std::shared_ptr<Segment> segment);

std::shared_ptr<Segment> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);

std::vector<std::shared_ptr<Segment> > readAllEntities();

static std::shared_ptr<MySqlSegmentService> getInstance(gicapods::ConfigService* configService);
};


#endif
