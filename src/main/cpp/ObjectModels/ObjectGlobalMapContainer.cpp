
#include "ObjectGlobalMapContainer.h"
#include "EntityToModuleStateStats.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "AtomicLong.h"
#include "DateTimeUtil.h"
#include "CollectionUtil.h"

long ObjectGlobalMapContainer::maxLimitOfObjects = 1000;
long ObjectGlobalMapContainer::maxLimitOfMapEntries = 10000;
bool ObjectGlobalMapContainer::checkMemoryLeakUponCreationProperty = false;

std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<gicapods::AtomicLong> > >
ObjectGlobalMapContainer::globalMap = std::make_shared<tbb::concurrent_hash_map<std::string, std::shared_ptr<gicapods::AtomicLong> > >();
ObjectGlobalMapContainer::ObjectGlobalMapContainer() {
        isAllowedToRun = std::make_shared<gicapods::AtomicBoolean>(true); //this is the flag that thread waits for
        threadIsFinished = std::make_shared<gicapods::AtomicBoolean>(false);
        lastTimeThreadRan = DateTimeUtil::getNowInSecond();
}

std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<gicapods::AtomicLong> > >
ObjectGlobalMapContainer::getMap() {
        return globalMap;
}
TimeType ObjectGlobalMapContainer::getWhenAppStarted() {
        static TimeType appStartedTime =  DateTimeUtil::getNowInSecond();
        return appStartedTime;
}

void ObjectGlobalMapContainer::setMaxLimitOfObjects(long maxLimitOfObjects) {
        ObjectGlobalMapContainer::maxLimitOfObjects = maxLimitOfObjects;
}

long ObjectGlobalMapContainer::getMaxLimitOfObjects() {
        return ObjectGlobalMapContainer::maxLimitOfObjects;
}

long ObjectGlobalMapContainer::getMaxLimitOfMapEntries() {
        return maxLimitOfMapEntries;
}

void ObjectGlobalMapContainer::checkMemoryLeakUponCreation(std::string className, long numberOfObjectsCreated) {
        if (checkMemoryLeakUponCreationProperty == false ) {
                return;
        }
        auto now = DateTimeUtil::getNowInSecond();
        auto timeDiff = now - ObjectGlobalMapContainer::getWhenAppStarted();
        if (numberOfObjectsCreated >= ObjectGlobalMapContainer::getMaxLimitOfObjects() &&
            timeDiff >= 60 ) {
                //we only show leaks after the second minute of the program, we don't want to see leaks in the start
                if (gicapods::Util::allowedToCall(_FL_, 10000)) {
                        LOG(ERROR) << "memory leak alert : classname : "<< className << " # alive objects : "<<numberOfObjectsCreated;
                        if (numberOfObjectsCreated >= 30000) {
                                // gicapods::Util::showStackTrace();
                        }

                }
                // if (StringUtil::containsCaseInSensitive(className, "Object")) {

                // }
        }
        return;
        // if (CollectionUtil::containsInList(className, highRateClasses)) {
        //         LOG_EVERY_N(ERROR, 1000) << "memory leak alert : classname : "<< className << " value incremented to : "<<numberOfObjectsCreated;
        // } else {
        //         if (numberOfObjectsCreated >= 5000) {
        //                 LOG_EVERY_N(ERROR, 100) << "memory leak alert : classname : "<< className << " value incremented to : "<<numberOfObjectsCreated;
        //                 if (StringUtil::containsCaseInSensitive(className, "AtomicDouble.cpp")) {
        //                         if (gicapods::Util::allowedToCall(_FL_, 100)) {
        //                                 // gicapods::Util::showStackTrace();
        //                         }
        //
        //                 }
        //         }
        // }

        // throwEx("memory leak spotted by " + className +  " value : " +
        //         StringUtil::toStr(numberOfObjectsCreated);

        // LOG(ERROR) << "classname : "<< className << " value incremented to : "<<numberOfObjectsCreated;

}

void ObjectGlobalMapContainer::setMaxLimitOfMapEntries(long maxLimitOfMapEntries) {
        ObjectGlobalMapContainer::maxLimitOfMapEntries = maxLimitOfMapEntries;
}
//

void ObjectGlobalMapContainer::checkMemoryLeakStatusThread() {
        while(isAllowedToRun->getValue()) {
                if (DateTimeUtil::getNowInSecond() - lastTimeThreadRan < 60) {
                        //sleep for one second
                        gicapods::Util::sleepViaBoost(_L_, 1);
                        continue;
                }

                checkMemoryLeakStatus();

                lastTimeThreadRan = DateTimeUtil::getNowInSecond();
        }

        threadIsFinished->setValue(true);
}

ObjectGlobalMapContainer::~ObjectGlobalMapContainer() {
        isAllowedToRun->setValue(false); //set the flag to false to make the thread stop
        while(!threadIsFinished->getValue()) {
                LOG(ERROR) << "waiting for the ObjectGlobalMapContainer thread to stop";
                gicapods::Util::sleepViaBoost(_L_, 1);
        }
}

void ObjectGlobalMapContainer::checkMemoryLeakStatus() {

        auto map = ObjectGlobalMapContainer::getMap();
        for (auto&& pair : *map) {
                auto className = pair.first;
                auto value = pair.second->getValue();
                if (value >= ObjectGlobalMapContainer::getMaxLimitOfObjects()) {
                        LOG(ERROR) << "memory leak alert : classname : "<< className << " # alive objects : "<<value;

                }
        }


}
