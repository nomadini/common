
#include "GUtil.h"
#include "EventLock.h"
EventLock::EventLock(
								std::string eventId,
								std::string eventType) {
								this->eventId = eventId;
								this->eventType = eventType;

}

EventLock::~EventLock() {
}


std::string EventLock::toJson() {
								auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString (value);
}

void EventLock::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
								JsonUtil::addMemberToValue_FromPair (doc, "eventId", eventId, value);
								JsonUtil::addMemberToValue_FromPair (doc, "eventType", eventType, value);
}

std::shared_ptr<EventLock> EventLock::fromJsonValue(RapidJsonValueType value) {

								std::shared_ptr<EventLock> eventLock = std::make_shared<EventLock>(
																JsonUtil::GetStringSafely(value, "eventId"),
																JsonUtil::GetStringSafely(value, "eventType"));
								return eventLock;
}

std::shared_ptr<EventLock> EventLock::fromJson(std::string jsonString) {
								auto document = parseJsonSafely(jsonString);
								return fromJsonValue(*document);
}
