//
// Created by User on 6/12/2016.
//

#include "MySqlExchangeKeyService.h"
#include "MySqlDriver.h"
#include "CollectionUtil.h"
#include "DateTimeUtil.h"
#include <boost/foreach.hpp>


MySqlExchangeKeyService::MySqlExchangeKeyService(MySqlDriver* driver) : Object(__FILE__) {
        this->driver = driver;
}

void
MySqlExchangeKeyService::readAllSyncedExchangeKyes(
        std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<std::set<std::string> > > > exchangeToSyncedIds,
        int limit) {
        std::string query = "SELECT "
                            " exchangeName,"
                            " exchangeId, "
                            " nomadiniDeviceId"
                            " FROM exchangeSyncedIds limit " + StringUtil::toStr(limit);

        auto res = driver->executeQuery (query);

        int count = 0;
        while (res->next ()) {
                auto exchangeName = MySqlDriver::getString( res, 1);
                auto exchangeId = MySqlDriver::getString( res, 2);
                auto nomadiniDeviceId = MySqlDriver::getString( res, 3);
                tbb::concurrent_hash_map<std::string, std::shared_ptr<std::set<std::string> > >::accessor accessor;

                if(!exchangeToSyncedIds->find(accessor, exchangeName)) {
                        exchangeToSyncedIds->insert(accessor, exchangeName);
                        accessor->second = std::make_shared<std::set<std::string> > ();
                }
                accessor->second->insert(exchangeId + "___" + nomadiniDeviceId);

        }

}

void MySqlExchangeKeyService::insertExchangeKey(std::vector<std::pair<std::string, std::string> > allPairs) {
        if (allPairs.empty()) {
                return;
        }
        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();

        std::string queryStr =
                "INSERT INTO exchangeSyncedIds"
                " ("
                " exchangeName,"
                " exchangeId,"
                " nomadiniDeviceId,"
                " created_at"
                " ) VALUES __VALUES_OF_ALLROW__  ;";

        std::vector<std::string> allValues;
        for(auto pair :  allPairs) {
                std::string oneRecordValues =
                        " "
                        " ("
                        " '__exchangeName__',"
                        " '__exchangeId__',"
                        " '__nomadiniDeviceId__',"
                        " '__created_at__'"
                        " ) ";

                auto exchangeIdAndNomadiniDeviceIdVector = StringUtil::tokenizeString(pair.second, "___");
                assertAndThrow(exchangeIdAndNomadiniDeviceIdVector.size() == 2);
                oneRecordValues = StringUtil::replaceString (oneRecordValues, "__exchangeName__",
                                                             pair.first);

                oneRecordValues = StringUtil::replaceString (oneRecordValues, "__exchangeId__",  exchangeIdAndNomadiniDeviceIdVector.at(0));
                oneRecordValues = StringUtil::replaceString (oneRecordValues, "__nomadiniDeviceId__",
                                                             exchangeIdAndNomadiniDeviceIdVector.at(1));


                oneRecordValues = StringUtil::replaceString (oneRecordValues, "__created_at__", date);

                allValues.push_back(oneRecordValues);
        }
        auto properAllValues = StringUtil::joinArrayAsString(allValues, ",");
        queryStr = StringUtil::replaceString (queryStr, "__VALUES_OF_ALLROW__", properAllValues);

        driver->executedUpdateStatement (queryStr);


}

void MySqlExchangeKeyService::deleteAll() {
        driver->deleteAll("exchangeSyncedIds");
}
