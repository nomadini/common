#include "IntWrapper.h"
#include "JsonUtil.h"

#include "GUtil.h"
IntWrapper::IntWrapper (int v)  : Object(__FILE__) {
								value = std::make_shared<gicapods::AtomicLong>();
								setValue(v);
}

IntWrapper::IntWrapper ()  : Object(__FILE__) {
								value = std::make_shared<gicapods::AtomicLong>();
}

int IntWrapper::getValue() {
								return value->getValue();
}

void IntWrapper::setValue(int v) {
								value->setValue(v);
}

void IntWrapper::increment() {
								value->addValue(1);

}

IntWrapper::~IntWrapper() {

}


void IntWrapper::addPropertiesToJsonValue(RapidJsonValueType valueArg, DocumentType* doc) {
								JsonUtil::addMemberToValue_FromPair (doc, "value", value->getValue(), valueArg);
}

std::shared_ptr<IntWrapper> IntWrapper::fromJsonValue(RapidJsonValueType value) {
								std::shared_ptr<IntWrapper> feature = std::make_shared<IntWrapper>(
																JsonUtil::GetIntSafely(value, "value"));
								return feature;
}

std::shared_ptr<IntWrapper> IntWrapper::fromJson(std::string jsonString) {
								auto document = parseJsonSafely(jsonString);
								return fromJsonValue(*document);
}

std::string IntWrapper::toJson() {
								auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString (value);
}
