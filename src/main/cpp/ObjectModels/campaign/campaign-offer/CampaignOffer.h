#ifndef CampaignOffer_h
#define CampaignOffer_h


class StringUtil;
#include <memory>
#include <string>
#include <vector>
#include <set>
#include <unordered_map>
#include "JsonTypeDefs.h"

#include "Poco/DateTime.h"
#include "Object.h"

class CampaignOffer : public Object {

private:
public:
int id;
int campaignId;
int offerId;
Poco::DateTime createdAt;
Poco::DateTime updatedAt;

CampaignOffer();

std::string toJson();

virtual ~CampaignOffer();
static std::string getEntityName();
static std::shared_ptr<CampaignOffer> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};

#endif
