#include "Poco/DateTime.h"
#include "Poco/Timestamp.h"
#include "CampaignOffer.h"
#include "JsonUtil.h"
#include <memory>
#include "DateTimeUtil.h"

CampaignOffer::CampaignOffer() : Object(__FILE__) {
        id = 0;
        campaignId = 0;
        offerId = 0;

}


std::shared_ptr<CampaignOffer> CampaignOffer::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<CampaignOffer> campaign = std::make_shared<CampaignOffer>();

        campaign->id = JsonUtil::GetIntSafely(value, "id");
        campaign->campaignId = JsonUtil::GetIntSafely(value, "campaignId");
        campaign->offerId = JsonUtil::GetIntSafely(value, "offerId");
        campaign->createdAt = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"createdAt"));
        campaign->updatedAt = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"updatedAt"));

        return campaign;
}

std::string CampaignOffer::getEntityName() {
        return "CampaignOffer";
}
void CampaignOffer::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "campaignId", campaignId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "offerId", offerId, value);

        JsonUtil::addMemberToValue_FromPair (doc, "createdAt", DateTimeUtil::dateTimeToStr(createdAt), value);
        JsonUtil::addMemberToValue_FromPair (doc, "updatedAt", DateTimeUtil::dateTimeToStr(updatedAt), value);
}

std::string CampaignOffer::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);

}

CampaignOffer::~CampaignOffer() {

}
