#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTime.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormat.h"
#include "Campaign.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "TestUtil.h"
#include <string>
#include <memory>
#include "StringUtil.h"
#include "DateTimeUtil.h"

#include "AdvertiserCostModel.h"

std::string Campaign::getEntityName() {
        static std::string entityName = "campaign";
        return entityName;
}

Campaign::Campaign() : Object(__FILE__) {
        id = 0;
        advertiserId = 0;
        maxImpression = 0;
        dailyMaxImpression = 0;
        maxBudget = 0;
        dailyMaxBudget = 0;
        cpm = 0;
        advCostModel = std::shared_ptr<AdvertiserCostModel> ();
        platformCostModel = std::shared_ptr<AdvertiserCostModel> ();
}

void Campaign::validate() {
        assertAndThrow(id > 0);
        TestUtil::assertNotEmpty(name);
        TestUtil::assertNotEmpty(status);

        assertAndThrow(advertiserId > 0);
        assertAndThrow(maxImpression > 0);
        assertAndThrow(dailyMaxImpression > 0);

        assertAndThrow(maxBudget > 0);
        assertAndThrow(dailyMaxBudget > 0);

        assertAndThrow(cpm > 0);
}

std::string Campaign::toString() {
        return toJson ();
}

std::shared_ptr<Campaign> Campaign::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<Campaign> campaign = std::make_shared<Campaign>();

        campaign->setId(JsonUtil::GetIntSafely(value, "id"));
        campaign->setName(JsonUtil::GetStringSafely(value, "name"));

        campaign->setDescription(JsonUtil::GetStringSafely(value,"description"));
        campaign->setAdvertiserId(JsonUtil::GetIntSafely(value,"advertiserId"));
        campaign->setStatus(JsonUtil::GetStringSafely(value,"status"));

        campaign->setMaxImpression (JsonUtil::GetLongSafely(value,"maxImpression"));
        campaign->setDailyMaxImpression (JsonUtil::GetLongSafely(value,"dailyMaxImpression"));
        campaign->setStartDate (DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"startDate")));
        campaign->setEndDate (DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"endDate")));
        campaign->setCreatedAt (DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"createdAt")));
        campaign->setUpdatedAt (DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"updatedAt")));

        campaign->setMaxBudget (JsonUtil::GetDoubleSafely(value,"maxBudget"));
        campaign->setDailyMaxBudget (JsonUtil::GetDoubleSafely(value,"dailyMaxBudget"));
        campaign->setCpm (JsonUtil::GetDoubleSafely(value,"cpm"));
        campaign->advCostModel = AdvertiserCostModel::parseCostModel(JsonUtil::GetStringSafely(value, "advCostModel"));
        campaign->platformCostModel = AdvertiserCostModel::parseCostModel(JsonUtil::GetStringSafely(value, "platformCostModel"));

        return campaign;
}


void Campaign::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "name", name, value);
        JsonUtil::addMemberToValue_FromPair (doc, "status", status, value);
        JsonUtil::addMemberToValue_FromPair (doc, "description", description, value);
        JsonUtil::addMemberToValue_FromPair (doc, "advertiserId", advertiserId, value);


        JsonUtil::addMemberToValue_FromPair (doc, "maxImpression", maxImpression, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dailyMaxImpression", dailyMaxImpression, value);
        JsonUtil::addMemberToValue_FromPair (doc, "startDate", DateTimeUtil::dateTimeToStr(startDate), value);
        JsonUtil::addMemberToValue_FromPair (doc, "endDate", DateTimeUtil::dateTimeToStr(endDate), value);
        JsonUtil::addMemberToValue_FromPair (doc, "createdAt", DateTimeUtil::dateTimeToStr(createdAt), value);
        JsonUtil::addMemberToValue_FromPair (doc, "updatedAt", DateTimeUtil::dateTimeToStr(updatedAt), value);

        JsonUtil::addMemberToValue_FromPair (doc, "maxBudget", maxBudget, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dailyMaxBudget", dailyMaxBudget, value);
        JsonUtil::addMemberToValue_FromPair (doc, "cpm", cpm, value);
        JsonUtil::addMemberToValue_FromPair (doc, "advCostModel", advCostModel->toJson(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "platformCostModel", platformCostModel->toJson(), value);
}

std::string Campaign::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);

}

Campaign::~Campaign() {

}


int Campaign::getId() {
        return id;
}

std::string Campaign::getName() {
        return name;
}

std::string Campaign::getStatus() {
        return status;
}

std::string Campaign::getDescription() {
        return description;
}

int Campaign::getAdvertiserId() {
        return advertiserId;
}

long Campaign::getMaxImpression() {
        return maxImpression;
}

long Campaign::getDailyMaxImpression() {
        return dailyMaxImpression;
}

double Campaign::getMaxBudget() {
        return maxBudget;
}

double Campaign::getDailyMaxBudget() {
        return dailyMaxBudget;
}

double Campaign::getCpm() {
        return cpm;
}

Poco::DateTime Campaign::getStartDate() {
        return startDate;
}

Poco::DateTime Campaign::getEndDate() {
        return endDate;
}

Poco::DateTime Campaign::getCreatedAt() {
        return createdAt;
}

Poco::DateTime Campaign::getUpdatedAt() {
        return updatedAt;
}

void Campaign::setId(int id) {
        this->id = id;
}

void Campaign::setName(std::string name) {
        this->name = name;
}

void Campaign::setStatus(std::string status) {
        this->status = status;
}

void Campaign::setDescription(std::string description) {
        this->description = description;
}

void Campaign::setAdvertiserId(int advertiserId) {
        this->advertiserId = advertiserId;
}

void Campaign::setMaxImpression(long maxImpression) {
        this->maxImpression = maxImpression;
}

void Campaign::setDailyMaxImpression(long dailyMaxImpression) {
        this->dailyMaxImpression = dailyMaxImpression;
}

void Campaign::setMaxBudget(double maxBudget) {
        this->maxBudget = maxBudget;
}

void Campaign::setDailyMaxBudget(double dailyMaxBudget) {
        this->dailyMaxBudget = dailyMaxBudget;
}

void Campaign::setCpm(double cpm) {
        this->cpm = cpm;
}

void Campaign::setStartDate(Poco::DateTime startDate) {
        this->startDate = startDate;
}

void Campaign::setEndDate(Poco::DateTime endDate) {
        this->endDate = endDate;
}

void Campaign::setCreatedAt(Poco::DateTime createdAt) {
        this->createdAt = createdAt;
}

void Campaign::setUpdatedAt(Poco::DateTime updatedAt) {
        this->updatedAt = updatedAt;
}
