
#include "CampaignRealTimeInfo.h"
#include "AtomicLong.h"
#include "AtomicDouble.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "DateTimeUtil.h"

CampaignRealTimeInfo::CampaignRealTimeInfo()  : Object(__FILE__) {
        Poco::DateTime now;
        timeReported = now;
        numOfImpressionsServedInCurrentDateUpToNow = std::make_shared<gicapods::AtomicLong>();
        platformCostSpentInCurrentDateUpToNow = std::make_shared<gicapods::AtomicDouble>();
        numOfImpressionsServedOverallUpToNow = std::make_shared<gicapods::AtomicLong>();
        platformCostSpentOverallUpToNow = std::make_shared<gicapods::AtomicDouble>();
        id = 0;
        campaignId = 0;
}

std::string CampaignRealTimeInfo::toString() {
        return this->toJson();
}
std::string CampaignRealTimeInfo::getEntityName() {
        return "CampaignRealTimeInfo";
}

std::shared_ptr<CampaignRealTimeInfo> CampaignRealTimeInfo::fromJson(std::string jsonString) {
        std::shared_ptr<CampaignRealTimeInfo> cmp = std::make_shared<CampaignRealTimeInfo>();

        auto doc = parseJsonSafely(jsonString);
        MLOG(3)<<"original jsonString for CampaignRealTimeInfo : "<<jsonString;
        bool result;
        cmp->campaignId = JsonUtil::GetIntSafely(*doc, "campaignId");
        cmp->timeReported = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(*doc, "timeReported"));

        cmp->numOfImpressionsServedInCurrentDateUpToNow->setValue(
                JsonUtil::GetLongSafely(*doc,"impressionShownToday"));
        cmp->platformCostSpentInCurrentDateUpToNow->setValue(
                JsonUtil::GetDoubleSafely(*doc, "platformCostSpentInCurrentDateUpToNow"));
        cmp->lastTimeAdShown = JsonUtil::GetStringSafely(*doc,"lastTimeAdShown");
        cmp->numOfImpressionsServedOverallUpToNow->setValue(
                JsonUtil::GetLongSafely(*doc, "impressionsShownToDate"));
        cmp->platformCostSpentOverallUpToNow->setValue(
                JsonUtil::GetDoubleSafely(*doc, "platformCostSpentOverallUpToNow"));
        return cmp;
}

std::string CampaignRealTimeInfo::toJson() {
        std::string json;
        auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "campaignId",
                                            campaignId, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "timeReported", DateTimeUtil::dateTimeToStr(timeReported), value);
        JsonUtil::addMemberToValue_FromPair(doc.get(),
                                            "impressionShownToday", numOfImpressionsServedInCurrentDateUpToNow->getValue(), value);
        JsonUtil::addMemberToValue_FromPair(doc.get(),
                                            "platformCostSpentInCurrentDateUpToNow", platformCostSpentInCurrentDateUpToNow->getValue(), value);
        JsonUtil::addMemberToValue_FromPair(doc.get(),
                                            "lastTimeAdShown", lastTimeAdShown, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(),
                                            "impressionsShownToDate", numOfImpressionsServedOverallUpToNow->getValue(), value);
        JsonUtil::addMemberToValue_FromPair(doc.get(),
                                            "platformCostSpentOverallUpToNow", platformCostSpentOverallUpToNow->getValue(), value);
        std::string jsonString = JsonUtil::valueToString(value);
        MLOG(3)<<"json created for CampaignRealTimeInfo : "<<jsonString;
        return jsonString;
}

CampaignRealTimeInfo::~CampaignRealTimeInfo() {

}


void CampaignRealTimeInfo::assign(std::shared_ptr<CampaignRealTimeInfo> from, std::shared_ptr<CampaignRealTimeInfo> to) {
        to->platformCostSpentOverallUpToNow = from->platformCostSpentOverallUpToNow;
        to->platformCostSpentInCurrentDateUpToNow = from->platformCostSpentInCurrentDateUpToNow;
        to->numOfImpressionsServedOverallUpToNow = from->numOfImpressionsServedOverallUpToNow;
        to->lastTimeAdShown = from->lastTimeAdShown;
        to->numOfImpressionsServedInCurrentDateUpToNow = from->numOfImpressionsServedInCurrentDateUpToNow;
        to->campaignId = from->campaignId;
        to->timeReported = from->timeReported;
        MLOG(3)<<"updated cmp realtime from   "<<from->toJson()<<" to to"<<to->toJson();
}

bool CampaignRealTimeInfo::isEqual(std::shared_ptr<CampaignRealTimeInfo> with) {
        return (this->timeReported == with->timeReported)
               &&  (this->campaignId == with->campaignId)
               &&  this->platformCostSpentOverallUpToNow->getValue() == with->platformCostSpentOverallUpToNow->getValue()
               &&  this->platformCostSpentInCurrentDateUpToNow->getValue() == with->platformCostSpentInCurrentDateUpToNow->getValue()
               &&  this->numOfImpressionsServedOverallUpToNow->getValue() == with->numOfImpressionsServedOverallUpToNow->getValue()
               &&  this->numOfImpressionsServedInCurrentDateUpToNow->getValue() == with->numOfImpressionsServedInCurrentDateUpToNow->getValue();
}

std::shared_ptr<CampaignRealTimeInfo> CampaignRealTimeInfo::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<CampaignRealTimeInfo> campaignRealTimeInfo = std::make_shared<CampaignRealTimeInfo>();
        campaignRealTimeInfo->campaignId = JsonUtil::GetIntSafely(value, "campaignId");
        campaignRealTimeInfo->timeReported  = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value, "timeReported"));
        campaignRealTimeInfo->numOfImpressionsServedInCurrentDateUpToNow->setValue(JsonUtil::GetIntSafely(value,"impressionShownToday"));
        campaignRealTimeInfo->platformCostSpentInCurrentDateUpToNow->setValue(JsonUtil::GetDoubleSafely(value,"platformCostSpentInCurrentDateUpToNow"));
        campaignRealTimeInfo->lastTimeAdShown = JsonUtil::GetStringSafely(value,"lastTimeAdShown");
        campaignRealTimeInfo->numOfImpressionsServedOverallUpToNow->setValue(JsonUtil::GetLongSafely(value,"impressionsShownToDate"));
        campaignRealTimeInfo->platformCostSpentOverallUpToNow->setValue(JsonUtil::GetDoubleSafely(value,"platformCostSpentOverallUpToNow"));

        return campaignRealTimeInfo;
}

void CampaignRealTimeInfo::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "campaignId", campaignId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "timeReported", DateTimeUtil::dateTimeToStr(timeReported), value);
        JsonUtil::addMemberToValue_FromPair (doc, "impressionShownToday", numOfImpressionsServedInCurrentDateUpToNow->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "platformCostSpentInCurrentDateUpToNow", platformCostSpentInCurrentDateUpToNow->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "lastTimeAdShown", lastTimeAdShown, value);
        JsonUtil::addMemberToValue_FromPair (doc, "impressionsShownToDate", numOfImpressionsServedOverallUpToNow->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "platformCostSpentOverallUpToNow", platformCostSpentOverallUpToNow->getValue(), value);
}
