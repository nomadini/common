
#ifndef AdvertiserCostModel_H
#define AdvertiserCostModel_H
#include "Object.h"
#include <memory>
#include <string>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "JsonTypeDefs.h"
#include "Poco/DateTime.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "GUtil.h"


class AdvertiserFixedCostEntry : public Object {
public:
std::string name;
long fixedValue;

AdvertiserFixedCostEntry() : Object(__FILE__) {
}

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "name", name, value);
        JsonUtil::addMemberToValue_FromPair (doc, "fixed_value", fixedValue, value);
}

static std::shared_ptr<AdvertiserFixedCostEntry> fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<AdvertiserFixedCostEntry>  advertiserFixedCostEntry = std::make_shared<AdvertiserFixedCostEntry>();
        advertiserFixedCostEntry->fixedValue = JsonUtil::GetLongSafely(value,"fixed_value");
        advertiserFixedCostEntry->name =  JsonUtil::GetStringSafely(value,"name");
        return advertiserFixedCostEntry;
}

};

class AdvertiserFixedCost : public Object {
public:


AdvertiserFixedCost() : Object(__FILE__) {
}

std::vector<std::shared_ptr<AdvertiserFixedCostEntry> > costs;
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonArrayUtil::addArrayAsAMemberToValue(doc,costs, value);
}

static std::shared_ptr<AdvertiserFixedCost> fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<AdvertiserFixedCost>  advertiserFixedCost = std::make_shared<AdvertiserFixedCost>();
        advertiserFixedCost->costs =
                JsonArrayUtil::getArrayFromValueMemeber<AdvertiserFixedCostEntry>(value);
        return advertiserFixedCost;
}

};


class AdvertiserImpBasedPercentageCostEntry : public Object {
public:

AdvertiserImpBasedPercentageCostEntry() : Object(__FILE__) {
}
std::string name;
long percentageValue;

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "name", name, value);
        JsonUtil::addMemberToValue_FromPair (doc, "percentage_value", percentageValue, value);
}

static std::shared_ptr<AdvertiserImpBasedPercentageCostEntry> fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<AdvertiserImpBasedPercentageCostEntry>  advertiserImpBasedPercentageCostEntry =
                std::make_shared<AdvertiserImpBasedPercentageCostEntry>();
        advertiserImpBasedPercentageCostEntry->percentageValue = JsonUtil::GetLongSafely(value,"percentage_value");
        advertiserImpBasedPercentageCostEntry->name =  JsonUtil::GetStringSafely(value,"name");
        return advertiserImpBasedPercentageCostEntry;
}
};

class AdvertiserImpBasedPercentageCost : public Object {
public:

AdvertiserImpBasedPercentageCost() : Object(__FILE__) {
}
std::vector<std::shared_ptr<AdvertiserImpBasedPercentageCostEntry> > costs;

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonArrayUtil::addArrayAsAMemberToValue(doc,costs, value);
}

static std::shared_ptr<AdvertiserImpBasedPercentageCost> fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<AdvertiserImpBasedPercentageCost>  advertiserImpBasedPercentageCost = std::make_shared<AdvertiserImpBasedPercentageCost>();
        advertiserImpBasedPercentageCost->costs =
                JsonArrayUtil::getArrayFromValueMemeber<AdvertiserImpBasedPercentageCostEntry>(value);
        return advertiserImpBasedPercentageCost;
}

};

class AdvertiserImpBasedFixedCostEntry : public Object {
public:


std::string name;
long fixedValue;

AdvertiserImpBasedFixedCostEntry() : Object(__FILE__) {
}
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "name", name, value);
        JsonUtil::addMemberToValue_FromPair (doc, "fixed_value", fixedValue, value);
}

static std::shared_ptr<AdvertiserImpBasedFixedCostEntry> fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<AdvertiserImpBasedFixedCostEntry>  advertiserImpBasedFixedCostEntry = std::make_shared<AdvertiserImpBasedFixedCostEntry>();
        advertiserImpBasedFixedCostEntry->fixedValue = JsonUtil::GetLongSafely(value,"fixed_value");
        advertiserImpBasedFixedCostEntry->name =  JsonUtil::GetStringSafely(value,"name");
        return advertiserImpBasedFixedCostEntry;
}
};

class AdvertiserImpBasedFixedCost : public Object {
public:

AdvertiserImpBasedFixedCost() : Object(__FILE__) {
}
std::vector<std::shared_ptr<AdvertiserImpBasedFixedCostEntry> > costs;

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

        JsonArrayUtil::addArrayAsAMemberToValue(doc,costs, value);
}

static std::shared_ptr<AdvertiserImpBasedFixedCost> fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<AdvertiserImpBasedFixedCost>  advertiserImpBasedFixedCost = std::make_shared<AdvertiserImpBasedFixedCost>();
        advertiserImpBasedFixedCost->costs =
                JsonArrayUtil::getArrayFromValueMemeber<AdvertiserImpBasedFixedCostEntry>(value);
        return advertiserImpBasedFixedCost;
}
};

/*
   {
   "adv_fixed_cost" : [
   {
    "name" : "cost-for-feature-a",
          "fixed_value" : 10
    }
   ],
   "adv_imp_based_perc_cost" : [
   {
    "name" : "cost-for-a",
          "percentage_value" : 10
    }
   ]
   ,

   "adv_imp_based_fixed_cost" : [
   {
    "name" : "cost-for-b",
          "fixed_value" : 10
    }
   ]
   }
 */

class AdvertiserCostModel : public Object {
public:
std::shared_ptr<AdvertiserFixedCost> advertiserFixedCost;
std::shared_ptr<AdvertiserImpBasedPercentageCost> advertiserImpBasedPercentageCost;
std::shared_ptr<AdvertiserImpBasedFixedCost> advertiserImpBasedFixedCost;

AdvertiserCostModel() : Object(__FILE__) {
        advertiserFixedCost = std::make_shared<AdvertiserFixedCost> ();
        advertiserImpBasedPercentageCost = std::make_shared<AdvertiserImpBasedPercentageCost> ();
        advertiserImpBasedFixedCost = std::make_shared<AdvertiserImpBasedFixedCost>();

}

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        RapidJsonValueTypeNoRef fixedCostArray(rapidjson::kArrayType);
        advertiserFixedCost->addPropertiesToJsonValue(fixedCostArray, doc);
        RapidJsonValueTypeNoRef fixedCostArray1(rapidjson::kArrayType);
        advertiserImpBasedPercentageCost->addPropertiesToJsonValue(fixedCostArray1, doc);
        RapidJsonValueTypeNoRef fixedCostArray2(rapidjson::kArrayType);
        advertiserImpBasedFixedCost->addPropertiesToJsonValue(fixedCostArray2, doc);

        std::string nameOfArray = "adv_fixed_cost";
        RapidJsonValueTypeNoRef nameOfArrayValue ((char *) nameOfArray.c_str (), doc->GetAllocator ());
        value.AddMember (nameOfArrayValue, fixedCostArray, doc->GetAllocator ());
        std::string nameOfArray1 = "adv_imp_based_perc_cost";
        RapidJsonValueTypeNoRef nameOfArrayValue2 ((char *) nameOfArray1.c_str (), doc->GetAllocator ());
        value.AddMember (nameOfArrayValue2, fixedCostArray1, doc->GetAllocator ());

        std::string nameOfArray2 = "adv_imp_based_fixed_cost";
        RapidJsonValueTypeNoRef nameOfArrayValue3 ((char *) nameOfArray2.c_str (), doc->GetAllocator ());
        value.AddMember (nameOfArrayValue3, fixedCostArray2, doc->GetAllocator ());

}

static std::shared_ptr<AdvertiserCostModel> fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<AdvertiserCostModel>  advertiserCostModel = std::make_shared<AdvertiserCostModel>();

        if (!JsonUtil::hasMemeber(value, "adv_fixed_cost")) {
                throwEx("adv_fixed_cost member is not found in document");
        }
        if (!JsonUtil::hasMemeber(value, "adv_imp_based_perc_cost")) {
                throwEx("adv_imp_based_perc_cost member is not found in document");
        }
        if (!JsonUtil::hasMemeber(value, "adv_imp_based_fixed_cost")) {
                throwEx("adv_imp_based_fixed_cost member is not found in document");
        }

        advertiserCostModel->advertiserFixedCost = AdvertiserFixedCost::fromJsonValue(value["adv_fixed_cost"]);
        advertiserCostModel->advertiserImpBasedPercentageCost = AdvertiserImpBasedPercentageCost::fromJsonValue(value["adv_imp_based_perc_cost"]);
        advertiserCostModel->advertiserImpBasedFixedCost = AdvertiserImpBasedFixedCost::fromJsonValue(value["adv_imp_based_fixed_cost"]);
        return advertiserCostModel;
}

static std::shared_ptr<AdvertiserCostModel> parseCostModel(std::string json) {
        auto document = parseJsonSafely(json);
        return fromJsonValue(*document);
}
std::string toJson() {
        auto doc = JsonUtil::createDcoumentAsObjectDoc();
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}
};

#endif
