#include "Campaign.h"
#include "StringUtil.h"
#include "MySqlDriver.h"
#include <boost/foreach.hpp>
#include "TargetGroupCacheService.h"
#include "CampaignCacheServiceMock.h"
#include "MySqlCampaignService.h"
#include "JsonArrayUtil.h"


CampaignCacheServiceMock::CampaignCacheServiceMock(MySqlCampaignService* mySqlCampaignService,
                                                   TargetGroupCacheService* targetGroupCacheService,
                                                   HttpUtilService* httpUtilService,
                                                   std::string dataMasterUrl,
                                                   EntityToModuleStateStats* entityToModuleStateStats,
                                                   std::string appName) :
        CampaignCacheService(mySqlCampaignService,
                             targetGroupCacheService,
                             httpUtilService,
                             dataMasterUrl,
                             entityToModuleStateStats,appName) {
}
