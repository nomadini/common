#include "Campaign.h"
#include "Poco/DateTime.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTimeFormat.h"
#include "CampaignTestHelper.h"
#include "DateTimeUtil.h"
#include "StringUtil.h"
#include "GUtil.h"
#include "RandomUtil.h"
CampaignTestHelper::CampaignTestHelper() {
}
std::shared_ptr<Campaign> CampaignTestHelper::createSampleCampaign() {

        std::shared_ptr<Campaign> camp = std::make_shared<Campaign>();
        camp->setId(RandomUtil::sudoRandomNumber(1000));
        camp->setAdvertiserId(RandomUtil::sudoRandomNumber(1000));
        camp->setName( "comcast-campaign01");
        camp->setStatus("active");
        camp->setDescription( "I am test campaign");

        camp->setCpm(4.00);
        camp->setMaxImpression( 1000);
        camp->setDailyMaxImpression(1000);

        camp->setMaxBudget ( 100);
        camp->setDailyMaxBudget( 100);
        Poco::DateTime now;
        camp->setStartDate (now);
        Poco::Timespan span (30 * Poco::Timespan::DAYS);
        camp->setEndDate(now + span);
        camp->setCreatedAt (now);
        camp->setUpdatedAt(now);
        return camp;
}

void CampaignTestHelper::thenBothCampaignsAreEqual(std::shared_ptr<Campaign> actualCampaign, std::shared_ptr<Campaign> expectedCampaign) {

        EXPECT_THAT( actualCampaign->getId(), testing::Eq(expectedCampaign->getId()));

        EXPECT_THAT( actualCampaign->getName(), testing::Eq(expectedCampaign->getName()));

        EXPECT_THAT( actualCampaign->getStatus(),
                     testing::Eq(expectedCampaign->getStatus()));

        EXPECT_THAT( actualCampaign->getDescription(),
                     testing::Eq(expectedCampaign->getDescription()));

        EXPECT_THAT( actualCampaign->getAdvertiserId(),
                     testing::Eq(expectedCampaign->getAdvertiserId()));

        EXPECT_THAT( actualCampaign->getMaxImpression(),
                     testing::Eq(expectedCampaign->getMaxImpression()));

        EXPECT_THAT( actualCampaign->getDailyMaxImpression(),
                     testing::Eq(expectedCampaign->getDailyMaxImpression()));

        EXPECT_THAT( actualCampaign->getCpm(),
                     testing::Eq(expectedCampaign->getCpm()));

        EXPECT_THAT( actualCampaign->getStartDate(),
                     testing::Eq(expectedCampaign->getStartDate()));

        EXPECT_THAT( actualCampaign->getEndDate(),
                     testing::Eq(expectedCampaign->getEndDate()));

        EXPECT_THAT( actualCampaign->getCreatedAt (),
                     testing::Eq(expectedCampaign->getCreatedAt()));

        EXPECT_THAT( actualCampaign->getUpdatedAt (), testing::Eq(expectedCampaign->getUpdatedAt()));

}
