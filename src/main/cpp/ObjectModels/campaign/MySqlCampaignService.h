//
// Created by Mahmoud Taabodi on 2/15/16.
//

#ifndef MySqlCampaignService_h
#define MySqlCampaignService_h



#include "Object.h"
#include "CampaignTypeDefs.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "DataProvider.h"
#include "MySqlService.h"

class MySqlCampaignService;


class MySqlCampaignService : public DataProvider<Campaign>, public MySqlService<int, Campaign>, public Object {

public:

MySqlDriver* driver;

MySqlCampaignService(MySqlDriver* driver);

virtual ~MySqlCampaignService();

void updateCampaignStatus(std::shared_ptr<Campaign> tg);

std::string getSelectAllQueryStatement();
std::string allFields;
std::shared_ptr<Campaign> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);
std::string getInsertObjectSqlStatement(std::shared_ptr<Campaign> campaign);
std::string getReadByIdSqlStatement(int id);

//delete this later after you have merged MySqlService and DataProvider
std::vector<std::shared_ptr<Campaign>> readAllEntities();
virtual void deleteAll();
};

#endif
