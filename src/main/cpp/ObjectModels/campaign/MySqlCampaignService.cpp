#include "GUtil.h"


#include "Campaign.h"
#include "MySqlCampaignService.h"
#include "MySqlDriver.h"
#include "DateTimeUtil.h"
#include "AdvertiserCostModel.h"
MySqlCampaignService::MySqlCampaignService(MySqlDriver* driver) :
        MySqlService(driver, nullptr),  Object(__FILE__) {
        this->driver = driver;
        allFields =
                " `name`, "
                " `status`,"
                " `description`, "
                " `advertiser_domain`, "
                " `advertiser_id`,"
                " `max_impression`,"
                " `daily_max_impression`,"
                " `max_budget`,"
                " `daily_max_budget`,"
                " `cpm`,"
                " `start_date`, "
                " `end_date`, "
                " `created_at`,"
                " `updated_at`, "
                " `platform_cost_model`,"
                " `adv_cost_model`";
}

std::vector<std::shared_ptr<Campaign>> MySqlCampaignService::readAllEntities() {
        return this->readAll();
}
std::string MySqlCampaignService::getSelectAllQueryStatement() {
        static std::string selectAllQueryStatement = " SELECT "
                                                     " `id`, "
                                                     + allFields +
                                                     " FROM `campaign`";
        return selectAllQueryStatement;
}

std::string MySqlCampaignService::getInsertObjectSqlStatement(std::shared_ptr<Campaign> campaign) {
        std::string queryStr =
                " INSERT INTO `campaign`"
                " ("
                + allFields +
                ")"
                " VALUES"
                " ( "
                " '__name__',"
                " '__status__',"
                " '__description__',"
                " __advertiser_id__,"
                " __max_impression__,"
                " __daily_max_impression__,"
                " __max_budget__,"
                " __daily_max_budget__,"
                " __cpm__,"
                " '__start_date__',"
                " '__end_date__',"
                " '__platform_cost_model__',"
                " '__adv_cost_model__',"
                " '__created_at__',"
                " '__updated_at__');";

        queryStr = StringUtil::replaceString (queryStr, "__name__",
                                              campaign->getName());
        queryStr = StringUtil::replaceString (queryStr, "__status__",
                                              campaign->getStatus());
        queryStr = StringUtil::replaceString (queryStr, "__description__",
                                              campaign->getDescription());
        queryStr = StringUtil::replaceString (queryStr, "__advertiser_id__",
                                              StringUtil::toStr (campaign->getAdvertiserId()));

        queryStr = StringUtil::replaceString (queryStr, "__max_impression__",
                                              StringUtil::toStr (campaign->getMaxImpression()));
        queryStr = StringUtil::replaceString (queryStr, "__daily_max_impression__",
                                              StringUtil::toStr (campaign->getDailyMaxImpression()));
        queryStr = StringUtil::replaceString (queryStr, "__max_budget__",
                                              StringUtil::toStr (campaign->getMaxBudget()));
        queryStr = StringUtil::replaceString (queryStr, "__daily_max_budget__",
                                              StringUtil::toStr (campaign->getDailyMaxBudget()));
        queryStr = StringUtil::replaceString (queryStr, "__cpm__",
                                              StringUtil::toStr (campaign->getCpm()));

        queryStr = StringUtil::replaceString (queryStr, "__start_date__",
                                              DateTimeUtil::dateTimeToStr(campaign->getStartDate()));
        queryStr = StringUtil::replaceString (queryStr, "__end_date__",
                                              DateTimeUtil::dateTimeToStr(campaign->getEndDate()));
        queryStr = StringUtil::replaceString (queryStr, "__adv_cost_model__", campaign->advCostModel->toJson());
        queryStr = StringUtil::replaceString (queryStr, "__platform_cost_model__", campaign->platformCostModel->toJson());

        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);
        return queryStr;
}

std::shared_ptr<Campaign> MySqlCampaignService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<Campaign> obj = std::make_shared<Campaign> ();

        obj->setId(res->getInt ("id"));
        obj->setName(res->getString ("name"));
        obj->setStatus(res->getString ("status"));
        obj->setDescription(res->getString ("description"));
        obj->setAdvertiserId(res->getInt ("advertiser_id"));
        obj->setMaxImpression(res->getInt64 ("max_impression"));
        obj->setDailyMaxImpression(res->getInt64 ("daily_max_impression"));
        obj->setMaxBudget(res->getInt64 ("max_budget"));
        obj->setDailyMaxBudget(res->getInt64 ("daily_max_budget"));
        obj->setCpm(res->getDouble ("cpm"));
        obj->setStartDate(DateTimeUtil::parseDateTime(res->getString ("start_date")));
        obj->setEndDate(DateTimeUtil::parseDateTime(res->getString ("end_date")));
        obj->setCreatedAt(DateTimeUtil::parseDateTime(res->getString ("created_at")));
        obj->setUpdatedAt(DateTimeUtil::parseDateTime(res->getString ("updated_at")));

        auto pltCostModel = MySqlDriver::getString( res, "platform_cost_model");
        if (pltCostModel.empty()) {
                obj->platformCostModel = std::make_shared<AdvertiserCostModel>();
        } else {
                obj->platformCostModel = AdvertiserCostModel::parseCostModel(pltCostModel);
        }

        auto advCostModel = MySqlDriver::getString( res, "adv_cost_model");
        if (advCostModel.empty()) {
                obj->advCostModel = std::make_shared<AdvertiserCostModel>();
        } else {
                obj->advCostModel = AdvertiserCostModel::parseCostModel(advCostModel);
        }

        MLOG(3) << "reading campaign from mysql " << obj->toJson ();
        return obj;
}

std::string MySqlCampaignService::getReadByIdSqlStatement(int id) {
        std::string query = "SELECT "
                            " `id`,"
                            + allFields +
                            " FROM campaign where id = '__id__'";


        query = StringUtil::replaceString (query, "__id__", StringUtil::toStr(id));
        return query;
}

void MySqlCampaignService::updateCampaignStatus(std::shared_ptr<Campaign> cmp) {
        MLOG(3) << "updating cmp status to  " << cmp->getStatus() << " for campaignId : " << cmp->getId ();
        std::string query (
                "UPDATE campaign SET status = '__status__' WHERE id = '__id__';");
        query = StringUtil::replaceString (query, "__status__", cmp->getStatus());
        query = StringUtil::replaceString (query, "__id__", StringUtil::toStr (cmp->getId()));

        driver->executedUpdateStatement (query);
}

void MySqlCampaignService::deleteAll() {
        driver->deleteAll("campaign");
}

MySqlCampaignService::~MySqlCampaignService() {
}
