#ifndef CampaignRealTimeInfo_H
#define CampaignRealTimeInfo_H

#include "AtomicLong.h"
#include "AtomicDouble.h"
#include "JsonTypeDefs.h"

#include <vector>
#include <memory>
#include "Poco/DateTime.h"
class CampaignRealTimeInfo;


#include "Object.h"
/*
   This information resides in mysql databases, the campaign_real_time_info table
   in mysql has a record for every day, and tracks that day budget spent, plus the total budget spent by
   that date. this table will be populated by bidderPacer threads which are responsible for aggregating the
   hourly information for the campaign  real time info.
 */
class CampaignRealTimeInfo : public Object {

private:

public:
int id;
Poco::DateTime timeReported;  //in format YYYYMMDD
int campaignId;

std::shared_ptr<gicapods::AtomicLong> numOfImpressionsServedInCurrentDateUpToNow;  //this will be compared against cmp->dailyMaxImpression
std::shared_ptr<gicapods::AtomicLong> numOfImpressionsServedOverallUpToNow;  //this will be compared against cmp->maxImpression

std::shared_ptr<gicapods::AtomicDouble> platformCostSpentInCurrentDateUpToNow;  //this will be compared against cmp->dailyMaxBudget
std::shared_ptr<gicapods::AtomicDouble> platformCostSpentOverallUpToNow;  // this will be compared against cmp->maxBudget

std::string lastTimeAdShown;  //this should be populated using this function getNowInseconds(), because I compare it using the same function too


CampaignRealTimeInfo();

std::string toString();
static std::string getEntityName();

static std::shared_ptr<CampaignRealTimeInfo> fromJson(std::string jsonString);

std::string toJson();

virtual ~CampaignRealTimeInfo();

static void assign(std::shared_ptr<CampaignRealTimeInfo> from, std::shared_ptr<CampaignRealTimeInfo> to);

bool isEqual(std::shared_ptr<CampaignRealTimeInfo> with);


static std::shared_ptr<CampaignRealTimeInfo> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};

#endif
