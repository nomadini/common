
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"

#include "EntityToModuleStateStats.h"
#include "UnknownRequestHandler.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "HttpUtil.h"
#include <string>
#include <memory>
#include <boost/exception/all.hpp>

UnknownRequestHandler::UnknownRequestHandler(EntityToModuleStateStats* entityToModuleStateStats) {
								this->entityToModuleStateStats = entityToModuleStateStats;
								//we showStackTrace to see where this is being created from
								gicapods::Util::showStackTrace();
}

void UnknownRequestHandler::handleRequest(
								Poco::Net::HTTPServerRequest& request,
								Poco::Net::HTTPServerResponse& response) {
								LOG(ERROR) << "request.getURI() : " << request.getURI ();

								entityToModuleStateStats->addStateModuleForEntity (
																"ServingDataByUnknownHandler:URI:" + request.getURI (),
																"UnknownRequestHandler",
																"ALL",
																EntityToModuleStateStats::exception);


								std::string jsonResponse =
																"{\"status\": \"request is unknown, handler is not defined\"}";

								MLOG(3)<<"unknown response "<<jsonResponse;
								std::ostream& ostr = response.send();
								ostr << jsonResponse;
								return;

}
