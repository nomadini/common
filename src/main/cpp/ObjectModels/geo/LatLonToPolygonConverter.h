#ifndef LatLonToPolygonConverter_H
#define LatLonToPolygonConverter_H



#include <memory>
#include <string>
#include <vector>
#include <set>
class LatLonToPolygonConverter;
class EntityToModuleStateStats;
/**

   This snippet converts a circle to polygon

   source of original code is here  : https://github.com/arg20/circle-to-radius/blob/master/index.js


   The GeoJSON spec does not support circles. If you wish to create an area that represents a circle, your best bet is to create a polygon that roughly approximates the circle. In the limit of the number of edges becoming infinite, your Polygon will match a circle.
   this is in Javascript, this class will implement this in c++

   Usage in Javascript
   //////////////////////////////////////////////////////////////////////////////////////////////
   const circleToPolygon = require('circle-to-polygon');

   const coordinates = [-27.4575887, -58.99029]; //[lon, lat]
   const radius = 100;                           // in meters
   const numberOfEdges = 32;                     //optional that defaults to 32

   let polygon = circleToPolygon(coordinates, radius, numberOfEdges);

   \/*
 *
   polygon:

   type = 'Polygon'
   coordinates = [ [ [ -27.457588699999995, -58.98939168471588 ],
                    [ -27.457248533454194, -58.98940894514629 ],
                    [ -27.456921438327615, -58.9894600631796 ],
                    [ -27.45661998386292, -58.98954307452298 ],
                    [ -27.4563557542251, -58.989654789313285 ],
                    [ -27.456138903413915, -58.98979091466893 ],
                    [ -27.45597776508136, -58.98994621962397 ],
                    [ -27.45587853224417, -58.99011473611232 ],
                    [ -27.455845019204208, -58.99028998828441 ],
                    [ -27.455878514839235, -58.99046524134828 ],
                    [ -27.455977732921237, -58.99063376037627 ],
                    [ -27.456138861394688, -58.99078906913211 ],
                    [ -27.456355708743818, -58.990925198971134 ],
                    [ -27.45661994184369, -58.99103691824481 ],
                    [ -27.45692140616749, -58.99111993338897 ],
                    [ -27.457248516049262, -58.99117105396192 ],
                    [ -27.457588699999995, -58.99118831528412 ],
                    [ -27.45792888395073, -58.99117105396192 ],
                    [ -27.458255993832502, -58.99111993338897 ],
                    [ -27.458557458156303, -58.99103691824481 ],
                    [ -27.458821691256173, -58.990925198971134 ],
                    [ -27.459038538605306, -58.99078906913211 ],
                    [ -27.45919966707875, -58.99063376037627 ],
                    [ -27.459298885160756, -58.99046524134828 ],
                    [ -27.459332380795786, -58.99028998828441 ],
                    [ -27.459298867755823, -58.99011473611232 ],
                    [ -27.459199634918626, -58.98994621962397 ],
                    [ -27.45903849658608, -58.98979091466893 ],
                    [ -27.45882164577489, -58.989654789313285 ],
                    [ -27.458557416137072, -58.98954307452298 ],
                    [ -27.45825596167238, -58.9894600631796 ],
                    [ -27.4579288665458, -58.98940894514629 ],
                    [ -27.457588699999995, -58.98939168471588 ] ] ]






   //////////////////// CODE ///////////////
   function toRadians(angleInDegrees) {
   return angleInDegrees * Math.PI / 180;
   }

   function toDegrees(angleInRadians) {
   return angleInRadians * 180 / Math.PI;
   }

   function offset(c1, distance, bearing) {
   var lat1 = toRadians(c1[1]);
   var lon1 = toRadians(c1[0]);
   var dByR = distance / 6378137; // distance divided by 6378137 (radius of the earth) wgs84
   var lat = Math.asin(
    Math.sin(lat1) * Math.cos(dByR) +
    Math.cos(lat1) * Math.sin(dByR) * Math.cos(bearing));
   var lon = lon1 + Math.atan2(
      Math.sin(bearing) * Math.sin(dByR) * Math.cos(lat1),
      Math.cos(dByR) - Math.sin(lat1) * Math.sin(lat));
   return [toDegrees(lon), toDegrees(lat)];
   }

   module.exports = function circleToPolygon(center, radius, numberOfSegments) {
   var n = numberOfSegments ? numberOfSegments : 32;
   var flatCoordinates = [];
   var coordinates = [];
   for (let i = 0; i < n; ++i) {
    flatCoordinates.push.apply(flatCoordinates, offset(center, radius, 2 * Math.PI * i / n));
   }
   flatCoordinates.push(flatCoordinates[0], flatCoordinates[1]);

   for (let i = 0, j = 0; j < flatCoordinates.length; j += 2) {
    coordinates[i++] = flatCoordinates.slice(j, j + 2);
   }

   return {
    type: 'Polygon',
    coordinates: [coordinates]
   };
   };
 */
class LatLonToPolygonConverter {

public:
EntityToModuleStateStats* entityToModuleStateStats;

LatLonToPolygonConverter(EntityToModuleStateStats* entityToModuleStateStats);

virtual ~LatLonToPolygonConverter();

double toRadians(double angleInDegrees);
double toDegrees(double angleInRadians);
std::tuple<double, double> offset(double lonArg, double latArg, int distance, double bearing);
std::string circleToPolygon(double lon, double lat, int radius, int numberOfSegments);

};

#endif
