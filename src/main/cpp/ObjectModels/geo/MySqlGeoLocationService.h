#ifndef MySqlGeoLocationService_h
#define MySqlGeoLocationService_h




#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "GeoLocation.h"
#include "MySqlGeoLocationService.h"
#include "DataProvider.h"
#include "HttpUtilService.h"
class MySqlGeoLocationService;




class MySqlGeoLocationService : public DataProvider<GeoLocation> {

public:
std::shared_ptr<std::vector<std::shared_ptr<GeoLocation> > > allGeoLocations;
std::shared_ptr<std::unordered_map<int, std::shared_ptr<GeoLocation> > > allGeoLocationsMap;

MySqlDriver* driver;

std::string dataMasterUrl;

HttpUtilService* httpUtilService;

MySqlGeoLocationService(MySqlDriver* driver,
                        HttpUtilService* httpUtilService,
                        std::string dataMasterUrl);

std::vector<std::shared_ptr<GeoLocation> > readAllEntities();

std::shared_ptr<GeoLocation> read(int id);

std::vector<std::shared_ptr<GeoLocation> > readAllWithIds(std::vector<int> allGeoLocationIds);

void update(std::shared_ptr<GeoLocation> obj);

void insert(std::shared_ptr<GeoLocation> obj);

virtual void deleteAll();

virtual ~MySqlGeoLocationService();

std::shared_ptr<GeoLocation> getObjectFromResultSet(std::shared_ptr<ResultSetHolder> res);

std::shared_ptr<std::vector<std::shared_ptr<GeoLocation> > > getAllGeoLocations();

std::shared_ptr<std::unordered_map<int, std::shared_ptr<GeoLocation> > >  getAllGeoLocationsMap();

};


#endif
