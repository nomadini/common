#ifndef LatLonToMGRSConverter_H
#define LatLonToMGRSConverter_H



#include <memory>
#include <string>
#include <vector>
#include <set>
class LatLonToMGRSConverter;
class EntityToModuleStateStats;


class LatLonToMGRSConverter {

public:
EntityToModuleStateStats* entityToModuleStateStats;

LatLonToMGRSConverter(EntityToModuleStateStats* entityToModuleStateStats);

virtual ~LatLonToMGRSConverter();

std::string convert(double lat, double lon, int percisionLevel = 3);
std::tuple<std::string, std::string, std::string> convertToAllMGPRSValues(double lat, double lon);
void example();
};

#endif
