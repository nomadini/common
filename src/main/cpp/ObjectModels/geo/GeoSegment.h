#ifndef GeoSegment_H
#define GeoSegment_H

#include <memory>
#include <string>
#include <vector>
#include <unordered_map>
#include "Object.h"

class GeoSegment : public Object {

private:

public:
int id;
std::string description;
std::string segmentName;
double lat;
double lon;
double segmentRadiusInMile;
int geoSegmentListId;
GeoSegment();

std::string toString();

std::string toJson();

virtual ~GeoSegment();
};


#endif
