#ifndef GeoLocationCacheService_h
#define GeoLocationCacheService_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>

#include "GeoLocation.h"
#include "HttpUtilService.h"
#include "GeoLocation.h"
#include "CacheService.h"
#include "ObjectVectorHolder.h"
#include "EntityProviderService.h"
#include "ConcurrentHashMap.h"
class MySqlGeoLocationService;
class GeoLocation;
class EntityToModuleStateStats;

class GeoLocationCacheService;



class GeoLocationCacheService : public CacheService<GeoLocation> {

private:


public:
EntityToModuleStateStats* entityToModuleStateStats;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, GeoLocation> > mapOfStateNamesToGeoLocations;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, GeoLocation> > mapOfCountryNamesToGeoLocations;

GeoLocationCacheService(
        MySqlGeoLocationService* mySqlGeoLocationService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::string appName);

void clearOtherCaches();

void populateOtherMapsAndLists(std::vector<std::shared_ptr<GeoLocation> > allEntities);

std::string getEntityName();

virtual ~GeoLocationCacheService();
};


#endif
