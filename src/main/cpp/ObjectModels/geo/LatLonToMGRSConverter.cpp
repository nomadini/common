#include "LatLonToMGRSConverter.h"
#include "CollectionUtil.h"
#include "EntityToModuleStateStats.h"
#include "StringUtil.h"
#include "GUtil.h"

#include <iostream>
#include <GeographicLib/UTMUPS.hpp>
#include <GeographicLib/MGRS.hpp>
using namespace GeographicLib;

LatLonToMGRSConverter::LatLonToMGRSConverter(EntityToModuleStateStats* entityToModuleStateStats) {
        this->entityToModuleStateStats = entityToModuleStateStats;
}

LatLonToMGRSConverter::~LatLonToMGRSConverter() {

}

void LatLonToMGRSConverter::example() {

        double lat = 33.3, lon = 44.4; // Baghdad
        double lat1 = 33.34, lon1 = 44.45; // Baghdad1
        double lat2 = 33.37, lon2 = 44.48; // Baghdad1
        for (int i=-1; i< 12; i++) {
                std::cout<<"percision level : "<< i <<std::endl;
                convert(lat, lon,i);
                convert(lat1, lon1,i);
                convert(lat2, lon2,i);
                std::cout<<std::endl;
        }

}
/*

   docs for library : http://geographiclib.sourceforge.net/html/MGRS_8hpp_source.html
   MGRS doc : https://en.wikipedia.org/wiki/Military_grid_reference_system
   4Q .....................GZD only, precision level 6° × 8° (in most cases)
   4QFJ ...................GZD and 100 km Grid Square ID, precision level 100 km  --> percisionLevel = 0
   4QFJ 1 6 ...............precision level 10 km                                  --> percisionLevel = 1
   4QFJ 12 67 .............precision level 1 km                                   --> percisionLevel = 2
   4QFJ 123 678 ...........precision level 100 m                                  --> percisionLevel = 3
   4QFJ 1234 6789 .........precision level 10 m                                   --> percisionLevel = 4
   4QFJ 12345 67890 .......precision level 1 m                                    --> percisionLevel = 5

 */
std::string LatLonToMGRSConverter::convert(double lat, double lon, int percisionLevel) {
        assertAndThrow(percisionLevel >= -1 && percisionLevel <= 11);
        int zone;
        bool northp;
        double x, y;
        UTMUPS::Forward(lat, lon, zone, northp, x, y);
        std::string mgrs;
        MGRS::Forward(zone, northp, x, y, lat, percisionLevel, mgrs);
        // std::cout << mgrs << "\n";
        return mgrs;
}

std::tuple<std::string, std::string, std::string>
LatLonToMGRSConverter::convertToAllMGPRSValues(double lat, double lon) {

        std::tuple<std::string, std::string, std::string> tuple = std::make_tuple("", "", "");
        std::get<0>(tuple) = convert(lat, lon,2); //level 1 km
        std::get<1>(tuple) = convert(lat, lon,3); //level 100 m
        std::get<2>(tuple) = convert(lat, lon,4); //level 10 m

        return tuple;
}
