#include "GUtil.h"
#include "MySqlDriver.h"
#include "CollectionUtil.h"
#include "MySqlGeoLocationService.h"
#include "MySqlDriver.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "GeoLocation.h"
#include <boost/foreach.hpp>
#include "JsonArrayUtil.h"

MySqlGeoLocationService::MySqlGeoLocationService(MySqlDriver* driver,
                                                 HttpUtilService* httpUtilService,
                                                 std::string dataMasterUrl) {
        this->driver = driver;
        this->dataMasterUrl = dataMasterUrl;
        this->httpUtilService = httpUtilService;
        this->allGeoLocations = std::make_shared<std::vector<std::shared_ptr<GeoLocation> > > ();
        this->allGeoLocationsMap = std::make_shared<std::unordered_map<int, std::shared_ptr<GeoLocation> > > ();
}

std::vector<std::shared_ptr<GeoLocation> > MySqlGeoLocationService::readAllWithIds(std::vector<int> allGeoLocationIds) {
        assertAndThrow(!allGeoLocationIds.empty());
        std::vector<std::shared_ptr<GeoLocation> > geoLocationsList;
        std::string queryStr = "SELECT `ID`,"
                               " `country`,"
                               " `state`,"
                               " `city`,"
                               " `created_at`,"
                               " `updated_at`"
                               " FROM `geolocation` where ID in (__IDS__) ;";


        auto idsInStr = CollectionUtil::toCommaSeperatedString(allGeoLocationIds);

        queryStr = StringUtil::replaceString (queryStr, "__IDS__", idsInStr);
        auto res = driver->executeQuery (queryStr);
        while (res->next ()) {
                geoLocationsList.push_back (getObjectFromResultSet(res));
        }

        return geoLocationsList;
}

std::vector<std::shared_ptr<GeoLocation> > MySqlGeoLocationService::readAllEntities() {

        std::vector<std::shared_ptr<GeoLocation> > geoLocationsList;
        std::string query = "SELECT `ID`,"
                            " `country`,"
                            " `state`,"
                            " `city`,"
                            " `created_at`,"
                            " `updated_at`"
                            " FROM `geolocation`;";



        auto res = driver->executeQuery (query);

        while (res->next ()) {
                geoLocationsList.push_back (getObjectFromResultSet(res));

        }

        return geoLocationsList;
}

std::shared_ptr<GeoLocation> MySqlGeoLocationService::read(int id) {
        std::string query = "SELECT `ID`,"
                            " `country`,"
                            " `state`,"
                            " `city`,"
                            " `created_at`,"
                            " `updated_at`"
                            " FROM `geolocation`;";



        auto res = driver->executeQuery (query);

        while (res->next ()) {
                return getObjectFromResultSet(res);
        }


        throwEx("no geolocation found for id " + StringUtil::toStr (id));
}

std::shared_ptr<GeoLocation> MySqlGeoLocationService::getObjectFromResultSet(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<GeoLocation> geoLocationPtr = std::make_shared<GeoLocation> ();

        MLOG(3) << "geolocation obj loaded id : " << res->getInt (1);   // getInt(1) returns the first colum
        int id = res->getInt (1);   //this is done for the converting to map function
        std::string country = MySqlDriver::getString( res, 2);
        std::string state = MySqlDriver::getString( res, 3);
        std::string city = MySqlDriver::getString( res, 4);
        geoLocationPtr->id = id;
        geoLocationPtr->setCountry(country);
        geoLocationPtr->setState(state);
        geoLocationPtr->setCity(city);
        return geoLocationPtr;
}

void MySqlGeoLocationService::update(std::shared_ptr<GeoLocation> obj) {
        throwEx("not implemented");

}

void MySqlGeoLocationService::insert(std::shared_ptr<GeoLocation> obj) {

        MLOG(3) << "inserting new GeoLocation in db : " << obj->toJson ();
        std::string queryStr =
                "INSERT INTO geolocation"
                " ("
                " country,"
                " state,"
                " city,"
                " created_at,"
                " updated_at)  "
                " VALUES"
                " ("
                " '__country__',"
                " '__state__',"
                " '__city__',"
                " '__created_at__',"
                " '__updated_at__'"
                " );";

        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "__country__",
                                              StringUtil::toLowerCase (obj->getCountry()));
        queryStr = StringUtil::replaceString (queryStr, "__state__",
                                              StringUtil::toLowerCase (obj->getState()));
        queryStr = StringUtil::replaceString (queryStr, "__city__", StringUtil::toLowerCase (obj->getCity()));

        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);

        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);


        //MLOG(3) << "queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);
        obj->id = driver->getLastInsertedId ();


}

void MySqlGeoLocationService::deleteAll() {
        driver->deleteAll("geolocation");
}


std::shared_ptr<std::vector<std::shared_ptr<GeoLocation> > > MySqlGeoLocationService::getAllGeoLocations() {
        return allGeoLocations;
}


std::shared_ptr<std::unordered_map<int, std::shared_ptr<GeoLocation> > > MySqlGeoLocationService::getAllGeoLocationsMap() {
        return allGeoLocationsMap;
}

MySqlGeoLocationService::~MySqlGeoLocationService() {

}
