#include "GUtil.h"
#include "MySqlDriver.h"
#include "CollectionUtil.h"
#include "MySqlGeoSegmentListService.h"
#include "MySqlDriver.h"
#include "MySqlGeoSegmentService.h"
#include "MySqlDriver.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include <boost/foreach.hpp>
#include "JsonArrayUtil.h"
MySqlGeoSegmentListService::MySqlGeoSegmentListService(MySqlDriver* driver,
                                                       HttpUtilService* httpUtilService,
                                                       std::string dataMasterUrl) {
        this->dataMasterUrl = dataMasterUrl;
        this->httpUtilService = httpUtilService;
        this->driver = driver;
        mySqlGeoSegmentService = std::make_shared<MySqlGeoSegmentService> (driver);
        this->allGeoSegmentList = std::make_shared<std::vector<std::shared_ptr<GeoSegmentList>> >();
        this->allGeoSegmentListMap = std::make_shared<std::unordered_map<int, std::shared_ptr<GeoSegmentList>>>  ();
}

MySqlGeoSegmentListService::~MySqlGeoSegmentListService() {

}

std::vector<std::shared_ptr<GeoSegmentList>> MySqlGeoSegmentListService::readAll() {
        std::vector<std::shared_ptr<GeoSegmentList>> all;
        std::string query = "SELECT `id`,"
                            "`name`,"
                            "status, "
                            "`advertiser_id`,"
                            "`datecreated`,"
                            "`datemodified`"
                            "FROM `geosegmentList`;";



        auto res = driver->executeQuery (query);

        while (res->next ()) {

                std::shared_ptr<GeoSegmentList> geoSegmentList = std::make_shared<GeoSegmentList> ();
                geoSegmentList->id = res->getInt (1);
                geoSegmentList->name = MySqlDriver::getString( res, 2);
                geoSegmentList->status = MySqlDriver::getString( res, 3);
                geoSegmentList->advertiserId = res->getInt (4);
                geoSegmentList->geoSegments = mySqlGeoSegmentService->readAllByListId (geoSegmentList->id);

                all.push_back (geoSegmentList);

        }

        return all;
}

std::shared_ptr<GeoSegmentList> MySqlGeoSegmentListService::read(int id) {
        std::shared_ptr<GeoSegmentList> obj  = std::make_shared<GeoSegmentList> ();
        std::string query = "SELECT `ID`," //1
                            " `NAME`," //2
                            " `ADVERTISER_ID`," //3
                            " `status`," //5
                            " `created_at`," //6
                            " `updated_at`" //7
                            " FROM `geosegmentlist` where ID = '__ID__'";


        query = StringUtil::replaceString (query, "__ID__", StringUtil::toStr (id));

        auto res = driver->executeQuery (query);

        while (res->next ()) {
                obj->id = res->getInt (1);
                obj->name = MySqlDriver::getString( res, 2);
                obj->advertiserId = res->getInt (3);
                obj->status = MySqlDriver::getString( res, 4);
        }


        return obj;

}

void MySqlGeoSegmentListService::update(std::shared_ptr<GeoSegmentList> obj) {
        throwEx("not implemented");

}

void MySqlGeoSegmentListService::insert(std::shared_ptr<GeoSegmentList> obj) {

        MLOG(3) << "inserting new GeoSegmentList in db : " << obj->toJson ();
        std::string queryStr =
                "INSERT INTO geosegmentlist"
                " ("
                " NAME,"
                " ADVERTISER_ID,"
                " STATUS,"
                " created_at,"
                " updated_at)  "
                " VALUES"
                " ("
                " '__NAME__',"
                " '__ADVERTISER_ID__',"
                " '__STATUS__',"
                " '__created_at__',"
                " '__updated_at__'"
                " );";

        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "__NAME__",
                                              obj->name);
        queryStr = StringUtil::replaceString (queryStr, "__ADVERTISER_ID__",
                                              StringUtil::toStr (obj->advertiserId));
        queryStr = StringUtil::replaceString (queryStr, "__STATUS__", obj->status);

        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);

        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);


        //MLOG(3) << "queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);
        obj->id = driver->getLastInsertedId ();

        for(std::shared_ptr<GeoSegment> geoSegment :  obj->geoSegments) {
                mySqlGeoSegmentService->insert (geoSegment);
        }


}

void MySqlGeoSegmentListService::deleteAll() {
        driver->deleteAll("geosegmentlist");

}

std::shared_ptr<std::vector<std::shared_ptr<GeoSegmentList>> > MySqlGeoSegmentListService::getAllGeoSegmentLists() {
        return allGeoSegmentList;
}

std::shared_ptr<std::unordered_map<int, std::shared_ptr<GeoSegmentList>>> MySqlGeoSegmentListService::getAllGeoSegmentListMap() {
        return allGeoSegmentListMap;
}
