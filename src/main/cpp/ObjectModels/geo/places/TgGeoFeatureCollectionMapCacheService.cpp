#include "TargetGroup.h"
#include "StringUtil.h"
#include <boost/foreach.hpp>
#include "CollectionUtil.h"
#include "TgGeoFeatureCollectionMapCacheService.h"
#include "MySqlTargetGroupService.h"
#include "MySqlCreativeService.h"
#include "MySqlTgGeoFeatureCollectionMapService.h"
#include "JsonArrayUtil.h"
#include "JsonUtil.h"
TgGeoFeatureCollectionMapCacheService::TgGeoFeatureCollectionMapCacheService(
        MySqlTgGeoFeatureCollectionMapService* mySqlTgGeoFeatureCollectionMapService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,std::string appName)  :
        CacheService<TgGeoFeatureCollectionMap>
                (httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName),
        Object(__FILE__) {

        this->mySqlTgGeoFeatureCollectionMapService= mySqlTgGeoFeatureCollectionMapService;
        this->entityToModuleStateStats = entityToModuleStateStats;
}

void TgGeoFeatureCollectionMapCacheService::clearOtherCaches() {

}

void TgGeoFeatureCollectionMapCacheService::populateOtherMapsAndLists(std::vector<std::shared_ptr<TgGeoFeatureCollectionMap> > allMappings) {
        NULL_CHECK(targetGroupIdsWithGeoCollectionAssignments);
        NULL_CHECK(allTgGeoFeatureCollectionKeys);

        entityToModuleStateStats->addStateModuleForEntity ("TG_GEO_FEATURE_COLLECTION_MAP_READ_FROM_DB_SIZE : " + StringUtil::toStr(allMappings.size()),
                                                           "TgGeoFeatureCollectionMapCacheService",
                                                           "ALL");
        for(std::shared_ptr<TgGeoFeatureCollectionMap> mapping :  allMappings) {
                allTgGeoFeatureCollectionKeys->insert(mapping->tgGeoCollectionKey);
                targetGroupIdsWithGeoCollectionAssignments->insert(mapping->targetGroupId);
        }
}

TgGeoFeatureCollectionMapCacheService::~TgGeoFeatureCollectionMapCacheService() {

}
