
#include "TgGeoFeatureCollectionMap.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
TgGeoFeatureCollectionMap::TgGeoFeatureCollectionMap() : Object(__FILE__) {
        id = 0;
        targetGroupId = 0;
        geoFeatureCollectionId = 0;
        tgGeoCollectionKey = "uninitialized";
}


std::string TgGeoFeatureCollectionMap::toString() {
        return this->toJson();
}

std::string TgGeoFeatureCollectionMap::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}
std::string TgGeoFeatureCollectionMap::getEntityName() {
        return "TgGeoFeatureCollectionMap";
}
TgGeoFeatureCollectionMap::~TgGeoFeatureCollectionMap() {

}


std::shared_ptr<TgGeoFeatureCollectionMap> TgGeoFeatureCollectionMap::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<TgGeoFeatureCollectionMap> geoFeature = std::make_shared<TgGeoFeatureCollectionMap>();

        geoFeature->id = JsonUtil::GetIntSafely(value, "id");
        geoFeature->targetGroupId = JsonUtil::GetIntSafely(value, "targetGroupId");
        geoFeature->geoFeatureCollectionId = JsonUtil::GetIntSafely(value, "geoFeatureCollectionId");
        geoFeature->tgGeoCollectionKey = JsonUtil::GetStringSafely(value, "tgGeoCollectionKey");
        geoFeature->createdAt = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"createdAt"));
        geoFeature->updatedAt = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"updatedAt"));

        return geoFeature;
}

void TgGeoFeatureCollectionMap::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "targetGroupId", targetGroupId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "geoFeatureCollectionId", geoFeatureCollectionId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "tgGeoCollectionKey", tgGeoCollectionKey, value);
        JsonUtil::addMemberToValue_FromPair (doc, "createdAt", DateTimeUtil::dateTimeToStr(createdAt), value);
        JsonUtil::addMemberToValue_FromPair (doc, "updatedAt", DateTimeUtil::dateTimeToStr(updatedAt), value);

}
