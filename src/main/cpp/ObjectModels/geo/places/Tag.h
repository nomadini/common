/*
 * Tag.h
 *
 *  Created on: Aug 27, 2015
 *      Author: mtaabodi
 */

#ifndef Tag_H_
#define Tag_H_


#include <memory>
#include <string>
#include <vector>
#include <unordered_map>

#include "JsonTypeDefs.h"

#include "Poco/DateTime.h"
#include "Object.h"

class Tag;


class Tag : public Object {

public:

int id;
int parentId;
std::string name;
std::string description;


Tag();

std::string toString();

std::string toJson();

static std::shared_ptr<Tag> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
static std::shared_ptr<Tag> fromJson(std::string jsonString);
virtual ~Tag();

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_EVENTLOG_H_ */
