#include "GUtil.h"


#include "Tag.h"
#include "MySqlTagService.h"
#include "MySqlDriver.h"
#include "DateTimeUtil.h"

MySqlTagService::MySqlTagService(MySqlDriver* driver)
        : MySqlService(driver, nullptr),Object(__FILE__) {
        this->driver = driver;
}

std::vector<std::shared_ptr<Tag> > MySqlTagService::readAllEntities() {
        return this->readAll();
}
std::string MySqlTagService::getSelectAllQueryStatement() {
        static std::string selectAllQueryStatement = " SELECT "
                                                     " TAG.`ID`, "
                                                     " TAG.`PARENT_ID`, "
                                                     " TAG.`NAME`, "
                                                     " TAG.`DESCR`"
                                                     " FROM `TAG`";
        return selectAllQueryStatement;
}

std::string MySqlTagService::getInsertObjectSqlStatement(std::shared_ptr<Tag> tag) {
        throwEx("not supported");
}

std::shared_ptr<Tag> MySqlTagService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<Tag> obj = std::make_shared<Tag> ();
        obj->id = res->getInt ("ID");
        obj->parentId = res->getInt ("PARENT_ID");
        obj->description = MySqlDriver::getString( res, "DESCR");
        obj->name = MySqlDriver::getString( res, "NAME");
        MLOG(3) << "reading tag from mysql " << obj->toJson ();
        return obj;
}

std::string MySqlTagService::getReadByIdSqlStatement(int id) {
        std::string query =
                " SELECT "
                " TAG.`ID`, "
                " TAG.`PARENT_ID`, "
                " TAG.`NAME`, "
                " TAG.`DESCR`"
                " FROM `TAG`"
                " WHERE ID = '__id__'";


        query = StringUtil::replaceString (query, "__id__", StringUtil::toStr(id));
        return query;
}

void MySqlTagService::deleteAll() {
        throwEx("not supported");
}

MySqlTagService::~MySqlTagService() {
}
