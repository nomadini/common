#include "GUtil.h"


#include "TagPlace.h"
#include "MySqlTagPlaceService.h"
#include "MySqlDriver.h"
#include "DateTimeUtil.h"

MySqlTagPlaceService::MySqlTagPlaceService(MySqlDriver* driver)
        : MySqlService(driver, nullptr),Object(__FILE__) {
        this->driver = driver;
}

std::vector<std::shared_ptr<TagPlace> > MySqlTagPlaceService::readAllEntities() {
        return this->readAll();
}
std::string MySqlTagPlaceService::getSelectAllQueryStatement() {
        static std::string selectAllQueryStatement = " SELECT "
                                                     " TAG_PLACE.`PLACE_ID`, "
                                                     " TAG_PLACE.`TAG_ID` "
                                                     " FROM `TAG_PLACE` LIMIT " + _toStr(maxNumberOfPlacesToLoad->getValue());
        return selectAllQueryStatement;
}

std::string MySqlTagPlaceService::getInsertObjectSqlStatement(std::shared_ptr<TagPlace> deal) {
        throwEx("not supported");
}

std::shared_ptr<TagPlace> MySqlTagPlaceService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<TagPlace> obj = std::make_shared<TagPlace> ();
        obj->placeId = res->getInt ("PLACE_ID");
        obj->tagId = res->getInt ("TAG_ID");
        MLOG(3) << "reading deal from mysql " << obj->toJson ();
        return obj;
}

std::string MySqlTagPlaceService::getReadByIdSqlStatement(int id) {
        throwEx("not supported");
}

void MySqlTagPlaceService::deleteAll() {
        throwEx("not supported");
}

MySqlTagPlaceService::~MySqlTagPlaceService() {
}
