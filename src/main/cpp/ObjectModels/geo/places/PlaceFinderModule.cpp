#include "PlaceFinderModule.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "DateTimeUtil.h"

#include "EntityToModuleStateStats.h"
#include "JsonArrayUtil.h"
#include "Place.h"
#include "TreeOfPlacesHolder.h"

PlaceFinderModule::PlaceFinderModule(std::shared_ptr<TreeOfPlacesHolder> treeOfPlacesHolder) : Object(__FILE__) {
								this->treeOfPlacesHolder = treeOfPlacesHolder;
}

PlaceFinderModule::~PlaceFinderModule() {

}


std::vector<std::shared_ptr<Place> > PlaceFinderModule::findNearestFeatures(
								double deviceLat,
								double deviceLon,
								int maxNumebrOfWantedFeatures) {

								std::vector<std::shared_ptr<Place> > indexesOfNearestFeatures;

								std::vector<BoxToPlaceValue> nearestResultSet =
																treeOfPlacesHolder->getNearestPlaces(deviceLat, deviceLon, maxNumebrOfWantedFeatures);
								MLOG(2)<< "nearestResultSet size "<< nearestResultSet.size();

								for(BoxToPlaceValue const& v :  nearestResultSet) {
																MLOG(2)<<"nearest geofeatures : "<< v.second->toJson() << std::endl;
																indexesOfNearestFeatures.push_back(v.second);
								}


								std::vector<BoxToPlaceValue> intersectingResultSet =
																treeOfPlacesHolder->getIntersectingPlaces(deviceLat, deviceLon, maxNumebrOfWantedFeatures);
								MLOG(2)<< "intersectingResultSet size "<< intersectingResultSet.size();


								for(BoxToPlaceValue const& v :  intersectingResultSet) {
																MLOG(2)<<"intersecting geofeatures : "<< v.second->toJson() << std::endl;
																indexesOfNearestFeatures.push_back(v.second);
								}

								//this is another way of doing this
								// Box query_box(Point(0, 0), Point(5, 5));
								// treeOfPlacesHolder->treeOfPlaces->query(boost::geometry::index::intersects(query_box), std::back_inserter(result_s));


								return indexesOfNearestFeatures;
}

std::vector<std::shared_ptr<Place> > PlaceFinderModule::findIntersectingFeatures(double deviceLat, double deviceLon, int maxNumebrOfWantedFeatures) {
								std::vector<std::shared_ptr<Place> > indexesOfIntersectingFeatures;

								// find 5 nearest RTreeValue to a Point
								// std::vector<RTreeValue> result_n;
								// treeOfPlacesHolder->treeOfPlaces->query(boost::geometry::index::nearest(Point(0, 0), maxNumebrOfWantedFeatures), std::back_inserter(result_n));

								return indexesOfIntersectingFeatures;
}
