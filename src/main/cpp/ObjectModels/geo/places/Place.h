/*
 * Place.h
 *
 *  Created on: Aug 27, 2015
 *      Author: mtaabodi
 */

#ifndef Place_H_
#define Place_H_


#include <memory>
#include <string>
#include <vector>
#include <unordered_map>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/geometries/polygon.hpp>

#include <boost/geometry/index/rtree.hpp>
#include "JsonTypeDefs.h"


#include "Poco/DateTime.h"
#include "Object.h"
namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

#include <boost/geometry/geometries/polygon.hpp>
class Place;
class Tag;

typedef bg::model::point<float, 2, bg::cs::cartesian> Point;
// typedef bg::model::point<float, 2, bg::cs::geographic<bg::degree>> Point;
typedef bg::model::box<Point> Box;
typedef bg::model::polygon<Point, false, false> Polygon; // ccw, open polygon
typedef std::pair<Box, unsigned> RTreeValue;
typedef std::pair<Box, std::shared_ptr<Place> > BoxToPlaceValue;
typedef boost::geometry::index::rtree< BoxToPlaceValue, boost::geometry::index::rstar<16, 4> > TreeOfPlacesType;



class Place : public Object {

public:


int id;
double radius;
double lon;
double lat;
std::string description;
std::string address;
std::string city;
std::string state;
std::string postalCode;
std::string polygonStr;
std::shared_ptr<Polygon> polygon;
std::shared_ptr<Polygon> constructPolygon(std::vector<std::pair<float,float> > points);
std::vector<std::pair<float,float> > extractPointsFromFeatureInJson(std::string featuresInJson);

//these are the tags that this place belongs to
std::vector<std::shared_ptr<Tag> > tags;

Place();

std::string toString();

std::string toJson();

static std::shared_ptr<Place> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
static std::shared_ptr<Place> fromJson(std::string jsonString);
virtual ~Place();

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_EVENTLOG_H_ */
