//
// Created by Mahmoud Taabodi on 2/14/16.
//

#include "MaxMindService.h"
#include <string.h>
#include "GUtil.h"
#include "StringUtil.h"


MaxMindService::MaxMindService() {
        this->mmdb = std::make_shared<MMDB_s> ();
}
MaxMindService::~MaxMindService() {

}

void MaxMindService::closeDatabase() {
        MMDB_close(&(*mmdb));
}

void MaxMindService::openDatabase(std::string fname) {

        int status = MMDB_open(fname.c_str(), MMDB_MODE_MMAP, &(*mmdb));

        if (MMDB_SUCCESS != status) {
                throwEx("failed to open database file : "+ fname);
        }

}

std::string MaxMindService::dup_entry_string_or_bail(MMDB_entry_data_s entry_data)
{
        char *string = (char*) strndup(entry_data.utf8_string, entry_data.data_size);
        if (NULL == string) {
                throwEx("strndup failed");
        }

        //THIS IS HOW YOU CONVERT CHAR* to std::string in a safe way
        //we pass free function as a deleter and create a shared_ptr of char
        //then from that we create std::string
        std::shared_ptr<char> p(string, &free);
        std::string str(p.get());
        return str;

}

bool MaxMindService::readIpInfo(const std::string& ipAddress, MMDB_lookup_result_s& result) {

        int gai_error, mmdb_error;
        result =
                MMDB_lookup_string(&(*mmdb), (char *) ipAddress.c_str(), &gai_error, &mmdb_error);

        if (0 != gai_error) {
                return false;
        }

        if (MMDB_SUCCESS != mmdb_error) {
                return false;
        }

        return true;
}

std::string MaxMindService::getCity(MMDB_lookup_result_s& result) {
        std::string empty;
        MMDB_entry_data_s entry_data;
        int status = MMDB_get_value(&result.entry, &entry_data,
                                    "city", "names", "en", NULL);

        if (MMDB_SUCCESS == status && entry_data.has_data) {

                if (entry_data.type == MMDB_DATA_TYPE_BOOLEAN) {

                } else if (entry_data.type == MMDB_DATA_TYPE_UINT32) {

                } else if (entry_data.type == MMDB_DATA_TYPE_UTF8_STRING) {
                        // VLOG(0) << "soosan : data is "<< dup_entry_string_or_bail(entry_data);
                        return dup_entry_string_or_bail(entry_data);
                }
                //MMDB_DATA_TYPE_DOUBLE
                //MMDB_DATA_TYPE_ARRAY

        }
        return empty;
}

double MaxMindService::getLatitude(MMDB_lookup_result_s& result) {
        MMDB_entry_data_s entry_data;
        int status = MMDB_get_value (&result.entry, &entry_data,
                                     "location", "latitude", NULL);

        if (MMDB_SUCCESS == status && entry_data.has_data
            && entry_data.type == MMDB_DATA_TYPE_DOUBLE) {
                return entry_data.double_value;
        }

        return 0;
}

double MaxMindService::getLongitude(MMDB_lookup_result_s& result) {
        MMDB_entry_data_s entry_data;
        int status = MMDB_get_value (&result.entry, &entry_data,
                                     "location", "longitude", NULL);

        if (MMDB_SUCCESS == status && entry_data.has_data
            && entry_data.type == MMDB_DATA_TYPE_DOUBLE) {
                return entry_data.double_value;
        }

        return 0;
}

int MaxMindService::getMetroCode(MMDB_lookup_result_s& result) {
        MMDB_entry_data_s entry_data;
        int status = MMDB_get_value (&result.entry, &entry_data,
                                     "location", "metro_code", NULL);

        if (MMDB_SUCCESS == status && entry_data.has_data
            && entry_data.type == MMDB_DATA_TYPE_UINT32) {
                return (int) entry_data.uint16;
        }

        return 0;
}

std::string MaxMindService::getTimeZone(MMDB_lookup_result_s& result) {
        std::string empty;
        MMDB_entry_data_s entry_data;
        int status = MMDB_get_value(&result.entry, &entry_data,
                                    "location", "time_zone", NULL);
        if (MMDB_SUCCESS == status && entry_data.has_data
            && entry_data.type == MMDB_DATA_TYPE_UTF8_STRING) {
                return dup_entry_string_or_bail(entry_data);
        }
        return empty;
}

std::string MaxMindService::getPostalCode(MMDB_lookup_result_s& result) {
        std::string empty;
        MMDB_entry_data_s entry_data;
        int status = MMDB_get_value(&result.entry, &entry_data,
                                    "postal", "code", NULL);
        if (MMDB_SUCCESS == status && entry_data.has_data
            && entry_data.type == MMDB_DATA_TYPE_UTF8_STRING) {
                return dup_entry_string_or_bail(entry_data);
        }
        return empty;
}

std::string MaxMindService::getStateName(MMDB_lookup_result_s& result) {
        std::string empty;
        MMDB_entry_data_s entry_data;
        int status = MMDB_get_value(&result.entry, &entry_data,
                                    "subdivisions", "0", "names", "en", NULL);
        if (MMDB_SUCCESS == status && entry_data.has_data
            && entry_data.type == MMDB_DATA_TYPE_UTF8_STRING) {
                return dup_entry_string_or_bail(entry_data);
        }
        return empty;
}

std::string MaxMindService::getStateCode(MMDB_lookup_result_s& result) {
        std::string empty;
        MMDB_entry_data_s entry_data;
        int status = MMDB_get_value(&result.entry, &entry_data,
                                    "subdivisions", "0", "iso_code", NULL);
        if (MMDB_SUCCESS == status && entry_data.has_data
            && entry_data.type == MMDB_DATA_TYPE_UTF8_STRING) {
                return dup_entry_string_or_bail(entry_data);
        }
        return empty;
}

std::string MaxMindService::getCountryCode(MMDB_lookup_result_s& result) {
        std::string empty;
        MMDB_entry_data_s entry_data;
        int status = MMDB_get_value(&result.entry, &entry_data,
                                    "country", "iso_code", NULL);
        if (MMDB_SUCCESS == status && entry_data.has_data
            && entry_data.type == MMDB_DATA_TYPE_UTF8_STRING) {
                return dup_entry_string_or_bail(entry_data);
        }
        return empty;
}

void testMaxMind()
{
        std::shared_ptr<MaxMindService> maxMindService = std::make_shared<MaxMindService>();

        const char* fname = "/home/maxmind_data/GeoIP2-City.mmdb";
        maxMindService->openDatabase(StringUtil::toStr(fname));

        char* ipstr = "38.126.130.44";//work
        ipstr = "69.206.241.208";//home
        MMDB_lookup_result_s result;
        maxMindService->readIpInfo(StringUtil::toStr(ipstr), result);

        MMDB_entry_data_list_s *entry_data_list = NULL;

        maxMindService->getCity(result);
        maxMindService->getCountryCode (result);
        maxMindService->getLatitude (result);
        maxMindService->getLongitude (result);
        maxMindService->getTimeZone (result);


        int exit_code = 0;
        if (result.found_entry) {
                int status = MMDB_get_entry_data_list(&result.entry,
                                                      &entry_data_list);

                if (MMDB_SUCCESS != status) {
                        fprintf(
                                stderr,
                                "Got an error looking up the entry data - %s\n",
                                MMDB_strerror(status));
                        exit_code = 4;
                        //test
                        goto end;
                }

                if (NULL != entry_data_list) {
                        MMDB_dump_entry_data_list(stdout, entry_data_list, 2);
                }
        } else {
                fprintf(
                        stderr,
                        "\n  No entry for this IP address (%s) was found\n\n",
                        ipstr);
                exit_code = 5;
        }
end:
        MMDB_free_entry_data_list(entry_data_list);
//    exit(exit_code);
}
