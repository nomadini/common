#ifndef TransAppMessage_H
#define TransAppMessage_H


#include "AdHistory.h"
#include "Object.h"
#include <string>
#include <memory>
class Device;

#include "Object.h"
class TransAppMessage;




class TransAppMessage : public std::enable_shared_from_this<TransAppMessage>, public Object {

private:

public:

std::string version;
std::string sourceAppName;
std::string destinationAppName;

std::string transactionId;
int chosenTargetGroupId;
int chosenCreativeId;

std::shared_ptr<AdHistory> adHistory;

double bidPrice;

std::string userTimeZone;
int userTimeZonDifferenceWithUTC;

std::string deviceUserAgent;
std::string deviceIp;
std::shared_ptr<Device> device;
double deviceLat;
double deviceLon;
std::string deviceCountry;
std::string deviceCity;
std::string deviceState;
std::string deviceZipcode;
std::string deviceIpAddress;
std::vector<std::string> siteCategory;
std::string siteDomain;
std::string sitePage;
std::string sitePublisherDomain;
std::string sitePublisherName;


TransAppMessage();

std::string toJson();

static std::shared_ptr<TransAppMessage> fromJson(std::string jsonString);

virtual ~TransAppMessage();

};

#endif
