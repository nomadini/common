//
// Created by Mahmoud Taabodi on 7/17/16.
//

#ifndef COMMON_CACHESERVICE_H
#define COMMON_CACHESERVICE_H

#include <memory>
#include <string>
#include <vector>
#include <set>

#include "HttpUtilService.h"
#include "DataProvider.h"
#include "CacheService.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "StringUtil.h"
#include "GUtil.h"
#include "ConcurrentHashMap.h"
#include "CacheService.h"
#include "Object.h"
#include <memory>
#include <string>
#include <vector>
#include <set>

#include "FileUtil.h"
#include "JsonUtil.h"
#include "JsonMapUtil.h"
#include "HttpUtilService.h"
#include "DataProvider.h"
#include "DateTimeMacro.h"
#include "ConcurrentHashMap.h"
#include "Object.h"

class EntityToModuleStateStats;

/*
   provides a general interface to all templated CacheService<T> classes
   for making it easy to work with them
 */
class CacheServiceInterface {
public:

virtual void reloadCachesViaHttp() = 0;
virtual void clearCaches() = 0;
virtual void clearOtherCaches() = 0;
virtual std::string getEntityName() = 0;
virtual std::string processReadAllByClient(
        std::string requestBody,
        int& numberOfEntities) = 0;
};

/*
   provides a facade for both CacheService<T>
   and CacheServiceServer<T>
 */
template<class T>
class CacheService : public CacheServiceInterface {


private:
//these are the data containers that this cache provides out of the box
std::shared_ptr<gicapods::ConcurrentHashMap<int, T> > allEntitiesMap;
std::shared_ptr<gicapods::ConcurrentHashMap<int, T> > allEntitiesMapPreviousVersion;

public:

std::string cacheFileNameOnDisk;
std::string generalCacheInfoFileName;
std::string currentHashOfData;
std::string dataMasterUrl;
std::string entityName;
std::string appName;

HttpUtilService* httpUtilService;
TimeType lastTimeCacheWasUpdatedInSecond;
EntityToModuleStateStats* entityToModuleStateStats;
int timeoutForHashCallInMillis;
int timeoutForDataCallInMillis;
CacheService(
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::string appName) {
        NULL_CHECK(httpUtilService);
        NULL_CHECK(entityToModuleStateStats);
        this->httpUtilService = httpUtilService;
        this->dataMasterUrl = dataMasterUrl;
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->entityName = T::getEntityName();
        allEntitiesMap = std::make_shared<gicapods::ConcurrentHashMap<int, T > >();
        allEntitiesMapPreviousVersion = std::make_shared<gicapods::ConcurrentHashMap<int, T> >();
        currentHashOfData = "HASH_NOT_CALCULATED_YET";
        FileUtil::createDirectory("/tmp/cache/" + appName);
        cacheFileNameOnDisk = "/tmp/cache/" + appName + "/" + getEntityName() + ".txt";
        generalCacheInfoFileName = "/tmp/cache/" + appName + "/" + getEntityName() + "-general-info.txt";
        timeoutForHashCallInMillis = 1000;
        timeoutForDataCallInMillis = 1000;
}


std::string getEntityName() {
        return entityName;
}


std::string prepareRequest(std::string entity, std::string function, std::string params) {
        DocumentPtr doc = std::make_shared<DocumentType> ();
        doc->SetObject ();
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        JsonUtil::addMemberToValue_FromPair (doc.get(), "entity", StringUtil::toStr(entity), value);
        JsonUtil::addMemberToValue_FromPair (doc.get(), "function",  StringUtil::toStr(function), value);
        JsonUtil::addMemberToValue_FromPair (doc.get(), "params",  StringUtil::toStr(params), value);

        return JsonUtil::valueToString (value);
}


std::vector<int> getAllEntityIds() {
        std::vector<int> allIds;
        auto map = this->allEntitiesMap->getCopyOfMap();
        for(auto pair : *map) {
                allIds.push_back (pair.second->id);
        }
        return allIds;
}


std::string getLatestHashOfDataFromServer() {
        std::string responseFromDataMaster;
        assertAndThrow(!dataMasterUrl.empty());
        try {
                MLOG(10) << "calling server for hash of entity : "<< getEntityName();
                auto request = prepareRequest (getEntityName(), "readHashOfAll", "");
                responseFromDataMaster = httpUtilService->sendPostRequest(
                        dataMasterUrl,
                        request,
                        timeoutForHashCallInMillis,
                        20);


        }  catch(std::exception const &e) {
                LOG(ERROR) << "hitting dataMasterUrl failed : "<< dataMasterUrl;
                gicapods::Util::showStackTrace(&e);
                entityToModuleStateStats->
                addStateModuleForEntity("ERROR_IN_GETTING_HASH_OF_DATA_OVER_HTTP",
                                        "CacheService" + getEntityName(),
                                        EntityToModuleStateStats::all,
                                        EntityToModuleStateStats::exception
                                        );
        }

        MLOG(10) << "responseFromDataMaster : "<< responseFromDataMaster;

        //if responseFromDataMaster is empty, we throw
        if (responseFromDataMaster.empty()) {
                throwEx("failed to get response from dataMaster");
        }

        MLOG(10) << "hash of data from server side : "<< responseFromDataMaster;
        return responseFromDataMaster;
}


void reloadCachesViaHttp() {
        //first we reload the caches from disk for faster startup of the app
        //this feature is very good for writing tests easily
        //dont ever remove it
        auto reloadedFromDisk = reloadCachesFromDisk();
        if (reloadedFromDisk == true) {
                MLOG(3) << "SKIPPING call to data master...using local data";
                return;
        }
        //now we attempt to reload from the http server
        std::string serverHash = getLatestHashOfDataFromServer();
        if (!StringUtil::equalsCaseSensitive(currentHashOfData, serverHash)) {
                MLOG(10) << getEntityName() << " : serverHash is different than clientHash "
                         <<" server : " << serverHash
                         <<" vs client : " <<currentHashOfData;
                getDataFromServerAndPopulateLocalStorage();
                //we persist the latest content to a file for later retrieval
                persistLatestCacheContentToDisk();
                //we update last time the cache was updated to check it for health later
                persistGeneralInfoOfCache();
        } else {
                MLOG(10)<< getEntityName() << " : current data has already been loaded....ignoring a reload";
        }



}


void persistGeneralInfoOfCache() {
        auto map =
                std::make_shared<std::unordered_map<std::string,std::string> > ();
        map->insert(std::make_pair("lastTimeRelodedDataFromServer",
                                   _toStr(DateTimeUtil::getNowInSecond())));
        auto jsonMapContent = JsonMapUtil::convertMapToJsonArray(*map);
        MLOG(3)<< getEntityName() << " going to persist general info cache to disk , jsonMapContent  : "<< jsonMapContent;
        FileUtil::writeStringToFile(generalCacheInfoFileName, jsonMapContent);
}


bool reloadCachesFromDisk() {

        auto generalInfoOfCache =  readGeneralInfoOfCache();
        auto foundPtr = generalInfoOfCache->find("lastTimeRelodedDataFromServer");
        if ( foundPtr ==
             generalInfoOfCache->end()) {
                throwEx("lastTimeRelodedDataFromServer is required");
        }
        auto ageOfPersistentData = DateTimeUtil::getNowInSecond() - ConverterUtil::convertTo<TimeType>(foundPtr->second);
        //todo : change this later
        int acceptableAgeOfPersistedDataInSeconds = 60;
        if ( ageOfPersistentData < acceptableAgeOfPersistedDataInSeconds) {
                MLOG(10) << "persistent cache is less than "
                         <<acceptableAgeOfPersistedDataInSeconds
                         <<" seconds..its still good to use";

        } else {
                MLOG(10) << "Persistent cached data is too old...reloading from dataMaster rather than disk";
                return false;
        }

        std::string cacheRawContent = FileUtil::readFileInString(cacheFileNameOnDisk);
        MLOG(10)<< getEntityName() << " : going to reload caches from disk , cacheRawContent : "<< cacheRawContent;

        if (cacheRawContent.empty()) {
                return false;
        }



        std::unordered_map<int, T>  allEntitiesMapRaw;
        JsonMapUtil::readMapFromJsonString(cacheRawContent, allEntitiesMapRaw);
        for (auto& pair : allEntitiesMapRaw) {
                int key = pair.first;
                std::shared_ptr<T> value = std::make_shared<T> (pair.second);
                allEntitiesMap->put(key, value);
        }
        MLOG(10)<< getEntityName() << " : loaded  "<< allEntitiesMap->size() << " entities from data master";
        return true;
}


virtual void clearOtherCaches() {

}


virtual void populateOtherMapsAndLists(std::vector<std::shared_ptr<T> > allEntities) {

}


std::shared_ptr<std::unordered_map<std::string,std::string> > readGeneralInfoOfCache() {

        std::string generalCacheInfoFileNameContent = FileUtil::readFileInString(generalCacheInfoFileName);
        auto map =
                std::make_shared<std::unordered_map<std::string,std::string> > ();

        if (generalCacheInfoFileNameContent.empty()) {
                map->insert(std::make_pair("lastTimeRelodedDataFromServer",
                                           _toStr(DateTimeUtil::getNowInSecond())));
                return map;
        }

        MLOG(10)<<"generalCacheInfoFileNameContent : "<< generalCacheInfoFileNameContent;
        std::unordered_map<std::string, std::string>  allGeneralInfoOnFile;
        JsonMapUtil::readMapFromJsonString(generalCacheInfoFileNameContent, allGeneralInfoOnFile);
        for (auto& pair : allGeneralInfoOnFile) {
                map->insert(std::make_pair( pair.first, pair.second));
        }

        return map;
}



void persistLatestCacheContentToDisk() {
        auto jsonMapContent = JsonMapUtil::convertMapToJsonArray(*allEntitiesMap);
        MLOG(10)<< getEntityName() << " : going to persist cache to disk , jsonMapContent  : "<< jsonMapContent;
        MLOG(10)<< getEntityName() << " : going to persist cache to disk , fileName  : "<< cacheFileNameOnDisk;
        FileUtil::writeStringToFile(cacheFileNameOnDisk, jsonMapContent);
}


void updateCurrentHashOfData(std::string dataAsJson) {
        currentHashOfData = StringUtil::hashStringBySha1(dataAsJson);
}


void getDataFromServerAndPopulateLocalStorage() {
        std::string responseFromDataMaster;
        assertAndThrow(!dataMasterUrl.empty());
        try {
                MLOG(10) << "calling server for entity : "<< getEntityName();
                auto request = prepareRequest (getEntityName(), "readAll", "");
                int numberOfTries = 60;
                responseFromDataMaster = httpUtilService->sendPostRequest (
                        dataMasterUrl,
                        request,
                        timeoutForDataCallInMillis,
                        numberOfTries);
        }  catch(std::exception const &e) {
                LOG(ERROR) << "hitting dataMasterUrl failed : "<< dataMasterUrl;
                // gicapods::Util::showStackTrace(&e);
                entityToModuleStateStats->
                addStateModuleForEntity("ERROR_IN_GETTING_DATA_OVER_HTTP",
                                        "CacheService" + getEntityName(),
                                        EntityToModuleStateStats::all,
                                        EntityToModuleStateStats::exception
                                        );
        }



        //if responseFromDataMaster is empty, we throw
        if (responseFromDataMaster.empty()) {
                throwEx("failed to get response from dataMaster");
        }

        MLOG(10) << "response from server side : "<< responseFromDataMaster;
        updateCurrentHashOfData(responseFromDataMaster);
        auto allEntitiesFromServer = parseResponse (responseFromDataMaster);
        this->clearCaches ();
        populateMapsAndLists (*allEntitiesFromServer);
        clearOtherCaches();
        populateOtherMapsAndLists(*getAllEntities());

        //releasing the object
        allEntitiesFromServer.reset();
        printCacheContent();
}


std::shared_ptr<gicapods::ConcurrentHashMap<int, T > > getAllEntitiesMap() {
        return this->allEntitiesMap;
}


std::shared_ptr<std::vector<std::shared_ptr<T> > > getAllEntities() {
        auto allEntities = std::make_shared<std::vector<std::shared_ptr<T> > >();
        auto map = this->allEntitiesMap->getCopyOfMap();
        for(auto pair : *map) {
                allEntities->push_back (pair.second);
        }
        return allEntities;
}


std::string processReadAllByClient(
        std::string requestBody,
        int& numberOfEntities) {
        assertAndThrow(!allEntitiesMap->empty());
        auto map = *allEntitiesMap->getCopyOfMap();
        entityToModuleStateStats->addStateModuleForEntity (
                getEntityName() + "_AllEntitiesMap_SIZE:" +
                StringUtil::toStr(map.size()),
                "CacheService",
                "ALL");

        auto responseToClient  = JsonMapUtil::convertMapValuesToJsonArrayString (map);
        MLOG(10)<<"sending response to requester : "<<std::endl<<
                responseToClient;

        numberOfEntities = allEntitiesMap->size();
        return responseToClient;
}


std::shared_ptr<std::vector<std::shared_ptr<T> > > parseResponse(std::string reponseInJson) {
        /*
           we have a method  JsonArrayUtil::convertStringArrayToArrayOfObject<T>(reponseInJson) here.
           but we dont use it here becuase in that case, we have to include every single T.h in JsonArrayUtil.cpp
         */
        std::shared_ptr<std::vector<std::shared_ptr<T> > >
        objectsParsed = std::make_shared<std::vector<std::shared_ptr<T> > >();

        auto document = parseJsonSafely(reponseInJson);
        assertAndThrow(document->IsArray ());

        for (rapidjson::SizeType i = 0; i < document->Size (); i++)   // rapidjson uses SizeType instead of size_t.
        {
                auto &obj = (*document)[i];
                assertAndThrow(obj.IsObject ());
                auto entity = T::fromJsonValue (obj);
                objectsParsed->push_back (entity);
        }

        return objectsParsed;
}


void printCacheContent() {

        int numberOfItemsPrinted = 0;
        auto map = allEntitiesMap->getCopyOfMap();
        for (typename tbb::concurrent_hash_map<int, std::shared_ptr<T> >::iterator it = map->begin();
             it != map->end();
             ++it)
        {
                numberOfItemsPrinted++;
                MLOG(10) << getEntityName() << " map entry , key : " << it->first << ", value: "<<it->second->toJson();
                if (numberOfItemsPrinted >= 30) {
                        MLOG(10) << "30 items logged, not logging all";
                        break;
                }
        }
}


~CacheService() {

}


std::shared_ptr<T> findByEntityId(int id) {
        assertAndThrow(allEntitiesMap != NULL);

        //if current map is empty, we serv from old map
        if(allEntitiesMap->empty()) {
                auto mapEntry = allEntitiesMapPreviousVersion->getOptional (id);
                if (mapEntry != nullptr) {
                        return mapEntry;
                }
        }

        auto mapEntry = allEntitiesMap->getOptional (id);
        if (mapEntry != nullptr) {
                return mapEntry;
        }
        printCacheContent();
        throwEx(getEntityName() + " is not found in map for id: " + StringUtil::toStr (id) +
                ", size of allEntitiesMap is " + _toStr(allEntitiesMap->size()));
        return nullptr;  //just for clang warning
}


void populateMapsAndLists(std::vector<std::shared_ptr<T> > allEntitiesFromDB) {


        //first,we populate the old version map here too
        auto map = this->allEntitiesMap->getCopyOfMap();
        for(auto pair : *map) {
                this->allEntitiesMapPreviousVersion->put(pair.first, pair.second);

        }


        //now we load populate new data to current map
        MLOG(10) << " " << allEntitiesFromDB.size () << " allEntities were loaded....";

        for(std::shared_ptr<T> adv :  allEntitiesFromDB) {
                this->getAllEntitiesMap()->put (adv->id, adv);
        }
        entityToModuleStateStats->addStateModuleForEntity (getEntityName() + "_AllEntitiesMap_SIZE_AFTER_POPULATION:" +
                                                           StringUtil::toStr(allEntitiesMap->size()),
                                                           "CacheService",
                                                           "ALL");

        //this is  a strict check for now, to make sure all the entities are loaded for testing
        //and all the caches are populated, TODO : later remove it
        if(this->getAllEntitiesMap()->empty()) {
                LOG(WARNING) << "warning in populating entity : " << getEntityName()
                             <<" all entities map is empty...no data populated!";

                entityToModuleStateStats->addStateModuleForEntity (getEntityName() + "_AllEntitiesMap_IS_EMPTY_AFTER_POPULATION",
                                                                   "CacheService",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::exception);
                assertAndWarn(!this->getAllEntitiesMap()->empty());
        }


}


void clearCaches() {
        allEntitiesMapPreviousVersion->clear();
        this->getAllEntitiesMap()->clear ();
}


bool existById(int id) {

        //if the new map is empty, we search the old map
        if (this->getAllEntitiesMap()->empty()) {
                return this->allEntitiesMapPreviousVersion->exists (id);
        } else {
                return this->getAllEntitiesMap()->exists (id);
        }
}
};

#endif //COMMON_CACHESERVICE_H
