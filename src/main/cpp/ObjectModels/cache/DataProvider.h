//
// Created by Mahmoud Taabodi on 7/17/16.
//

#ifndef COMMON_DataProvider_H
#define COMMON_DataProvider_H

#include <memory>
#include <string>
#include <vector>
#include <set>
#include "Object.h"
template<class T>
class DataProvider : public Object {

public:

virtual std::vector<std::shared_ptr<T> > readAllEntities()=0;

DataProvider<T>() : Object(__FILE__) {

}
virtual ~DataProvider<T>() {

}

};

#endif //COMMON_DataProvider_H
