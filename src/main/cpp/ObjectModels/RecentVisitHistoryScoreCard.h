#ifndef RecentVisitHistoryScoreCard_h
#define RecentVisitHistoryScoreCard_h


#include <atomic>
#include <memory>
#include <string>
#include <unordered_map>
#include "JsonTypeDefs.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "JsonArrayUtil.h"
#include "DateTimeUtil.h"
#include "ScoreRecord.h"
#include "Object.h"
class TgScoreEligibilityRecord;
/*
   it has information on if a targetGroupId has been approved or not
   RecentVisitHistoryScoreCard looks like this :
   {
     "ScoreEligibility": [{
       "tgScoreEligibility": [{
         "targetGroupId": 1,
         "id": 2,
         "score": 3.240000009536743,
         "scoreBase": 3.240000009536743,
         "eligibile": "yes",
         "scoreHistory": [{
           "scoreRecorded": 3.240000009536743,
           "scoreBase": 3.240000009536743,
           "timeScoredInUtc": 10
         }]
       }]
     }]
   }
 */

class RecentVisitHistoryScoreCard : public Object {
public:
std::shared_ptr<std::unordered_map<int, std::shared_ptr<TgScoreEligibilityRecord> > > targetGroupIdToQualification;


RecentVisitHistoryScoreCard();

virtual ~RecentVisitHistoryScoreCard();

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
static std::shared_ptr<RecentVisitHistoryScoreCard> fromJsonValue(RapidJsonValueType value);

std::string toJson();
static std::shared_ptr<RecentVisitHistoryScoreCard> fromJson(std::string jsnString);
};

#endif
