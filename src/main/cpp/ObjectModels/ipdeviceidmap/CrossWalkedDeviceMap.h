#ifndef CrossWalkedDeviceMap_H
#define CrossWalkedDeviceMap_H


#include "JsonTypeDefs.h"
#include <memory>
#include <string>
#include <vector>
#include <set>

class Device;
#include "Object.h"
/**
   represents the final product of crosswalking a device to others
 */
class CrossWalkedDeviceMap : public Object {

public:
std::shared_ptr<Device> sourceDevice;
std::vector<std::shared_ptr<Device> > linkedDevices;

CrossWalkedDeviceMap();

std::string toString();
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
std::string toJson();
static std::shared_ptr<CrossWalkedDeviceMap> fromJson(std::string json);
static std::shared_ptr<CrossWalkedDeviceMap> fromJsonValue(RapidJsonValueType value);
virtual ~CrossWalkedDeviceMap();
};


#endif
