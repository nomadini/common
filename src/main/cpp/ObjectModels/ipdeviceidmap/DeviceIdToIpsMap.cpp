
#include "StringUtil.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "DeviceIdToIpsMap.h"
#include <memory>
#include <string>
#include <vector>
#include <set>
#include "DateTimeUtil.h"
#include "Device.h"
#include "CassandraService.h"
#include "JsonArrayUtil.h"

DeviceIdToIpsMap::DeviceIdToIpsMap() : Object(__FILE__) {

}

DeviceIdToIpsMap::~DeviceIdToIpsMap() {

}

std::string DeviceIdToIpsMap::toString() {
								return this->toJson();

}

std::string DeviceIdToIpsMap::toJson() {

								auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString(value);
}

void DeviceIdToIpsMap::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

								JsonUtil::addMemberToValue_FromPair(doc, "deviceId", device->getDeviceId(), value);
								JsonUtil::addMemberToValue_FromPair(doc, "deviceType", device->getDeviceType(), value);

								JsonArrayUtil::addMemberToValue_FromPair(
																doc,
																"ips",
																ips,
																value
																);
}

std::shared_ptr<DeviceIdToIpsMap> DeviceIdToIpsMap::fromJsonValue(RapidJsonValueType value) {
								auto deviceIdToIpsMap = std::make_shared<DeviceIdToIpsMap>();
								deviceIdToIpsMap->device =
																std::make_shared<Device>(
																								JsonUtil::GetStringSafely (value, "deviceId"),
																								JsonUtil::GetStringSafely (value, "deviceType"));
								JsonArrayUtil::getArrayFromValueMemeber(
																value,
																"ips",
																deviceIdToIpsMap->ips);
								return deviceIdToIpsMap;
}

std::shared_ptr<DeviceIdToIpsMap> DeviceIdToIpsMap::fromJson(std::string jsonModel) {
								auto document = parseJsonSafely(jsonModel);
								return fromJsonValue(*document);

}
