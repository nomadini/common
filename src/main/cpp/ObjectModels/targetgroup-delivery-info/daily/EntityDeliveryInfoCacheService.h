#ifndef TargetGroupDailyDeliveryInfoCacheService_h
#define TargetGroupDailyDeliveryInfoCacheService_h

#include <string>
#include <memory>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "BWEntry.h"
class MySqlDriver;
class TargetGroupCacheService;
class EntityRealTimeDeliveryInfo;
#include "Poco/UniqueExpireCache.h"
#include "Poco/ExpirationDecorator.h"
#include "HttpUtilService.h"
#include "HttpUtilService.h"
class EntityToModuleStateStats;
#include "Object.h"

class EntityDeliveryInfoCacheService;

class EntityDeliveryInfoCacheService : public Object {

private:
std::shared_ptr<gicapods::ConcurrentHashMap<int, EntityRealTimeDeliveryInfo > > allTgRealTimeDeliveryInfoMap;
std::shared_ptr<gicapods::ConcurrentHashMap<int, EntityRealTimeDeliveryInfo > > allCampaignRealTimeDeliveryInfoMap;

public:

EntityToModuleStateStats* entityToModuleStateStats;

EntityDeliveryInfoCacheService(EntityToModuleStateStats* entityToModuleStateStats);

std::shared_ptr<gicapods::ConcurrentHashMap<int, EntityRealTimeDeliveryInfo > > getAllTgRealTimeDeliveryInfoMap();
std::shared_ptr<gicapods::ConcurrentHashMap<int, EntityRealTimeDeliveryInfo > > getAllCampaignRealTimeDeliveryInfoMap();

std::string processReadAllByClient(std::string requestBody, int sizeOfMap);

bool isRealTimeInfoOldForTg(int targetGroupId);
};


#endif
