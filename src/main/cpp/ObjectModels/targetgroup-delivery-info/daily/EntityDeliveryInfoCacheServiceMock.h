#ifndef TargetGroupDailyDeliveryInfoCacheServiceMock_h
#define TargetGroupDailyDeliveryInfoCacheServiceMock_h

#include <string>
#include <memory>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "BWEntry.h"
class MySqlDriver;
class TargetGroupCacheService;
#include "Poco/UniqueExpireCache.h"
#include "Poco/ExpirationDecorator.h"
#include "EntityDeliveryInfoCacheService.h"
class EntityDeliveryInfoCacheService;
#include "gmock/gmock.h"
class EntityToModuleStateStats;

class EntityDeliveryInfoCacheServiceMock;




class EntityDeliveryInfoCacheServiceMock : public EntityDeliveryInfoCacheService {

public:
EntityDeliveryInfoCacheServiceMock(EntityToModuleStateStats* entityToModuleStateStats);


MOCK_METHOD1(processReadAll, std::string(std::string));
};


#endif
