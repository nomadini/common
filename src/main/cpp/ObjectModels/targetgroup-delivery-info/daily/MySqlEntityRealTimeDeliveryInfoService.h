#ifndef MySqlTgRealTimeDeliveryInfoService_h
#define MySqlTgRealTimeDeliveryInfoService_h

#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "mysql_connection.h"
#include <unordered_map>
#include "MySqlDriver.h"
#include "Poco/DateTime.h"
#include <cppconn/resultset.h>
class EntityRealTimeDeliveryInfo;
class MySqlEntityRealTimeDeliveryInfoService;
#include "Object.h"


//will use the impression and impression_compressed to give the result for
//current hour, current day and total of impressions and different costs
class MySqlEntityRealTimeDeliveryInfoService : public Object {

public:

MySqlDriver* mysqlDriverAlpha1;
MySqlDriver* mysqlDriverMango;

MySqlEntityRealTimeDeliveryInfoService(
        MySqlDriver* mysqlDriverAlpha1,
        MySqlDriver* mysqlDriverMango
        );

std::vector<std::shared_ptr<EntityRealTimeDeliveryInfo> > getTgRealTimeDeliveryInfos(
        std::string entityName, std::vector<int> allTargetGroupsIds);

virtual ~MySqlEntityRealTimeDeliveryInfoService();

private:

void fetchResults(
        MySqlDriver* driver,
        std::string tableName,
        Poco::DateTime startDate,
        Poco::DateTime endDate,
        std::unordered_map<int, std::tuple<long, double, double, double> >& tupleResult,
        std::string overridingTimeClause,
        std::string entityName
        );

std::tuple<long, double, double, double> getRelatedDeliveryTuple(
        std::unordered_map<int, std::tuple<long, double, double, double> >& map, int tgId);

std::unordered_map<int, std::tuple<long, double, double, double> > readSumOfCurrentHourDeliveryInfos(
        std::string entityName
        );

std::unordered_map<int, std::tuple<long, double, double, double> > readSumOfCurrentDayDeliveryInfos (
        std::string entityName
        );

std::unordered_map<int, std::tuple<long, double, double, double> > readSumOfTotalDeliveryInfos (
        std::string entityName
        );
};

#endif
