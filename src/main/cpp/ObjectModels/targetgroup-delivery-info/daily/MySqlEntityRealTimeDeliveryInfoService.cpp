#include "GUtil.h"
#include "TargetGroup.h"
#include "MySqlEntityRealTimeDeliveryInfoService.h"
#include "MySqlDriver.h"
#include "EntityRealTimeDeliveryInfo.h"
#include <boost/foreach.hpp>
#include "AtomicLong.h"
#include "DateTimeUtil.h"
#include <cppconn/resultset.h>

MySqlEntityRealTimeDeliveryInfoService::
MySqlEntityRealTimeDeliveryInfoService(
        MySqlDriver* mysqlDriverAlpha1,
        MySqlDriver* mysqlDriverMango
        )  : Object(__FILE__) {
        this->mysqlDriverAlpha1 = mysqlDriverAlpha1;
        this->mysqlDriverMango = mysqlDriverMango;
}

std::vector<std::shared_ptr<EntityRealTimeDeliveryInfo> >
MySqlEntityRealTimeDeliveryInfoService::getTgRealTimeDeliveryInfos(
        std::string entityName, std::vector<int>  allEntityIds) {

        auto currentHourDeliveryInfo = readSumOfCurrentHourDeliveryInfos(entityName);

        auto currentDayDeliveryInfo = readSumOfCurrentDayDeliveryInfos (entityName);

        auto totalDeliveryInfo = readSumOfTotalDeliveryInfos (entityName);

        std::vector<std::shared_ptr<EntityRealTimeDeliveryInfo> > allDeliveryInfos;
        for (int entityId : allEntityIds) {
                auto tgRealTimeDeliveryInfo = std::make_shared<EntityRealTimeDeliveryInfo>();
                tgRealTimeDeliveryInfo->entityId = entityId;
                tgRealTimeDeliveryInfo->entityName = entityName;

                auto currentHourDeliveryInfoForTg = getRelatedDeliveryTuple(currentHourDeliveryInfo, entityId);
                auto currentDayDeliveryInfoForTg = getRelatedDeliveryTuple(currentDayDeliveryInfo, entityId);
                auto currentTotalDeliveryInfoForTg = getRelatedDeliveryTuple(totalDeliveryInfo, entityId);
                tgRealTimeDeliveryInfo->numOfImpressionsServedInCurrentHourUpToNow->setValue(std::get<0>(currentHourDeliveryInfoForTg));
                tgRealTimeDeliveryInfo->numOfImpressionsServedInCurrentDateUpToNow->setValue(std::get<0>(currentDayDeliveryInfoForTg));
                tgRealTimeDeliveryInfo->numOfImpressionsServedOverallUpToNow->setValue(std::get<0>(currentTotalDeliveryInfoForTg));

                tgRealTimeDeliveryInfo->platformCostSpentInCurrentHourUpToNow->setValue(std::get<2>(currentHourDeliveryInfoForTg));
                tgRealTimeDeliveryInfo->platformCostSpentInCurrentDateUpToNow->setValue(std::get<2>(currentDayDeliveryInfoForTg));
                tgRealTimeDeliveryInfo->platformCostSpentOverallUpToNow->setValue(std::get<2>(currentTotalDeliveryInfoForTg));

                tgRealTimeDeliveryInfo->advertiserCostSpentInCurrentHourUpToNow->setValue(std::get<3>(currentHourDeliveryInfoForTg));
                tgRealTimeDeliveryInfo->advertiserCostSpentInCurrentDateUpToNow->setValue(std::get<3>(currentDayDeliveryInfoForTg));
                tgRealTimeDeliveryInfo->advertiserCostSpentOverallUpToNow->setValue(std::get<3>(currentTotalDeliveryInfoForTg));

                MLOG(10) <<"tgRealTimeDeliveryInfo : " <<tgRealTimeDeliveryInfo->toJson();
                allDeliveryInfos.push_back(tgRealTimeDeliveryInfo);
        }
        return allDeliveryInfos;
}

std::tuple<long, double, double, double> MySqlEntityRealTimeDeliveryInfoService::getRelatedDeliveryTuple(
        std::unordered_map<int, std::tuple<long, double, double, double> >& map, int entityId) {
        std::tuple<long, double, double, double> tuple  =  std::make_tuple(0,0,0,0);
        auto pair = map.find(entityId);
        if (pair != map.end() ) {
                std::get<0>(tuple) = std::get<0>(pair->second);
                std::get<1>(tuple) = std::get<2>(pair->second);
                std::get<2>(tuple) = std::get<3>(pair->second);
        }
        return tuple;
}

std::unordered_map<int, std::tuple<long, double, double, double> > MySqlEntityRealTimeDeliveryInfoService::readSumOfCurrentHourDeliveryInfos (
        std::string entityName
        ) {
        std::unordered_map<int, std::tuple<long, double, double, double> > targetGroupIdToHourlyDeliveryInfos;
        auto startDate = DateTimeUtil::getDateWithHourPercision(0);
        auto endDate = DateTimeUtil::getDateWithHourPercision(1);
        std::string ignoreMeTimeClause = "";
        fetchResults(mysqlDriverAlpha1,
                     "impression_compressed",
                     startDate,
                     endDate,
                     targetGroupIdToHourlyDeliveryInfos,
                     ignoreMeTimeClause,
                     entityName);
        return targetGroupIdToHourlyDeliveryInfos;
}

std::unordered_map<int, std::tuple<long, double, double, double> > MySqlEntityRealTimeDeliveryInfoService::readSumOfCurrentDayDeliveryInfos (
        std::string entityName
        ) {
        std::unordered_map<int, std::tuple<long, double, double, double> > targetGroupIdToDailyDeliveryInfos;
        auto startDate = DateTimeUtil::getDateWithDayPercision(0);
        auto endDate = DateTimeUtil::getDateWithDayPercision(1);
        std::string ignoreMeTimeClause = "";
        fetchResults(mysqlDriverMango,
                     "impression_compressed",
                     startDate,
                     endDate,
                     targetGroupIdToDailyDeliveryInfos,
                     ignoreMeTimeClause,
                     entityName);
        return targetGroupIdToDailyDeliveryInfos;
}

std::unordered_map<int, std::tuple<long, double, double, double> >
MySqlEntityRealTimeDeliveryInfoService::readSumOfTotalDeliveryInfos (
        std::string entityName
        ) {

        std::unordered_map<int, std::tuple<long, double, double, double> > targetGroupIdToTotalDeliveryInfos;
        std::string overridingTimeClause = "DONT USE ANY TIME";
        Poco::DateTime startDate;
        Poco::DateTime endDate;
        fetchResults(mysqlDriverMango,
                     "impression_compressed",
                     startDate,
                     endDate,
                     targetGroupIdToTotalDeliveryInfos,
                     overridingTimeClause,
                     entityName);
        return targetGroupIdToTotalDeliveryInfos;
}

void MySqlEntityRealTimeDeliveryInfoService::fetchResults(
        MySqlDriver* driver,
        std::string tableName,
        Poco::DateTime startDate,
        Poco::DateTime endDate,
        std::unordered_map<int, std::tuple<long, double, double, double> >& tupleResult,
        std::string overridingTimeClause,
        std::string entityName) {

        std::string timeClause = " and created_at >= '__start_date__' and created_at <= '__end_date__'";
        timeClause = StringUtil::replaceString (timeClause, "__start_date__",
                                                DateTimeUtil::dateTimeToStr(startDate));

        timeClause = StringUtil::replaceString (timeClause, "__end_date__",
                                                DateTimeUtil::dateTimeToStr(endDate));
        if(!overridingTimeClause.empty()) {
                timeClause = "";
        }
        std::string query =
                "select " + entityName + "_id, sum(count) AS count , "
                " sum(exchangeCostInCpm) / 1000 AS exchangeCostInCpm,"
                " sum(platformCostInCpm) / 1000 AS platformCostInCpm, "
                " sum(advertiserCostInCpm) / 1000 AS advertiserCostInCpm "
                " from " + tableName + " where event_type='impression' "
                + timeClause +
                " group by " + entityName + "_id";

        auto res = driver->executeQuery (query);


        while (res->next ()) {
                std::tuple<long, double, double, double> tuple = std::make_tuple(0,0,0, 0);
                std::get<0>(tuple) = res->getInt64 ("count");
                std::get<1>(tuple) = res->getDouble ("exchangeCostInCpm");
                std::get<2>(tuple) = res->getDouble ("platformCostInCpm");
                std::get<3>(tuple) = res->getDouble ("advertiserCostInCpm");
                tupleResult.insert(std::make_pair(
                                           res->getInt (1),
                                           tuple
                                           ));
        }


}

MySqlEntityRealTimeDeliveryInfoService::~MySqlEntityRealTimeDeliveryInfoService() {
}
