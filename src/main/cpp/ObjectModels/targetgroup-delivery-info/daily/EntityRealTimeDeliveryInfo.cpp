#include "AtomicLong.h"
#include "AtomicDouble.h"
#include "TargetGroup.h"
#include "EntityRealTimeDeliveryInfo.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "DateTimeUtil.h"
#include "NetworkUtil.h"
#include <string>
#include <memory>

EntityRealTimeDeliveryInfo::EntityRealTimeDeliveryInfo()  : Object(__FILE__) {

        numOfImpressionsServedInCurrentHourUpToNow = std::make_shared<gicapods::AtomicLong>(0);
        numOfImpressionsServedInCurrentDateUpToNow = std::make_shared<gicapods::AtomicLong>(0);
        numOfImpressionsServedOverallUpToNow= std::make_shared<gicapods::AtomicLong>(0);

        platformCostSpentInCurrentHourUpToNow = std::make_shared<gicapods::AtomicDouble>(0);
        platformCostSpentInCurrentDateUpToNow = std::make_shared<gicapods::AtomicDouble>(0);
        platformCostSpentOverallUpToNow = std::make_shared<gicapods::AtomicDouble>(0);

        advertiserCostSpentInCurrentHourUpToNow = std::make_shared<gicapods::AtomicDouble>(0);
        advertiserCostSpentInCurrentDateUpToNow = std::make_shared<gicapods::AtomicDouble>(0);
        advertiserCostSpentOverallUpToNow = std::make_shared<gicapods::AtomicDouble>(0);

        Poco::DateTime now;
        timeReported = now;
        entityId = 0;
}

std::string EntityRealTimeDeliveryInfo::toString() {
        return this->toJson();
}

std::shared_ptr<EntityRealTimeDeliveryInfo> EntityRealTimeDeliveryInfo::fromJson(std::string jsonString) {
        std::shared_ptr<EntityRealTimeDeliveryInfo> tg = std::make_shared<EntityRealTimeDeliveryInfo>();
        //  MLOG(3) << "original jsonString for EntityRealTimeDeliveryInfo : " << jsonString;
        auto doc = parseJsonSafely(jsonString);
        bool result;
        tg->entityId = JsonUtil::GetIntSafely(*doc, "entityId");
        tg->entityName = JsonUtil::GetStringSafely(*doc, "entityName");
        tg->timeReported = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(*doc, "timeReported"));

        tg->numOfImpressionsServedInCurrentHourUpToNow->setValue(
                JsonUtil::GetLongSafely(*doc, "numOfImpressionsServedInCurrentHourUpToNow"));

        tg->numOfImpressionsServedInCurrentDateUpToNow->setValue(
                JsonUtil::GetLongSafely(*doc, "numOfImpressionsServedInCurrentDateUpToNow"));

        tg->numOfImpressionsServedOverallUpToNow->setValue(
                JsonUtil::GetLongSafely(*doc, "numOfImpressionsServedOverallUpToNow"));


        tg->platformCostSpentInCurrentHourUpToNow->setValue(
                JsonUtil::GetDoubleSafely(*doc, "platformCostSpentInCurrentHourUpToNow"));

        tg->platformCostSpentInCurrentDateUpToNow->setValue(
                JsonUtil::GetDoubleSafely(*doc, "platformCostSpentInCurrentDateUpToNow"));

        tg->platformCostSpentOverallUpToNow->setValue(
                JsonUtil::GetDoubleSafely(*doc, "platformCostSpentOverallUpToNow"));

        tg->validate();
        return tg;
}

void EntityRealTimeDeliveryInfo::validate() {

        if(numOfImpressionsServedInCurrentDateUpToNow->getValue() > 100000) {
                throwEx("numOfImpressionsServedInCurrentDateUpToNow is out of bound " + StringUtil::toStr(numOfImpressionsServedInCurrentDateUpToNow->getValue()));
        }
        if(numOfImpressionsServedOverallUpToNow->getValue() > 10000000) {
                throwEx("numOfImpressionsServedOverallUpToNow is out of bound " + StringUtil::toStr(numOfImpressionsServedOverallUpToNow->getValue()));
        }

        if(platformCostSpentOverallUpToNow->getValue() > 100000) {
                throwEx("platformCostSpentOverallUpToNow is out of bound " + StringUtil::toStr(platformCostSpentOverallUpToNow->getValue()));
        }

        if(platformCostSpentInCurrentDateUpToNow->getValue() > 10000) {
                throwEx("platformCostSpentInCurrentDateUpToNow is out of bound " + StringUtil::toStr(platformCostSpentInCurrentDateUpToNow->getValue()));
        }

}
void EntityRealTimeDeliveryInfo::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "entityId", entityId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "entityName", entityName, value);
        JsonUtil::addMemberToValue_FromPair (doc, "timeReported", DateTimeUtil::dateTimeToStr(timeReported), value);

        JsonUtil::addMemberToValue_FromPair (doc, "numOfImpressionsServedInCurrentHourUpToNow", numOfImpressionsServedInCurrentHourUpToNow->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "numOfImpressionsServedInCurrentDateUpToNow", numOfImpressionsServedInCurrentDateUpToNow->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "numOfImpressionsServedOverallUpToNow", numOfImpressionsServedOverallUpToNow->getValue(), value);

        JsonUtil::addMemberToValue_FromPair (doc, "platformCostSpentInCurrentHourUpToNow", platformCostSpentInCurrentHourUpToNow->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "platformCostSpentOverallUpToNow", platformCostSpentOverallUpToNow->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "platformCostSpentInCurrentDateUpToNow", platformCostSpentInCurrentDateUpToNow->getValue(), value);

}

std::shared_ptr<EntityRealTimeDeliveryInfo> EntityRealTimeDeliveryInfo::fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeDeliveryInfoList = std::make_shared<EntityRealTimeDeliveryInfo>();
        tgRealTimeDeliveryInfoList->entityId = JsonUtil::GetIntSafely(value, "entityId");
        tgRealTimeDeliveryInfoList->entityName = JsonUtil::GetStringSafely(value, "entityName");
        tgRealTimeDeliveryInfoList->timeReported  = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"timeReported"));

        tgRealTimeDeliveryInfoList->numOfImpressionsServedInCurrentHourUpToNow->setValue(JsonUtil::GetIntSafely(value,"numOfImpressionsServedInCurrentHourUpToNow"));
        tgRealTimeDeliveryInfoList->numOfImpressionsServedInCurrentDateUpToNow->setValue(JsonUtil::GetIntSafely(value,"numOfImpressionsServedInCurrentDateUpToNow"));
        tgRealTimeDeliveryInfoList->numOfImpressionsServedOverallUpToNow->setValue(JsonUtil::GetLongSafely(value,"numOfImpressionsServedOverallUpToNow"));

        tgRealTimeDeliveryInfoList->platformCostSpentInCurrentHourUpToNow->setValue(JsonUtil::GetDoubleSafely(value,"platformCostSpentInCurrentHourUpToNow"));
        tgRealTimeDeliveryInfoList->platformCostSpentInCurrentDateUpToNow->setValue(JsonUtil::GetDoubleSafely(value,"platformCostSpentInCurrentDateUpToNow"));
        tgRealTimeDeliveryInfoList->platformCostSpentOverallUpToNow->setValue(JsonUtil::GetDoubleSafely(value,"platformCostSpentOverallUpToNow"));

        return tgRealTimeDeliveryInfoList;
}

std::string EntityRealTimeDeliveryInfo::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);

}

EntityRealTimeDeliveryInfo::~EntityRealTimeDeliveryInfo() {

}
