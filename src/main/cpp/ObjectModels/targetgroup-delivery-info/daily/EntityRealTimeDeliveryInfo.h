#ifndef EntityRealTimeDeliveryInfo_H
#define EntityRealTimeDeliveryInfo_H



#include "AtomicLong.h"
#include "AtomicDouble.h"
#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include "JsonTypeDefs.h"

#include "Poco/DateTime.h"
#include "Object.h"
/*
   This information resides in mysql databases, the targetgroup_real_time_info table
   in mysql has a record for every hour, and tracks that day budget spent, plus the total budget spent by
   that date. this table will be populated by bidderPacer threads which are responsible for aggregating the
   hourly information for the target group real time info. this is read by Bidder to make
   decision on how much it can bid for a targetgroup,
 */

class EntityRealTimeDeliveryInfo : public Object {

private:

public:
int entityId;
std::string entityName;
Poco::DateTime timeReported;

/*
   these are the CUMULATIVE values, it means they are *UpToNow
 */
std::shared_ptr<gicapods::AtomicLong> numOfImpressionsServedInCurrentHourUpToNow;
std::shared_ptr<gicapods::AtomicLong> numOfImpressionsServedInCurrentDateUpToNow;  //this will be compared against tg->dailyMaxImpression
std::shared_ptr<gicapods::AtomicLong> numOfImpressionsServedOverallUpToNow;  //this will be compared against tg->maxImpression

std::shared_ptr<gicapods::AtomicDouble> platformCostSpentInCurrentHourUpToNow;
std::shared_ptr<gicapods::AtomicDouble> platformCostSpentInCurrentDateUpToNow;  //this will be compared against tg->dailyMaxBudget
std::shared_ptr<gicapods::AtomicDouble> platformCostSpentOverallUpToNow;  // this will be compared against tg->maxBudget

std::shared_ptr<gicapods::AtomicDouble> advertiserCostSpentInCurrentHourUpToNow;
std::shared_ptr<gicapods::AtomicDouble> advertiserCostSpentInCurrentDateUpToNow;
std::shared_ptr<gicapods::AtomicDouble> advertiserCostSpentOverallUpToNow;



EntityRealTimeDeliveryInfo();
std::string toString();

static std::shared_ptr<EntityRealTimeDeliveryInfo> fromJson(std::string jsonString);

std::string toJson();

virtual ~EntityRealTimeDeliveryInfo();

static std::shared_ptr<EntityRealTimeDeliveryInfo> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
void validate();

};

#endif
