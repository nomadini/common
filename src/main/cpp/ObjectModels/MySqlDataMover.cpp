#include "GUtil.h"
#include "MySqlDataMover.h"
#include "MySqlDriver.h"
#include "DateTimeUtil.h"
#include "CollectionUtil.h"
#include "JsonArrayUtil.h"
#include "MySqlMetricService.h"

template<class T>
MySqlDataMover<T>::MySqlDataMover(
        MySqlDriver* driver) : Object(__FILE__) {
        NULL_CHECK(driver);
        this->driver = driver;
        lastTimeDataMoved = DateTimeUtil::getNowInSecond();
        coreFieldsToRead = traits.getCoreFieldsToRead();
}

template<class T>
int MySqlDataMover<T>::insertDataFromTempToMainTable(
        std::set<std::string> allDatesToInsert, std::string templateTableName) {
        int numberOfRecordsInserted = 0;
        for (auto date : allDatesToInsert) {
                auto query = "insert into "+ traits.getMainTableName() +" (" +
                             traits.getCoreFieldsToRead()
                             +") select "+traits.getCoreFieldsToRead()
                             +" from " + templateTableName
                             +"  where created_at = '" + date + "'";
                MLOG(3) << "query to insert data : "<<query;
                int numberOfUpdates = driver->executedUpdateStatement (query);
                numberOfRecordsInserted += numberOfUpdates;
                LOG(ERROR) << "rows got inserted : "<<numberOfUpdates<<" in table "<< traits.getMainTableName();
        }
        return numberOfRecordsInserted;
}

template<class T>
int MySqlDataMover<T>::deleteAllCurrentDataWithDatesInMainTable(std::set<std::string> allDatesToInsert) {
        int numberOfRecordsDeleted = 0;
        for(auto date  : allDatesToInsert) {
                auto query = "delete from " + traits.getMainTableName() + " where created_at = '" + date+"'";
                LOG(ERROR) << "query to delete current dates " << query;
                int numberOfUpdates = driver->executedUpdateStatement (query);
                LOG(ERROR) << numberOfUpdates<< " rows got deleted from main table " << traits.getMainTableName();
                numberOfRecordsDeleted += numberOfUpdates;
        }
        return numberOfRecordsDeleted;
}

template<class T>
std::set<std::string>  MySqlDataMover<T>::readAllDistinctDates(std::string tableName) {
        auto query = "select distinct created_at from " + tableName;

        auto res = driver->executeQuery (query);

        std::set<std::string> tables;
        while (res->next ()) {
                tables.insert(res->getString(1));
        }
        return tables;
}

template<class T>
void MySqlDataMover<T>::dropTempTable(std::string tempTable) {
        auto query = "DROP TABLE " + tempTable;
        int numberOfUpdates = driver->executedUpdateStatement (query);
        if (numberOfUpdates == 1) {
                LOG(ERROR) <<"table was dropped : " << tempTable;
        }

}

template<class T>
std::set<std::string>  MySqlDataMover<T>::readAllTablesWithTempData() {
        auto query = "show tables like '%" + traits.getMainTableName() + "_%'";
        MLOG(10) << "readAllTablesWithTempData query : "<< query;
        auto res = driver->executeQuery (query);

        std::set<std::string> tables;
        while (res->next ()) {
                tables.insert(res->getString(1));

        }
        LOG(ERROR) << "all tables with tempData : "<<JsonArrayUtil::convertSetToJson(tables);
        return tables;
}

template<class T>
void MySqlDataMover<T>::inserterThread() {

        try {

                std::set<std::string>  allTablesWithTempData = readAllTablesWithTempData();
                for (auto tempTable : allTablesWithTempData) {
                        std::set<std::string> allDatesToInsert = readAllDistinctDates(tempTable);

                        int numberOfRecordsDeleted =
                                deleteAllCurrentDataWithDatesInMainTable(allDatesToInsert);
                        int numberOfRecordsInserted =
                                insertDataFromTempToMainTable(allDatesToInsert, tempTable);

                        dropTempTable(tempTable);
                        if (numberOfRecordsInserted != numberOfRecordsInserted) {
                                LOG(ERROR) << "ATTENTION : diffrenet # records deleted : "
                                           << numberOfRecordsInserted
                                           <<"vs inserted : "
                                           << numberOfRecordsInserted;
                        }
                }
                //this is out of for loop , will be updated even if there is no data to move
                lastTimeDataMoved =  DateTimeUtil::getNowInSecond();
        } catch(std::exception &e) {
                gicapods::Util::showStackTrace(&e);
        }

}


template<class T>
MySqlDataMover<T>::~MySqlDataMover() {
}

#include "ImpressionEventDto.h"
template class MySqlDataMover<ImpressionEventDto>;
