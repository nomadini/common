#ifndef DeviceIdService_h
#define DeviceIdService_h
#include "Object.h"
#include <memory>
#include <string>
#include <vector>
namespace gicapods { template <typename K, typename V> class ConcurrentHashMap; }
namespace gicapods { template <typename K> class ConcurrentHashSet; }
#include "AtomicLong.h"
#include "SizeAndTimeBasedLRUCache.h"
#include "AerospikeDriver.h"
class EntityToModuleStateStats;
#include "DeviceVisitCountHistory.h"

class DeviceIdService;


class DeviceIdService : public Object {

public:
std::shared_ptr<gicapods::SizeAndTimeBasedLRUCache<std::string, std::shared_ptr<DeviceVisitCountHistory> > > deviceIdToVisitCounts;
std::shared_ptr<gicapods::SizeAndTimeBasedLRUCache<std::string, std::shared_ptr<std::string> > > persistentFrequentDeviceIds;
AerospikeDriverInterface* aeroSpikeDriver;

std::string snamespace;
std::string set;
std::string key;
std::string shortTermVisitCountBin;
std::string longTermVisitCountBin;
int numberOfDeviceUniqueDeviceIdsSeenLimit;
int numberOfVisitsByAllBidderLimit;
int visitPeriodToCountForInSeconds;
DeviceIdService(EntityToModuleStateStats* entityToModuleStateStatsArg);

EntityToModuleStateStats* entityToModuleStateStats;

virtual ~DeviceIdService();

void validate(std::string deviceId);

bool isBadDevice(std::string deviceId);

void persistAsBadDeviceIdInMysql(std::string deviceId);

long getVisitsByAll(std::string deviceId,
                    std::shared_ptr<DeviceVisitCountHistory> deviceVisitCountHistory);

void queueDeviceIdForPersistentStore(std::string deviceId);

};


#endif
