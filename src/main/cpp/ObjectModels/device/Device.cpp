
#include "StringUtil.h"
#include "DateTimeUtil.h"
#include "JsonMapUtil.h"
#include "JsonUtil.h"
#include "StringUtil.h"
#include "GUtil.h"
#include "JsonArrayUtil.h"
#include "Device.h"
#include "RandomUtil.h"

std::string Device::GENERAL_BIDDING_DEVICE_TYPE_DESKTOP = "DESKTOP";
std::string Device::GENERAL_BIDDING_DEVICE_TYPE_MOBILE_PHONE = "MOBILE_PHONE";
std::string Device::GENERAL_BIDDING_DEVICE_TYPE_TABLET = "TABLET";

std::string Device::GENERAL_BIDDING_DEVICE_OS_TYPE_MAC = "MAC";
std::string Device::GENERAL_BIDDING_DEVICE_OS_TYPE_WINDOWS = "WINDOWS";
std::string Device::GENERAL_BIDDING_DEVICE_OS_TYPE_LINUX = "LINUX";

Device::Device(const std::string& deviceId,const std::string& deviceType) : Object(__FILE__) {
        validDevice = false;
        setDeviceIdAndType(deviceId, deviceType);
}
Device::Device() : Object(__FILE__) {
        validDevice = false;

}


std::shared_ptr<RandomUtil> Device::getDeviceIdRandomizer() {
        static std::shared_ptr<RandomUtil> deviceIdRandomizer =
                std::make_shared<RandomUtil> (100000);

        return deviceIdRandomizer;
}


std::string Device::getName() {
        return "Device";
}

std::string Device::getDeviceUniqueName() {
        return "Device";
}

std::string Device::toJson() {
        auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "deviceId", deviceId,value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "deviceType", deviceType,value);
        return JsonUtil::valueToString(value);
}

std::shared_ptr<Device> Device::fromJson(std::string jsonModel) {
        auto doc = parseJsonSafely(jsonModel);
        auto device = std::make_shared<Device> (
                JsonUtil::GetStringSafely(*doc, "deviceId"),
                JsonUtil::GetStringSafely(*doc, "deviceType")
                );
        return device;
}

Device::~Device() {

}

void Device::setDeviceIdAndType(std::string deviceId, std::string deviceTypeArg) {
        if(deviceTypeArg.empty()) {
                deviceTypeArg = "DESKTOP";
        }

        if(StringUtil::equalsIgnoreCase (deviceTypeArg, "PERSONAL_COMPUTER")) {
                deviceTypeArg = "DESKTOP";
        }

        if(!StringUtil::equalsIgnoreCase (deviceTypeArg, Device::GENERAL_BIDDING_DEVICE_TYPE_DESKTOP) &&
           !StringUtil::equalsIgnoreCase (deviceTypeArg, Device::GENERAL_BIDDING_DEVICE_TYPE_MOBILE_PHONE) &&
           !StringUtil::equalsIgnoreCase (deviceTypeArg, Device::GENERAL_BIDDING_DEVICE_TYPE_TABLET)) {
                throwEx("wrong type of device :  " +  deviceTypeArg);
        }

        if (!StringUtil::containsCaseInSensitive(deviceId, "nomadini")) {
                throwEx("deviceId should contain nomadini , deviceId : " + deviceId);
        }


        if (StringUtil::containsCaseInSensitive(deviceId, "nomadini")) {
                //this is very important...so basically
                //we are saying any cookie that we set on a device
                // we call that device DESKTOP
                // for example if a user calls our action pixels
                // from a mobile browser, when we set the cookie "NomadiniDeviceId"
                // we call it a DESKTOP device
                assertAndThrow(StringUtil::equalsIgnoreCase(deviceTypeArg, Device::GENERAL_BIDDING_DEVICE_TYPE_DESKTOP));
        }
        this->deviceId = deviceId;
        this->deviceType = deviceTypeArg;
        this->deviceType = StringUtil::toUpperCase(this->deviceType);
        this->deviceUniqueName =
                validDevice = true;
}

std::string Device::getDeviceType() {
        if(!validDevice) {
                gicapods::Util::showStackTrace();
        }
        assertAndThrow(validDevice);
        return deviceType;
}
std::string Device::getDeviceId() {
        if(!validDevice) {
                gicapods::Util::showStackTrace();
        }
        assertAndThrow(validDevice);
        return deviceId;
}

std::string Device::createStandardDeviceId() {
        auto timePortionOfId = StringUtil::toStr(DateTimeUtil::getNowInMicroSecond());
        auto twelveCharsOfTime = StringUtil::getLastNCharactersOfString(timePortionOfId, 12);

        auto finalId = StringUtil::random_string_pure(4) +
                       twelveCharsOfTime +
                       "nomadini_" +
                       _toStr(getDeviceIdRandomizer()->randomNumberV2());
        //this part is for being able to sample the device
        LOG_EVERY_N(ERROR, 10000) << google::COUNTER <<"th id generated is "<<finalId;

        return finalId;
}

std::shared_ptr<Device> Device::buildAStandardDeviceIdBasedOn(std::string exchangeUserId, std::string deviceType) {
        int numberOfExistingCharacters = exchangeUserId.size();
        std::string finalId = "";
        if (numberOfExistingCharacters >= 16) {
                auto sixteenExchangeCharacters = StringUtil::getLastNCharactersOfString(exchangeUserId, 16);
                finalId =  sixteenExchangeCharacters + "nomadini_" + _toStr(
                        getDeviceIdRandomizer()->randomNumberV2());
        } else {
                finalId = StringUtil::random_string_pure(16 - numberOfExistingCharacters) + exchangeUserId + "nomadini_" +   _toStr(
                        getDeviceIdRandomizer()->randomNumberV2());
        }
        return std::make_shared<Device>(finalId, StringUtil::toUpperCase(deviceType));
}

std::shared_ptr<Device> Device::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<Device> device = std::make_shared<Device>(
                JsonUtil::GetStringSafely(value, "deviceId"),
                JsonUtil::GetStringSafely(value, "deviceType"));


        return device;
}

void Device::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

        JsonUtil::addMemberToValue_FromPair (doc, "deviceId", deviceId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "deviceType", deviceType, value);

}
