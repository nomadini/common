#ifndef DEVICE_H
#define DEVICE_H
#include "Object.h"
#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include "JsonTypeDefs.h"
class RandomUtil;
class Device;
class Device : public Object {

private:
static std::shared_ptr<RandomUtil> getDeviceIdRandomizer();
//we are making this private to make the caller
//use the setDeviceIdAndType function, then using that
// we can sanitize, validate, and analyze the incoming deviceIds
std::string deviceId;
std::string deviceType;
std::string deviceUniqueName;

//this flag makes sure the device is valid
bool validDevice;

void setDeviceIdAndType(std::string deviceId, std::string deviceType);

public:

Device(const std::string& deviceId, const std::string& deviceType);

Device(/*this is used in JsonMapUtil.cpp */);

static std::string GENERAL_BIDDING_DEVICE_TYPE_DESKTOP;
static std::string GENERAL_BIDDING_DEVICE_TYPE_MOBILE_PHONE;
static std::string GENERAL_BIDDING_DEVICE_TYPE_TABLET;

static std::string GENERAL_BIDDING_DEVICE_OS_TYPE_MAC;
static std::string GENERAL_BIDDING_DEVICE_OS_TYPE_WINDOWS;
static std::string GENERAL_BIDDING_DEVICE_OS_TYPE_LINUX;

std::string toJson();
static std::shared_ptr<Device> fromJson(std::string jsonModel);

virtual ~Device();

std::string getName();
std::string getDeviceType();
std::string getDeviceId();
std::string getDeviceUniqueName();
static std::shared_ptr<Device> fromJsonValue(RapidJsonValueType value);
static std::string createStandardDeviceId();
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
static std::shared_ptr<Device> buildAStandardDeviceIdBasedOn(std::string exchangeUserId, std::string deviceType);
};

#endif
