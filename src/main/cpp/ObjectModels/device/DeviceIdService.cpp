#include "DeviceIdService.h"
#include "DeviceVisitCountHistory.h"
#include "ConcurrentHashSet.h"
#include "ConcurrentHashMap.h"
#include "EntityToModuleStateStats.h"
#include "AtomicLong.h"
DeviceIdService::DeviceIdService(EntityToModuleStateStats* entityToModuleStateStatsArg) : Object(__FILE__) {
        NULL_CHECK(entityToModuleStateStatsArg);
        this->entityToModuleStateStats = entityToModuleStateStatsArg;
        deviceIdToVisitCounts =
                std::make_shared<gicapods::SizeAndTimeBasedLRUCache<
                                         std::string, std::shared_ptr<DeviceVisitCountHistory> > >(
                        10000,
                        30,
                        "DeviceVisitCountHistory");
        deviceIdToVisitCounts->entityToModuleStateStats = entityToModuleStateStats;
        this->numberOfDeviceUniqueDeviceIdsSeenLimit = 100; //TODO config driven
        this->numberOfVisitsByAllBidderLimit = 10;
        this->visitPeriodToCountForInSeconds = 6000;
        //this is a processed set that we read from db,
        persistentFrequentDeviceIds =
                std::make_shared<gicapods::SizeAndTimeBasedLRUCache<std::string,
                                                                    std::shared_ptr<std::string> > >(
                        10000,
                        30,
                        "persistentFrequentDeviceIds");
        persistentFrequentDeviceIds->entityToModuleStateStats = entityToModuleStateStats;
        //we should read these values from mysql

        snamespace = "deviceVisitCounts";
        set = "deviceVisitCounts";
        shortTermVisitCountBin = "stvisitCount";
        longTermVisitCountBin = "ltvisitCount";
}

DeviceIdService::~DeviceIdService() {

}

void DeviceIdService::validate(std::string deviceId) {

}

bool DeviceIdService::isBadDevice(std::string deviceId) {
        //first we increment the number of visit
        auto deviceVisitCountHistory = deviceIdToVisitCounts->get(deviceId);
        if (deviceVisitCountHistory == boost::none) {
                deviceVisitCountHistory = std::make_shared<DeviceVisitCountHistory>();
        }

        (*deviceVisitCountHistory)->numberOfVisitsBythisBidder->increment();


        //after we have enough data, first we ask the persistent store
        if (persistentFrequentDeviceIds->containKey(deviceId)) {
                //we check the local map to see if
                //its a bad device id

                entityToModuleStateStats->
                addStateModuleForEntity("DEVICE_ID_FOUND_IN_PERSISTENT_STORE",
                                        "DeviceIdService",
                                        EntityToModuleStateStats::all);
                return true;
        }


        //the deviceId was not in persistent store,
        //now we ask aerospike to get all visits by all bidders
        long visitsByAll = getVisitsByAll(deviceId, *deviceVisitCountHistory);
        if ( visitsByAll >= numberOfVisitsByAllBidderLimit) {
                entityToModuleStateStats->
                addStateModuleForEntity("TOO_MANY_VISITS_TO_ALL_BIDDERS",
                                        "DeviceIdService",
                                        EntityToModuleStateStats::all,
                                        EntityToModuleStateStats::important);

                queueDeviceIdForPersistentStore(deviceId);
                return true;
        }

        entityToModuleStateStats->
        addStateModuleForEntity("GOOD_DEVICE",
                                "DeviceIdService",
                                EntityToModuleStateStats::all);
        return false;
}

void DeviceIdService::queueDeviceIdForPersistentStore(std::string deviceId) {
        persistentFrequentDeviceIds->put(deviceId, nullptr);
}

long DeviceIdService::getVisitsByAll(
        std::string deviceId,
        std::shared_ptr<DeviceVisitCountHistory> deviceVisitCountHistory) {

        //we check if we have too many visits on this bidder, we update our number
        //with aerospike
        if (deviceVisitCountHistory->numberOfVisitsBythisBidder->getValue() > 30) {
                int valueToAddToCache =
                        deviceVisitCountHistory->numberOfVisitsBythisBidder->getValue();
                deviceVisitCountHistory->numberOfVisitsBythisBidder->setValue(0);
                long vistisByAll = aeroSpikeDriver->addValueAndRead(
                        snamespace,
                        set,
                        deviceId,
                        shortTermVisitCountBin,
                        valueToAddToCache,
                        visitPeriodToCountForInSeconds
                        );
                entityToModuleStateStats->
                addStateModuleForEntity("UPDATE_LOCAL_NUMBER_WITH_AERO",
                                        "DeviceVisitCountHistory",
                                        EntityToModuleStateStats::all);
                deviceVisitCountHistory->numberOfVisitsByAllBiddersInPeriod->setValue(vistisByAll);
        }
        //we check if we have ever asked aerospike about the visit counts for this device
        else if (deviceVisitCountHistory->numberOfVisitsByAllBiddersInPeriod->getValue() == -1) {
                //we have to ask aerospike for the value for the first time
                long vistisByAll = aeroSpikeDriver->addValueAndRead(
                        snamespace,
                        set,
                        deviceId,
                        shortTermVisitCountBin,
                        deviceVisitCountHistory->numberOfVisitsBythisBidder->getValue(),
                        visitPeriodToCountForInSeconds);
                entityToModuleStateStats->
                addStateModuleForEntity("ASKED_AERO_THE_VISIT_COUNT_FOR_FIRST_TIME",
                                        "DeviceVisitCountHistory",
                                        EntityToModuleStateStats::all);
                deviceVisitCountHistory->numberOfVisitsByAllBiddersInPeriod->setValue(vistisByAll);
        }

        return deviceVisitCountHistory->numberOfVisitsByAllBiddersInPeriod->getValue();
}
