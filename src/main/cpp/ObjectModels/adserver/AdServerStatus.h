/*
 * AdServerStatus.h
 *
 *  Created on: Aug 24, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_BIDDER_AdServerStatusENGINE_AdServerStatus_H_
#define GICAPODS_GICAPODSSERVER_SRC_BIDDER_AdServerStatusENGINE_AdServerStatus_H_


#include <memory>
#include <string>
#include "Object.h"

/**
 * a AdServerStatus has the information that captures the current status of adServer
 *
 */
class AdServerStatus;



class AdServerStatus : public Object {

public:

int numberOfImpressionsShownInLastMinute;

int numberOfClicksInLastMinute;

std::string lastTimeImpressionWasShown;

std::string lastTimeARequestWasProcessed;


AdServerStatus();

std::string toJson();

static std::shared_ptr<AdServerStatus> fromJson(std::string json);

virtual ~AdServerStatus();
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_BIDDER_AdServerStatusENGINE_AdServerStatus_H_ */
