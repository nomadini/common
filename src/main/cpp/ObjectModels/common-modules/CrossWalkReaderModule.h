#ifndef CrossWalkReaderModule_H
#define CrossWalkReaderModule_H

#include "Object.h"
#include <memory>
#include <string>
class IpToDeviceIdsMapCassandraService;
class DeviceIdToIpsMapCassandraService;
#include "TargetGroupFilterStatistic.h"
class EntityToModuleStateStats;
#include "IpToDeviceIdsMap.h"
#include "DeviceIdToIpsMap.h"

class Device;

template <class T>
class AeroCacheService;
class CrossWalkedDeviceMap;
class CrossWalkReaderModule : public Object {

private:

public:
IpToDeviceIdsMapCassandraService* ipToDeviceIdsMapCassandraService;
DeviceIdToIpsMapCassandraService* deviceIdToIpsMapCassandraService;
EntityToModuleStateStats* entityToModuleStateStats;
AeroCacheService<CrossWalkedDeviceMap>* crossWalkedDeviceMapAerospikeCacheService;

CrossWalkReaderModule();

std::string getName();

std::string toString();

std::vector<std::shared_ptr<Device> > readNearByDevices( std::shared_ptr<Device> device);
void writeKeyValueInCache(std::shared_ptr<CrossWalkedDeviceMap> finalValue);

std::vector<std::shared_ptr<Device> > getAllLinkedDevices(std::shared_ptr<DeviceIdToIpsMap> deviceIdToIpsMap);

std::shared_ptr<CrossWalkedDeviceMap>
tryFetchResultFromCache(std::shared_ptr<CrossWalkedDeviceMap> crossWalkedDeviceMap);

void cacheDeviceLinks(std::shared_ptr<CrossWalkedDeviceMap> crossWalkedDeviceMap,
                      std::vector<std::shared_ptr<Device> > allDevices);
virtual ~CrossWalkReaderModule();

};



#endif
