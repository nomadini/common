#ifndef FeatureGuardService_H
#define FeatureGuardService_H


#include <memory>
#include <string>
class IpToDeviceIdsMapCassandraService;
class DeviceIdToIpsMapCassandraService;
#include "TargetGroupFilterStatistic.h"
class EntityToModuleStateStats;
#include "IpToDeviceIdsMap.h"
#include "SizeBasedLRUCache.h"
#include "MySqlHighPerformanceService.h"
#include "DeviceIdToIpsMap.h"
#include <boost/optional.hpp>

class Device;
class MySqlFeatureRegistryService;
class MySqlFeatureUnderReviewService;

template <class T>
class AeroCacheService;
class CrossWalkedDeviceMap;
class Feature;
#include "Object.h"
namespace gicapods {
template <class K, class V>
class LRUCache;
}

/*
   how does feature guard service work ?


   the first thing that it does, it checks if a feature is allowed to be recorded in the system

        look up the featureHash from in memory,
            if it exists and status is ACTIVE--> it returns true as allowed to write
            if it doesnt exist-->
                read the siteDomain featureHash from aerospike
                    if it doesn't exist...submitTheDomainFoReviewIfNotExistingInCurrentSet and returns false
                    if it exists and status is ACTIVE--->it returns true as allowed to write

 */

class FeatureGuardService : public Object {

private:

public:

MySqlFeatureUnderReviewService* mySqlFeatureUnderReviewService;
MySqlHighPerformanceService<std::string, Feature>* mySqlHighPerformanceFeatureUnderReviewService;
MySqlFeatureRegistryService* mySqlFeatureRegistryService;
EntityToModuleStateStats* entityToModuleStateStats;
AeroCacheService<Feature>* featuresAerospikeCacheService;

std::shared_ptr<gicapods::SizeBasedLRUCache<std::string, std::shared_ptr<Feature> > > featuresCache;

FeatureGuardService();

std::string getName();

std::string toString();

bool isAllowedToRecord(std::string featureCategory,
                       std::string featureValue,
                       bool submitFeatureForReview);

boost::optional<std::shared_ptr<Feature> > fetchFeatureFromInMemoryMap(
        std::string featureCategory,
        std::string featureValue
        );

boost::optional<std::shared_ptr<Feature> > fetchFeatureFromInRemoteCache(
        std::string featureCategory,
        std::string featureValue
        );

void submitFeatureForReview(
        std::string featureCategory,
        std::string featureValue
        );

virtual ~FeatureGuardService();
};



#endif
