#include "RealTimeFeatureRegistryCacheUpdaterService.h"
#include "DateTimeUtil.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "DeviceIdToIpsMap.h"
#include "EntityToModuleStateStats.h"
#include "AtomicLong.h"

#include "IpToDeviceIdsMapCassandraService.h"
#include "DeviceIdToIpsMapCassandraService.h"
#include "Device.h"
#include "AeroCacheService.h"
#include "StringUtil.h"
#include "JsonArrayUtil.h"
#include "Feature.h"
#include "SizeBasedLRUCache.h"
#include "MySqlFeatureUnderReviewService.h"
#include "MySqlFeatureRegistryService.h"
#include "CollectionUtil.h"

RealTimeFeatureRegistryCacheUpdaterService::RealTimeFeatureRegistryCacheUpdaterService() : Object(__FILE__) {

}

std::string RealTimeFeatureRegistryCacheUpdaterService::getName() {
        return "RealTimeFeatureRegistryCacheUpdaterService";
}

//reads all the features under review and update their status
//this is run from BidderAsyncJobService, this is set to runEveryHour for now
void RealTimeFeatureRegistryCacheUpdaterService::updateFeaturesUnderReview() {

        //we do this, to move the geo features like MGRS100 type, to feature_registry
        //automatically..just to keep a record of our geo features, that enter the system
        mySqlFeatureUnderReviewService->activateGeoFeatures();


        std::vector<std::shared_ptr<Feature> > allFeaturesUnderReview =
                mySqlFeatureUnderReviewService->readAllFeaturesUnderReview();

        std::vector<std::shared_ptr<Feature> > allFeaturesToWrite;
        std::set<std::string> allFeaturesToWriteSet;
        for (auto featureUnderReview : allFeaturesUnderReview) {

                if (
                        (StringUtil::equalsIgnoreCase(featureUnderReview->getStatus(), Feature::ACTIVE_STATUS)
                         || StringUtil::equalsIgnoreCase(featureUnderReview->getStatus(), Feature::BLOCKED_STATUS))
                        &&  !StringUtil::equalsIgnoreCase(featureUnderReview->type, Feature::MGRS100)) {
                        //we only record features that are not MGRS100 and they are not active or blocked
                        if (!CollectionUtil::containsInList(featureUnderReview->getName(), allFeaturesToWriteSet)) {
                                allFeaturesToWrite.push_back(featureUnderReview);
                                allFeaturesToWriteSet.insert(featureUnderReview->getName());
                        }

                }

        }
        MLOG(3) << "allFeatures to register : " << JsonArrayUtil::convertListToJson(allFeaturesToWrite);
        mySqlFeatureRegistryService->writeFeatures(allFeaturesToWrite);
        mySqlFeatureUnderReviewService->deleteFromReview(allFeaturesToWrite);
}

//this is run from BidderAsyncJobService
void RealTimeFeatureRegistryCacheUpdaterService::populateAerospikeFeatureCache() {

        std::vector<std::shared_ptr<Feature> > allFeatures =
                mySqlFeatureRegistryService->readAllFeatures(
                        DateTimeUtil::getNowPlusHoursInMySqlFormat(-20)
                        );

        LOG(ERROR) << "loaded these : " << allFeatures.size() << " to insert into aerospike";

        for(auto feature : allFeatures) {
                MLOG(10) << "putting feature in aerospike : " << feature->getName();
                featuresAerospikeCacheService->putDataInCache(feature);
        }

}

RealTimeFeatureRegistryCacheUpdaterService::~RealTimeFeatureRegistryCacheUpdaterService() {

}
