#include "DeviceHistoryUpdaterModule.h"
#include "DateTimeUtil.h"
#include "EntityToModuleStateStats.h"
#include "ConcurrentHashSet.h"
#include "AtomicLong.h"
#include "ConcurrentHashMap.h"

#include "Feature.h"
#include "DeviceFeatureHistory.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "FeatureGuardService.h"


DeviceHistoryUpdaterModule::
DeviceHistoryUpdaterModule(
        DeviceFeatureHistoryCassandraService* deviceFeatureHistoryCassandraService) : Object(__FILE__) {
        this->deviceFeatureHistoryCassandraService = deviceFeatureHistoryCassandraService;
}


std::string DeviceHistoryUpdaterModule::getName() {
        return "DeviceHistoryUpdaterModule";
}


void DeviceHistoryUpdaterModule::process(
        std::string featureType,
        std::string featureValue,
        std::shared_ptr<Device> device) {

        //we don't submit the feature for review here...
        //because we don't sample any call to this function
        //and that will lead to too many entries in features_under_review table in mysql
        NULL_CHECK(featureGuardService);
        if (!featureGuardService->isAllowedToRecord(
                    featureType,
                    featureValue,
                    false /*submitFeatureForReview*/)) {
                entityToModuleStateStats->addStateModuleForEntity("FEATURE_IS_NOT_ALLOWED_TO_RECORD:"+ featureType,
                                                                  "DeviceHistoryUpdaterModule",
                                                                  EntityToModuleStateStats::all);
                return;
        }

        entityToModuleStateStats->addStateModuleForEntity("FEATURE_IS_ALLOWED_TO_RECORD",
                                                          "DeviceHistoryUpdaterModule",
                                                          EntityToModuleStateStats::all);
        assertAndThrow(!featureValue.empty());
        if (isAValidAndSensibleDevice(device)) {
                entityToModuleStateStats->addStateModuleForEntity("SENSIBLE_DEVICE",
                                                                  "DeviceHistoryUpdaterModule",
                                                                  EntityToModuleStateStats::all);
                prepFeatureForWrite(featureType, featureValue, device);
        } else {
                entityToModuleStateStats->addStateModuleForEntity("BAD_DEVICE_SEEN",
                                                                  "DeviceHistoryUpdaterModule",
                                                                  EntityToModuleStateStats::all);
        }

}


bool DeviceHistoryUpdaterModule::isAValidAndSensibleDevice(std::shared_ptr<Device> device) {
        //device->getDeviceId
        //increment number of seen in 10 minutes
        NULL_CHECK(deviceIdService);
        if(deviceIdService->isBadDevice(device->getDeviceId()) ) {
                entityToModuleStateStats->addStateModuleForEntity("FILTERED_BECAUSE_OF_BAD_DEVICE",
                                                                  "DeviceHistoryUpdaterModule",
                                                                  EntityToModuleStateStats::all);
                return false;
        }

        if (!deviceHistorySizeSampler->isPartOfSample()) {
                entityToModuleStateStats->addStateModuleForEntity("NOT_PART_OF_SAMPLE",
                                                                  "DeviceHistoryUpdaterModule",
                                                                  EntityToModuleStateStats::all);
                return false;
        }

        return true;
}


void DeviceHistoryUpdaterModule::prepFeatureForWrite(
        std::string featureType,
        std::string featureValue,
        std::shared_ptr<Device> device) {
        assertAndThrow(!featureValue.empty());

        auto deviceFeatureHistory = std::make_shared<DeviceFeatureHistory>(device);
        auto feature = std::make_shared<Feature>(featureType, featureValue, 0);
        auto featureHistory = std::make_shared<FeatureHistory>(feature, DateTimeUtil::getNowInMilliSecond());

        deviceFeatureHistory->getFeatures()->push_back(featureHistory);

        deviceFeatureHistoryCassandraService->pushToWriteBatchQueue(deviceFeatureHistory);
        entityToModuleStateStats->addStateModuleForEntity("writing_feature_type:" + featureType,
                                                          "DeviceHistoryUpdaterModule",
                                                          EntityToModuleStateStats::all,
                                                          EntityToModuleStateStats::important);
}


DeviceHistoryUpdaterModule::~DeviceHistoryUpdaterModule() {

}
