#include "GUtil.h"
#include "BadDevice.h"
#include "MySqlBadDeviceService.h"
#include "MySqlDriver.h"
#include "StringUtil.h"
#include "DateTimeUtil.h"
#include <boost/foreach.hpp>
#include "ConcurrentQueueFolly.h"

MySqlBadDeviceService::MySqlBadDeviceService(MySqlDriver* driver)  : MySqlService(driver, nullptr),Object(__FILE__) {
        this->driver = driver;
}

MySqlBadDeviceService::~MySqlBadDeviceService() {

}

std::shared_ptr<BadDevice> MySqlBadDeviceService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<BadDevice> obj = std::make_shared<BadDevice> ();

        obj->id = res->getInt ("id");
        obj->numberOfTimesSeenInPeriod = res->getInt ("number_Of_Times_Seen_In_Period");
        obj->deviceId = MySqlDriver::getString( res, "device_id");
        obj->deviceType = MySqlDriver::getString( res, "device_type");
        obj->dateCreated = MySqlDriver::getString( res, "date_created");
        return obj;
}

std::string MySqlBadDeviceService::getSelectAllQueryStatement() {
        static std::string selectAllQueryStatement = "SELECT "
                                                     " device_id,"
                                                     " device_type,"
                                                     " id,"
                                                     " number_Of_Times_Seen_In_Period,"
                                                     " date_created"
                                                     " FROM `bad_device`";
        return selectAllQueryStatement;
}

std::string MySqlBadDeviceService::getReadByIdSqlStatement(std::string deviceId) {
        std::string query = "SELECT "
                            " device_id,"
                            " device_type,"
                            " id,"
                            " number_Of_Times_Seen_In_Period,"
                            " date_created"
                            " FROM bad_device where device_id = '__device_id__'";


        query = StringUtil::replaceString (query, "__device_id__", deviceId);
        return query;
}

std::string MySqlBadDeviceService::getInsertObjectSqlStatement(std::shared_ptr<BadDevice> badDevice) {
        std::string queryStr =
                " INSERT INTO `bad_device`"
                " ("
                " device_id,"
                " device_type,"
                " number_Of_Times_Seen_In_Period,"
                " date_created"
                " )"
                " VALUES"
                " ( "
                " '__device_id__',"
                " '__device_type__',"
                " '__number_Of_Times_Seen_In_Period__',"
                " '__created_at__'"
                " );";

        queryStr = StringUtil::replaceString (queryStr, "__device_id__",
                                              badDevice->deviceId);
        queryStr = StringUtil::replaceString (queryStr, "__device_type__",
                                              badDevice->deviceType);

        queryStr = StringUtil::replaceString (queryStr, "__number_Of_Times_Seen_In_Period__",
                                              StringUtil::toStr(badDevice->numberOfTimesSeenInPeriod));

        queryStr = StringUtil::replaceString (queryStr, "__created_at__",
                                              DateTimeUtil::getNowInYYYMMDDFormat());
        return queryStr;
}

std::vector<std::shared_ptr<BadDevice>> MySqlBadDeviceService::readAllEntities() {
        return this->readAll();
}
