#ifndef MySqlBadDeviceService_h
#define MySqlBadDeviceService_h




#include "BadDevice.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include <tbb/concurrent_hash_map.h>
#include "DataProvider.h"
#include "MySqlService.h"
class EntityToModuleStateStats;
#include "ConcurrentQueueFolly.h"
#include "Object.h"
#include "MySqlModelService.h"

class MySqlBadDeviceService;




class MySqlBadDeviceService :
        public DataProvider<BadDevice>, public MySqlService<std::string, BadDevice>, public Object {

public:

MySqlDriver* driver;

MySqlBadDeviceService(MySqlDriver* driver);

std::shared_ptr<BadDevice> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);
std::string getSelectAllQueryStatement();
std::string getReadByIdSqlStatement(std::string id);
std::string getInsertObjectSqlStatement(std::shared_ptr<BadDevice> geoFeature);
std::vector<std::shared_ptr<BadDevice>> readAllEntities();
virtual ~MySqlBadDeviceService();

};


#endif
