#include "BadDevice.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>

BadDevice::BadDevice()   : Object(__FILE__) {
        id = 0;
        numberOfTimesSeenInPeriod = 0;
}


std::string BadDevice::toString() {
        return toJson ();
}

std::string BadDevice::getEntityName() {
        return "BadDevice";
}
void BadDevice::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "numberOfTimesSeenInPeriod", numberOfTimesSeenInPeriod, value);
        JsonUtil::addMemberToValue_FromPair (doc, "deviceId", deviceId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "deviceType", deviceType, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateCreated", dateCreated, value);
}

std::shared_ptr<BadDevice> BadDevice::fromJsonValue(RapidJsonValueType value) {


        std::shared_ptr<BadDevice> badDevice = std::make_shared<BadDevice>();
        badDevice->id = JsonUtil::GetIntSafely(value, "id");
        badDevice->numberOfTimesSeenInPeriod  =JsonUtil::GetIntSafely(value,"numberOfTimesSeenInPeriod");
        badDevice->deviceId = JsonUtil::GetStringSafely(value,"deviceId");
        badDevice->deviceType = JsonUtil::GetStringSafely(value,"deviceType");
        badDevice->dateCreated = JsonUtil::GetIntSafely(value,"dateCreated");
        return badDevice;
}

std::string BadDevice::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);

}

BadDevice::~BadDevice() {

}
