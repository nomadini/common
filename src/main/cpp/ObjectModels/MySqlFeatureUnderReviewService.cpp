#include "GUtil.h"
#include "MySqlFeatureUnderReviewService.h"
#include "MySqlDriver.h"
#include "DateTimeUtil.h"
#include "Feature.h"
#include "CollectionUtil.h"
#include "StringUtil.h"

MySqlFeatureUnderReviewService::MySqlFeatureUnderReviewService(
        MySqlDriver* driver,
        EntityToModuleStateStats* entityToModuleStateStats) :
        MySqlService(driver, entityToModuleStateStats),Object(__FILE__) {
        this->driver = driver;
}

std::shared_ptr<Feature> MySqlFeatureUnderReviewService::mapResultSetToObjectStatic(std::shared_ptr<ResultSetHolder> res) {

        std::shared_ptr<Feature> feature =
                std::make_shared<Feature>(
                        res->getString ("type"),
                        MySqlDriver::getString( res, "name"),
                        0);
        feature->id = res->getInt ("id");
        feature->status = MySqlDriver::getString( res, "status");

        return feature;
}

std::shared_ptr<Feature> MySqlFeatureUnderReviewService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        return MySqlFeatureUnderReviewService::mapResultSetToObjectStatic(res);
}

std::vector<std::shared_ptr<Feature> > MySqlFeatureUnderReviewService::readAllFeaturesUnderReview() {
        std::vector<std::shared_ptr<Feature> > allFeatures;
        std::string query = "SELECT "
                            " `id`,"
                            " `name`,"
                            " `type`, "
                            " `status` from features_under_review";


        auto res = driver->executeQuery (query);

        while (res->next ()) {
                allFeatures.push_back(mapResultSetToObject(res));
        }

        return allFeatures;
}

void MySqlFeatureUnderReviewService::deleteFromReview(std::vector<std::shared_ptr<Feature> > allFeaturesToWrite) {
        if (allFeaturesToWrite.empty()) {
                return;
        }
        std::string query = " DELETE from features_under_review where id in (__IDS__);";

        std::vector<std::string> allValues;
        for(auto feature : allFeaturesToWrite) {
                allValues.push_back(_toStr(feature->id));
        }

        auto properAllValues = StringUtil::joinArrayAsString(allValues, ",");
        MLOG(3) << "properAllValues : " << properAllValues;
        query = StringUtil::replaceString (query, "__IDS__", properAllValues);

        LOG(ERROR) << "deleting all features under review : " << query;
        driver->executedUpdateStatement (query);
}

std::string MySqlFeatureUnderReviewService::getReadByIdSqlStatement(std::string nameOfFeature) {

        std::string query = "SELECT "
                            " id,"
                            " name,"
                            " type,"
                            " status"
                            " FROM features_under_review where name = '__nameOfFeature__'";

        query = StringUtil::replaceString (query, "__nameOfFeature__", nameOfFeature);
        return query;
}

std::string MySqlFeatureUnderReviewService::convertRecordToValueString(std::shared_ptr<Feature> feature) {

        std::string oneRowValue =
                " "
                " ("
                " '__name__',"
                " '__type__', "
                " '__status__' "
                " ) ";


        oneRowValue = StringUtil::replaceString (oneRowValue, "__name__", feature->name);
        oneRowValue = StringUtil::replaceString (oneRowValue, "__type__", feature->type);
        oneRowValue = StringUtil::replaceString (oneRowValue, "__status__", feature->status);

        // oneRowValue = StringUtil::replaceString (oneRowValue, "__created_at__", DateTimeUtil::dateTimeToStr(metricDbRecord->createdAt));
        // oneRowValue = StringUtil::replaceString (oneRowValue, "__updated_at__", DateTimeUtil::dateTimeToStr(metricDbRecord->updatedAt));

        return oneRowValue;
}

std::string MySqlFeatureUnderReviewService::getSelectAllQueryStatement() {
        static std::string selectAllQueryStatement = "SELECT "
                                                     " id,"
                                                     " name,"
                                                     " type,"
                                                     " status "

                                                     " FROM `features_under_review`";
        return selectAllQueryStatement;
}

std::string MySqlFeatureUnderReviewService::getInsertObjectSqlStatement(std::shared_ptr<Feature> feature) {
        std::string queryStr =
                " INSERT INTO `features_under_review`"
                " ("
                " name,"
                " type,"
                " status"
                " )"
                " VALUES"
                " ( "
                " '__name__',"
                " '__type__',"
                " '__status__'"
                " );";

        queryStr = StringUtil::replaceString (queryStr, "__name__",
                                              feature->name);
        queryStr = StringUtil::replaceString (queryStr, "__type__",
                                              feature->type);

        queryStr = StringUtil::replaceString (queryStr, "__status__",
                                              feature->status);


        return queryStr;
}

void MySqlFeatureUnderReviewService::activateGeoFeatures() {
        std::string query = "update features_under_review set status='ACTIVE' where type = 'MGRS100'";
        driver->executedUpdateStatement (query);
}

MySqlFeatureUnderReviewService::~MySqlFeatureUnderReviewService() {
}
