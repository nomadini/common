/*
 * WiretapCassandraService.h
 *
 *  Created on: Jul 18, 2015
 *      Author: mtaabodi
 */

#ifndef WiretapCassandraService_H_
#define WiretapCassandraService_H_

class CassandraDriverInterface;

#include "Wiretap.h"
#include "Object.h"
class DateTimeUtil;


namespace gicapods { class ConfigService; }
#include "HttpUtilService.h"
#include "CassandraService.h"
class EntityToModuleStateStats;
class Device;

class AsyncThreadPoolService;
class WiretapCassandraService;

#include "CassandraService.h"



class WiretapCassandraService : public CassandraService<Wiretap>, public Object {

private:
CassandraDriverInterface* cassandraDriver;
HttpUtilService* httpUtilService;
EntityToModuleStateStats* entityToModuleStateStats;
public:


WiretapCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,
        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService);

virtual ~WiretapCassandraService();

void record(std::shared_ptr<Wiretap> wiretap);

virtual void deleteAll();
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_CASSANDRA_ADHISTORYCASSANDRASERVICE_H_ */
