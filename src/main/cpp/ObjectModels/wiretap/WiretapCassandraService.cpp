#include "CassandraDriverInterface.h"

#include "Wiretap.h"
#include "DateTimeUtil.h"
#include "WiretapCassandraService.h"
#include "ConfigService.h"
WiretapCassandraService::WiretapCassandraService(
        CassandraDriverInterface* cassandraDriver,
        HttpUtilService* httpUtilService,
        EntityToModuleStateStats* entityToModuleStateStats,
        AsyncThreadPoolService* asyncThreadPoolService,
        gicapods::ConfigService* configService) :
        CassandraService(cassandraDriver,
                         entityToModuleStateStats,
                         asyncThreadPoolService,
                         configService), Object("WiretapCassandraService") {
        assertAndThrow(cassandraDriver != NULL);
        assertAndThrow(httpUtilService != NULL);
        assertAndThrow(entityToModuleStateStats != NULL);

        this->cassandraDriver = cassandraDriver;
        this->httpUtilService = httpUtilService;
        this->entityToModuleStateStats = entityToModuleStateStats;
}


void WiretapCassandraService::record(std::shared_ptr<Wiretap> wiretap) {
        pushToWriteBatchQueue(wiretap);
}

void WiretapCassandraService::deleteAll() {
        cassandraDriver->deleteAll ("adhistory");
}

WiretapCassandraService::~WiretapCassandraService() {

}
