
#include "Device.h"
#include "GUtil.h"
#include "Wiretap.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
Wiretap::Wiretap() : Object("Wiretap") {

}

Wiretap::~Wiretap() {

}

std::string Wiretap::toString() {
								return this->toJson();
}

std::string Wiretap::getName() {
								return "Wiretap";
}


std::shared_ptr<Wiretap> Wiretap::fromJsonValue(RapidJsonValueType value) {

								std::shared_ptr<Wiretap> wiretap = std::make_shared<Wiretap>();

								wiretap->eventId = JsonUtil::GetStringSafely(value,"eventId");
								wiretap->eventTime = JsonUtil::GetLongSafely(value,"eventTime");
								wiretap->appName = JsonUtil::GetStringSafely(value,"appName");
								wiretap->appVersion = JsonUtil::GetStringSafely(value,"appVersion");
								wiretap->appHost = JsonUtil::GetStringSafely(value,"appHost");
								wiretap->moduleName = JsonUtil::GetStringSafely(value,"moduleName");
								wiretap->request = JsonUtil::GetStringSafely(value,"request");
								wiretap->response = JsonUtil::GetStringSafely(value,"response");

								return wiretap;

}
std::shared_ptr<Wiretap> Wiretap::fromJson(std::string jsonString) {
								auto document = parseJsonSafely(jsonString);
								return fromJsonValue(*document);
}

std::string Wiretap::toJson() {
								auto doc = JsonUtil::createADcoument(rapidjson::kObjectType);
								RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
								addPropertiesToJsonValue(value, doc.get());
								return JsonUtil::valueToString(value);
}

void Wiretap::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

								JsonUtil::addMemberToValue_FromPair(doc, "eventId", eventId, value);
								JsonUtil::addMemberToValue_FromPair(doc, "eventTime", eventTime, value);
								JsonUtil::addMemberToValue_FromPair(doc, "appName", appName, value);
								JsonUtil::addMemberToValue_FromPair(doc, "appVersion", appVersion, value);
								JsonUtil::addMemberToValue_FromPair(doc, "appHost", appHost, value);
								JsonUtil::addMemberToValue_FromPair(doc, "moduleName", moduleName, value);
								JsonUtil::addMemberToValue_FromPair(doc, "request", request, value);
								JsonUtil::addMemberToValue_FromPair(doc, "response", response, value);
}
