#include "GlobalWhiteListModelEntry.h"
#include "JsonUtil.h"
#include "StringUtil.h"

GlobalWhiteListModelEntry::GlobalWhiteListModelEntry()  : Object(__FILE__) {
        id = 0;
}

GlobalWhiteListModelEntry::~GlobalWhiteListModelEntry() {

}

std::shared_ptr<GlobalWhiteListModelEntry> GlobalWhiteListModelEntry::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<GlobalWhiteListModelEntry> bwList = std::make_shared<GlobalWhiteListModelEntry>();

        bwList->setId(JsonUtil::GetIntSafely(value, "id"));
        bwList->setDomainName(JsonUtil::GetStringSafely(value, "domainName"));
        bwList->domainType = JsonUtil::GetStringSafely(value, "domainType");
        bwList->status  = JsonUtil::GetStringSafely(value, "status");

        return bwList;
}

std::string GlobalWhiteListModelEntry::getEntityName() {
        return "GlobalWhiteListModelEntry";
}
void GlobalWhiteListModelEntry::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair(doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair(doc, "domainName", domainName, value);
        JsonUtil::addMemberToValue_FromPair(doc, "domainType", domainType, value);
        JsonUtil::addMemberToValue_FromPair(doc, "status", status, value);

}

std::string GlobalWhiteListModelEntry::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}


void GlobalWhiteListModelEntry::setId(int id) {
        this->id = id;
}

void GlobalWhiteListModelEntry::setDomainName(std::string domainName) {
        this->domainName = StringUtil::toLowerCase (domainName);
}

void GlobalWhiteListModelEntry::setCreatedAt(std::string createdAt) {
        this->createdAt = createdAt;
}

void GlobalWhiteListModelEntry::setUpdatedAt(std::string updatedAt) {
        this->updatedAt = updatedAt;
}

int GlobalWhiteListModelEntry::getId() {
        return id;
}

std::string GlobalWhiteListModelEntry::getDomainName() {
        return domainName;
}

std::string GlobalWhiteListModelEntry::getCreatedAt() {
        return createdAt;
}

std::string GlobalWhiteListModelEntry::getUpdatedAt() {
        return updatedAt;
}
