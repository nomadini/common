#ifndef MySqlClientService_h
#define MySqlClientService_h

#include "Object.h"
class Client;
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "DataProvider.h"

class MySqlClientService;




class MySqlClientService : public DataProvider<Client>, public Object {

public:
MySqlDriver* driver;

MySqlClientService(MySqlDriver* driver);

virtual ~MySqlClientService();

void parseTheAttribute(std::shared_ptr<Client> obj, std::string attributesFromDB);

std::vector<std::shared_ptr<Client> > readAllClients();

void insert(std::shared_ptr<Client> Client);

virtual void deleteAll();

std::vector<std::shared_ptr<Client> > readAllEntities();

};

#endif
