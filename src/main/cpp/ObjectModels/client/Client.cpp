#include "Client.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "CollectionUtil.h"
#include "TestUtil.h"
#include <boost/foreach.hpp>
#include "DateTimeUtil.h"
Client::Client() : Object(__FILE__) {
        id = 0;
}

std::string Client::toString() {
        return toJson ();
}

std::shared_ptr<Client> Client::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<Client> client = std::make_shared<Client>();

        client->id = JsonUtil::GetIntSafely(value, "id");

        client->name = JsonUtil::GetStringSafely(value,"name");
        client->status = JsonUtil::GetStringSafely(value,"status");

        client->updatedAt = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"updatedAt"));
        client->createdAt = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"createdAt"));

        return client;
}

void Client::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

        JsonUtil::addMemberToValue_FromPair ( doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (  doc, "name", name, value);
        JsonUtil::addMemberToValue_FromPair (  doc, "status", status, value);
        JsonUtil::addMemberToValue_FromPair (  doc, "createdAt", DateTimeUtil::dateTimeToStr(createdAt), value);
        JsonUtil::addMemberToValue_FromPair (  doc, "updatedAt", DateTimeUtil::dateTimeToStr(updatedAt), value);
}

std::string Client::getEntityName() {
        return "Client";
}

std::string Client::toJson() {
        auto doc = JsonUtil::createDcoumentAsObjectDoc();
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}

Client::~Client() {

}
