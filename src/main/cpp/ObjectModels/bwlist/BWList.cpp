
#include "BWList.h"

#include "JsonUtil.h"
#include "StringUtil.h"
#include <boost/foreach.hpp>
#include "JsonArrayUtil.h"
#include "GUtil.h"

std::string BWList::WHITE_LIST_TYPE = "whitelist";
std::string BWList::BLACK_LIST_TYPE = "blacklist";

BWList::BWList()   : Object(__FILE__) {
        this->bwEntries = std::make_shared<std::vector<std::shared_ptr<BWEntry> > >();
        clientId = 0;
        id = 0;
}

BWList::~BWList() {

}


std::shared_ptr<BWList> BWList::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<BWList> bwList = std::make_shared<BWList>();
        bwList->bwEntries = std::make_shared<std::vector<std::shared_ptr<BWEntry> > >();

        bwList->setId(JsonUtil::GetIntSafely(value, "id"));
        bwList->setName(JsonUtil::GetStringSafely(value, "name"));

        bwList->setListType (JsonUtil::GetStringSafely(value,"listType"));
        bwList->setClientId(JsonUtil::GetIntSafely(value,"clientId"));

        JsonArrayUtil::getArrayFromValueMemeber(value, "bwEntries", *(bwList->bwEntries));

        return bwList;
}

void BWList::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

        JsonUtil::addMemberToValue_FromPair(doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair(doc, "name", name, value);
        JsonUtil::addMemberToValue_FromPair(doc, "listType", listType, value);
        JsonUtil::addMemberToValue_FromPair(doc, "clientId", clientId, value);
        JsonArrayUtil::addMemberToValue_FromArrayOfObjects(doc, value, "bwEntries", *bwEntries);
}

std::string BWList::toJson() {

        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}

void BWList::setId(int id) {
        this->id = id;
}

void BWList::setListType(std::string listType) {

        if (!StringUtil::equalsIgnoreCase(listType, BWList::WHITE_LIST_TYPE) &&
            !StringUtil::equalsIgnoreCase(listType, BWList::BLACK_LIST_TYPE)) {
                throwEx ("wrong listType :" + listType);
        }

        this->listType = listType;
}

void BWList::setName(std::string name) {
        this->name = name;
}

void BWList::setClientId(int clientId) {
        this->clientId = clientId;
}

int BWList::getId() {
        return id;
}

std::string BWList::getListType() {
        return listType;
}

std::string BWList::getName() {
        return name;
}
int BWList::getClientId() {
        return clientId;
}
std::string BWList::getCreatedAt() {
        return createdAt;
}
std::string BWList::getUpdatedAt() {
        return updatedAt;
}

void BWList::setBwEntries(std::shared_ptr<std::vector<std::shared_ptr<BWEntry> > > bwEntries) {
        this->bwEntries = bwEntries;
}

std::shared_ptr<std::vector<std::shared_ptr<BWEntry> > > BWList::getBwEntries() {
        return this->bwEntries;
}

void BWList::setCreatedAt(std::string createdAt) {
        this->createdAt = createdAt;
}

void BWList::setUpdatedAt(std::string updatedAt) {
        this->updatedAt = updatedAt;
}
