#include "GUtil.h"
#include "MySqlDriver.h"
#include "CollectionUtil.h"
#include "MySqlBWEntryService.h"
#include "MySqlDriver.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include <Util/DateTimeUtil.h>
#include "BWEntry.h"

MySqlBWEntryService::MySqlBWEntryService(MySqlDriver* driver)  : Object(__FILE__) {
        this->driver = driver;

}

MySqlBWEntryService::~MySqlBWEntryService() {

}

std::vector<std::shared_ptr<BWEntry> > MySqlBWEntryService::readAll() {
        std::vector<std::shared_ptr<BWEntry> > all;
        std::string query = "SELECT `ID`," //1
                            " `domain_name`," //2
                            " `bwlist_id`," //3
                            " `created_at`," //6
                            " `updated_at`" //7
                            " FROM bwentries";


        auto res = driver->executeQuery (query);

        while (res->next ()) {
                std::shared_ptr<BWEntry> bwEntry = std::make_shared<BWEntry> ();
                bwEntry->setId(res->getInt (1));
                //removing bad characters that might come up because of copy paste in ui
                auto domaniName = res->getString (2);
                domaniName = StringUtil::replaceString(domaniName, "\r", "");
                domaniName = StringUtil::replaceString(domaniName, " ", "");
                domaniName = StringUtil::replaceString(domaniName, "\n", "");
                if(domaniName.empty()) {
                        continue;
                }
                bwEntry->setDomainName(domaniName);
                bwEntry->setBwListId(res->getInt (3));

                all.push_back (bwEntry);
        }



        return all;
}

std::shared_ptr<std::vector<std::shared_ptr<BWEntry> > > MySqlBWEntryService::readAllByBWListId(int id) {
        std::shared_ptr<std::vector<std::shared_ptr<BWEntry> > > all = std::make_shared<std::vector<std::shared_ptr<BWEntry> > >();
        std::string query = "SELECT `ID`," //1
                            " `domain_name`," //2
                            " `bwlist_id`," //3
                            " `created_at`," //6
                            " `updated_at`" //7
                            " FROM bwentries where bwlist_id = __BW_LIST_ID__";

        query = StringUtil::replaceString (query, "__BW_LIST_ID__", StringUtil::toStr (id));
        MLOG(3) << "query : " << query;


        auto res = driver->executeQuery (query);


        while (res->next ()) {
                std::shared_ptr<BWEntry> bwEntry  = std::make_shared<BWEntry> ();
                bwEntry->setId(res->getInt (1));
                bwEntry->setDomainName(res->getString (2));
                bwEntry->setBwListId(res->getInt (3));

                all->push_back (bwEntry);
        }




        return all;

}

std::shared_ptr<BWEntry> MySqlBWEntryService::read(int id) {
        std::shared_ptr<BWEntry> bwEntry  = std::make_shared<BWEntry> ();
        std::string query = "SELECT `ID`," //1
                            " `domain_name`," //2
                            " `bwlist_id`," //3
                            " `created_at`," //6
                            " `updated_at`" //7
                            " FROM bwentries where ID = __ID__";

        query = StringUtil::replaceString (query, "__ID__", StringUtil::toStr (id));


        auto res = driver->executeQuery (query);


        while (res->next ()) {

                bwEntry->setId(res->getInt (1));
                bwEntry->setDomainName(res->getString (2));
                bwEntry->setBwListId (res->getInt (3));

        }



        return bwEntry;
}

void MySqlBWEntryService::update(std::shared_ptr<BWEntry> obj) {

}

void MySqlBWEntryService::insert(std::shared_ptr<BWEntry> obj) {

        MLOG(3) << "inserting new BWEntry in db : " << obj->toJson ();
        std::string queryStr =
                "INSERT INTO bwentries"
                " ("
                " domain_name,"
                " bwlist_id,"
                " created_at,"
                " updated_at)  "
                " VALUES"
                " ("
                " '__domain_name__',"
                " '__bwlist_id__',"
                " '__created_at__',"
                " '__updated_at__'"
                " );";
        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "__domain_name__",
                                              StringUtil::toLowerCase (obj->getDomainName()));
        queryStr = StringUtil::replaceString (queryStr, "__bwlist_id__",
                                              StringUtil::toStr (obj->getBwListId()));

        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);

        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);


        //MLOG(3) << "queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);
        obj->setId(driver->getLastInsertedId ());


}

void MySqlBWEntryService::deleteAll() {

        driver->deleteAll("bwentries");

}
