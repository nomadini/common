/*
 * MySqlBWListServiceMock.h
 *
 *  Created on: Aug 2, 2015
 *      Author: mtaabodi
 */

#ifndef MySqlBWListServiceMock_H_
#define MySqlBWListServiceMock_H_

#include <string>
#include <memory>
#include <curl/curl.h>
#include <unordered_map>
#include "MySqlBWListService.h"
#include "gmock/gmock.h"
#include "MySqlDriverMock.h"

class MySqlBWListServiceMock;



class MySqlBWListServiceMock : public MySqlBWListService {

public:
std::shared_ptr<MySqlDriverMock> mySqlDriverMock;
MySqlBWListServiceMock() : MySqlBWListService(nullptr){

}

virtual ~MySqlBWListServiceMock() {

}
MOCK_METHOD1(readWithBwEntries, std::shared_ptr<BWList> (int bwListId));

};
#endif /* GICAPODS_GICAPODSSERVER_SRC_UTIL_MySqlBWListServiceMock_H_ */
