

#include "AsyncThreadPoolService.h"
#include "EntityToModuleStateStats.h"
#include "Poco/Exception.h"
#include <thread>

#include "DateTimeUtil.h"
#include "GUtil.h"
AsyncThreadPoolService::AsyncThreadPoolService()  : Object(__FILE__) {
        this->queueOfTasks =
                std::make_shared<tbb::concurrent_queue<std::shared_ptr<NomadiniTask> > > ();
        this->queueOfRunningTasks =
                std::make_shared<tbb::concurrent_queue<std::shared_ptr<NomadiniTask> > > ();
        this->queueOfTasksWithDelay =
                std::make_shared<tbb::concurrent_queue<std::shared_ptr<NomadiniTask> > > ();
        this->threadPool = std::make_shared<Poco::ThreadPool>();

        submitTasksIntervalInSecond = 1;
        isAllowedToRun = std::make_shared<gicapods::AtomicBoolean>(true); //this is the flag that thread waits for
        threadIsFinished = std::make_shared<gicapods::AtomicBoolean>(false);
}


AsyncThreadPoolService::~AsyncThreadPoolService() {
        isAllowedToRun->setValue(false); //set the flag to false to make the thread stop
        while(!threadIsFinished->getValue()) {
                LOG(ERROR) << "waiting for the AsyncThreadPoolService thread to stop";
                gicapods::Util::sleepViaBoost(_L_, 1);
        }
}

void AsyncThreadPoolService::submitTask(std::shared_ptr<NomadiniTask> task) {
        entityToModuleStateStats->addStateModuleForEntity("TASK_SUBMITTED",
                                                          "AsyncThreadPoolService",
                                                          "ALL");
        queueOfTasks->push(task);
}

void AsyncThreadPoolService::submitTaskWithDelay(std::shared_ptr<NomadiniTask> task) {
        entityToModuleStateStats->addStateModuleForEntity("TASK_TO_DELAY_QUEUE",
                                                          "AsyncThreadPoolService",
                                                          "ALL");
        queueOfTasksWithDelay->push(task);
}



void AsyncThreadPoolService::runTasksInSeperateThread() {
        gicapods::Util::sleepViaBoost(_L_, 1);
        std::thread mainThread(&AsyncThreadPoolService::runTasks, this);
        mainThread.detach();

}

void AsyncThreadPoolService::runTasks() {

        NULL_CHECK(entityToModuleStateStats);
        while(isAllowedToRun->getValue()) {

                int numberOfTasksToSubmitAtOnce = 32;
                if (queueOfTasks->unsafe_size() > 0) {
                        for(int i=0; i< numberOfTasksToSubmitAtOnce; i++) {
                                submitOneTask(queueOfTasks);
                        }
                        entityToModuleStateStats->
                        addStateModuleForEntity("SUBMITTING_TASKS_FROM_QUEUE",
                                                "AsyncThreadPoolService",
                                                "ALL");
                        threadPool->joinAll();
                }

                if (queueOfTasksWithDelay->unsafe_size() > 0) {
                        for(int i=0; i< numberOfTasksToSubmitAtOnce; i++) {
                                submitOneTask(queueOfTasksWithDelay);
                        }
                        entityToModuleStateStats->
                        addStateModuleForEntity("SUBMITTING_TASKS_WITH_DELAY",
                                                "AsyncThreadPoolService",
                                                "ALL");
                        threadPool->joinAll();
                }

                if (queueOfRunningTasks->unsafe_size() > 0) {
                        deleteDoneTasks();
                }

                if ( queueOfTasks->unsafe_size() > 500) {
                        LOG_EVERY_N(ERROR, 1) <<"number of tasks in queue is too high : " << queueOfTasks->unsafe_size();
                }

                gicapods::Util::sleepViaBoost(_L_, submitTasksIntervalInSecond);
        }

        threadIsFinished->setValue(true);
}
void AsyncThreadPoolService::deleteDoneTasks() {
        long timeNowInMillis = DateTimeUtil::getNowInMilliSecond();
        int numberOfInspectionsInOneRound = queueOfRunningTasks->unsafe_size();
        while (queueOfRunningTasks->unsafe_size() > 0) {
                std::shared_ptr<NomadiniTask> taskPtr;
                if(queueOfRunningTasks->try_pop(taskPtr)) {

                        numberOfInspectionsInOneRound++;
                        long millisFinishedAgo = timeNowInMillis  - taskPtr->timeFinishedInMillis;
                        if (millisFinishedAgo > 10000) {
                                //task finished 10 seconds ago,
                                //we don't put it back in queue, so it will be deleted autoically

                        } else {
                                //delete didn't happen, push back in queueOfRunningTasks
                                queueOfRunningTasks->push(taskPtr);
                        }
                }

                if (numberOfInspectionsInOneRound >= 1000) {
                        //we break out of loop after 1000 inspections
                        break;
                }
        }
}
void AsyncThreadPoolService::submitOneTask(
        std::shared_ptr<tbb::concurrent_queue<std::shared_ptr<NomadiniTask> > > sourceQueueOfTasks) {
        std::shared_ptr<NomadiniTask> task;
        try {

                if (!sourceQueueOfTasks->try_pop(task)) {
                        return;
                }

                //we create shared_ptr from raw


                if (threadPool->available() > 0) {

                        if (task->isReadyToBeSubmitted() == true) {
                                entityToModuleStateStats->addStateModuleForEntity("TASK_STARTED",
                                                                                  "AsyncThreadPoolService",
                                                                                  "ALL");
                                threadPool->start(*task);
                                //task successfully submitted to thread
                                //now we push to queueOfRunningTasks
                                queueOfRunningTasks->push(task);
                        }
                } else {
                        entityToModuleStateStats->addStateModuleForEntity("NO_THREAD_AVAILABLE",
                                                                          "AsyncThreadPoolService",
                                                                          "ALL");
                        //sleep a little bit more to wait for threads to
                        // become availabe again
                        gicapods::Util::sleepViaBoost(_L_, 1);

                        //we didn't submit this task, pushing it back
                        sourceQueueOfTasks->push(task);
                }
        } catch(...) {
                //pushing task back to queue in case of failure
                LOG(ERROR) << "task failed";

                if (task != nullptr) {
                        sourceQueueOfTasks->push(task);
                }
                entityToModuleStateStats->addStateModuleForEntity("TASK_FAILED",
                                                                  "AsyncThreadPoolService",
                                                                  "ALL");
                gicapods::Util::showStackTrace();
                LOG(ERROR) <<"error writing executing task";

        }

}
