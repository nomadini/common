#ifndef UaParser_h
#define UaParser_h

#include <string>
#include "ConcurrentHashMap.h"

#include <string>
#include "Object.h"
#include "JsonTypeDefs.h"
#include <string>
#include <memory>
#include <boost/thread.hpp>
namespace uacpp {
/*
   Based on this project : https://github.com/ua-parser/uap-cpp/blob/master/UaParser.h
 */
struct Generic {
        std::string family;
};

struct Device : Generic {
        std::string model;
        std::string brand;
        std::string toString() const {
                return "Device:{" + model + "," + brand +"}";
        }
};

struct Agent : Generic {
        std::string major;
        std::string minor;
        std::string patch;
        std::string patch_minor;

        std::string toString() const {
                return family + " " + toVersionString();
        }

        std::string toVersionString() const {
                return (major.empty() ? "0" : major) + "." + (minor.empty() ? "0" : minor) + "." + (patch.empty() ? "0" : patch);
        }
};

struct UserAgent {
        Device device;

        Agent os;
        Agent browser;

        std::string toFullString() const {
                return browser.toString() + "/" + os.toString() + "/"+ device.toString();
        }

        bool isSpider() const {
                return device.family == "Spider";
        }
};

class UserAgentClass : public Object {
public:
UserAgent ua;
UserAgentClass();

std::string toJson();

static std::shared_ptr<UserAgentClass> fromJson(std::string jsonString);
virtual ~UserAgentClass();
};


class UaParser;
class UaParser {

boost::shared_mutex _access;
public:
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, UserAgentClass> > userAgentCache;
explicit UaParser(const std::string& regexes_file_path);

UserAgent parse(const std::string&) const;

std::shared_ptr<UserAgentClass> get(const std::string&);

static std::shared_ptr<UaParser> instance();

virtual ~UaParser();

private:
const std::string regexes_file_path_;
const void* ua_store_;
};
}

#endif
