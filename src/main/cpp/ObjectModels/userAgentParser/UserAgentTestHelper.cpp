
#include "UserAgentTestHelper.h"
#include <string>
#include <memory>
#include <vector>

#include "UserAgentTestHelper.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "RandomUtil.h"
#include "FileUtil.h"
#include "StringUtil.h"

#include "UaParser.h"

std::shared_ptr <std::vector<std::string> > UserAgentTestHelper::getUserAgentSamples() {
        static std::shared_ptr <std::vector<std::string> > userAgentSamples =
                std::make_shared < std::vector < std::string > > ();
        return userAgentSamples;

}

std::string UserAgentTestHelper::getRandomUserAgentFromFiniteSet() {
        if (UserAgentTestHelper::getUserAgentSamples ()->empty ()) {
                //TODO : config driven
                auto file = "/home/workspace/Common/src/main/cpp/ObjectModels/userAgentParser/UserAgentSamples.txt";
                std::vector<std::string> allUas = FileUtil::readFileLineByLine(file);
                auto uaParser = uacpp::UaParser::instance();
                uaParser->get("");
                for (auto ua : allUas) {
                        UserAgentTestHelper::getUserAgentSamples ()->push_back (ua);
                        //this is for testing user agent
                        MLOG(3)<<"ua to parse :"<<ua;
                        uaParser->get(ua);
                }
        }

        return UserAgentTestHelper::getUserAgentSamples ()->at (RandomUtil::sudoRandomNumber (getUserAgentSamples ()->size() - 1));

}
