//
// Created by Mahmoud Taabodi on 2/15/16.
//

#ifndef MySqlDataMover_h
#define MySqlDataMover_h




class ImpressionEventDto;
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "MySqlService.h"
#include "MySqlMoveableEntityTraits.h"
#include "Object.h"
/**
   This class moves any entity that has a temp table in mysql to the main table in mysql.
   we usually use this class to move the result of a spark job to the final directory.
 */
template<class T>
class MySqlDataMover : public Object {

private:
std::string coreFieldsToRead;

public:

MySqlMoveableEntityTraits<T> traits;

MySqlDriver* driver;
long lastTimeDataMoved;
MySqlDataMover(MySqlDriver* driver);

virtual ~MySqlDataMover();


void inserterThread();

std::set<std::string> readAllTablesWithTempData();
std::set<std::string> readAllDistinctDates(std::string tableName);
int deleteAllCurrentDataWithDatesInMainTable(std::set<std::string> allDatesToInsert);
void dropTempTable(std::string tempTable);
int insertDataFromTempToMainTable(std::set<std::string> allDatesToInsert, std::string tableName);

};

#endif
