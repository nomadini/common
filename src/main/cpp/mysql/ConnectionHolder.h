#ifndef ConnectionHolder_h
#define ConnectionHolder_h



#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include "mysql_connection.h"
#include "DateTimeMacro.h"
#include <vector>
#include <memory>
#include "Object.h"
namespace gicapods { class ConfigService; }


#include <boost/thread.hpp>
class ConnectionHolder;




class ConnectionHolder : public Object {

private:
gicapods::ConfigService* configService;

sql::Driver *driver;
std::string urlConnection;
std::string username;
std::string password;
std::string schema;

//can't make it a smart pointer! because of unimplemented virutal functions in sql::connection class
sql::Connection* connection;
int indexInPool;
bool busy;
TimeType lastTimeAcquired;
TimeType timeConnectionCreated;
std::shared_ptr<boost::shared_mutex> mutex;
//only connectionHolder can create an underlying connection
void recreateConnection();

public:

bool isBusy();
void markBusy();
void markFree();
TimeType getLastTimeAcquired();
void setIndexInPool(int indexInPool);

sql::Connection* getConnection();
void resetConnection();
ConnectionHolder(sql::Driver *driver,
                 std::string urlConnection,
                 std::string username,
                 std::string password,
                 std::string schema);
virtual ~ConnectionHolder();
};

#endif
