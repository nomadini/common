

#include "ConnectionHolder.h"
#include "DateTimeUtil.h"
#include "MySqlDriver.h"

ConnectionHolder::ConnectionHolder(
        sql::Driver *driver,
        std::string urlConnection,
        std::string username,
        std::string password,
        std::string schema)   : Object(__FILE__) {


        assertAndThrow(!urlConnection.empty());
        assertAndThrow(!username.empty());
        assertAndThrow(!password.empty());
        assertAndThrow(!schema.empty());
        this->driver = driver;
        this->urlConnection = urlConnection;
        this->username = username;
        this->password = password;
        this->schema = schema;
        mutex = std::make_shared<boost::shared_mutex> ();
        indexInPool = -1;
        lastTimeAcquired  =  0;
        timeConnectionCreated = DateTimeUtil::getNowInSecond();
        busy = false;
        connection = nullptr;
        recreateConnection();
}

void ConnectionHolder::recreateConnection() {
        //we don't add lock here , because its being called by
        //constructor and another function which is guarded : resetConnection

        for (int i=0; i<10; i++) {
                try {

                        if(connection != nullptr) {
                                //this is how we free connection
                                delete connection;
                        }

                        connection = driver->connect (urlConnection.c_str (),
                                                      username.c_str (), password.c_str ());

                        //this is needed to stop mysql crashing app after eight hours
                        bool myTrue = true;
                        connection->setClientOption("OPT_RECONNECT", &myTrue);
                        connection->setClientOption ("libmysql_debug", "d:t:O,/tmp/client.trace");
                        connection->setClientOption ("libmysql_debug", "f");
                        connection->setSchema (schema.c_str());
                        MLOG(3)<<"connection was established";
                        timeConnectionCreated = DateTimeUtil::getNowInSecond();
                        break;
                } catch (const std::exception &e) {
                        gicapods::Util::showStackTrace(&e);
                        gicapods::Util::sleepViaBoost(_L_, 5);
                }
                LOG(ERROR)<< "failed to created connection..trying again in a few seconds ";
        }

        if (connection == NULL) {
                LOG(ERROR) << "error connecting to mysql database, urlConnection : "
                           << urlConnection;
                throwEx ("couldnt connect to db");
        }
}

sql::Connection* ConnectionHolder::getConnection() {
        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(*mutex);
        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access
        return connection;
}

void ConnectionHolder::resetConnection() {
        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(*mutex);
        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access

        //mysql server times out a connection after 8 hours by default
        //so we want to make sure, we reconnect after 20 minutes or something
        TimeType nowInSeconds = DateTimeUtil::getNowInSecond();
        if ((nowInSeconds - timeConnectionCreated) >= 1200) { //TODO : config driven
                MLOG(3)<<"reconnecting.....";
                //we recreate a connection after n secons, because of
                //a bug in mysql driver that segfaults on idle connections
                //after 8 hours by default

                recreateConnection();
        }

}

void ConnectionHolder::markBusy() {
        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(*mutex);
        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access

        busy = true;
        lastTimeAcquired = DateTimeUtil::getNowInSecond();
}

TimeType ConnectionHolder::getLastTimeAcquired() {
        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(*mutex);
        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access
        return lastTimeAcquired;
}

void ConnectionHolder::markFree() {
        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(*mutex);
        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access

        busy = false;

}

void ConnectionHolder::setIndexInPool(int indexInPool) {
        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(*mutex);
        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access
        this->indexInPool = indexInPool;
}

bool ConnectionHolder::isBusy() {
        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(*mutex);
        // get exclusive access
        boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
        // now we have exclusive access
        return busy;
}

ConnectionHolder::~ConnectionHolder() {
        if(connection != nullptr) {
                connection->close();
                delete connection;
        }
}
