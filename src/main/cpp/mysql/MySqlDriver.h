#ifndef MySqlDriver_h
#define MySqlDriver_h



#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include "mysql_connection.h"
#include <vector>
#include <memory>
#include "StringUtil.h"//this is used in all mysql implementations
#include "GUtil.h"
#include "Poco/DateTime.h"
#include "ResultSetHolder.h"
#include "Object.h"
class ConnectionPool;
class ConnectionHolder;
#define LOG_MYSQL_ERROR

class MySqlDriver;
namespace gicapods {
class ConfigService;
}



class MySqlDriver : public Object {

public:
sql::Driver *driver;

std::shared_ptr<ConnectionPool> pool;


MySqlDriver(std::string urlConnection,
												std::string username,
												std::string password,
												std::string schema,
												int poolSize);

// std::shared_ptr<sql::ResultSet> executeQuery(std::string query);
std::shared_ptr<ResultSetHolder> executeQuery(std::string query);
int getLastInsertedId();

virtual int executedUpdateStatement(std::string query);

virtual ~MySqlDriver();

void deleteAll(std::string tableName);

static std::string getString(std::shared_ptr<ResultSetHolder> resultset, int index);
static std::string getString(std::shared_ptr<ResultSetHolder> resultset, std::string index);

static Poco::DateTime parseDateTime(std::shared_ptr<ResultSetHolder> resultset, int index);
static Poco::DateTime parseDateTime(std::shared_ptr<ResultSetHolder> resultset, std::string index);


std::shared_ptr<ConnectionHolder> getConnectionFromPool();
static std::shared_ptr<MySqlDriver> getInstance(gicapods::ConfigService* configService);
};

#endif
