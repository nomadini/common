#ifndef MySqlDriver_Mock_h
#define MySqlDriver_Mock_h

class MySqlDriver;
#include "gmock/gmock.h"
namespace gicapods { class ConfigService; }
class MySqlDriverMock;




class MySqlDriverMock: public MySqlDriver {

public:
  MySqlDriverMock(gicapods::ConfigService* configService): MySqlDriver(configService){

  }
};

#endif
