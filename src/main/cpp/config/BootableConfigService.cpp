/*
 * BootableConfigServiceParser.h
 *
 *  Created on: Mar 4, 2015
 *      Author: mtaabodi
 */


#include "GUtil.h"
#include "BootableConfigService.h"
#include "StringUtil.h"
#include "FileUtil.h"
#include "JsonUtil.h"
#include "JsonMapUtil.h"
#include "ConfigService.h"
#include "ConverterUtil.h"
#include <boost/foreach.hpp>

gicapods::BootableConfigService::BootableConfigService(std::string commonPropertyFileName) {
								this->commonPropertyFileName = commonPropertyFileName;
}

gicapods::BootableConfigService::~BootableConfigService() {

}

std::shared_ptr<std::unordered_map<std::string, std::string> > gicapods::BootableConfigService::loadProperties(
								std::string appName,
								std::string cluster,
								std::string host) {

								auto allProps = std::make_shared<std::unordered_map<std::string, std::string> >();

								addCommonProperties(allProps, commonPropertyFileName);
								addGeneralProperties(allProps, appName);

								std::string propFileName= "/home/workspace/Common/Config/" + appName+"-custom.properties";
								std::string rawContentOfFile = FileUtil::readFileInString(propFileName);
								if (rawContentOfFile.empty()) {
																LOG(INFO)<<"no custom properties file found.skipping...";
																return allProps;
								}


								addCustomProperties(
																rawContentOfFile,
																allProps,
																cluster,
																host);
								return allProps;

}

void gicapods::BootableConfigService::addCommonProperties(
								std::shared_ptr<std::unordered_map<std::string, std::string> > allProps,
								std::string commonPropertyFileName) {
								auto allCommonProps  =
																configService->readProperties(commonPropertyFileName, false);

								for(auto&& entry : *allCommonProps) {
																ConfigService::upsertProperty(allProps, entry.first, entry.second);
								}

}

void gicapods::BootableConfigService::addGeneralProperties(
								std::shared_ptr<std::unordered_map<std::string, std::string> > allProps,
								std::string appName) {
								//first we load all properties in appName.properties
								//then we load appName-custom.txt that has a json which has cluster and host related properties
								//which override the already loaded properties
								auto allGeneralProps  =
																configService->readProperties(appName + ".properties", false);

								for(auto&& entry : *allGeneralProps) {
																ConfigService::upsertProperty(allProps, entry.first, entry.second);
								}
}

void gicapods::BootableConfigService::addCustomProperties(
								std::string rawContentOfFile,
								std::shared_ptr<std::unordered_map<std::string, std::string> > allProps,
								std::string cluster,
								std::string host) {
								MLOG(3)<<"rawContentOfFile : "<<rawContentOfFile;
								auto doc = parseJsonSafely(rawContentOfFile);
								addPropertiesFrom(doc,"clusters", allProps, cluster);
								addPropertiesFrom(doc, "hosts", allProps, host);
}
void gicapods::BootableConfigService::addPropertiesFrom(
								DocumentPtr doc,
								std::string propertyCategory,
								std::shared_ptr<std::unordered_map<std::string, std::string> > allGeneralProps,
								std::string propertyCategoryValue) {

								if (doc->HasMember (propertyCategory.c_str())) {
																RapidJsonValueType clustersValue = (*doc)[propertyCategory.c_str()];
																assertAndThrow(clustersValue.IsArray ());
																for (rapidjson::SizeType i = 0; i < clustersValue.Size (); i++) {
																								RapidJsonValueType oneClusterValue = clustersValue[i];
																								std::string nameOfCluster =
																																JsonUtil::getValueOfMember<std::string>(oneClusterValue, "ownerOfProperties");
																								if (StringUtil::equalsIgnoreCase(nameOfCluster, propertyCategoryValue)) {
																																for (auto m = oneClusterValue.MemberBegin ();
																																					m != oneClusterValue.MemberEnd (); ++m) {
																																								std::string keyOfProp = JsonUtil::GetStringUnsafely (m->name);
																																								std::string valueOfProp = JsonUtil::GetStringUnsafely (m->value);
																																								LOG(INFO) << "adding "<< keyOfProp << ":"<< valueOfProp<< " from "<< propertyCategoryValue;
																																								ConfigService::upsertProperty(allGeneralProps, keyOfProp, valueOfProp);
																																}
																								}
																}
								}
}
