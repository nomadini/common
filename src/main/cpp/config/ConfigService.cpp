/*
 * ConfigServiceParser.h
 *
 *  Created on: Mar 4, 2015
 *      Author: mtaabodi
 */


#include "GUtil.h"
#include "ConfigService.h"
#include "StringUtil.h"
#include "JsonMapUtil.h"
#include "FileUtil.h"
#include "ConverterUtil.h"
#include <boost/foreach.hpp>

gicapods::ConfigService::ConfigService(
								std::string filePathName,
								std::string commonPropertyFileName) {
								allLoadedProperties = std::make_shared<std::unordered_map<std::string, std::string> >();

								if (!commonPropertyFileName.empty()) {
																readProperties(commonPropertyFileName);
								} else {
																LOG(INFO) << "not loading any common property";
								}
								if (!filePathName.empty()) {
																readProperties(filePathName);
								}

}

bool gicapods::ConfigService::propertyExists(std::string propertyName) {
								return (allLoadedProperties->find(propertyName) != allLoadedProperties->end());
}

template<typename T>
void gicapods::ConfigService::get(std::string propertyName, T& propertyHolder) {
								propertyHolder = ConverterUtil::convertTo<T>(get(propertyName));
}

int gicapods::ConfigService::getAsInt(std::string propertyName) {
								return ConverterUtil::convertTo<int>(get(propertyName));
}

long gicapods::ConfigService::getAsLong(std::string propertyName){
								return ConverterUtil::convertTo<long>(get(propertyName));
}

bool gicapods::ConfigService::getAsBoolean(std::string propertyName) {
								return ConverterUtil::convertTo<bool>(get(propertyName));
}

bool gicapods::ConfigService::getAsBooleanFromString(std::string propertyName) {
								auto prop = get(propertyName);
								if (StringUtil::equalsIgnoreCase(prop, "true")) {
																return true;
								}

								return false;
}

double gicapods::ConfigService::getAsDouble(std::string propertyName) {
								return ConverterUtil::convertTo<double>(get(propertyName));
}

template<>
void gicapods::ConfigService::get(std::string propertyName, std::string& propertyHolder) {
								propertyHolder = get(propertyName);
}

void gicapods::ConfigService::clearAllProperties() {
								allLoadedProperties->clear();
}

void gicapods::ConfigService::addProperties(std::shared_ptr<std::unordered_map<std::string, std::string> > propMap) {
								for (auto&& entryPair : *propMap) {
																upsertProperty(entryPair.first, entryPair.second);
								}
}

std::string gicapods::ConfigService::get(std::string propertyName) {
								MLOG(10)<<"looking for property : "<<propertyName;

								if (propertyExists(propertyName)) {
																return allLoadedProperties->find(propertyName)->second;
								} else {
																MLOG(10) << "printing all properties.....";
																LOG(ERROR) << "all properties loaded : "<< JsonMapUtil::convertMapToJsonArray(*allLoadedProperties);
																LOG(ERROR)<<"property was not found : " << propertyName;
																gicapods::Util::showStackTrace();
																EXIT(StringUtil::toStr("property ")+ propertyName
																					+ StringUtil::toStr(" doesnt exist"));

								}
}

std::shared_ptr<std::unordered_map<std::string, std::string> >
gicapods::ConfigService::readProperties(std::string filePathName, bool addToLoadedMap) {
								filePathName = StringUtil::toLowerCase(filePathName);
								MLOG(3)<<"reading properties from file : "<<filePathName;
								auto mapOfProperties =
																std::make_shared<std::unordered_map<std::string, std::string> >();
								if(filePathName.empty()) {
																EXIT("filePathName is empty");

								}
								filePathName = StringUtil::toStr("/home/workspace/Common/Config/").append(filePathName);

								// LOG(INFO)<<"FileUtil::getCurrentDirectory() "<<FileUtil::getCurrentDirectory();

								if(!FileUtil::checkIfFileExists(filePathName))
								{
																EXIT("file : " + filePathName+ " doesn't exist");
								}

								Poco::AutoPtr<Poco::Util::PropertyFileConfiguration> localPropertyLoader
																= new Poco::Util::PropertyFileConfiguration();
								localPropertyLoader->load(filePathName);

								std::vector<std::string> allRootKeys;
								localPropertyLoader->keys(allRootKeys);
								MLOG(2)<<"loading properties from file : "<< filePathName;
								for(std::string key :  allRootKeys) {
																//  LOG(INFO) << " found key " << key<< "in property file : "<<filePathName;

																if (localPropertyLoader->hasProperty(key)) {
																								auto prop= localPropertyLoader->getString(key);
																								if (addToLoadedMap) {
																																upsertProperty(key, prop);
																								}
																								mapOfProperties->insert(std::make_pair(key, prop));
																} else {
																								EXIT("propery key  has no value, key:  " + key);
																}
								}

								return mapOfProperties;
}

std::shared_ptr<std::unordered_map<std::string, std::string> > gicapods::ConfigService::getAllLoadedProperties() {
								return allLoadedProperties;
}

void gicapods::ConfigService::upsertProperty(std::shared_ptr<std::unordered_map<std::string, std::string> > allLoadedProperties,
																																													std::string name, std::string value) {
								//this function first removes any existing property
								auto it=allLoadedProperties->find(name);
								if (it != allLoadedProperties->end()) {
																allLoadedProperties->erase (it);
								}

								allLoadedProperties->insert(std::make_pair(name, value));
}

void gicapods::ConfigService::upsertProperty(std::string name, std::string value) {
								upsertProperty(allLoadedProperties, name, value);
}

gicapods::ConfigService::~ConfigService() {
}


template void gicapods::ConfigService::get<int>(std::string, int&);
template void gicapods::ConfigService::get<double>(std::string, double&);
template void gicapods::ConfigService::get<bool>(std::string, bool&);
template void gicapods::ConfigService::get<long>(std::string, long&);
template void gicapods::ConfigService::get<std::string>(std::string, std::string&);
