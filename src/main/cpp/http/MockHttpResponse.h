//
// Created by Mahmoud Taabodi on 5/8/16.
//

#ifndef BIDDER_MOCKHTTPRESPONSE_H
#define BIDDER_MOCKHTTPRESPONSE_H

#include "Poco/Net/Net.h"
#include "Poco/Net/HTTPServerResponse.h"
#include <istream>
#include <memory>

#include "gmock/gmock.h"

class MockHttpResponse : public Poco::Net::HTTPServerResponse {

public:

std::shared_ptr<std::ostringstream> responseStream;

MockHttpResponse();
/// Creates the HTTPServerResponseImpl.

~MockHttpResponse();
/// Destroys the HTTPServerResponseImpl.

void sendContinue();
/// Sends a 100 Continue response to the
/// client.

std::ostream& send();
/// Sends the response header to the client and
/// returns an output stream for sending the
/// response body.
///
/// The returned stream is valid until the response
/// object is destroyed.
///
/// Must not be called after sendFile(), sendBuffer()
/// or redirect() has been called.

void sendFile(const std::string& path, const std::string& mediaType);
/// Sends the response header to the client, followed
/// by the content of the given file.
///
/// Must not be called after send(), sendBuffer()
/// or redirect() has been called.
///
/// Throws a FileNotFoundException if the file
/// cannot be found, or an OpenFileException if
/// the file cannot be opened.

void sendBuffer(const void* pBuffer, std::size_t length);
/// Sends the response header to the client, followed
/// by the contents of the given buffer.
///
/// The Content-Length header of the response is set
/// to length and chunked transfer encoding is disabled.
///
/// If both the HTTP message header and body (from the
/// given buffer) fit into one single network packet, the
/// complete response can be sent in one network packet.
///
/// Must not be called after send(), sendFile()
/// or redirect() has been called.

void redirect(const std::string& uri, HTTPStatus status = HTTP_FOUND);
/// Sets the status code, which must be one of
/// HTTP_MOVED_PERMANENTLY (301), HTTP_FOUND (302),
/// or HTTP_SEE_OTHER (303),
/// and sets the "Location" header field
/// to the given URI, which according to
/// the HTTP specification, must be absolute.
///
/// Must not be called after send() has been called.

void requireAuthentication(const std::string& realm);
/// Sets the status code to 401 (Unauthorized)
/// and sets the "WWW-Authenticate" header field
/// according to the given realm.

bool sent() const;
/// Returns true if the response (header) has been sent.

std::string getResponseBody();

};


#endif //BIDDER_MOCKHTTPRESPONSE_H
