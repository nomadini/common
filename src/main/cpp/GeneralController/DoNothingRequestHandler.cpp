
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"

#include "EntityToModuleStateStats.h"
#include "DoNothingRequestHandler.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include "HttpUtil.h"
#include <string>
#include <memory>
#include <boost/exception/all.hpp>

DoNothingRequestHandler::DoNothingRequestHandler(EntityToModuleStateStats* entityToModuleStateStats) {
								this->entityToModuleStateStats = entityToModuleStateStats;

}

void DoNothingRequestHandler::handleRequest(
								Poco::Net::HTTPServerRequest& request,
								Poco::Net::HTTPServerResponse& response) {
								this->entityToModuleStateStats->
								addStateModuleForEntity(
																"DOING_NOTHING",
																"DoNothingRequestHandler",
																EntityToModuleStateStats::all);
								std::ostream& ostr = response.send();
								ostr << "";
								return;

}
