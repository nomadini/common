
#include "EntityToModuleStateStatsHandler.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include <boost/exception/all.hpp>

#include "StringUtil.h"
#include "HttpUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "CollectionUtil.h"
#include "EntityToModuleStateStats.h"

EntityToModuleStateStatsHandler::EntityToModuleStateStatsHandler(EntityToModuleStateStats* entityToModuleStateStats) {
        this->entityToModuleStateStats =  entityToModuleStateStats;

}


void EntityToModuleStateStatsHandler::handleRequest(
        Poco::Net::HTTPServerRequest& request,
        Poco::Net::HTTPServerResponse& response) {
        std::string jsonResponse = "";

        try {
                std::string requestBody = HttpUtil::getRequestBody(request);
                MLOG(3)<<"requestBody : "<< requestBody;
                auto queryParams  = HttpUtil::getMapOfQueryParams(request);
                std::string function = HttpUtil::getRequiredParamFromQueryParameters("function", queryParams);

                if(StringUtil::equalsIgnoreCase(function, "getValueOfStateModuleForEntity")) {
                        MLOG(3)<<"function : getValueOfStateModuleForEntity";
                        std::string state = HttpUtil::getRequiredParamFromQueryParameters("state", queryParams);
                        std::string module = HttpUtil::getRequiredParamFromQueryParameters("module", queryParams);
                        std::string entityId = HttpUtil::getRequiredParamFromQueryParameters("entityId", queryParams);
                        jsonResponse = StringUtil::toStr(entityToModuleStateStats->getValueOfStateModuleForEntity(state, module, entityId));

                } else if(StringUtil::equalsIgnoreCase(function, "getAllPossibleStatesForModuleAndEntity")) {
                        MLOG(3)<<"function : getAllPossibleStatesForModuleAndEntity";
                        std::string module = HttpUtil::getRequiredParamFromQueryParameters("module", queryParams);
                        std::string entityId = HttpUtil::getRequiredParamFromQueryParameters("entityId", queryParams);
                        auto allStates  = entityToModuleStateStats->getAllPossibleStatesForModuleAndEntity(module, entityId);
                        jsonResponse = JsonArrayUtil::convertListToJson(allStates);
                } else if(StringUtil::equalsIgnoreCase(function, "getAllPossibleStatesForModule")) {
                        MLOG(3)<<"function : getAllPossibleStatesForModule";
                        std::string module = HttpUtil::getRequiredParamFromQueryParameters("module", queryParams);
                        auto allStates  = entityToModuleStateStats->getAllPossibleStatesForModule(module);
                        jsonResponse = JsonArrayUtil::convertListToJson(allStates);
                } else if(StringUtil::equalsIgnoreCase(function, "getAllPossibleStates")) {
                        MLOG(3)<<"function : getAllPossibleStates";
                        auto allStates = entityToModuleStateStats->getAllPossibleStates();
                        jsonResponse = JsonArrayUtil::convertListToJson(allStates);
                }  else if(StringUtil::equalsIgnoreCase(function, "getAllModules")) {
                        auto allModules = entityToModuleStateStats->getAllModules();
                        MLOG(3)<<"allModules : "<<JsonArrayUtil::convertListToJson(allModules);
                        jsonResponse = JsonArrayUtil::convertListToJson(allModules);

                }

                MLOG(3)<<"jsonResponse : "<<jsonResponse;

        } catch (std::exception const&  e) {
                LOG(ERROR) <<"error happening when handling request  " << boost::diagnostic_information(e);
        }

        catch (...) {
                LOG(ERROR)<<"unknown error happening when handling request";
        }

        MLOG(3)<<"setting response headers ";
        response.set("Access-Control-Allow-Origin", "*");
        response.set("Access-Control-Allow-Credentials", "true");

        std::unordered_map<std::string,std::string > headers;

        HttpUtil::getResponseHeaders(headers, response);

        std::ostream& ostr = response.send();

        MLOG(3)<<"response : "<<jsonResponse;
        ostr << jsonResponse;
        return;

}

EntityToModuleStateStatsHandler::~EntityToModuleStateStatsHandler() {
}
