
#include "DashboardRequestServiceHandler.h"
#include "JsonUtil.h"
#include <boost/exception/all.hpp>

#include "StringUtil.h"
#include "HttpUtil.h"
#include "JsonMapUtil.h"
#include "ConfigService.h"
#include "FileUtil.h"
#include "CollectionUtil.h"
#include "GUtil.h"
#include "JsonArrayUtil.h"


DashboardRequestServiceHandler::DashboardRequestServiceHandler(
        gicapods::ConfigService* configService) {
        this->configService = configService;
}


void DashboardRequestServiceHandler::handleRequest(
        Poco::Net::HTTPServerRequest& request,
        Poco::Net::HTTPServerResponse& response) {
        std::string baseDirectory =
                "/home/workspace/Bidder/src/main/resources/Dashboard/pages/";


        MLOG(3) << "request.getURI() : " << request.getURI ();
        auto tokens = StringUtil::tokenizeString(request.getURI (), "/dashboard/");
        MLOG(3) << "tokens : "<< JsonArrayUtil::convertListToJson(tokens);
        //if a resource is loading, we serve the content of the resource
        if (
                StringUtil::endsWith (request.getURI (), ".css") ||
                StringUtil::endsWith (request.getURI (), ".html") ||
                StringUtil::endsWith (request.getURI (), ".js")) {
                if (  StringUtil::endsWith (request.getURI (), ".css") ) {
                        response.setContentType ("text/css");
                }
                std::string assetFileName = baseDirectory + tokens[0];
                std::string fileContent = FileUtil::readFileInString (assetFileName);
                std::ostream &ostr = response.send ();
                ostr << fileContent;
                return;
        }

        std::string dashboardPage =
                baseDirectory + "dashboard.html";

        std::string fileContent = FileUtil::readFileInString (dashboardPage);

        // HttpUtil::printRequestProperties (request);
        // HttpUtil::printQueryParamsOfRequest (request);

        // response.setChunkedTransferEncoding (true);
        // response.setContentType ("text/html; charset=ISO-8859-1 ");
        std::ostream &ostr = response.send ();
        ostr << fileContent;
}

DashboardRequestServiceHandler::~DashboardRequestServiceHandler() {
}
