#ifndef PersistentMetricsRequestServiceHandler_H
#define PersistentMetricsRequestServiceHandler_H

#include <memory>
#include <string>
#include <unordered_map>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
namespace gicapods {
class ConfigService;
}

class MySqlTargetGroupFilterCountDbRecordService;
class MySqlMetricService;
class PersistentMetricsRequestServiceHandler;
class MySqlTgBiddingPerformanceMetricDtoService;


class PersistentMetricsRequestServiceHandler : public Poco::Net::HTTPRequestHandler {

public:

MySqlMetricService* mySqlMetricService;
MySqlTargetGroupFilterCountDbRecordService*  mySqlTargetGroupFilterCountDbRecordService;
MySqlTgBiddingPerformanceMetricDtoService*  mySqlTgBiddingPerformanceMetricDtoService;

void handleRequest(Poco::Net::HTTPServerRequest& request,
                   Poco::Net::HTTPServerResponse& response);

PersistentMetricsRequestServiceHandler(
        MySqlMetricService* mySqlMetricService,
        MySqlTargetGroupFilterCountDbRecordService*  mySqlTargetGroupFilterCountDbRecordService,
        MySqlTgBiddingPerformanceMetricDtoService*  mySqlTgBiddingPerformanceMetricDtoService);

std::string processModuleMetricDetails(std::unordered_map<std::string, std::string> queryParams);
std::string processFilterCountDetails(std::unordered_map<std::string, std::string> queryParams);
std::string processFilterCountReadAllByHostInLastNMinutesWithPercentagesAbove(std::unordered_map<std::string, std::string> queryParams);
std::string processImportantMetricsDetails(std::unordered_map<std::string, std::string> queryParams);
std::string readAllMetricsByHost(std::unordered_map<std::string, std::string> queryParams);
std::string processExceptionAndErrorMetricsDetails(std::unordered_map<std::string, std::string> queryParams);
std::string readBidAndConfirmedWinsDetails(std::unordered_map<std::string, std::string> queryParams);
virtual ~PersistentMetricsRequestServiceHandler();
};

#endif
