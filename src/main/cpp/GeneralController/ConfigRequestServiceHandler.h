#ifndef ConfigRequestServiceHandler_H
#define ConfigRequestServiceHandler_H

#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
namespace gicapods {
class ConfigService;
class BootableConfigService;
}

class ConfigRequestServiceHandler;




/*
   requests like this will give you the value of a propery : curl http://localhost:9989/configservice/get/port
   requests like this will give you all loaded properties : curl http://localhost:9989/configservice/getAllLoadedProperties
   requests like this will allow you to add properites : curl http://localhost:9989/configservice/addProperty/mysql-password/abc
 */
class ConfigRequestServiceHandler : public Poco::Net::HTTPRequestHandler {

public:

gicapods::ConfigService* configService;
gicapods::BootableConfigService* bootableConfigService;

void handleRequest(Poco::Net::HTTPServerRequest& request,
                   Poco::Net::HTTPServerResponse& response);

ConfigRequestServiceHandler(
        gicapods::ConfigService* configService,
        gicapods::BootableConfigService* bootableConfigService
        );

virtual ~ConfigRequestServiceHandler();
};

#endif
