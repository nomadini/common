#ifndef CassandraRequestHandlerFactory_H
#define CassandraRequestHandlerFactory_H

#include <memory>
#include <string>
#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerRequest.h"
#include <unordered_map>
class TargetGroupCacheService;
class CampaignCacheService;
class CreativeCacheService;

class EntityToModuleStateStats;
#include "Poco/Logger.h"
#include "Poco/LogStream.h"

#include <boost/thread.hpp>
#include <thread>
#include <unordered_map>
#include <memory>

#include "AerospikeDriver.h"
#include "CacheService.h"
#include "EntityProviderService.h"
class CassandraDriverInterface;
class BidConfirmedWinsContainerPerTargetGroupAndBidder;
class TgFilterMeasuresService;
class BeanFactory;
class MySqlTgPerformanceTrackService;
class MySqlEntityRealTimeDeliveryInfoService;
class BeanFactory;
class MySqlMetricService;
class CassandraRequestHandlerFactory;
class CommonRequestHandlerFactory;


class CassandraRequestHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory {

void setupCommandsToInterfaces();

public:
bool isAppHealthy();

EntityToModuleStateStats* entityToModuleStateStats;
BeanFactory* beanFactory;
void init();
CassandraRequestHandlerFactory();
Poco::Net::HTTPRequestHandler *createRequestHandler(const Poco::Net::HTTPServerRequest &request);
};

#endif
