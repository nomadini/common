#ifndef DataRequestHandler_H
#define DataRequestHandler_H

#include "SignalHandler.h"
#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
class EntityToModuleStateStats;
#include "HttpUtilService.h"
#include "AerospikeDriver.h"
class CassandraDriverInterface;
#include <boost/thread.hpp>
#include <thread>
#include "AtomicBoolean.h"

class EntityRealTimeDeliveryInfo;
class BidConfirmedWinsContainerPerTargetGroupAndBidder;
class TgFilterMeasuresService;
class MySqlTgPerformanceTrackService;
class MySqlEntityRealTimeDeliveryInfoService;

class TgGeoFeatureCollectionMapCacheService;
class MySqlMetricService;
class Client;
class EntityProviderServiceInterface;
class DataRequestHandler;
class CacheServiceInterface;


class DataRequestHandler : public Poco::Net::HTTPRequestHandler {

public:
std::string keyOfRequest;
HttpUtilService* httpUtilService;
std::shared_ptr<std::unordered_map<std::string, CacheServiceInterface*> > entityToFetcherServiceMap;
std::shared_ptr<std::unordered_map<std::string, EntityProviderServiceInterface*> > entityToProviderServiceMap;
std::shared_ptr<gicapods::AtomicBoolean> isReadyToServe;
EntityToModuleStateStats* entityToModuleStateStats;


MySqlTgPerformanceTrackService* mySqlTgPerformanceTrackService;
MySqlEntityRealTimeDeliveryInfoService* mySqlEntityRealTimeDeliveryInfoService;
TgGeoFeatureCollectionMapCacheService* tgGeoFeatureCollectionMapCacheService;

TgFilterMeasuresService* tgFilterMeasuresService;

DataRequestHandler(
								EntityToModuleStateStats* entityToModuleStateStats,
								std::shared_ptr<std::unordered_map<std::string, CacheServiceInterface*> > entityToFetcherServiceMap,
								std::shared_ptr<std::unordered_map<std::string, EntityProviderServiceInterface*> > entityToProviderServiceMap);
std::string findKeyFromRequestBody(
								std::string requestBody,
								std::string& entity,
								std::string& function );

void handleRequest(Poco::Net::HTTPServerRequest& request,
																			Poco::Net::HTTPServerResponse& response);

std::string processReadAllForTargetGroupIdToPerformanceMetricMap(std::string requestBody, int& numberOfEntities);
std::string processReadAllTargetGroupTrackingPerformances(std::string requestBody, int& numberOfEntities);

};

#endif
