#ifndef DateTimeService_h
#define DateTimeService_h



#include <iostream>
#include "Poco/Timestamp.h"
#include "Poco/DateTime.h"
#include "Poco/DateTimeParser.h"
#include "Poco/DateTimeFormat.h"
#include "Poco/LocalDateTime.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormatter.h"

using Poco::Timestamp;
using Poco::DateTime;
using Poco::DateTimeParser;
using Poco::DateTimeFormat;
using Poco::DateTime;
using Poco::DateTimeFormatter;

#include "FileUtil.h"
#include "Object.h"
#include "DateTimeMacro.h"
#include "ConverterUtil.h"
#include <boost/date_time/local_time/local_time.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"
using namespace boost::posix_time;
using namespace boost::local_time;
#include "Object.h"

typedef boost::posix_time::ptime Time;

class DateTimeService;



class DateTimeService : public Object {

boost::local_time::tz_database tz;

public:

DateTimeService();
virtual ~DateTimeService();
virtual std::string getNowPlusSecondsInMySqlFormat(int seconds);

virtual std::string getNowPlusHoursInMySqlFormat(int hour);

virtual std::string getNowPlusDaysInMySqlFormat(int day);

virtual std::string getNowInMySqlFormat();

virtual void initTimeZoneObject();

virtual int getUtcOffsetBasedOnTimeZone(std::string timeZoneName);

virtual TimeType StringToMicro(std::string str);

virtual TimeType MicroToSecond(TimeType micro);

virtual TimeType getNowInMicroSecond();

virtual TimeType getNowInSecond();

virtual TimeType getDaysInSeconds(int days);

virtual TimeType getNowInMilliSecond();

virtual TimeType getNowHourInMilliSecond();

virtual TimeType getNowMinuteInMilliSecond();

/*
 * returns current time in this format 2001-08-23
 * which determines the partition key of device history (rowkey)
 * and pixel_stats
 *
 */
virtual std::string getNowInYYYMMDDFormat();

virtual std::string getNDayAgoInStringFormat(int daysAgo);

/**
 * this retuns the active hour in UTC
 */
virtual int getCurrentHourAsIntegerInUTC();

virtual int getMinuteNowAsIntegerInUTC();

virtual int getSecondNowAsIntegerInUTC();

virtual std::string getNowInStringFormat(Poco::Timestamp now, const std::string &format);

/**
   returns the 5 if the minute now is 2 or 3 or 4 or 5 .
   return 25 if the minute now is 21, 22, 23, 24
 */
virtual int getClosest5thMinuteNowAsIntegerInUTC();

virtual TimeType getThisHourSinceEpoch();

virtual TimeType getNowInseconds();

virtual TimeType getNowInNanoSecond();

virtual TimeType getNowInUniversalFormat();

//-----------------------------------------------------------------------------
// Format current time (calculated as an offset in current day) in this form:
//
//     "hh:mm:ss.SSS" (where "SSS" are milliseconds)
//-----------------------------------------------------------------------------
virtual std::string getTimeNowAsString();

virtual std::string getCurrentDateAsString();

virtual std::string getWeekdayBasedOnUTC();

virtual int getWeekDayNumberBasedOnUTC();

virtual int getWeekDayNumberBasedOnUserOffset(int userOffset);
};

#endif
