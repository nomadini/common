
#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "NetworkUtil.h"
#include "StringUtil.h"
#include "DeviceTestHelper.h"
 #include <unistd.h> //gethostname
std::string NetworkUtil::getHostName() {

        char hostname[100];
        char username[100];
        int result;
        result = gethostname(hostname, 100);
        return StringUtil::toStr(hostname);

//        result = getlogin_r(username, LOGIN_NAME_MAX);

//        result = printf("Hello %s, you are logged in to %s.\n",
//                        username, hostname);

}

std::string NetworkUtil::getLocalIpAddress() {
        std::string ipAddress;
        struct ifaddrs * ifAddrStruct = NULL;
        struct ifaddrs * ifa = NULL;
        void * tmpAddrPtr = NULL;

        getifaddrs(&ifAddrStruct);

        for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
                if (!ifa->ifa_addr) {
                        continue;
                }
                if (ifa->ifa_addr->sa_family == AF_INET) { // check it is IP4
                        // is a valid IP4 Address
                        tmpAddrPtr = &((struct sockaddr_in *) ifa->ifa_addr)->sin_addr;
                        char addressBuffer[INET_ADDRSTRLEN];
                        inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
                        printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer);
                        ipAddress = StringUtil::toStr(ifa->ifa_name);
                } else if (ifa->ifa_addr->sa_family == AF_INET6) { // check it is IP6
                        // is a valid IP6 Address
                        tmpAddrPtr = &((struct sockaddr_in6 *) ifa->ifa_addr)->sin6_addr;
                        char addressBuffer[INET6_ADDRSTRLEN];
                        inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
                        printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer);
                        ipAddress = StringUtil::toStr(ifa->ifa_name);

                }
        }
        if (ifAddrStruct != NULL)
                freeifaddrs(ifAddrStruct);
        return ipAddress;
}
