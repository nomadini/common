#include "RandomUtilDecimalGenerator.h"
#include "GUtil.h"
#include <boost/multiprecision/random.hpp>
#include <random>
#include <iostream>
#include <stdlib.h>

RandomUtilDecimalGenerator::RandomUtilDecimalGenerator(int decimalLowerBound,
                                                       int decimalUpperBound) : Object(__FILE__) {

        randomDevice = std::make_shared<std::random_device>();
        longRandomGenerator = std::make_shared<std::mt19937> ((*randomDevice)());
        decimalRandomFinder = std::make_shared<std::uniform_real_distribution<> >(decimalLowerBound, decimalUpperBound);
}

/**
   uses normal distribution , you can use the gamma distribution if needed too
 */
double RandomUtilDecimalGenerator::randomDecimalV2() {
        return (*decimalRandomFinder)(*longRandomGenerator);
}

RandomUtilDecimalGenerator::~RandomUtilDecimalGenerator() {

}
