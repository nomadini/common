
#ifndef DateTimeServiceMock_H
#define DateTimeServiceMock_H

#include "DateTimeService.h"

#include "gmock/gmock.h"

class DateTimeServiceMock;


class DateTimeServiceMock : public DateTimeService {

public:

MOCK_METHOD1(getUtcOffsetBasedOnTimeZone, int (std::string timeZoneName));
MOCK_METHOD0(getCurrentHourAsIntegerInUTC, int (void));
};


#endif /* PIXEL_DEVICE_HISTORY_CASSANDRA_SERVICE_MOCK */
