/*
 * RandomUtilDecimalGenerator.h
 *
 *  Created on: Aug 9, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_UTIL_RandomUtilDecimalGenerator_H_
#define GICAPODS_GICAPODSSERVER_SRC_UTIL_RandomUtilDecimalGenerator_H_

#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <string>
#include <memory>
#include <random>
#include "Object.h"

class RandomUtilDecimalGenerator : public Object {

public:

std::shared_ptr<std::uniform_real_distribution<> > decimalRandomFinder;
std::shared_ptr<std::mt19937> longRandomGenerator;
std::shared_ptr<std::random_device> randomDevice;
RandomUtilDecimalGenerator(int decimalLowerBound, int decimalUpperBound);

/* if you want to determine something based on a probability
   look at this method happensWithProb in StatisticsUtil
 */
double randomDecimalV2();
virtual ~RandomUtilDecimalGenerator();
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_UTIL_RandomUtilDecimalGenerator_H_ */
