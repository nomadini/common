/*
 * HttpUtil.h
 *
 *  Created on: Aug 2, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_UTIL_HTTPUTIL_H_
#define GICAPODS_GICAPODSSERVER_SRC_UTIL_HTTPUTIL_H_



#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServerResponseImpl.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPCookie.h"

#include <string>
#include <memory>
#include <curl/curl.h>
#include <unordered_map>



class HttpUtil {
public:
static constexpr const char* nomadiniDeviceId = "nomadiniDeviceId";

static void downloadFileUsingScpWithCurl(std::string url, std::string destinationFileName);
static void getResponseHeaders(std::unordered_map<std::string,std::string >& headers,
                               Poco::Net::HTTPServerResponse& res);

static void setRequestHeaders(std::unordered_map<std::string,std::string > headers,
                              Poco::Net::HTTPServerRequest& request);
static std::string getRequiredParamFromQueryParameters(
        std::string paramName,
        std::unordered_map <std::string, std::string> queryParams);

static void sendHttpPostViaCurl(const std::string & url,
                                const std::string & postData,
                                int numberOfTries);

static std::unordered_map<std::string, std::string> getMapOfQueryParams(
        Poco::Net::HTTPServerRequest& request);

static void printQueryParamsOfRequest(Poco::Net::HTTPServerRequest& request);

static std::unordered_map<std::string, std::string> getCookiesFromRequest(const Poco::Net::HTTPServerRequest& request);

static std::string sendGetRequest(const std::string & url,
                                  int timeoutInMillis,
                                  int numberOfTries);

static std::string sendGetRequestAndGetFullResponse(const std::string & url,
                                                    Poco::Net::HTTPResponse& response,
                                                    int timeoutInMillis,
                                                    int numberOfTries);

static std::string sendGetRequestAndGetFullResponse(
        const std::string & cookieKey,
        const std::string & cookieValue,
        Poco::Net::HTTPResponse& response,
        const std::string & url,
        int timeoutInMillis,
        int numberOfTries);

static std::string sendGetRequestAndGetFullResponse(
        std::unordered_map<std::string, std::string> cookieMap,
        Poco::Net::HTTPResponse& response,
        const std::string & url,
        int timeoutInMillis,
        int numberOfTries);

static std::string sendPostRequestAndGetFullResponse(const std::string & url,
                                                     const std::string & requestBody,
                                                     Poco::Net::HTTPResponse& res,
                                                     int timeoutInMillis,
                                                     int numberOfTries);


static std::string sendPostRequest(Poco::Net::HTTPClientSession& session,
                                   const std::string & url,
                                   const std::string & requestBody,
                                   int numberOfTries);

static std::string sendPostRequest(
        const std::string & url,
        const std::string & requestBody,
        int timeoutInMillis,
        int numberOfTries);

static void setEmptyResponse(Poco::Net::HTTPServerResponse& response);

static void setBadRequestResponse(Poco::Net::HTTPServerResponse& response, std::string msg);
/**
 * returns the request body of an http request
 */
static std::string getRequestBody(Poco::Net::HTTPServerRequest& request);

static void printRequestProperties(Poco::Net::HTTPServerRequest& request);

static void printCookies(Poco::Net::HTTPServerRequest& request);

static std::string getDomainFromUrl(std::string url);

static std::string getDomainFromRequest(Poco::Net::HTTPServerRequest& request);

static std::unordered_map<std::string, std::string> getMapOfQueryParams(const Poco::Net::HTTPServerRequest& request);

static void setOneCookieOnRequest(Poco::Net::HTTPRequest& request, const std::string& key, const std::string& value);
static void setCookiesOnRequest(Poco::Net::HTTPRequest& request, std::unordered_map<std::string, std::string> cookies);
static void setCookiesOnRequest(Poco::Net::HTTPRequest& request, Poco::Net::NameValueCollection cookies);
static std::string getCookieFromResponse(Poco::Net::HTTPResponse& response, const std::string& cookieName);
static std::string getCookieFromResponse(Poco::Net::HTTPServerResponse& response, const std::string& cookieName);
static std::unordered_map<std::string, std::string> getCookiesFromResponse(Poco::Net::HTTPResponse& response);
static std::unordered_map<std::string, std::string> getCookiesFromResponse(Poco::Net::HTTPServerResponse& response);
static std::unordered_map<std::string, std::string> getCookiesFromRequest(Poco::Net::HTTPServerRequest& request);
static void addCookieToResponse(std::string cookieName,
                                std::string cookieValue,
                                Poco::Net::HTTPServerResponse* httpResponse);

static void setNoContentAndEmptyResponse(Poco::Net::HTTPServerResponse &response, std::string responseStr);
};
#endif /* GICAPODS_GICAPODSSERVER_SRC_UTIL_HTTPUTIL_H_ */
