/*
 * NetworkUtil.h
 *
 *  Created on: Aug 27, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_UTIL_NETWORKUTIL_H_
#define GICAPODS_GICAPODSSERVER_SRC_UTIL_NETWORKUTIL_H_

#include <string>
class NetworkUtil {
public:

static std::string getLocalIpAddress();

static std::string getHostName();
};


#endif /* GICAPODS_GICAPODSSERVER_SRC_UTIL_NETWORKUTIL_H_ */
