
#ifndef COMMON_COLLECTIONUTIL_H
#define COMMON_COLLECTIONUTIL_H

#include <unordered_map>
#include <vector>
#include <set>
#include <unordered_set>
#include <string>
#include <random>
#include <map>
#include <boost/function.hpp>
#include <tbb/concurrent_hash_map.h>
#include <tbb/concurrent_unordered_set.h>
#include "JsonMapUtil.h"
#include "JsonArrayUtil.h"
#include "JsonPairUtil.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include <string>
#include <memory>
#include <tbb/concurrent_hash_map.h>
#include "GeoLocation.h"
#include <algorithm>
#include <iterator>
#include <sstream>
#include "TargetGroup.h"
#include "JsonMapUtil.h"
#include "JsonArrayUtil.h"
#include "JsonPairUtil.h"
#include "JsonTypeDefs.h"

#include "GUtil.h"
#include "JsonTypeDefs.h"

#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include <tbb/concurrent_hash_map.h>
#include <algorithm>
#include <iterator>
#include <sstream>

//using unnamed namespace to fix the multiple definition problem
namespace {

class CollectionUtil {
public:
CollectionUtil() {

}
template<typename T>
static std::string toCommaSeperatedString(std::vector<T> v) {
        std::ostringstream ss;
        if(v.empty()) {
                return "";
        }
        std::copy(v.begin(), v.end() - 1, std::ostream_iterator<T>(ss, ", "));
        ss << v.back();

        return ss.str();
}

template<typename T>
static std::string toCommaSeperatedString(std::set<T> v) {
        std::ostringstream ss;
        if(v.empty()) {
                return "";
        }
        std::copy(v.begin(), v.end() - 1, std::ostream_iterator<T>(ss, ", "));
        ss << v.back();

        return ss.str();
}

template<typename T>
static void sortVectorAsc(std::vector<T>& myArray) {
        //ascending order
        std::sort(myArray.begin(),myArray.end());
}

template<typename T>
static void sortVectorAsc(std::set<T>& myArray) {
        //ascending order
        std::sort(myArray.begin(),myArray.end());
}

template<typename T>
static void sortVectorDesc(std::vector<T>& myArray) {
        //descending order
        std::sort(myArray.rbegin(),myArray.rend());
}

template<typename T>
static std::vector<T> getFirstNElements(std::vector<T>& vec, int n) {
        typename std::vector<T>::const_iterator first = vec.begin();
        typename std::vector<T>::const_iterator last = vec.begin() + n;
        std::vector<T> newVec(first, last);
        return newVec;
}

template<typename T>
static void printTheMap(
        std::unordered_map<int, T> map) {
        MLOG (10) << "printing the content of map";
        for (typename std::unordered_map<int, T>::iterator ii = map.begin ();
             ii != map.end (); ++ii) {
                MLOG (10) << "key : " << (*ii).first << "  : value : " << (*ii).second->toJson ();
        }

}


static void printFeatureScoreMap(
        std::unordered_map<std::string, double> map) {

        for (auto &kv : map) {
                MLOG (10) << "feaure : " << kv.first << " has score : " << kv.second;
        }
}

static std::unordered_map<std::string, int> convertJsonToFeatureIndexMap(
        std::string mapInJson) {
        std::unordered_map<std::string, int> featureUniqueIdMap;

        auto document = parseJsonSafely(mapInJson);
        throwEx("fix the commented code later");
//        auto features = (*document)["features"]; // Using a reference for consecutive access is handy and faster.
//        assert (features.IsArray ());
//
//        for (rapidjson::SizeType i = 0;
//             i < features.Size (); i++) // rapidjson uses rapidjson::SizeType instead of size_t.
//        {
//           auto featureStringObject = (*document)["features"][i];
//        }
        return featureUniqueIdMap;
}

/***
 * defining methods with templates here!!!, they cant be defined in cpp files
 *
 */

template<typename T>
static void printVector(std::vector<T> fooVector) {
        MLOG (10) << "printVector......";
        std::string allStr;
        for (typename std::vector<T>::iterator it = fooVector.begin ();
             it != fooVector.end (); ++it) {
                allStr += StringUtil::toStr ((*it)) + StringUtil::toStr (",");
        }
        MLOG (10) << "vector content : " << allStr;
}

template<typename T>
static std::unordered_map<int, T> convertListToMap(std::vector<T> list) {
        std::unordered_map<int, T> map;
        for (auto crt : list) {
                MLOG(10)<<"inserting entry in map with id : "<< crt->id;
                map.insert (std::pair<int, T> (crt->id, crt));
        }
        return map;
}


template<typename K, typename V>
static void insertIntoMap(K key, V value, tbb::concurrent_hash_map<K, V>& map) {
        typename tbb::concurrent_hash_map<K, V>::accessor a;
        map.insert (a, key);
        a->second = value;
}

template<typename T>
static tbb::concurrent_hash_map<int, T> convertListToConcurrentMap(std::vector<T> list) {
        tbb::concurrent_hash_map<int, T> map;
        for (auto crt : list) {
                typename tbb::concurrent_hash_map<int, T>::accessor a;
                map.insert (a, crt->getId());
                a->second = crt;
        }
        return map;
}

template<typename K, typename V>
static std::vector<K> convertKeysOfConcurrentMapToList(tbb::concurrent_hash_map<K, V> map) {
        std::vector<K> list;
        typename tbb::concurrent_hash_map<K, V>::iterator iter;
        for (iter = map.begin ();
             iter != map.end ();
             iter++) {
                list.push_back (iter->first);
        }
        return list;
}

template<typename K, typename V>
static std::vector<V> convertValuesOfConcurrentMapToList(tbb::concurrent_hash_map<K, V> map) {
        std::vector<V> list;
        typename tbb::concurrent_hash_map<K, V>::iterator iter;
        for (iter = map.begin ();
             iter != map.end ();
             iter++) {
                list.push_back (iter->second);
        }
        return list;
}


template<typename T>
static bool valueExistInSet(std::set<T> vec, T target) {
        bool isPresent =
                (std::find (vec.begin (), vec.end (), target) != vec.end ());
        return isPresent;
}

template<typename T>
static bool valueExistInSet(std::unordered_set<T> vec, T target) {
        bool isPresent =
                (std::find (vec.begin (), vec.end (), target) != vec.end ());
        return isPresent;
}

template<typename K, typename V>
static std::set<K> convertKeysOfConcurrentMapToSet(tbb::concurrent_hash_map<K, V> map) {
        std::set<K> list;
        typename tbb::concurrent_hash_map<K, V>::iterator iter;
        for (iter = map.begin ();
             iter != map.end ();
             iter++) {
                list.insert (iter->first);
        }
        return list;
}

template<typename K, typename V>
static std::set<V> convertValuesOfConcurrentMapToSet(tbb::concurrent_hash_map<K, V> map) {
        std::set<V> list;
        typename tbb::concurrent_hash_map<K, V>::iterator iter;
        for (iter = map.begin ();
             iter != map.end ();
             iter++) {
                list.insert (iter->second);
        }
        return list;
}


template<typename K, typename V>
static std::vector<K> getMapKeySet(std::unordered_map<K, V> &map) {
        std::vector<K> list;
        for (typename std::unordered_map<K, V>::iterator ii = map.begin ();
             ii != map.end (); ++ii) {
                list.push_back ((*ii).first);
        }
        return list;
}

template<typename K, typename V>
static void deleteFromMap(K key, std::unordered_map<K, V> &map) {
        map.erase (key);
}

template<typename K, typename V>
static std::vector<V> getMapValues(std::unordered_map<K, V> &map) {
        std::vector<V> list;
        for (typename std::unordered_map<K, V>::iterator ii = map.begin ();
             ii != map.end (); ++ii) {
                list.push_back ((*ii).second);
        }
        return list;
}

template<typename T>
static std::vector<T> shuffleTheList(std::vector<T> allRecords) {
        auto engine = std::default_random_engine {};
        std::shuffle (std::begin (allRecords), std::end (allRecords), engine);
        return allRecords;
}
template<typename T>
static std::shared_ptr<std::vector<T> >  shuffleTheList(std::shared_ptr<std::vector<T> > allRecords) {
        auto engine = std::default_random_engine {};
        std::shuffle (std::begin (*allRecords), std::end (*allRecords), engine);
        return allRecords;
}

template<typename T>
static std::set<T> shuffleTheList(std::set<T> allRecords) {
        auto engine = std::default_random_engine {};
        std::shuffle (std::begin (allRecords), std::end (allRecords), engine);
        return allRecords;
}

//this works well on strings and probably primitve types
template<typename T>
static bool containsInList(T target, std::vector<T> vec) {
        bool isPresent =
                (std::find (vec.begin (), vec.end (), target) != vec.end ());
        return isPresent;
}

template<typename T>
static bool containsInList(T target, std::set<T> vec) {
        bool isPresent =
                (std::find (vec.begin (), vec.end (), target) != vec.end ());
        return isPresent;
}

template<typename T>
static std::vector<T> combineTwoLists(std::vector<T> A, std::vector<T> B) {
        std::vector<T> AB;
        AB.reserve (A.size () + B.size ());         // preallocate memory
        AB.insert (AB.end (), A.begin (), A.end ());
        AB.insert (AB.end (), B.begin (), B.end ());
        return AB;
}


template<typename T>
static std::shared_ptr<std::vector<T> > combineTwoLists(
        std::shared_ptr<std::vector<T> > A,
        std::shared_ptr<std::vector<T> > B) {
        std::shared_ptr<std::vector<T> > AB = std::make_shared<std::vector<T> >();
        AB->reserve (A->size () + B->size ());         // preallocate memory
        AB->insert (AB->end (), A->begin (), A->end ());
        AB->insert (AB->end (), B->begin (), B->end ());
        return AB;
}

template<typename T>
static std::set<T> combineTwoLists(std::set<T> A, std::set<T> B) {
        std::set<T> AB;
        AB.reserve (A.size () + B.size ());         // preallocate memory
        AB.insert (AB.end (), A.begin (), A.end ());
        AB.insert (AB.end (), B.begin (), B.end ());
        return AB;
}

template<typename T>
static std::vector<T> getCommonItems(std::vector<T> s1, std::vector<T> s2) {
        std::vector<T> intersect;
        set_intersection (s1.begin (), s1.end (), s2.begin (), s2.end (),
                          std::inserter (intersect, intersect.begin ()));
        return intersect;
}

template<typename T>
static std::set<T> getCommonItems(std::set<T> s1, std::set<T> s2) {
        std::set<T> intersect;
        set_intersection (s1.begin (), s1.end (), s2.begin (), s2.end (),
                          std::inserter (intersect, intersect.begin ()));
        return intersect;
}

template<typename T>
static std::unordered_set<T> getCommonItems(std::unordered_set<T> s1, std::unordered_set<T> s2) {
        std::unordered_set<T> intersect;
        set_intersection (s1.begin (), s1.end (), s2.begin (), s2.end (),
                          std::inserter (intersect, intersect.begin ()));
        return intersect;
}

template<typename T>
static bool valueExistInList(std::vector<T> vec, T target) {
        bool isPresent =
                (std::find (vec.begin (), vec.end (), target) != vec.end ());
        return isPresent;
}

template<typename K, typename V>
static bool valueExistInMap(const std::unordered_map<K, V> &map, V value) {
        for (auto &kv : map) {
                if (kv.second.compare (value) == 0) {
                        return true;
                }
        }
        return false;
}

template<typename K, typename V>
static bool keyExistInMap(const std::unordered_map<K, V> &map, K key) {
        if (map.find (key) != map.end ()) {
                return true;
        }
        return false;
}


template<typename K, typename V>
static void putKeyValuePairInMap(K key, V value, std::unordered_map<K, V> &map) {
        map.insert (std::pair<K, V> (key, value));
}


template<typename T>
static std::vector<T> convertToList(T s1) {
        std::vector<T> s;
        s.push_back (s1);
        return s;
}

template<typename T>
static std::vector<T> convertSetToList(std::set<T> set) {
        std::vector<T> s;
        for (auto &entryInSet : set) {
                s.push_back (entryInSet);
        }
        return s;
}

template<typename T>
static std::set<T> convertListToSet(std::vector<T> list) {
        std::set<T> set;
        for (auto &entryInSet : list) {
                set.insert (entryInSet);
        }
        return set;
}

template<typename T>
static void addListToSet(std::set<T> &set, std::vector<T> list) {
        for (auto &entryInSet : list) {
                set.insert (entryInSet);
        }

}

template<typename A, typename B>
static std::pair<B, A> flip_pair(const std::pair<A, B> &p) {
        return std::pair<B, A> (p.second, p.first);
}

template<typename A, typename B>
static std::multimap<B, A> flip_map(const std::unordered_map<A, B> &src) {
        std::multimap<B, A> dst;
        std::transform (src.begin (), src.end (), std::inserter (dst, dst.begin ()),
                        flip_pair<A, B>);
        return dst;
}

template<typename K, typename V>
static bool areMapsEqual(tbb::concurrent_hash_map<K, V>& map1,
                         tbb::concurrent_hash_map<K, V>& map2) {
        if (map1.size() != map2.size()) {
                MLOG(10)<<"size difference : " <<  "map1.size : " << map1.size() << " : " << "map2.size : " << map2.size();

                return false;
        }
        typedef typename tbb::concurrent_hash_map<K, V>::iterator it_type;
        typedef typename tbb::concurrent_hash_map<K, V>::accessor accessorType;
        for(it_type iterator = map1.begin(); iterator != map1.end(); iterator++) {
                accessorType a;
                if (!map2.find(a, iterator->first)) {
                        MLOG(10)<<"didnt find key : " <<iterator->first;

                        return false;
                }
                auto value1 = iterator->second;
                auto value2 = a->second;

                if (value1 != value2) {
                        MLOG(10)<<"Map value not equal";
                        return false;
                }
        }
        return true;
}

template<typename K, typename V>
static bool areMapsEqual(std::unordered_map<K, V>& map1,
                         std::unordered_map<K, V>& map2) {
        if (map1.size() != map2.size()) {
                MLOG(10)<<"size difference : " <<  "map1.size : " << map1.size() << " : " << "map2.size : " << map2.size();

                return false;
        }
        typedef typename std::unordered_map<K, V>::iterator it_type;
        for(it_type iterator = map1.begin(); iterator != map1.end(); iterator++) {
                auto sameEntryInOtherMap = map2.find(iterator->first);
                if (sameEntryInOtherMap == map2.end()) {
                        MLOG(10)<<"didnt find key : " <<iterator->first;

                        return false;
                }
                auto value1 = iterator->second;
                auto value2 = sameEntryInOtherMap->second;

                if (value1 != value2) {
                        MLOG(10)<<"Map value not equal";
                        return false;
                }
        }
        return true;
}

template<typename K, typename V>
static bool areMapsEqualWithFunc(std::unordered_map<K, V>& map1,
                                 std::unordered_map<K, V>& map2, boost::function< bool(V, V) > equalCheckerFunc ) {
        if (map1.size() != map2.size()) {
                MLOG(10)<<"size difference : " <<  "map1.size : " << map1.size() << " : " << "map2.size : " << map2.size();
                return false;
        }
        typedef typename std::unordered_map<K, V>::iterator it_type;
        for(it_type iterator = map1.begin(); iterator != map1.end(); iterator++) {
                auto sameEntryInOtherMap = map2.find(iterator->first);
                if (sameEntryInOtherMap == map2.end()) {
                        MLOG(10)<<"didnt find key : " <<iterator->first;
                        return false;
                }
                auto value1 = iterator->second;
                auto value2 = sameEntryInOtherMap->second;

                if (!equalCheckerFunc(value1,value2)) {
                        MLOG(10)<<"Map object not equal";
                        return false;
                }
        }
        return true;
}

template<typename K, typename K1, typename V1>
static bool areMapOfMapsEqual(std::unordered_map<K, std::unordered_map<K1, V1> >& map1,
                              std::unordered_map<K, std::unordered_map<K1, V1> >& map2) {
        if (map1.size() != map2.size()) {
                return false;
        }

        typedef typename std::unordered_map<K, std::unordered_map<K1, V1> >::iterator it_type;
        for(it_type iterator = map1.begin(); iterator != map1.end(); iterator++) {
                auto sameEntryInOtherMap = map2.find(iterator->first);
                if (sameEntryInOtherMap == map2.end()) {
                        return false;
                }
                auto innerMap1 = iterator->second;
                auto innerMap2 = sameEntryInOtherMap->second;

                if (!areMapsEqual(innerMap1, innerMap2)) {
                        return false;
                }
        }

        return true;
}

template<typename K, typename K1, typename V1>
static void insertIntoMapOfMaps(
        std::unordered_map<K, std::unordered_map<K1, V1> >& outerMap,
        K outerKey,
        K1 innerKey) {

        auto tgPairPtr = outerMap.find(outerKey);

        if(tgPairPtr!= outerMap.end()) {
                // find the outerkey in the big map

                //innerMap
                auto targetGroupIdMap = tgPairPtr->second;

                auto crtsPairPtr = targetGroupIdMap.find(innerKey);
                if (crtsPairPtr != targetGroupIdMap.end()) {
                        //found the innerkey in small map
                        //we don't do anything
                } else {
                        //didnt find the innerKey in small map, now we insert it there
                        tgPairPtr->second.insert(std::make_pair(innerKey, 0));
                        LOG(INFO)<<"inserting data in inner map,  outerKey : "<< outerKey << ", innerKey : " << innerKey;
                }
        } else {
                //didnt find the big key in in big map
                typename std::unordered_map<K1, V1> newMap;
                newMap.insert(std::make_pair(innerKey, 0));
                outerMap.insert(std::make_pair(outerKey, newMap));
                // LOG(INFO)<<"inserting data in inner and outer map,  outerKey : "<< outerKey << ", innerKey : " << innerKey;
                // LOG(INFO)<<"inserting data in inner and outer map :  outerMap size : "<< outerMap.size();

        }
}

template<typename K, typename V>
static std::vector<K> getKeySetOfConcurrentMap(tbb::concurrent_hash_map<K, V> &map) {
        std::vector<K> list;
        for (typename tbb::concurrent_hash_map<K, V>::iterator ii = map.begin ();
             ii != map.end (); ++ii) {
                list.push_back ((*ii).first);
        }
        return list;
}

template<typename K, typename V>
static std::set<K> getKeySetOfMap(std::unordered_map<K, V> &map) {
        std::set<K> list;
        for (typename std::unordered_map<K, V>::iterator ii = map.begin ();
             ii != map.end (); ++ii) {
                list.insert (ii->first);
        }
        return list;
}

template<typename K, typename V>
static std::unordered_set<K> getUnorderedKeySetOfMap(std::unordered_map<K, V> &map) {
        std::unordered_set<K> list;
        for (typename std::unordered_map<K, V>::iterator ii = map.begin ();
             ii != map.end (); ++ii) {
                list.insert (ii->first);
        }
        return list;
}

template<typename K, typename V>
static std::unordered_set<K> getCommonKeysOfMaps(
        std::unordered_map<K, V> &map,
        std::unordered_map<K, V> &map2) {
        std::unordered_set<K>  keySet1 = getUnorderedKeySetOfMap<K, V>(map);
        std::unordered_set<K>  keySet2 = getUnorderedKeySetOfMap<K, V>(map2);

        return getCommonItems<K>(keySet1, keySet2);
}

template<typename K, typename V>
static std::unordered_map<K, V> removeKeysFromMap(
        std::unordered_set<K> keysToRemove,
        std::unordered_map<K, V> &map) {
        std::unordered_set<K> keySet1 = getUnorderedKeySetOfMap<K, V>(map);
        std::unordered_map<K, V> newMap;
        for (typename std::unordered_map<K, V>::iterator ii = map.begin ();
             ii != map.end (); ++ii) {
                if(!valueExistInSet(keysToRemove, ii->first)) {
                        newMap.insert(std::make_pair(ii->first, ii->second));
                }
        }
        return newMap;
}

template<typename K, typename V>
static void removeCommonKeysFromMaps(
        std::unordered_map<K, V> &map,
        std::unordered_map<K, V> &map2) {

        auto commonKeys = getCommonKeysOfMaps(map, map2);
        map = removeKeysFromMap(commonKeys, map);
        map2 = removeKeysFromMap(commonKeys, map2);
}


template<typename V>
static bool areListsEqual(std::vector<V> listA, std::vector<V> listB) {
        if (listA.size() != listB.size()) {
                return false;
        }
        if (getCommonItems<V>(listA, listB).size() != listA.size()) {
                return false;
        }

        return true;
}

template<typename K, typename V>
static std::vector<V> convertValuesOfMapToList(std::unordered_map<K, V> map) {
        std::vector<V> list;
        for (typename std::unordered_map<K, V>::iterator ii = map.begin ();
             ii != map.end (); ++ii) {
                list.push_back (ii->second);
        }
        return list;
}

template<typename K, typename V>
static std::vector<K> convertKeysOfMapToList(std::unordered_map<K, V> map) {
        std::vector<K> list;
        for (typename std::unordered_map<K, V>::iterator ii = map.begin ();
             ii != map.end (); ++ii) {
                list.push_back (ii->first);
        }
        return list;
}

};//end of class

template<>
std::unordered_map<int, std::shared_ptr<TargetGroup> > CollectionUtil::convertListToMap(std::vector<std::shared_ptr<TargetGroup> > list) {
        std::unordered_map<int, std::shared_ptr<TargetGroup> > map;
        for (auto crt : list) {
                MLOG(10)<<"inserting entry in map with id : "<< crt->getId();
                map.insert (std::pair<int, std::shared_ptr<TargetGroup> > (crt->getId(), crt));
        }
        return map;
}

template<>
std::unordered_map<int, std::string> CollectionUtil::convertListToMap(std::vector<std::string> list) {
        std::unordered_map<int, std::string> map;
        for (auto crt : list) {
                map.insert (std::pair<int, std::string> (std::hash<std::string>()(crt), crt));
        }
        return map;
}

}//namespace
#endif
