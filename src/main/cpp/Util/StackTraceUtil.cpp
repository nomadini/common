
#include <stdio.h>
#include <stdlib.h>
#include <execinfo.h>
#include <cxxabi.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>


#include "Poco/Util/ServerApplication.h"
using Poco::Util::ServerApplication;

#include "StackTraceUtil.h"

void StackTraceUtil::print_stacktrace11(FILE *out, unsigned int max_frames)
{
        // Application& app = Application::instance();
        fprintf(out, "stack trace:\n");

        // storage array for stack trace address data
        void* addrlist[max_frames+1];

        // retrieve current stack addresses
        int addrlen = backtrace(addrlist, sizeof(addrlist) / sizeof(void*));

        if (addrlen == 0) {
                fprintf(out, "  <empty, possibly corrupt>\n");
                return;
        }

        // resolve addresses into strings containing "filename(function+address)",
        // this array must be free()-ed
        char** symbollist = backtrace_symbols(addrlist, addrlen);

        // allocate string which will be filled with the demangled function name
        size_t funcnamesize = 256;
        char* funcname = (char*)malloc(funcnamesize);

        // iterate over the returned symbol lines. skip the first, it is the
        // address of this function.
        for (int i = 1; i < addrlen; i++)
        {
                char *begin_name = 0, *begin_offset = 0, *end_offset = 0;

                // find parentheses and +address offset surrounding the mangled name:
                // ./module(function+0x15c) [0x8048a6d]
                for (char *p = symbollist[i]; *p; ++p)
                {
                        if (*p == '(')
                                begin_name = p;
                        else if (*p == '+')
                                begin_offset = p;
                        else if (*p == ')' && begin_offset) {
                                end_offset = p;
                                break;
                        }
                }

                if (begin_name && begin_offset && end_offset
                    && begin_name < begin_offset)
                {
                        *begin_name++ = '\0';
                        *begin_offset++ = '\0';
                        *end_offset = '\0';

                        // mangled name is now in [begin_name, begin_offset) and caller
                        // offset in [begin_offset, end_offset). now apply
                        // __cxa_demangle():

                        int status;
                        char* ret = abi::__cxa_demangle(begin_name,
                                                        funcname, &funcnamesize, &status);
                        if (status == 0) {
                                funcname = ret; // use possibly realloc()-ed string
                                fprintf(out, "  %s : %s+%s\n",
                                        symbollist[i], funcname, begin_offset);
                                std::string funcnameStr(funcname);
                                std::string begin_offsetStr(begin_offset);

                                // app.logger().information(funcnameStr+" "+begin_offsetStr);
                        }
                        else {
                                // demangling failed. Output function name as a C function with
                                // no arguments.
                                fprintf(out, "  %s : %s()+%s\n",
                                        symbollist[i], begin_name, begin_offset);
                                std::string funcnameStr(funcname);
                                std::string begin_offsetStr(begin_offset);
                                // app.logger().information(funcnameStr+" "+begin_offsetStr);
                        }
                }
                else
                {
                        // couldn't parse the line? print the whole line.
                        fprintf(out, "  %s\n", symbollist[i]);
                        // app.logger().information("soosan stacktrace");
                }
        }

        free(funcname);
        free(symbollist);
}

void StackTraceUtil::print_trace11() {
        char pid_buf[30];
        sprintf(pid_buf, "--pid=%d", getpid());
        char name_buf[512];
        name_buf[readlink("/proc/self/exe", name_buf, 511)]=0;
        int child_pid = fork();
        if (!child_pid) {
                dup2(2,1); // redirect output to stderr
                fprintf(stdout,"stack trace for %s pid=%s\n",name_buf,pid_buf);
                execlp("gdb", "gdb", "--batch", "-n", "-ex", "thread", "-ex", "bt", name_buf, pid_buf, NULL);
                abort(); /* If gdb failed to start */
        } else {
                fprintf(stdout,"NO stack trace \n");

                waitpid(child_pid,NULL,0);
        }
}
