#include "StatisticsUtil.h"
#include "RandomUtilDecimalGenerator.h"
#include <boost/multiprecision/random.hpp>
#include <random>
#include <iostream>
#include "GUtil.h"

StatisticsUtil::StatisticsUtil() : Object(__FILE__) {
        percentRandomizer =
                std::make_shared<RandomUtilDecimalGenerator> (0, 1);

}

StatisticsUtil::~StatisticsUtil() {

}

bool StatisticsUtil::happensWithProb(double probability) {

        if(probability < 0 || probability > 1) {
                LOG_EVERY_N(ERROR, 1)<<"probability is not correct : "<<probability;
                assertAndThrow(false);
        }
        if (probability == 1) {
                return true;
        } else if (probability == 0) {
                return false;
        }
        //if a random decimal is greater than 10, it means 90% of time its greater than 10
        return percentRandomizer->randomDecimalV2() > (1 - probability);
}
