/*
 * FunctionUtil.h
 *
 *  Created on: Sep 7, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_UTIL_FUNCTIONUTIL_H_
#define GICAPODS_GICAPODSSERVER_SRC_UTIL_FUNCTIONUTIL_H_

class DeviceFeatureHistory;
// #include <boost/function.hpp>
// #include <boost/bind.hpp>
// #include <algorithm>
// #include <boost/function.hpp>
// #include <boost/bind.hpp>
// #include <boost/ref.hpp>
// #include <boost/thread/future.hpp>
// #include <boost/thread.hpp>



// class FunctionUtil {
// public:
// static void callFuncWithArgs( int times, boost::function< void() > func )
// {
//         for ( int i = 0; i < times; ++i )
//                 func();
// }
//
// };
//This is how you bind to a function
// std::function<void(std::shared_ptr<DeviceFeatureHistory>)> f_update_device =
// std::bind(DeviceFeatureHistoryCassandraService::upsertDeviceFeatureHistory, bh);


//this is how you call a function with different parameters
//void foo() { std::cout << "foo" << std::endl; }
//void bar( int x ) { std::cout << "bar(" << x << ")" << std::endl; }
//struct test {
//    void foo() { std::cout << "test::foo" << std::endl; }
//};
//void call( int times, boost::function< void() > f )
//{
//    for ( int i = 0; i < times; ++i )
//        f();
//}
//int main() {
//    call( 1, &foo );                   // no need to bind any argument
//    call( 2, boost::bind( &bar, 5 ) );
//    test t;
//    call( 1, boost::bind( &test::foo, &t ) ); // note the &t
//}
#endif /* GICAPODS_GICAPODSSERVER_SRC_UTIL_FUNCTIONUTIL_H_ */
