/*
 * Histogram.h
 *
 *  Created on: Aug 28, 2015
 *      Author: mtaabodi
 */

#ifndef Histogram_H_
#define Histogram_H_

#include <string>
#include <memory>
#include "AtomicLong.h"
#include <vector>
#include "Object.h"
class Histogram : public Object {
public:
// Pick whichever constructor feels more natural to you
Histogram(double min, double max, int numberOfBins);

void record(double datum); //records an observation
int bins() const;                        // Get the number of bins
int count(int bin) const;                // Get the number of data points in some bin
int countLowerOutliers() const;          // Get count of values below the minimum
int countUpperOutliers() const;          // Get count of values at or above the maximum
double lowerBound(int bin) const;        // Get the inclusive lower bound of a bin
double upperBound(int bin) const;        // Get the exclusive upper bound of a bin

void printBinCounts() const;
void printBinPercentages() const;
bool isAmongTopXPercentOfDataWithHighestValues(double datum, double percentageAskedFor) const;
virtual ~Histogram();

std::shared_ptr<gicapods::AtomicLong> totalCount;
private:
double min, max, binWidth;
int binCount;
double percentagePerBin;
std::shared_ptr<gicapods::AtomicLong> lowerOutlierCount, upperOutlierCount;
std::vector<std::shared_ptr<gicapods::AtomicLong> > recordHolder;
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_UTIL_NUMBERUTIL_H_ */
