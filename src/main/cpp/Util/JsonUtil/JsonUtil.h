#ifndef JSON_UTIL_h
#define JSON_UTIL_h


#include "rapidjson/reader.h"
#include "rapidjson/writer.h"
#include "rapidjson/error/error.h"
#include "rapidjson/encodings.h"
#include "rapidjson/error/en.h"
#include "rapidjson/stringbuffer.h"
#include "GUtil.h"
#include "NumberUtil.h"
#include "StringUtil.h"
#include "JsonTypeDefs.h"
#include "IntWrapper.h"
#define parseJsonSafely(args ...) JsonUtil::_parseJsonSafely(_L_,args)

//using unnamed namespace to fix the multiple definition problem
namespace {

class JsonUtil {

public:

inline static std::string docToString(DocumentType* document) {
        rapidjson::StringBuffer strbuf;
        rapidjson::Writer<rapidjson::StringBuffer> writer (strbuf);
        document->Accept (writer);
        return strbuf.GetString ();
}

inline static std::string valueToString(RapidJsonValueType value) {
        rapidjson::StringBuffer strbuf;
        rapidjson::Writer<rapidjson::StringBuffer> writer (strbuf);
        value.Accept (writer);
        return strbuf.GetString ();
}
inline static DocumentPtr createADcoument(rapidjson::Type type) {
        auto doc = std::make_shared<DocumentType>();
        if (type == rapidjson::kArrayType) {
                doc->SetArray ();
        } else if (type == rapidjson::kObjectType) {
                doc->SetObject ();
        }
        return doc;
}

inline static DocumentPtr createDcoumentAsObjectDoc() {
        auto doc = std::make_shared<DocumentType>();
        doc->SetObject ();
        return doc;
}

static void printObjectMemberTypes(DocumentType* document) {
        static const char *kTypeNames[] = {"Null", "False", "True", "Object",
                                           "Array", "String", "Number"};

        for (auto itr = document->MemberBegin ();
             itr != document->MemberEnd (); ++itr) {
                MLOG(3) << "Type of member " << itr->name.GetString () << " is " << kTypeNames[itr->value.GetType ()] << " " <<
                        _L_;
        }
}

inline static void handleParseError(DocumentType* doc) {
        throw std::logic_error ("parsing the json went wrong" +
                                StringUtil::toStr(GetParseError_En(doc->GetParseError()))
                                );
}

inline static void printTypeAndValueOfAllMembers(RapidJsonValueType impObjectValue) {
        for (auto m = impObjectValue.MemberBegin ();
             m != impObjectValue.MemberEnd (); ++m) {
                LOG(ERROR) << "member name : " << GetStringUnsafely (m->name) << _L_;
                printTypeAndValueOfJsonElement (m->value);
        }
}

inline static void assertJsonTypeAndThrow(RapidJsonValueType value, std::string typeExpected) {
        if(StringUtil::equalsIgnoreCase(typeExpected, "object")) {
                if (!value.IsObject()) {
                        LOG(ERROR) << "expected object but got different value";
                        printTypeAndValueOfJsonElement(value);
                        assertAndThrow(value.IsObject());
                }
        } else if(StringUtil::equalsIgnoreCase(typeExpected, "array")) {
                if (!value.IsArray()) {
                        LOG(ERROR) << "expected array but got different value";
                        printTypeAndValueOfJsonElement(value);
                        assertAndThrow(value.IsArray());
                }
        } else if(StringUtil::equalsIgnoreCase(typeExpected, "long")) {
                if (!value.IsInt64()) {
                        LOG(ERROR) << "expected long but got different value";
                        printTypeAndValueOfJsonElement(value);
                        assertAndThrow(value.IsInt64());
                }
        } else if(StringUtil::equalsIgnoreCase(typeExpected, "bool")) {
                if (!value.IsBool()) {
                        LOG(ERROR) << "expected bool but got different value";
                        printTypeAndValueOfJsonElement(value);
                        assertAndThrow(value.IsBool());
                }
        } else if(StringUtil::equalsIgnoreCase(typeExpected, "int")) {

                if (!value.IsInt()) {
                        LOG(ERROR) << "expected int but got different value";
                        printTypeAndValueOfJsonElement(value);
                        assertAndThrow(value.IsInt());
                }
        } else if(StringUtil::equalsIgnoreCase(typeExpected, "string")) {

                if (!value.IsString()) {
                        LOG(ERROR) << "expected string but got different value";
                        printTypeAndValueOfJsonElement(value);
                        assertAndThrow(value.IsString());
                }
        } else if(StringUtil::equalsIgnoreCase(typeExpected, "double")) {

                if (!value.IsDouble()) {
                        LOG(ERROR) << "expected double but got different value";
                        printTypeAndValueOfJsonElement(value);
                        assertAndThrow(value.IsDouble());
                }
        }


}

inline static void printTypeAndValueOfJsonElement(RapidJsonValueType val) {
        std::string type;
        std::string value;
        if (val.IsArray () == true) {
                type = StringUtil::toStr ("Array");
                LOG(ERROR)<<"--------begining of member values for Array---------";
                for (rapidjson::SizeType i = 0; i < val.Size (); i++) {
                        printTypeAndValueOfJsonElement(val[i]);
                }
                MLOG(3)<<"--------end of member values for Array---------";
                value = type;
        } else if (val.IsObject () == true) {
                type = StringUtil::toStr ("Object");

                LOG(ERROR)<<"--------begining of members values :---------";
                for (auto itr = val.MemberBegin(); itr != val.MemberEnd(); ++itr)
                {
                        std::string name = itr->name.GetString();
                        LOG(ERROR)<< "value name : "<<name;
                        printTypeAndValueOfJsonElement(itr->value);
                        // itr->name.GetString(), kTypeNames[itr->value.GetType()]);
                }
                LOG(ERROR)<<"--------end of member values---------";
                value = type;
        } else if (val.IsDouble () == true) {
                type = StringUtil::toStr ("Double");
                value = StringUtil::toStr (val.GetDouble ());
        } else if (val.IsInt () == true) {
                type = StringUtil::toStr ("Int");
                value = StringUtil::toStr (val.GetInt ());
        } else if (val.IsString () == true) {
                type = StringUtil::toStr ("String");
                value = val.GetString ();
        } else if (val.IsTrue () == true) {
                type = StringUtil::toStr ("True");
                value = StringUtil::toStr (val.GetBool ());
        } else if (val.IsFalse () == true) {
                type = StringUtil::toStr ("False");
                value = StringUtil::toStr (val.GetBool ());
        } else if (val.IsNull () == true) {
                type = StringUtil::toStr ("Null");
                value = StringUtil::toStr ("null");
        } else if (val.IsInt64 () == true) {
                type = StringUtil::toStr ("Int64");
                value = StringUtil::toStr (val.GetInt64());
        }

        LOG(ERROR) << " attention : json element type : " << type << " , value " << value;
        gicapods::Util::showStackTrace();
}

inline static DocumentPtr _parseJsonSafely(const std::string &caller,
                                           std::string jsonString) {

        DocumentPtr doc = std::make_shared<DocumentType>();

        if (doc->Parse<0> (jsonString.c_str ()).HasParseError ()) {

                LOG(ERROR) << "2caller :  " << caller <<
                        ", parseJsonSafely: error in parsing this : " << jsonString
                           << " error : "<< GetParseError_En(doc->GetParseError())
                           << " offset : " << (unsigned)doc->GetErrorOffset();

                //throw std::logic_error ("error in parsing string : " + jsonString);
                gicapods::Util::showStackTrace();
                throwEx("error in parsing string : " + jsonString);
        }

        assertAndThrow(doc->IsObject() || doc->IsArray());
        return doc;
}

inline static bool hasMemeber(RapidJsonValueType value, const std::string& fieldName) {
        assertJsonTypeAndThrow(value, "object");
        if (value.HasMember(fieldName.c_str())) {
                return true;
        } else {
                return false;
        }
}

inline static long GetLongSafely(RapidJsonValueType value, const std::string& fieldName) {
        assertJsonTypeAndThrow(value, "object");
        if (value.HasMember(fieldName.c_str())) {
                assertJsonTypeAndThrow(value[fieldName.c_str()], "long");
                return value[fieldName.c_str()].GetInt64();
        }
        throwEx(fieldName + " doesnt exist in value");
}

inline static bool GetBooleanSafely(RapidJsonValueType value, const std::string& fieldName) {
        assertJsonTypeAndThrow(value, "object");
        if (value.HasMember(fieldName.c_str())) {
                assertJsonTypeAndThrow(value[fieldName.c_str()], "bool");
                return value[fieldName.c_str()].GetBool();
        }
        throwEx(fieldName + " doesnt exist in value");
}

inline static int GetIntSafely(RapidJsonValueType value, const std::string& fieldName) {
        assertJsonTypeAndThrow(value, "object");
        if (value.HasMember(fieldName.c_str())) {
                assertJsonTypeAndThrow(value[fieldName.c_str()], "int");
                return value[fieldName.c_str()].GetInt();
        }
        throwEx(fieldName + " doesnt exist in value");
}


inline static std::string GetStringSafely(RapidJsonValueType value, const std::string& fieldName) {
        assertJsonTypeAndThrow(value, "object");
        if (value.HasMember(fieldName.c_str())) {
                assertJsonTypeAndThrow(value[fieldName.c_str()], "string");
                return value[fieldName.c_str()].GetString();
        }
        throwEx(fieldName + " doesnt exist in value");
}

inline static std::string GetStringSafely(RapidJsonValueType value,
                                          const std::string& fieldName,
                                          std::string defaultValue) {
        assertJsonTypeAndThrow(value, "object");
        if (value.HasMember(fieldName.c_str())) {
                assertJsonTypeAndThrow(value[fieldName.c_str()], "string");
                return value[fieldName.c_str()].GetString();
        }
        return defaultValue;
}

inline static double GetDoubleSafely(RapidJsonValueType value, const std::string& fieldName) {
        assertJsonTypeAndThrow(value, "object");
        if (value.HasMember(fieldName.c_str())) {
                assertJsonTypeAndThrow(value[fieldName.c_str()], "double");

                return NumberUtil::roundDouble(value[fieldName.c_str()].GetDouble (), 8);
        }
        throwEx(fieldName + " doesnt exist in value");
}

inline static bool GetBoolUnsafely(RapidJsonValueType value) {
        assertJsonTypeAndThrow(value, "bool");
        return value.GetBool();
}

inline static int GetIntUnsafely(RapidJsonValueType value) {
        assertJsonTypeAndThrow(value, "int");
        return value.GetInt ();
}


inline static std::string GetStringUnsafely(RapidJsonValueType value) {
        assertJsonTypeAndThrow(value, "string");
        return value.GetString ();
}

inline static double GetDoubleUnsafely(RapidJsonValueType value) {
        assertJsonTypeAndThrow(value, "double");
        return NumberUtil::roundDouble(value.GetDouble (), 8);
}

inline static long GetLongUnsafely(RapidJsonValueType value) {
        assertJsonTypeAndThrow(value, "long");
        return value.GetInt64 ();
}

template<class T>
inline static void populateValue(RapidJsonValueType value, T& objValue, DocumentType* doc) {
        objValue.addPropertiesToJsonValue (value, doc);
}

template <class T>
inline static void populateValue(RapidJsonValueType value, std::shared_ptr<T>& objValue, DocumentType* doc) {
        objValue->addPropertiesToJsonValue (value, doc);
}



template<class V>
inline static V getValueOfMember(RapidJsonValueType valueObject, std::string memberName) {
        if (!valueObject.HasMember(memberName.c_str())) {
                throwEx("no member exist with name " + memberName);
        }
        V finalValue;
        GetValueUnsafely(valueObject[memberName.c_str()], finalValue);
        return finalValue;
}

template<class V>
inline static void GetValueUnsafely(RapidJsonValueType valueObject, V& value) {
        const std::type_info& typeIdOfValue = typeid(value);
        if (StringUtil::equalsIgnoreCase(typeIdOfValue.name(), typeid(std::string).name()))  {
                // return GetStringUnsafely(value);
        }
        // value = V::fromJsonValue(valueObject);


        int status;
        char* realname = abi::__cxa_demangle(typeIdOfValue.name(), 0, 0, &status);
        if (realname !=nullptr) {
                std::string str(realname);
                LOG(ERROR)<< "wrong value type :  "<<str;

        } else {
                LOG(ERROR)<< "wrong value type :  "<<typeIdOfValue.name();
        }
        throwEx("wrong value type : " + StringUtil::toStr(typeIdOfValue.name()));
}

template<class V>
inline static void GetValueUnsafely(RapidJsonValueType valueObject, std::shared_ptr<V>& value) {
        value = V::fromJsonValue(valueObject);
}

inline static void removeMemberFromDoc(RapidJsonValueType doc, std::string memberName) {
        if (doc.HasMember(memberName.c_str())) {
                doc.RemoveMember(memberName.c_str());
        }
}

template <class T>
inline static std::string convertListToJson(std::vector<std::shared_ptr<T> > allEntities) {
        auto doc = std::make_shared<DocumentType>();
        doc->SetArray ();
        for (typename std::vector<std::shared_ptr<T> >::iterator it = allEntities.begin(); it != allEntities.end(); ++it) {
                RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
                (*it)->addPropertiesToJsonValue (value, doc);
                doc->PushBack (value, doc->GetAllocator ());
        }
        return docToString (doc.get());
}

inline static void addNameValuePairAsMemeberToValue(
        DocumentType* doc,
        const std::string &memberName,
        RapidJsonValueType objectValue,
        RapidJsonValueType parentObject) {
        RapidJsonValueTypeNoRef memberNameValue ((char *) memberName.c_str (), doc->GetAllocator ());

        if (parentObject.IsObject()) {
                //assertNoDuplicateField(memberName, parentObject); // TODO : fix this to use parent object
                parentObject.AddMember (memberNameValue, objectValue, doc->GetAllocator ());
        } else if (parentObject.IsArray()) {
                parentObject.PushBack (memberNameValue, doc->GetAllocator ());
        } else {
                printTypeAndValueOfJsonElement(parentObject);
                throwEx("unexpected object type : memberName : " + memberName);
        }

}

template <class T>
inline static std::vector<std::shared_ptr<T> > convertJsonToList(std::string json) {

        std::vector<std::shared_ptr<T> > objectsParsed;

        auto document = parseJsonSafely(json);
        assertJsonTypeAndThrow(*document, "array");

        for (rapidjson::SizeType i = 0; i < document->Size (); i++)   // rapidjson uses SizeType instead of size_t.
        {
                auto &obj = (*document)[i];
                assertJsonTypeAndThrow(obj, "object");
                auto entity = T::fromJsonValue (obj);
                objectsParsed.push_back (entity);
        }

        return objectsParsed;
}

template<class T>
inline static std::string convertToJsonStr(T* object) {
        auto doc = createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        object->addPropertiesToJsonValue(value, doc.get());
        return valueToString (value);
}



inline static void assertNoDuplicateField(const std::string &name, DocumentType* document) {
        if (name.empty ()) {
                throwEx("name of field cannot be empty in json");
        }

        if (document->IsObject () && document->HasMember (name.c_str ())) {
                throwEx(StringUtil::toStr ("this field is duplicate : ") + name);
        }
}

inline static bool isProperJson(std::string jsonString) {
        auto doc = std::make_shared<DocumentType>();
        if (doc->Parse<0> (jsonString.c_str ()).HasParseError ()) {
                return false;
        }
        return true;
}

inline static void addMemberToObject_From_String_Boolean_Pair(
        DocumentType* doc,
        const std::string &memberName,
        bool memberValue,
        RapidJsonValueType parentObject) {
        if (memberName.empty ()) {
                throwEx("memberName of field cannot be empty in json");
        }
        //	assertNoDuplicateField(memberName, parentObject);

        auto allocator = doc->GetAllocator ();

        RapidJsonValueTypeNoRef name (rapidjson::kStringType);
        name.SetString (memberName.c_str (), memberName.size (), doc->GetAllocator ());   //this is correct
        RapidJsonValueTypeNoRef value (rapidjson::kFalseType);
        value.SetBool (memberValue);   //this is correct

        parentObject.AddMember (name, value, allocator);
}

template<class T>
inline static void addMemberToValue_FromPair(DocumentType* doc,
                                             const std::string &memberName,
                                             std::shared_ptr<T> object,
                                             RapidJsonValueType objectMemberValue)
{
        RapidJsonValueTypeNoRef objectValue(rapidjson::kObjectType);
        object->addPropertiesToJsonValue(objectValue, doc);
        addNameValuePairAsMemeberToValue (doc, memberName, objectValue, objectMemberValue);
}

template<class T>
inline static void addMemberToValue_FromPair(DocumentType* doc,
                                             const std::string &memberName,
                                             T* object,
                                             RapidJsonValueType objectMemberValue)
{
        RapidJsonValueTypeNoRef objectValue(rapidjson::kObjectType);
        object->addPropertiesToJsonValue(objectValue, doc);
        addNameValuePairAsMemeberToValue (doc, memberName, objectValue, objectMemberValue);
}

/**
 * creates a member object of a document from a pair of strings.
 * something like this :  "id": "1"
 * that will be placed in a doc.this method doesnt put object in the document
 */
inline static void addMemberToValue_FromPair(DocumentType* doc,
                                             const std::string &memberName,
                                             std::string memberValue,
                                             RapidJsonValueType objectMemberValue) {

        auto allocator = doc->GetAllocator ();
        RapidJsonValueTypeNoRef name (rapidjson::kStringType);
        name.SetString (memberName.c_str (),
                        memberName.size (),
                        doc->GetAllocator ());                       //this is correct
        RapidJsonValueTypeNoRef value (rapidjson::kStringType);
        value.SetString (memberValue.c_str (),
                         memberValue.size (),
                         doc->GetAllocator ());

        if (objectMemberValue.IsObject()) {
                objectMemberValue.AddMember (name, value, doc->GetAllocator ());
        } else {
                assertJsonTypeAndThrow(objectMemberValue, "object");
        }
}


inline static void addMemberToValue_FromPair(DocumentType* doc,
                                             const std::string &memberName,
                                             RapidJsonValueType memberValue,
                                             RapidJsonValueType parentObject) {
        addNameValuePairAsMemeberToValue (doc, memberName, memberValue, parentObject);
}

inline static void addMemberToValue_FromPair(DocumentType* doc,
                                             const std::string &memberName,
                                             const unsigned long long memberValue,
                                             RapidJsonValueType objectMemberValue) {
        assertJsonTypeAndThrow(objectMemberValue, "object");
        auto allocator = doc->GetAllocator ();

        RapidJsonValueTypeNoRef name (rapidjson::kStringType);
        name.SetString (memberName.c_str (), memberName.size (), doc->GetAllocator ());   //this is correct
        RapidJsonValueTypeNoRef value (rapidjson::kStringType);
        value.SetInt64 ((long) memberValue);   //this is correct
        objectMemberValue.AddMember (name, value, doc->GetAllocator ());
}

inline static void addMemberToValue_FromPair(DocumentType* doc,
                                             const std::string &memberName,
                                             const char* memberValue,
                                             RapidJsonValueType parentObject) {
        addMemberToValue_FromPair(doc, memberName, StringUtil::toStr(memberValue), parentObject);
}

inline static void addMemberToValue_FromPair(DocumentType* doc,
                                             const int memberNameInt,
                                             const char* memberValue,
                                             RapidJsonValueType objectMemberValue) {
        assertJsonTypeAndThrow(objectMemberValue, "object");
        auto allocator = doc->GetAllocator ();

        RapidJsonValueTypeNoRef name (rapidjson::kStringType);
        std::string memberName = _toStr(memberNameInt);
        name.SetString (memberName.c_str (), memberName.size (), doc->GetAllocator ());   //this is correct
        RapidJsonValueTypeNoRef value (rapidjson::kStringType);
        value.SetString (memberValue,
                         strlen(memberValue),
                         doc->GetAllocator ());

        objectMemberValue.AddMember (name, value, doc->GetAllocator ());
}

inline static void addMemberToValue_FromPair(DocumentType* doc,
                                             const int memberNameInt,
                                             int memberValue,
                                             RapidJsonValueType objectMemberValue) {
        assertJsonTypeAndThrow(objectMemberValue, "object");
        auto allocator = doc->GetAllocator ();

        RapidJsonValueTypeNoRef name (rapidjson::kStringType);
        std::string memberName = _toStr(memberNameInt);
        name.SetString (memberName.c_str (), memberName.size (), doc->GetAllocator ());   //this is correct
        RapidJsonValueTypeNoRef value (rapidjson::kStringType);
        value.SetInt (memberValue);

        objectMemberValue.AddMember (name, value, doc->GetAllocator ());
}

inline static void addMemberToValue_FromPair(DocumentType* doc,
                                             const int memberNameInt,
                                             std::shared_ptr<IntWrapper> memberValue,
                                             RapidJsonValueType objectMemberValue) {
        assertJsonTypeAndThrow(objectMemberValue, "object");
        auto allocator = doc->GetAllocator ();

        RapidJsonValueTypeNoRef name (rapidjson::kStringType);
        std::string memberName = _toStr(memberNameInt);
        name.SetString (memberName.c_str (), memberName.size (), doc->GetAllocator ());   //this is correct
        RapidJsonValueTypeNoRef value (rapidjson::kStringType);
        value.SetInt ((*memberValue).getValue());

        objectMemberValue.AddMember (name, value, doc->GetAllocator ());
}

inline static void addMemberToValue_FromPair(DocumentType* doc,
                                             const std::string &memberName,
                                             int memberValue,
                                             RapidJsonValueType objectMemberValue) {
        if (memberName.empty ()) {
                throwEx("memberName of field cannot be empty in json");
        }
        //	assertNoDuplicateField(memberName, objectMemberValue);
        assertJsonTypeAndThrow(objectMemberValue, "object");
        RapidJsonValueTypeNoRef name (rapidjson::kStringType);
        name.SetString (memberName.c_str (), memberName.size (), doc->GetAllocator ());   //this is correct
        RapidJsonValueTypeNoRef value (rapidjson::kNumberType);
        value.SetInt (memberValue);   //this is correct

        objectMemberValue.AddMember (name, value, doc->GetAllocator());
}

inline static void addMemberToValue_FromPair(DocumentType* doc,
                                             const std::string &memberName,
                                             long memberValue,
                                             RapidJsonValueType parentObject) {
        if (memberName.empty ()) {
                throwEx("memberName of field cannot be empty in json");
        }
        //	assertNoDuplicateField(memberName, parentObject);
        assertJsonTypeAndThrow(parentObject, "object");
        auto allocator = doc->GetAllocator ();

        RapidJsonValueTypeNoRef name (rapidjson::kStringType);
        name.SetString (memberName.c_str (), memberName.size (), doc->GetAllocator ());   //this is correct
        RapidJsonValueTypeNoRef value (rapidjson::kNumberType);
        value.SetInt64 (memberValue);   //this is correct

        parentObject.AddMember (name, value, allocator);
}

inline static void addMemberToValue_FromPair(DocumentType* doc,
                                             const std::string &memberName,
                                             double memberValue,
                                             RapidJsonValueType parentObject) {
        if (memberName.empty ()) {
                throwEx("memberName of field cannot be empty in json");
        }
        //	assertNoDuplicateField(memberName, parentObject);
        assertJsonTypeAndThrow(parentObject, "object");
        auto allocator = doc->GetAllocator ();

        RapidJsonValueTypeNoRef name (rapidjson::kStringType);
        name.SetString (memberName.c_str (), memberName.size (), doc->GetAllocator ());   //this is correct
        RapidJsonValueTypeNoRef value (rapidjson::kNumberType);
        value.SetDouble (NumberUtil::roundDouble(memberValue, 8));   //this is correct

        parentObject.AddMember (name, value, allocator);

}

inline static void addBooleanMemberToValue_FromPair(DocumentType* doc,
                                                    const std::string &memberName,
                                                    bool memberValue,
                                                    RapidJsonValueType parentObject) {
        if (memberName.empty ()) {
                throwEx("memberName of field cannot be empty in json");
        }
        //	assertNoDuplicateField(memberName, parentObject);

        assertJsonTypeAndThrow(parentObject, "object");
        auto allocator = doc->GetAllocator ();

        RapidJsonValueTypeNoRef name (rapidjson::kStringType);
        name.SetString (memberName.c_str (), memberName.size (), doc->GetAllocator ());
        RapidJsonValueTypeNoRef value (rapidjson::kNumberType);
        value.SetBool (memberValue);

        parentObject.AddMember (name, value, allocator);
}
};


template<>
void JsonUtil::populateValue(RapidJsonValueType value, std::string& valStr, DocumentType* doc) {
        value.SetString (valStr.c_str (), valStr.size (), doc->GetAllocator ());
}

template<>
void JsonUtil::populateValue(RapidJsonValueType value, int& valStr, DocumentType* doc) {
        value.SetInt(valStr);
}

template<>
void JsonUtil::populateValue(RapidJsonValueType value, int const& valStr, DocumentType* doc) {
        value.SetInt(valStr);
}

template<>
void JsonUtil::populateValue(RapidJsonValueType value, long& valStr, DocumentType* doc) {
        value.SetInt64(valStr);
}

template<>
void JsonUtil::populateValue(RapidJsonValueType value, double& valStr, DocumentType* doc) {
        value.SetDouble(NumberUtil::roundDouble(valStr, 8));
}

template<>
void JsonUtil::populateValue(RapidJsonValueType value, bool& valStr, DocumentType* doc) {
        value.SetBool(valStr);
}

template<>
void JsonUtil::GetValueUnsafely(RapidJsonValueType valueObject, unsigned long long& value) {
        value = GetLongUnsafely(valueObject);
}

template<>
void JsonUtil::GetValueUnsafely(RapidJsonValueType valueObject, long& value) {
        value = GetLongUnsafely(valueObject);
}
template<>
void JsonUtil::GetValueUnsafely(RapidJsonValueType valueObject, int& value) {
        value = GetIntUnsafely(valueObject);
}
template<>
void JsonUtil::GetValueUnsafely(RapidJsonValueType valueObject, bool& value) {
        value = GetBoolUnsafely(valueObject);
}
template<>
void JsonUtil::GetValueUnsafely(RapidJsonValueType valueObject, double& value) {
        value = GetDoubleUnsafely(valueObject);
        value = NumberUtil::roundDouble(value, 8);
}

template<>
void JsonUtil::GetValueUnsafely(RapidJsonValueType valueObject, float& value) {
        value = GetDoubleUnsafely(valueObject);
        value = NumberUtil::roundDouble(value, 8);
}

template<>
void JsonUtil::GetValueUnsafely(RapidJsonValueType valueObject, std::string& value) {
        value = GetStringUnsafely(valueObject);
}

template<>
void JsonUtil::GetValueUnsafely(RapidJsonValueType valueObject, std::shared_ptr<IntWrapper>& value) {
        assertJsonTypeAndThrow(valueObject, "int");
        (*value).setValue(valueObject.GetInt ());
}
}
#endif
