#ifndef JSON_ARRAY_UTIL_H
#define JSON_ARRAY_UTIL_H


#include "ConverterUtil.h"
#include "rapidjson/document.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonTypeDefs.h"
#include <memory>
#include <string>
#include <vector>
#include <set>
#include <unordered_set>
#include <tbb/concurrent_unordered_set.h>


//using unnamed namespace to fix the multiple definition problem
namespace {

class JsonArrayUtil {

public:



inline static void getNthValueOfArray(RapidJsonValueType value,
                                      int index,
                                      RapidJsonValueType destinationValue) {


        if(!value.IsArray()) {
                JsonUtil::printTypeAndValueOfJsonElement(value);
                throwEx("memeber in doc is not array");
        }

        rapidjson::SizeType b = (rapidjson::SizeType) index;
        if(value.Size() >= (b+1)) {
                destinationValue = value[b];
        } else {
                throwEx(" array doesnt have enought members");
        }
}

inline static void getNthValueOfArray(RapidJsonValueType value,
                                      const std::string &memberName,
                                      rapidjson::SizeType b,
                                      RapidJsonValueType destinationValue) {
        if(!value.HasMember(memberName.c_str ())) {
                JsonUtil::printTypeAndValueOfJsonElement(value);
                throwEx("no memeber in doc : memeber name "+ memberName);
        }

        RapidJsonValueType array = value[memberName.c_str ()];
        if(!array.IsArray()) {
                throwEx("memeber in doc is not array : memeber name "+ memberName);
        }

        if(array.Size() >= (b+1)) {
                destinationValue = array[b];
        } else {
                throwEx(" array doesnt have enought members : memeber name "+ memberName);
        }
}

/**
 * creates a GenericValue that is an array of strings like this  ["value", "host", "mount"]
 */
template<typename T>
inline static void addMemberToArrayInDocument(DocumentType* doc,
                                              std::vector<T> valuesStr,
                                              RapidJsonValueType myArray) {
        auto allocator = doc->GetAllocator ();

        for (auto valStr : valuesStr) {
                RapidJsonValueTypeNoRef objValue (rapidjson::kStringType);
                valStr.addPropertiesToJsonValue(objValue, doc);
                myArray.PushBack (objValue, allocator);
        }
}

/**
 * adds an array to some object that will be in doc later ,
 * some array like  this "columns" : ["value", "host", "mount"]
 */
template<typename T>
inline static void addMemberToValue_FromPair(
        DocumentType* doc,
        const std::string &nameOfArray,
        std::vector<T> valuesStr,
        RapidJsonValueType parentValue) {
        RapidJsonValueTypeNoRef myArray (rapidjson::kArrayType);
        auto allocator = doc->GetAllocator ();

        for (auto valStr : valuesStr) {
                RapidJsonValueTypeNoRef objValue (rapidjson::kObjectType);
                valStr.addPropertiesToJsonValue(objValue, doc);
                myArray.PushBack (objValue, allocator);
        }
        RapidJsonValueTypeNoRef nameOfArrayValue ((char *) nameOfArray.c_str (), doc->GetAllocator ());

        parentValue.AddMember (nameOfArrayValue, myArray, doc->GetAllocator ());
}

template<typename T>
inline static void addArrayAsAMemberToValue(DocumentType* doc,
                                            std::vector<T> list,
                                            RapidJsonValueType parentValue) {
        assertAndThrow(parentValue.IsArray());
        for (auto val : list) {
                RapidJsonValueTypeNoRef objValue (rapidjson::kObjectType);
                val->addPropertiesToJsonValue(objValue, doc);
                parentValue.PushBack (objValue, doc->GetAllocator ());
        }

}
template<typename T>
inline static void addArrayAsAMemberToValue(DocumentType* doc,
                                            std::vector<std::shared_ptr<T> > list,
                                            RapidJsonValueType parentValue) {
        assertAndThrow(parentValue.IsArray());
        for (auto val : list) {
                RapidJsonValueTypeNoRef objValue(rapidjson::kObjectType);
                val->addPropertiesToJsonValue(objValue, doc);
                parentValue.PushBack (objValue, doc->GetAllocator ());
        }

}

/**
   this function will take an array of strings like adPositions in targetGroup and
   returns a json array string that is like this ["Header", "Footer", "Below the Fold"]
 */
inline static std::string convertArrayOfStringsToJsonArrayString(std::vector<std::string> strings) {
        DocumentPtr doc = createDocumentAsArray ();
        RapidJsonValueTypeNoRef valueObject(rapidjson::kArrayType);

        assertAndThrow(valueObject.IsArray());
        for (auto val : strings) {
                RapidJsonValueTypeNoRef objValue;

                objValue.SetString (val.c_str (), val.size (), doc->GetAllocator ());
                valueObject.PushBack (objValue, doc->GetAllocator ());
        }

        return JsonUtil::valueToString(valueObject);
}
/**
 * adds an array to some object that will be in doc later ,
 * some array like  this "columns" : ["value", "host", "mount"]
 */
template<typename T>
inline static void addMemberToValue_FromPair(
        DocumentType* doc,
        const std::string &nameOfArray,
        std::set<T> valuesStr,
        RapidJsonValueType parentValue) {
        RapidJsonValueTypeNoRef myArray (rapidjson::kArrayType);
        auto allocator = doc->GetAllocator ();

        for (auto valStr : valuesStr) {
                RapidJsonValueTypeNoRef objValue (rapidjson::kObjectType);
                valStr.addPropertiesToJsonValue(objValue, doc);
                myArray.PushBack (objValue, allocator);
        }
        RapidJsonValueTypeNoRef nameOfArrayValue ((char *) nameOfArray.c_str (), doc->GetAllocator ());

        parentValue.AddMember (nameOfArrayValue, myArray, doc->GetAllocator ());
}

template<typename T>
inline static void addMemberToValue_FromPair(
        DocumentType* doc,
        const std::string &nameOfArray,
        std::unordered_set<T> valuesStr,
        RapidJsonValueType parentValue) {
        RapidJsonValueTypeNoRef myArray (rapidjson::kArrayType);
        auto allocator = doc->GetAllocator ();

        for (auto valStr : valuesStr) {
                RapidJsonValueTypeNoRef objValue (rapidjson::kObjectType);
                valStr.addPropertiesToJsonValue(objValue, doc);
                myArray.PushBack (objValue, allocator);
        }
        RapidJsonValueTypeNoRef nameOfArrayValue ((char *) nameOfArray.c_str (), doc->GetAllocator ());

        parentValue.AddMember (nameOfArrayValue, myArray, doc->GetAllocator ());
}

/**
 * adds an array to some object that will be in doc later ,
 * some array like  this "columns" : ["value", "host", "mount"]
 */
template<typename T>
inline static void addMemberToValue_FromPair(
        DocumentType* doc,
        const std::string &nameOfArray,
        std::vector<std::shared_ptr<T> > valuesStr,
        RapidJsonValueType parentValue) {
        RapidJsonValueTypeNoRef myArray (rapidjson::kArrayType);
        auto allocator = doc->GetAllocator ();

        for (auto valStr : valuesStr) {
                RapidJsonValueTypeNoRef objValue (rapidjson::kObjectType);
                valStr->addPropertiesToJsonValue(objValue, doc);
                myArray.PushBack (objValue, allocator);
        }

        RapidJsonValueTypeNoRef nameOfArrayValue ((char *) nameOfArray.c_str (), doc->GetAllocator ());

        parentValue.AddMember (nameOfArrayValue, myArray, doc->GetAllocator ());
}

template<typename T>
inline static void addArrayAsAMemberToValue(DocumentType* doc,
                                            std::set<T> list, RapidJsonValueType parentValue) {
        assertAndThrow(parentValue.IsArray());
        for (auto val : list) {
                RapidJsonValueTypeNoRef objValue(rapidjson::kObjectType);
                val->addPropertiesToJsonValue(objValue, doc);
                parentValue.PushBack (objValue, doc->GetAllocator ());
        }

}

template<typename T>
inline static void addArrayAsAMemberToValue(DocumentType* doc,
                                            std::unordered_set<T> list, RapidJsonValueType parentValue) {
        assertAndThrow(parentValue.IsArray());
        for (auto val : list) {
                RapidJsonValueTypeNoRef objValue(rapidjson::kObjectType);
                val->addPropertiesToJsonValue(objValue, doc);
                parentValue.PushBack (objValue, doc->GetAllocator ());
        }

}


inline static void addArrayAsAMemberToValue(DocumentType* doc,
                                            tbb::concurrent_unordered_set<std::string> list,
                                            RapidJsonValueType parentValue) {
        assertAndThrow(parentValue.IsArray());
        for (auto val : list) {
                RapidJsonValueTypeNoRef objValue;

                objValue.SetString (val.c_str (), val.size (), doc->GetAllocator ());
                parentValue.PushBack (objValue, doc->GetAllocator ());
        }
}

/**
 * adds some number like 4 to array "columns":[4,"value","host","mount"]}
 */
inline static void addMemberToArray(DocumentType* doc,
                                    int number,
                                    RapidJsonValueType myArray) {


        assertAndThrow(myArray.IsArray ());
        RapidJsonValueTypeNoRef objValue;
        objValue.SetInt (number);

        myArray.PushBack (objValue, doc->GetAllocator ());
        // MLOG(3)<<"array is : " <<JsonUtil::valueToString(myArray);
}

/**
 * this method pushes an object to an array, it will
 * create something like this , the array and the object must be built already
 * "imp": [
   {
   "id": "1"
   }
   ]
 */

inline static void addMemberToArray(DocumentType* doc,
                                    RapidJsonValueType myArray,
                                    RapidJsonValueType myObject) {


        assertAndThrow(myArray.IsArray ());
        assertAndThrow(myObject.IsObject ());

        myArray.PushBack (myObject, doc->GetAllocator ());
}

inline static void addMemberToArray(DocumentType* doc, double number,
                                    RapidJsonValueType myArray) {

        auto allocator = doc->GetAllocator ();

        RapidJsonValueTypeNoRef objValue;
        objValue.SetDouble (number);

        myArray.PushBack (objValue, doc->GetAllocator ());
}

inline static void addMemberToArray(DocumentType* doc,
                                    std::string memberValue,
                                    RapidJsonValueType myArray) {

        auto allocator = doc->GetAllocator ();
        assertAndThrow(!memberValue.empty());
        RapidJsonValueTypeNoRef objValue (rapidjson::kStringType);
        objValue.SetString (memberValue.c_str (), memberValue.size (),
                            doc->GetAllocator ());   //this is correct

        myArray.PushBack (objValue, doc->GetAllocator ());
}

inline static void addMemberToArray(DocumentType* doc, bool val,
                                    RapidJsonValueType myArray) {


        auto allocator = doc->GetAllocator ();

        RapidJsonValueTypeNoRef objValue;
        objValue.SetBool (val);

        myArray.PushBack (objValue, doc->GetAllocator ());
}

/**
 * adds an array to some object that will be in doc later ,
 * , this array must already be built,
 * some array like  this "columns" : ["value", 3, 5]
 */
inline static void addArrayValueAsMemberToValue(DocumentType* doc,
                                                const std::string &nameOfArray, RapidJsonValueType myArray,
                                                RapidJsonValueType parentObjectMemberValue) {

        assertAndThrow(!nameOfArray.empty());
        auto allocator = doc->GetAllocator ();

        RapidJsonValueTypeNoRef nameOfArrayValue ((char *) nameOfArray.c_str (), doc->GetAllocator ());

        parentObjectMemberValue.AddMember (nameOfArrayValue, myArray,
                                           doc->GetAllocator ());
}

inline static DocumentPtr createDocumentAsArray() {
        auto doc = std::make_shared<DocumentType>();
        doc->SetArray ();
        return doc;
}

template<typename T>
inline static void getArrayFromValueMemeber(
        RapidJsonValueType value, const std::string &arrayName, std::vector<std::shared_ptr<T> >& allData) {
        assertAndThrow(!arrayName.empty());
        const char *nameOfElement = arrayName.c_str ();
        if (!value.HasMember (nameOfElement)) {
                JsonUtil::printTypeAndValueOfJsonElement(value);
                throwEx("value doesn't have member : " + arrayName);
        }
        RapidJsonValueType a = value[nameOfElement];   // Using a reference for consecutive access is handy and faster.
        assertAndThrow(a.IsArray ());
        // rapidjson uses SizeType instead of size_t.
        for (rapidjson::SizeType i = 0; i < a.Size (); i++) {
                std::shared_ptr<T> obj = T::fromJsonValue(a[i]);
                allData.push_back (obj);
                // allData.push_back (JsonUtil::GetValueUnsafely<T>(a[i]));

        }
}


/**
 * returns an array of integer from an array in a value in a document.
 * like passing the the whole document as an value and bcat , and getting all the bcat values
 *
   {
   "id": "1",
   "bidfloor": 0.03,
   "banner": {
   "h": 250,
   "w": 300,
   "pos": 0
   },
   "bcat" : [1,2,3]
   }
 */
template<typename T>
inline static void getArrayFromValueMemeber(
        RapidJsonValueType value, const std::string &arrayName, std::vector<T>& allData) {
        throwEx("implement it yourself for specific type");
}

template<class T>
inline static std::vector<std::shared_ptr<T> > getArrayOfObjectsFromMemberInValue(RapidJsonValueType value,
                                                                                  const std::string &arrayName) {
        std::vector<std::shared_ptr<T> > allData;
        assertAndThrow(!arrayName.empty());
        const char *nameOfElement = arrayName.c_str ();

        if (!value.HasMember (nameOfElement)) {
                JsonUtil::printTypeAndValueOfJsonElement(value);
                throwEx("value doesn't have member : " + arrayName);
        }

        RapidJsonValueType a = value[nameOfElement];                                                                // Using a reference for consecutive access is handy and faster.
        assertAndThrow(a.IsArray ());
        // rapidjson uses SizeType instead of size_t.
        for (rapidjson::SizeType i = 0; i < a.Size (); i++) {
                allData.push_back (T::fromJsonValue (a[i]));

        }
        return allData;
}


/**
   takes a Value like an inner object of bidRequest and parses the elements
   in that value to an array of strings
 */
inline static std::vector<std::string> getArrayOfStringsFromMemberInValue(RapidJsonValueType value,
                                                                          const std::string &arrayName) {
        std::vector<std::string> allData;
        assertAndThrow(!arrayName.empty());
        const char *nameOfElement = arrayName.c_str ();
        if (!value.HasMember (nameOfElement)) {
                JsonUtil::printTypeAndValueOfJsonElement(value);
                throwEx("value doesn't have member : " + arrayName);
        }
        RapidJsonValueType a = value[nameOfElement];   // Using a reference for consecutive access is handy and faster.
        assertAndThrow(a.IsArray ());
        // rapidjson uses SizeType instead of size_t.
        for (rapidjson::SizeType i = 0; i < a.Size (); i++) {
                allData.push_back (JsonUtil::GetStringUnsafely (a[i]));

        }
        return allData;
}



template<class T>
inline static void getArrayOfObjectsFromJsonString(
        std::string reponseInJson,  std::vector<std::shared_ptr<T> >& objectsParsed) {
        assertAndThrow(!reponseInJson.empty());
        auto document = parseJsonSafely(reponseInJson);
        assertAndThrow(document->IsArray ());

        for (rapidjson::SizeType i = 0; i < document->Size (); i++)   // rapidjson uses SizeType instead of size_t.
        {
                auto &obj = (*document)[i];
                assertAndThrow(obj.IsObject ());
                auto entity = T::fromJsonValue (obj);
                objectsParsed.push_back (entity);
        }
}

template<class T>
inline static void getArrayOfObjectsFromJsonString(
        std::string reponseInJson,  std::vector<T>& objectsParsed) {
        assertAndThrow(!reponseInJson.empty());
        auto document = parseJsonSafely(reponseInJson);
        assertAndThrow(document->IsArray ());

        for (rapidjson::SizeType i = 0; i < document->Size (); i++)   // rapidjson uses SizeType instead of size_t.
        {
                auto &obj = (*document)[i];
                auto entity = T::toStr (obj);
                objectsParsed.push_back (entity);
        }
}

template<class T>
inline static void getArrayOfObjectsFromJsonString(
        std::string reponseInJson,  std::set<std::shared_ptr<T> >& objectsParsed) {
        assertAndThrow(!reponseInJson.empty());
        auto document = parseJsonSafely(reponseInJson);
        assertAndThrow(document->IsArray ());

        for (rapidjson::SizeType i = 0; i < document->Size (); i++)   // rapidjson uses SizeType instead of size_t.
        {
                auto &obj = (*document)[i];
                assertAndThrow(obj.IsObject ());
                auto entity = T::fromJsonValue (obj);
                objectsParsed.push_back (entity);
        }
}


template<class T>
inline static void getArrayOfObjectsFromJsonString(
        std::string reponseInJson,  std::set<T>& objectsParsed) {
        assertAndThrow(!reponseInJson.empty());
        auto document = parseJsonSafely(reponseInJson);
        assertAndThrow(document->IsArray ());

        for (rapidjson::SizeType i = 0; i < document->Size (); i++)   // rapidjson uses SizeType instead of size_t.
        {
                auto &obj = (*document)[i];
                auto entity = T::toStr (obj);
                objectsParsed.insert (entity);
        }
}




template<class T>
inline static std::string convertListToJson(std::vector<std::shared_ptr<T> > allObjects) {
        auto doc = createDocumentAsArray ();
        for(std::shared_ptr<T> obj :  allObjects) {
                RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
                obj->addPropertiesToJsonValue (value, doc.get());
                doc->PushBack (value, doc->GetAllocator ());
        }
        return JsonUtil::docToString (doc.get());
}

template<class T>
inline static std::string convertListToJson(std::set<std::shared_ptr<T> > allObjects) {
        auto doc = createDocumentAsArray ();
        for(std::shared_ptr<T> obj :  allObjects) {
                RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
                obj->addPropertiesToJsonValue (value, doc.get());
                doc->PushBack (value, doc->GetAllocator ());
        }
        return JsonUtil::docToString (doc.get());
}

template<class T>
inline static std::string convertListToJson(std::vector<T> allObjects) {
        auto doc = JsonUtil::createADcoument (rapidjson::kArrayType);
        JsonArrayUtil::addArrayAsAMemberToValue (doc.get(), allObjects, *doc);
        return JsonUtil::valueToString (*doc);
}

template<typename T>
static std::string convertSetToJson(std::set<T> list) {
        auto doc = JsonUtil::createADcoument (rapidjson::kArrayType);
        JsonArrayUtil::addArrayAsAMemberToValue (doc.get(), list, *doc);
        return JsonUtil::valueToString (*doc);
}


template<typename T>
static std::string convertConcurrentSetToJson(tbb::concurrent_unordered_set<T> list) {
        auto doc = JsonUtil::createADcoument (rapidjson::kArrayType);
        JsonArrayUtil::addArrayAsAMemberToValue (doc.get(), list, *doc);
        return JsonUtil::valueToString (*doc);
}

template<typename T>
static std::string convertSetToJson(std::unordered_set<T> list) {
        auto doc = JsonUtil::createADcoument (rapidjson::kArrayType);
        JsonArrayUtil::addArrayAsAMemberToValue (doc.get(), list, *doc);
        return JsonUtil::valueToString (*doc);
}

template<class T>
inline static std::string convertListToJson(tbb::concurrent_unordered_set<T> allObjects) {
        auto doc = createDocumentAsArray ();
        for(auto obj :  allObjects) {
                RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
                obj.addPropertiesToJsonValue (value, doc.get());
                doc->PushBack (value, doc->GetAllocator ());
        }
        return JsonUtil::docToString (doc.get());
}


template<class T>
inline static std::string convertListToJson(std::set<T> allObjects) {
        auto doc = createDocumentAsArray ();
        for(std::shared_ptr<T> obj :  allObjects) {
                RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
                obj->addPropertiesToJsonValue (value, doc.get());
                doc->PushBack (value, doc->GetAllocator ());
        }
        return JsonUtil::docToString (doc.get());
}


template<class T>
inline static void addMemberToValue_FromArrayOfObjects(DocumentType* doc,
                                                       RapidJsonValueType objectMemberValue,
                                                       const std::string &nameOfArray,
                                                       std::vector<std::shared_ptr<T> > objects) {

        RapidJsonValueTypeNoRef bwEntriesArray(rapidjson::kArrayType);

        for(std::shared_ptr<T> obj :  objects) {
                RapidJsonValueTypeNoRef bWEntryPtrvalue(rapidjson::kObjectType);
                obj->addPropertiesToJsonValue (bWEntryPtrvalue, doc);
                addMemberToArray(doc, bwEntriesArray, bWEntryPtrvalue);
        }
        addArrayValueAsMemberToValue(doc, nameOfArray, bwEntriesArray, objectMemberValue);

}

template<class T>
inline static void addMemberToValue_FromArrayOfObjects(DocumentType* doc,
                                                       RapidJsonValueType objectMemberValue,
                                                       const std::string &nameOfArray,
                                                       std::vector<T> objects) {

        RapidJsonValueTypeNoRef bwEntriesArray(rapidjson::kArrayType);

        for(auto obj :  objects) {
                RapidJsonValueTypeNoRef bWEntryPtrvalue(rapidjson::kObjectType);
                obj.addPropertiesToJsonValue (bWEntryPtrvalue, doc);
                addMemberToArray(doc, bwEntriesArray, bWEntryPtrvalue);
        }
        addArrayValueAsMemberToValue(doc, nameOfArray, bwEntriesArray, objectMemberValue);
}






/////
template<typename T>
inline static void getArrayFromValueMemeber(
        RapidJsonValueType value, const std::string &arrayName, std::set<std::shared_ptr<T> >& allData) {
        assertAndThrow(!arrayName.empty());
        const char *nameOfElement = arrayName.c_str ();
        if (!value.HasMember (nameOfElement)) {
                JsonUtil::printTypeAndValueOfJsonElement(value);
                throwEx("value doesn't have member : " + arrayName);
        }
        RapidJsonValueType a = value[nameOfElement];   // Using a reference for consecutive access is handy and faster.
        assertAndThrow(a.IsArray ());
        // rapidjson uses SizeType instead of size_t.
        for (rapidjson::SizeType i = 0; i < a.Size (); i++) {
                std::shared_ptr<T> obj = T::fromJsonValue(a[i]);
                allData.insert (obj);
                // allData.push_back (JsonUtil::GetValueUnsafely<T>(a[i]));

        }
}

template<typename T>
inline static std::vector<std::shared_ptr<T> > getArrayFromValueMemeber(RapidJsonValueType value) {
        std::vector<std::shared_ptr<T> > allData;

        assertAndThrow(value.IsArray());
        // rapidjson uses SizeType instead of size_t.
        for (rapidjson::SizeType i = 0; i < value.Size (); i++) {
                std::shared_ptr<T> obj = T::fromJsonValue(value[i]);
                allData.push_back (obj);
                // allData.push_back (JsonUtil::GetValueUnsafely<T>(a[i]));

        }
        return allData;
}


/**
 * returns an array of integer from an array in a value in a document.
 * like passing the the whole document as an value and bcat , and getting all the bcat values
 *
   {
   "id": "1",
   "bidfloor": 0.03,
   "banner": {
   "h": 250,
   "w": 300,
   "pos": 0
   },
   "bcat" : [1,2,3]
   }
 */
template<typename T>
inline static void getArrayFromValueMemeber(
        RapidJsonValueType value, const std::string &arrayName, std::set<T>& allData) {
        throwEx("implement it yourself for specific type");
}

template <class T>
inline static std::shared_ptr<std::vector<std::shared_ptr<T> > > convertStringArrayToArrayOfObject(std::string reponseInJson) {

        std::shared_ptr<std::vector<std::shared_ptr<T> > >
        objectsParsed = std::make_shared<std::vector<std::shared_ptr<T> > >();

        auto document = parseJsonSafely(reponseInJson);
        assertAndThrow(document->IsArray ());

        for (rapidjson::SizeType i = 0; i < document->Size (); i++)   // rapidjson uses SizeType instead of size_t.
        {
                auto &obj = (*document)[i];
                assertAndThrow(obj.IsObject ());
                auto entity = T::fromJsonValue (obj);
                objectsParsed->push_back (entity);
        }

        return objectsParsed;
}
};


template<>
void JsonArrayUtil::getArrayOfObjectsFromJsonString(
        std::string reponseInJson,  std::vector<std::string>& objectsParsed) {
        assertAndThrow(!reponseInJson.empty());
        auto document = parseJsonSafely(reponseInJson);
        assertAndThrow(document->IsArray ());

        for (rapidjson::SizeType i = 0; i < document->Size (); i++)   // rapidjson uses SizeType instead of size_t.
        {
                auto &obj = (*document)[i];
                assertAndThrow(obj.IsString());
                objectsParsed.push_back (obj.GetString ());
        }
}

template<>
void JsonArrayUtil::getArrayOfObjectsFromJsonString(
        std::string reponseInJson, std::vector<int>& objectsParsed) {
        assertAndThrow(!reponseInJson.empty());
        auto document = parseJsonSafely(reponseInJson);
        assertAndThrow(document->IsArray ());

        for (rapidjson::SizeType i = 0; i < document->Size (); i++)   // rapidjson uses SizeType instead of size_t.
        {
                auto &obj = (*document)[i];
                if (obj.IsInt()) {
                        objectsParsed.push_back (obj.GetInt ());
                } else if (obj.IsString()) {
                        objectsParsed.push_back (ConverterUtil::convertTo<int>(obj.GetString ()));
                } else {
                        JsonUtil::printTypeAndValueOfJsonElement(obj);
                        throwEx("unknown type");
                }

        }
}
template<>
void JsonArrayUtil::getArrayOfObjectsFromJsonString(
        std::string reponseInJson,  std::set<std::string>& objectsParsed) {
        assertAndThrow(!reponseInJson.empty());
        auto document = parseJsonSafely(reponseInJson);
        assertAndThrow(document->IsArray ());

        for (rapidjson::SizeType i = 0; i < document->Size (); i++)   // rapidjson uses SizeType instead of size_t.
        {
                auto &obj = (*document)[i];
                assertAndThrow(obj.IsString());
                objectsParsed.insert (obj.GetString ());
        }
}

template<>
void JsonArrayUtil::getArrayOfObjectsFromJsonString(
        std::string reponseInJson, std::set<int>& objectsParsed) {
        assertAndThrow(!reponseInJson.empty());
        auto document = parseJsonSafely(reponseInJson);
        assertAndThrow(document->IsArray ());

        for (rapidjson::SizeType i = 0; i < document->Size (); i++)   // rapidjson uses SizeType instead of size_t.
        {
                auto &obj = (*document)[i];
                assertAndThrow(obj.IsInt());
                objectsParsed.insert (obj.GetInt ());
        }
}




template<>
std::string JsonArrayUtil::convertListToJson(std::set<std::string> allObjects) {
        auto doc = createDocumentAsArray ();
        for(std::string val :  allObjects) {
                RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
                value.SetString (val.c_str (), val.size (), doc->GetAllocator ());
                doc->PushBack (value, doc->GetAllocator ());
        }
        return JsonUtil::docToString (doc.get());
}

template<>
std::string JsonArrayUtil::convertListToJson(std::vector<std::string> allObjects) {
        auto doc = createDocumentAsArray ();
        for(std::string val :  allObjects) {
                RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
                value.SetString (val.c_str (), val.size (), doc->GetAllocator ());
                doc->PushBack (value, doc->GetAllocator ());
        }
        return JsonUtil::docToString (doc.get());
}

template<>
std::string JsonArrayUtil::convertListToJson(tbb::concurrent_unordered_set<std::string> allObjects) {
        auto doc = createDocumentAsArray ();
        for(auto obj :  allObjects) {
                RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
                value.SetString (obj.c_str (), obj.size (), doc->GetAllocator ());
                doc->PushBack (value, doc->GetAllocator ());
        }
        return JsonUtil::docToString (doc.get());
}



template<>
std::string JsonArrayUtil::convertSetToJson(std::set<int> list) {
        auto doc = createDocumentAsArray ();
        for(int obj : list) {
                RapidJsonValueTypeNoRef value;
                value.SetInt (obj);
                doc->PushBack (value, doc->GetAllocator ());
        }
        return JsonUtil::docToString (doc.get());
}

template<>
std::string JsonArrayUtil::convertListToJson(std::vector<int> allObjects) {
        auto doc = createDocumentAsArray ();
        for(int obj : allObjects) {
                RapidJsonValueTypeNoRef value;
                value.SetInt (obj);
                doc->PushBack (value, doc->GetAllocator ());
        }
        return JsonUtil::docToString (doc.get());
}


template<>
void JsonArrayUtil::getArrayFromValueMemeber(
        RapidJsonValueType value, const std::string &arrayName, std::set<int>& allData) {
        assertAndThrow(!arrayName.empty());
        const char *nameOfElement = arrayName.c_str ();
        if (!value.HasMember (nameOfElement)) {
                JsonUtil::printTypeAndValueOfJsonElement(value);
                throwEx("value doesn't have member : " + arrayName);
        }
        RapidJsonValueType a = value[nameOfElement];   // Using a reference for consecutive access is handy and faster.
        assertAndThrow(a.IsArray ());
        // rapidjson uses SizeType instead of size_t.
        for (rapidjson::SizeType i = 0; i < a.Size (); i++) {
                int value;
                JsonUtil::GetValueUnsafely(a[i], value);
                allData.insert (value);

        }
}

template<>
void JsonArrayUtil::getArrayFromValueMemeber(
        RapidJsonValueType value, const std::string &arrayName, std::set<std::string>& allData) {
        assertAndThrow(!arrayName.empty());
        const char *nameOfElement = arrayName.c_str ();
        if (!value.HasMember (nameOfElement)) {
                JsonUtil::printTypeAndValueOfJsonElement(value);
                throwEx("value doesn't have member : " + arrayName);
        }
        RapidJsonValueType a = value[nameOfElement];   // Using a reference for consecutive access is handy and faster.
        assertAndThrow(a.IsArray ());
        // rapidjson uses SizeType instead of size_t.
        for (rapidjson::SizeType i = 0; i < a.Size (); i++) {
                std::string value;
                JsonUtil::GetValueUnsafely<std::string>(a[i], value);
                allData.insert (value);

        }
}




template<>
void JsonArrayUtil::getArrayFromValueMemeber(
        RapidJsonValueType value, const std::string &arrayName, std::set<RapidJsonValueTypePtr >& allData) {
        assertAndThrow(!arrayName.empty());
        const char *nameOfElement = arrayName.c_str ();
        if (!value.HasMember (nameOfElement)) {
                throwEx("value doesn't have member : " + arrayName);
        }
        RapidJsonValueType a = value[nameOfElement];   // Using a reference for consecutive access is handy and faster.
        assertAndThrow(a.IsArray ());
        // rapidjson uses SizeType instead of size_t.
        for (rapidjson::SizeType i = 0; i < a.Size (); i++) {
                RapidJsonValueType element =  a[i];
                allData.insert (&element);
        }
}


template<>
void JsonArrayUtil::addArrayAsAMemberToValue(DocumentType* doc,
                                             std::set<std::string> list, RapidJsonValueType parentValue) {
        assertAndThrow(parentValue.IsArray());
        for (auto val : list) {
                RapidJsonValueTypeNoRef objValue;
                objValue.SetString (val.c_str(), val.size(), doc->GetAllocator ());
                parentValue.PushBack (objValue, doc->GetAllocator ());
        }

}

template<>
void JsonArrayUtil::addArrayAsAMemberToValue(DocumentType* doc,
                                             std::unordered_set<std::string> list, RapidJsonValueType parentValue) {
        assertAndThrow(parentValue.IsArray());
        for (auto val : list) {
                RapidJsonValueTypeNoRef objValue;
                objValue.SetString (val.c_str(), val.size(), doc->GetAllocator ());
                parentValue.PushBack (objValue, doc->GetAllocator ());
        }

}

template<>
void JsonArrayUtil::addMemberToValue_FromArrayOfObjects(DocumentType* doc,
                                                        RapidJsonValueType objectMemberValue,
                                                        const std::string &nameOfArray,
                                                        std::vector<double> objects) {
        assertAndThrow(objectMemberValue.IsArray());
        for (auto val : objects) {
                RapidJsonValueTypeNoRef objValue;
                objValue.SetDouble (val);
                objectMemberValue.PushBack (objValue, doc->GetAllocator ());
        }
}

template<>
void JsonArrayUtil::addArrayAsAMemberToValue(DocumentType* doc,
                                             std::vector<int> list,
                                             RapidJsonValueType parentValue) {
        assertAndThrow(parentValue.IsArray());
        for (auto val : list) {
                RapidJsonValueTypeNoRef objValue;
                objValue.SetInt (val);
                parentValue.PushBack (objValue, doc->GetAllocator ());
        }

}
template<>
void JsonArrayUtil::addArrayAsAMemberToValue(DocumentType* doc,
                                             std::vector<double> list,
                                             RapidJsonValueType parentValue) {
        assertAndThrow(parentValue.IsArray());
        for (auto val : list) {
                RapidJsonValueTypeNoRef objValue;
                objValue.SetDouble (val);
                parentValue.PushBack (objValue, doc->GetAllocator ());
        }

}

template<>
void JsonArrayUtil::addArrayAsAMemberToValue(DocumentType* doc,
                                             std::vector<std::string> list,
                                             RapidJsonValueType parentValue) {
        assertAndThrow(parentValue.IsArray());
        for (auto val : list) {
                RapidJsonValueTypeNoRef objValue;

                objValue.SetString (val.c_str (), val.size (), doc->GetAllocator ());
                parentValue.PushBack (objValue, doc->GetAllocator ());
        }
}



template<>
void JsonArrayUtil::getArrayFromValueMemeber(
        RapidJsonValueType value, const std::string &arrayName, std::vector<int>& allData) {
        assertAndThrow(!arrayName.empty());
        const char *nameOfElement = arrayName.c_str ();
        if (!value.HasMember (nameOfElement)) {
                JsonUtil::printTypeAndValueOfJsonElement(value);
                throwEx("value doesn't have member : " + arrayName);
        }
        RapidJsonValueType a = value[nameOfElement];   // Using a reference for consecutive access is handy and faster.
        assertAndThrow(a.IsArray ());
        // rapidjson uses SizeType instead of size_t.
        for (rapidjson::SizeType i = 0; i < a.Size (); i++) {
                int value;
                JsonUtil::GetValueUnsafely(a[i], value);
                allData.push_back (value);

        }
}

template<>
void JsonArrayUtil::getArrayFromValueMemeber(
        RapidJsonValueType value, const std::string &arrayName,
        std::vector<RapidJsonValueTypePtr >& allData) {
        assertAndThrow(!arrayName.empty());
        const char *nameOfElement = arrayName.c_str ();
        if (!value.HasMember (nameOfElement)) {
                JsonUtil::printTypeAndValueOfJsonElement(value);
                throwEx("value doesn't have member : " + arrayName);
        }
        RapidJsonValueType a = value[nameOfElement];   // Using a reference for consecutive access is handy and faster.
        assertAndThrow(a.IsArray ());
        // rapidjson uses SizeType instead of size_t.
        for (rapidjson::SizeType i = 0; i < a.Size (); i++) {
                RapidJsonValueType element =  a[i];
                allData.push_back (&element);
        }
}


template<>
void JsonArrayUtil::getArrayFromValueMemeber(
        RapidJsonValueType value, const std::string &arrayName, std::vector<std::string>& allData) {
        assertAndThrow(!arrayName.empty());
        const char *nameOfElement = arrayName.c_str ();
        if (!value.HasMember (nameOfElement)) {
                JsonUtil::printTypeAndValueOfJsonElement(value);
                throwEx("value doesn't have member : " + arrayName);
        }
        RapidJsonValueType a = value[nameOfElement];   // Using a reference for consecutive access is handy and faster.
        assertAndThrow(a.IsArray ());
        // rapidjson uses SizeType instead of size_t.
        for (rapidjson::SizeType i = 0; i < a.Size (); i++) {
                std::string value;
                JsonUtil::GetValueUnsafely<std::string>(a[i], value);
                allData.push_back (value);

        }
}


template<>
void JsonArrayUtil::addMemberToValue_FromPair(
        DocumentType* doc,
        const std::string &nameOfArray,
        std::vector<std::string> valuesStr,
        RapidJsonValueType parentValue) {
        RapidJsonValueTypeNoRef myArray (rapidjson::kArrayType);
        auto allocator = doc->GetAllocator ();

        for (auto valStr : valuesStr) {
                RapidJsonValueTypeNoRef objValue (rapidjson::kStringType);
                JsonUtil::populateValue(objValue, valStr, doc);
                myArray.PushBack (objValue, allocator);
        }
        RapidJsonValueTypeNoRef nameOfArrayValue ((char *) nameOfArray.c_str (), doc->GetAllocator ());

        parentValue.AddMember (nameOfArrayValue, myArray, doc->GetAllocator ());
}

template<>
void JsonArrayUtil::addMemberToValue_FromPair(
        DocumentType* doc,
        const std::string &nameOfArray,
        std::vector<int> valuesStr,
        RapidJsonValueType parentValue) {
        RapidJsonValueTypeNoRef myArray (rapidjson::kArrayType);
        auto allocator = doc->GetAllocator ();

        for (auto valStr : valuesStr) {
                RapidJsonValueTypeNoRef objValue (rapidjson::kNumberType);
                JsonUtil::populateValue(objValue, valStr, doc);
                myArray.PushBack (objValue, allocator);
        }
        RapidJsonValueTypeNoRef nameOfArrayValue ((char *) nameOfArray.c_str (), doc->GetAllocator ());

        parentValue.AddMember (nameOfArrayValue, myArray, doc->GetAllocator ());
}

template<>
void JsonArrayUtil::addMemberToValue_FromPair(
        DocumentType* doc,
        const std::string &nameOfArray,
        std::set<std::string> valuesStr,
        RapidJsonValueType parentValue) {
        RapidJsonValueTypeNoRef myArray (rapidjson::kArrayType);
        auto allocator = doc->GetAllocator ();

        for (auto valStr : valuesStr) {
                RapidJsonValueTypeNoRef objValue (rapidjson::kStringType);
                JsonUtil::populateValue(objValue, valStr, doc);
                myArray.PushBack (objValue, allocator);
        }
        RapidJsonValueTypeNoRef nameOfArrayValue ((char *) nameOfArray.c_str (), doc->GetAllocator ());

        parentValue.AddMember (nameOfArrayValue, myArray, doc->GetAllocator ());
}

template<>
void JsonArrayUtil::addMemberToValue_FromPair(
        DocumentType* doc,
        const std::string &nameOfArray,
        std::unordered_set<std::string> valuesStr,
        RapidJsonValueType parentValue) {
        RapidJsonValueTypeNoRef myArray (rapidjson::kArrayType);
        auto allocator = doc->GetAllocator ();

        for (auto valStr : valuesStr) {
                RapidJsonValueTypeNoRef objValue (rapidjson::kStringType);
                JsonUtil::populateValue(objValue, valStr, doc);
                myArray.PushBack (objValue, allocator);
        }
        RapidJsonValueTypeNoRef nameOfArrayValue ((char *) nameOfArray.c_str (), doc->GetAllocator ());

        parentValue.AddMember (nameOfArrayValue, myArray, doc->GetAllocator ());
}

template<>
void JsonArrayUtil::addMemberToValue_FromPair(
        DocumentType* doc,
        const std::string &nameOfArray,
        std::set<int> valuesStr,
        RapidJsonValueType parentValue) {
        RapidJsonValueTypeNoRef myArray (rapidjson::kArrayType);
        auto allocator = doc->GetAllocator ();

        for (auto valStr : valuesStr) {
                RapidJsonValueTypeNoRef objValue (rapidjson::kStringType);
                JsonUtil::populateValue(objValue, valStr, doc);
                myArray.PushBack (objValue, allocator);
        }
        RapidJsonValueTypeNoRef nameOfArrayValue ((char *) nameOfArray.c_str (), doc->GetAllocator ());

        parentValue.AddMember (nameOfArrayValue, myArray, doc->GetAllocator ());
}

}
#endif
