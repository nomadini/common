#ifndef JSON_TYPE_DEFS_h
#define JSON_TYPE_DEFS_h

#include "rapidjson/document.h"

#include <string>
#include <memory>
#include <vector>

//typedef DocumentPtr DocumentPtr;
// typedef rapidjson::Document DocumentType;
// typedef rapidjson::GenericValue<rapidjson::UTF8<char>, rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator> >& RapidJsonValueType;
// typedef rapidjson::GenericValue<rapidjson::UTF8<char>, rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator> > RapidJsonValueTypeNoRef;


//  //THIS DOESNT WORK ON ARM but has a better performance, there is a bug in rapidjson that
// //causes memory allocator to fail, you have to go back to using memory allocater when they fix the bug.
//  //this works for ARM because of some bug in rapidjson
// //typedef const rapidjson::GenericValue<rapidjson::UTF8<char>, rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator> > & RapidJsonValueType ;

typedef std::shared_ptr<rapidjson::GenericDocument<rapidjson::UTF8<>,rapidjson::CrtAllocator> > DocumentPtr;
typedef rapidjson::GenericDocument<rapidjson::UTF8<>, rapidjson::CrtAllocator> DocumentType;
typedef rapidjson::GenericValue <rapidjson::UTF8<char>, rapidjson::CrtAllocator>& RapidJsonValueType;
typedef rapidjson::GenericValue<rapidjson::UTF8<char>, rapidjson::CrtAllocator> RapidJsonValueTypeNoRef;
typedef rapidjson::GenericValue <rapidjson::UTF8<char>, rapidjson::CrtAllocator>* RapidJsonValueTypePtr;
#endif
