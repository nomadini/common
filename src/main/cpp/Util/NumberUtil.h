/*
 * NumberUtil.h
 *
 *  Created on: Aug 28, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_UTIL_NUMBERUTIL_H_
#define GICAPODS_GICAPODSSERVER_SRC_UTIL_NUMBERUTIL_H_



#include "Object.h"

//A.K.A MathUtil
class NumberUtil {
NumberUtil();
public:

static long getNearestMultipleToNumber(long value, int multiple);

static double roundDouble(double value, double precision = 4);

static bool areDoubleValuesEqual(double value1, double value2, double epsilon = 0.0000001);
};


#endif /* GICAPODS_GICAPODSSERVER_SRC_UTIL_NUMBERUTIL_H_ */
