//
//  Utils.h
//  PoconoDB
//
//  Created by Mahmoud Taabodi on 6/6/14.
//  Copyright (c) 2014 Mahmoud Taabodi. All rights reserved.
//

#ifndef GUTIL_UTILS_H
#define GUTIL_UTILS_H


#include "glog/logging.h"
#include "StackTraceUtil.h"
#include <boost/lexical_cast.hpp>
#include <memory>
#include <string>
#include <iostream>
#include <tbb/concurrent_hash_map.h>
#include "Poco/Timestamp.h"

/* for calculating the lat lon distance */
#define pi_gicapods 3.14159265358979323846
#define earthRadiusKm 6371.0
#define MILE 0.621371
#define __METHOD_NAME__ methodName(__PRETTY_FUNCTION__)
#define _L_ (toStrJustForUtil(" ")+toStrJustForUtil(_F_)+toStrJustForUtil(":")+toStrJustForUtil(":")+toStrJustForUtil(__LINE__)+toStrJustForUtil("_")+toStrJustForUtil(__func__))
#include "LogLevelManager.h"

//gives you FILE and LINE as string
#define _FL_ _toStr(__FILE__)+_toStr(__LINE__)
template<typename T>
extern std::string toStrJustForUtil(T i) {
								std::string str;
								try {
																str = boost::lexical_cast < std::string > (i);
								} catch (boost::bad_lexical_cast& e) {
																std::cout << "bad cast happening in converting to toStrJustForUtil";
								}
								return str;
}

#define _F_ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define _L1_ (toStrJustForUtil(_F_)+toStrJustForUtil(":")+toStrJustForUtil(":")+toStrJustForUtil(__LINE__)+toStrJustForUtil("_")+toStrJustForUtil(__func__))
#define throwEx(args ...) gicapods::Util::throwEx_(_L1_, args);
#define EXIT(args ...) gicapods::Util::_exit(_L1_, args);
#define assertAndThrow(argv) gicapods::Util::assertAndThrow_(_L1_, argv, #argv);
#define assertAndWarn(argv) gicapods::Util::assertAndWarn_(_L1_, argv, #argv);
#define NULL_CHECK(val) assertAndThrow(val != nullptr);
#define __CLASS_NAME__ className(__PRETTY_FUNCTION__)

namespace gicapods {
class Util {

public:
static bool exitOnErrorConditions;
static std::shared_ptr<tbb::concurrent_hash_map<std::string, long> > filelineNumberToCallCount;

static bool allowedToCall(std::string fileLineNum, int everyNtimes);
static void throwEx_(const std::string& caller, const std::string& msg, bool loud = false);
static void assertAndThrow_(const std::string& caller, bool resultOfOperation, const std::string& operation);
static void assertAndWarn_(const std::string& caller, bool resultOfOperation, const std::string& operation);
static int getProcessId();

static void myterminate();
static int parseLine(char* line);

static int getMemoryUsedByProcess();

static void getMemoryUsage(double& vm_usage, double& resident_set);

static void printMemoryUsage();

static std::string calc_request_id();

template<typename T>
static T calc_hash_code(T obj);

static void sleepMicroSecond(int microseconds);
static void sleepNanoSecond(int nanoseconds);
static void sleepMiliSecond(int x);
static void sleepMillis(int millis);
static void sleepViaBoost(std::string caller, int seconds);
static void _exit(std::string caller, std::string msg = "");

static double getLatLonDistanceInMile(
								double lat1d,
								double lon1d,
								double lat2d,
								double lon2d);

// This function converts decimal degrees to radians
static double deg2rad(double deg);

//  This function converts radians to decimal degrees
static double rad2deg(double rad);

/**
 * Returns the distance between two points on the Earth.
 * Direct translation from http://en.wikipedia.org/wiki/Haversine_formula
 * @param lat1d Latitude of the first point in degrees
 * @param lon1d Longitude of the first point in degrees
 * @param lat2d Latitude of the second point in degrees
 * @param lon2d Longitude of the second point in degrees
 * @return The distance between the two points in kilometers
 */
static double distanceEarthInKm(double lat1d, double lon1d, double lat2d,
																																double lon2d);

static void printTimeOfRunInSec(Poco::Timestamp& now);
static void printTimeOfRunInMilis(Poco::Timestamp& now);

void setThreadName(const pthread_t& thread, const std::string & name);

std::string methodName(const std::string& prettyFunction);
std::string className(const std::string& prettyFunction);

static void showStackTrace(std::exception const *e = nullptr);
};

}


#endif
