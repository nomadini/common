#include "ProgramOptionParser.h"
#include "GUtil.h"
#include <boost/multiprecision/random.hpp>
#include <random>
#include <iostream>



#include <boost/program_options.hpp>
using namespace boost::program_options;

#include <iostream>
using namespace std;
/* Auxiliary functions for checking input for validity. */

/* Function used to check that 'opt1' and 'opt2' are not specified
   at the same time. */
void ProgramOptionParser::conflicting_options(const variables_map& vm,
                                              const char* opt1, const char* opt2)
{
        if (vm.count(opt1) && !vm[opt1].defaulted()
            && vm.count(opt2) && !vm[opt2].defaulted())
                throw logic_error(string("Conflicting options '")
                                  + opt1 + "' and '" + opt2 + "'.");
}

/* Function used to check that of 'for_what' is specified, then
   'required_option' is specified too. */
void ProgramOptionParser::option_dependency(const variables_map& vm,
                                            const char* for_what, const char* required_option)
{
        if (vm.count(for_what) && !vm[for_what].defaulted())
                if (vm.count(required_option) == 0 || vm[required_option].defaulted())
                        throw logic_error(string("Option '") + for_what
                                          + "' requires option '" + required_option + "'.");
}


/******************
   EXAMPLE
 **************/

//
// int main(int argc, char* argv[]) {
//
//         std::string ofile;
//         std::string macrofile, libmakfile;
//         bool t_given = false;
//         bool b_given = false;
//         std::string mainpackage;
//         std::string depends = "deps_file";
//         std::string sources = "src_file";
//         std::string root = ".";
//
//         boost::program_options::options_description desc("Allowed options");
//         desc.add_options()
//         // First parameter describes option name/short name
//         // The second is parameter to option
//         // The third is description
//                 ("help,h", "print usage message")
//                 ("output,o", boost::program_options::value(&ofile), "pathname for output")
//                 ("macrofile,m", boost::program_options::value(&macrofile), "full pathname of macro.h")
//                 ("two,t", boost::program_options::bool_switch(&t_given), "preprocess both header and body")
//                 ("body,b", boost::program_options::bool_switch(&b_given), "preprocess body in the header context")
//                 ("libmakfile,l", boost::program_options::value(&libmakfile),
//                 "write include makefile for library")
//                 ("mainpackage,p", boost::program_options::value(&mainpackage),
//                 "output dependency information")
//                 ("depends,d", boost::program_options::value(&depends),
//                 "write dependencies to <pathname>")
//                 ("sources,s", boost::program_options::value(&sources), "write source package list to <pathname>")
//                 ("root,r", boost::program_options::value(&root), "treat <dirname> as project root directory");
//
//         boost::program_options::variables_map vm;
//         boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
//
//         if (vm.count("help")) {
//                 std::cout << desc << "\n";
//                 return 0;
//         }
//
//         ProgramOptionParser::conflicting_options(vm, "output", "two");
//         ProgramOptionParser::conflicting_options(vm, "output", "body");
//         ProgramOptionParser::conflicting_options(vm, "output", "mainpackage");
//         ProgramOptionParser::conflicting_options(vm, "two", "mainpackage");
//         ProgramOptionParser::conflicting_options(vm, "body", "mainpackage");
//
//         ProgramOptionParser::conflicting_options(vm, "two", "body");
//         ProgramOptionParser::conflicting_options(vm, "libmakfile", "mainpackage");
//         ProgramOptionParser::conflicting_options(vm, "libmakfile", "mainpackage");
//
//         ProgramOptionParser::option_dependency(vm, "depends", "mainpackage");
//         ProgramOptionParser::option_dependency(vm, "sources", "mainpackage");
//         ProgramOptionParser::option_dependency(vm, "root", "mainpackage");
//
//         std::cout << "two = " << vm["two"].as<bool>() << "\n";
// }
