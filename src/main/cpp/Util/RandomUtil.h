/*
 * RandomUtil.h
 *
 *  Created on: Aug 9, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_UTIL_RANDOMUTIL_H_
#define GICAPODS_GICAPODSSERVER_SRC_UTIL_RANDOMUTIL_H_

#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <string>
#include <memory>
#include "Object.h"
#include <random>

class RandomUtil : public Object {

public:

std::shared_ptr<std::uniform_int_distribution<> > longRandomFinder;
std::shared_ptr<std::mt19937> longRandomGenerator;
std::shared_ptr<std::random_device> randomDevice;
int longUpperRangeInclusive;
RandomUtil(int longUpperRange);

// static double randomDecimal(int lowerBound, int upperBound);
// static long randomNumber(long range);

static int sudoRandomNumber(int range);

long randomNumberV2();
virtual ~RandomUtil();
};
//
//#include <boost/multiprecision/gmp.hpp>
//#include <boost/multiprecision/random.hpp>
//
//int main()
//{
//   using namespace boost::multiprecision;
//   using namespace boost::random;
//   //
//   // We can repeat the above example, with other distributions:
//   //
//   uniform_real_distribution<mpf_float_50> ur(-20, 20);
//   gamma_distribution<mpf_float_50> gd(20);
//   independent_bits_engine<mt19937, 50L*1000L/301L, mpz_int> gen;
//   //
//   // Generate some values:
//   //
//   std::cout << std::setprecision(50);
//   for(unsigned i = 0; i < 20; ++i)
//      std::cout << ur(gen) << std::endl;
//   for(unsigned i = 0; i < 20; ++i)
//      std::cout << gd(gen) << std::endl;
//   return 0;
//}




#endif /* GICAPODS_GICAPODSSERVER_SRC_UTIL_RANDOMUTIL_H_ */
