#include "GUtil.h"
#include <iostream>

#include "FileUtil.h"
#include "ConverterUtil.h"
#include "DateTimeUtil.h"
#include "Poco/DateTime.h"
#include "Poco/Timespan.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include <boost/date_time/gregorian/gregorian.hpp>

#include "boost/date_time/local_time/posix_time_zone.hpp"

#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include "boost/date_time/time_zone_base.hpp"
using namespace boost::gregorian;

boost::local_time::tz_database DateTimeUtil::tz;
int DateTimeUtil::daysInSeconds = 86400;
int DateTimeUtil::minutesInSeconds = 60;


DateTimeUtil::DateTimeUtil() {

}

void DateTimeUtil::init() {
        std::string filename =
                "/var/data"  + StringUtil::toStr ("/date_time_zonespec.csv");
        tz.load_from_file (filename.c_str ());
}

std::string DateTimeUtil::getDateInYYYMMDDFormat(int daysPlusNow) {
        Poco::DateTime now;
        Poco::DateTime dt = now + daysPlusNow * 1000000;
        return DateTimeUtil::getNowInStringFormat (dt.timestamp (), "%Y-%m-%d");
}

Poco::DateTime DateTimeUtil::truncateDateToMinutePercision(Poco::DateTime date) {

        Poco::DateTime nowWithOnlyHour (
                date.year(),
                date.month(),
                date.day(),
                date.hour(),
                date.minute(),
                0,
                0,
                0);
        return nowWithOnlyHour;

}

Poco::DateTime DateTimeUtil::truncateDateToHourPercision(Poco::DateTime date) {
        Poco::DateTime nowWithOnlyHour (
                date.year(),
                date.month(),
                date.day(),
                date.hour(),
                0,
                0,
                0,
                0);
        return nowWithOnlyHour;

}

Poco::DateTime DateTimeUtil::truncateDateToDayPercision(Poco::DateTime date) {
        Poco::DateTime nowWithOnlyHour (
                date.year(),
                date.month(),
                date.day(),
                0,
                0,
                0,
                0,
                0);
        return nowWithOnlyHour;
}

Poco::DateTime DateTimeUtil::getDateWithSecondPercision(int secondsPlusNow) {
        Poco::DateTime now;
        Poco::Timespan diff (0, 0, 0, secondsPlusNow, 0);
        Poco::DateTime nowWithOnlyHour (
                now.year(),
                now.month(),
                now.day(),
                now.hour(),
                now.minute(),
                now.second(),
                0,
                0);
        nowWithOnlyHour += diff;
        return nowWithOnlyHour;

}

Poco::DateTime DateTimeUtil::getDateWithMinutePercision(int minutesPlusNow) {
        Poco::DateTime now;

        Poco::Timespan diff (0, 0, minutesPlusNow, 0, 0);
        Poco::DateTime nowWithOnlyHour (
                now.year(),
                now.month(),
                now.day(),
                now.hour(),
                now.minute(),
                0,
                0,
                0);
        nowWithOnlyHour += diff;
        return nowWithOnlyHour;

}

Poco::DateTime DateTimeUtil::getDateWithHourPercision(int hoursPlusNow) {
        Poco::Timespan diff (0, hoursPlusNow, 0, 0, 0);

        Poco::DateTime now;
        Poco::DateTime nowWithOnlyHour (
                now.year(),
                now.month(),
                now.day(),
                now.hour(),
                0,
                0,
                0,
                0);
        nowWithOnlyHour += diff;
        return nowWithOnlyHour;

}

Poco::DateTime DateTimeUtil::getDateWithDayPercision(int daysPlusNow) {

        Poco::DateTime now;
        Poco::Timespan diff (daysPlusNow,0, 0, 0, 0);


        Poco::DateTime nowWithOnlyHour (
                now.year(),
                now.month(),
                now.day(),
                0,
                0,
                0,
                0,
                0);
        nowWithOnlyHour += diff;
        return nowWithOnlyHour;

}


Poco::DateTime DateTimeUtil::getCurrentDateWithSecondPercision() {
        Poco::DateTime now;
        Poco::DateTime nowWithOnlyHour (
                now.year(),
                now.month(),
                now.day(),
                now.hour(),
                now.minute(),
                now.second(),
                0,
                0);
        return nowWithOnlyHour;

}

Poco::DateTime DateTimeUtil::getCurrentDateWithMinutePercision() {
        Poco::DateTime now;
        Poco::DateTime nowWithOnlyHour (
                now.year(),
                now.month(),
                now.day(),
                now.hour(),
                now.minute(),
                0,
                0,
                0);
        return nowWithOnlyHour;

}


Poco::DateTime DateTimeUtil::getCurrentDateWithHourPercision() {
        Poco::DateTime now;
        Poco::DateTime nowWithOnlyHour (
                now.year(),
                now.month(),
                now.day(),
                now.hour(),
                0,
                0,
                0,
                0);
        return nowWithOnlyHour;
}

std::vector<Poco::DateTime> DateTimeUtil::getHoursBetweenDates(
        Poco::DateTime startDate,
        Poco::DateTime endDate
        ) {
        std::vector<Poco::DateTime> allHours;
        Poco::DateTime startDateWithoutMinutesAndSeconds (
                startDate.year(),
                startDate.month(),
                startDate.day(),
                startDate.hour(),
                0,
                0,
                0,
                0);


        Poco::DateTime endDateWithoutMinutesAndSeconds (
                endDate.year(),
                endDate.month(),
                endDate.day(),
                endDate.hour() + 1,
                0,
                0,
                0,
                0);

        Poco::DateTime newHour = startDateWithoutMinutesAndSeconds;

        do {
                Poco::Timespan oneHour (1 * Poco::Timespan::HOURS);
                newHour += oneHour;
                allHours.push_back(newHour);
        } while(newHour <= endDateWithoutMinutesAndSeconds);

        return allHours;
}


Poco::DateTime DateTimeUtil::parseDateTime(const std::string& str) {
        assertAndThrow(!str.empty());
        int timeZoneDifferential = 0;
        Poco::DateTime datetime = Poco::DateTimeParser::parse("%Y-%m-%d %H:%M:%S", str, timeZoneDifferential);
        return datetime;
}

Poco::DateTime DateTimeUtil::parseDateTime(const std::string& str, const std::string& format) {
        assertAndThrow(!str.empty());
        assertAndThrow(!format.empty());
        int timeZoneDifferential = 0;
        return Poco::DateTimeParser::parse(format, str, timeZoneDifferential);
}

Poco::DateTime DateTimeUtil::parseDateTime(
        const std::string & fmt,
        const std::string & str,
        int & timeZoneDifferential
        ) {
        assertAndThrow(!str.empty());
        assertAndThrow(!fmt.empty());
        return Poco::DateTimeParser::parse(fmt, str, timeZoneDifferential);
}

std::string DateTimeUtil::dateTimeToStr(Poco::DateTime datetime) {
        return DateTimeUtil::getNowInStringFormat (datetime.timestamp (), "%Y-%m-%d %H:%M:%S");
}

std::string DateTimeUtil::dateTimeToStr(Poco::DateTime datetime, const std::string& format) {
        return DateTimeUtil::getNowInStringFormat (datetime.timestamp (), format);

}

std::string DateTimeUtil::getMinuteNowAsString() {
        Poco::DateTime now;
        return DateTimeUtil::getNowInStringFormat (now.timestamp (), "%S");
}

std::string DateTimeUtil::getSecondNowAsString() {
        Poco::DateTime now;
        return DateTimeUtil::getNowInStringFormat (now.timestamp (), "%M");
}


std::string DateTimeUtil::getCurrentDateWithHourPercisionAsString() {
        Poco::DateTime now;
        return DateTimeUtil::getNowInStringFormat (now.timestamp (), "%H");
}

std::string DateTimeUtil::getDayInMonthNowAsString() {
        Poco::DateTime now;
        return DateTimeUtil::getNowInStringFormat (now.timestamp (), "%d");
}



int DateTimeUtil::getClosest5thMinuteNowAsIntegerInUTC() {
        int minute = 0;
        Poco::DateTime now;
        int nowMinute = now.minute ();
        if (nowMinute / 5 == 0) { minute = nowMinute; }
        else if (nowMinute > 5) { minute = nowMinute + (nowMinute % 5); }
        else if (nowMinute < 5) { minute = 5; }
        return minute;
}


std::string DateTimeUtil::getNowPlusSecondsInMySqlFormat(int seconds) {
        Poco::DateTime now;
        Poco::Timespan span (seconds * Poco::Timespan::SECONDS);
        Poco::DateTime dt = now + span;
        return DateTimeUtil::getNowInStringFormat (dt.timestamp (), "%Y-%m-%d %H:%M:%S");
}

std::string DateTimeUtil::getNowPlusHoursInMySqlFormat(int hours) {
        Poco::DateTime now;
        Poco::Timespan span (hours * Poco::Timespan::HOURS);
        Poco::DateTime dt = now + span;
        return DateTimeUtil::getNowInStringFormat (dt.timestamp (), "%Y-%m-%d %H:%M:%S");
}

std::string DateTimeUtil::getNowPlusDaysInMySqlFormat(int days) {
        Poco::DateTime now;
        Poco::Timespan span (days * Poco::Timespan::DAYS);
        Poco::DateTime dt = now + span;
        return DateTimeUtil::getNowInStringFormat (dt.timestamp (), "%Y-%m-%d %H:%M:%S");
}

std::string DateTimeUtil::getNowInMySqlFormat() {
        Timestamp now;
        return DateTimeUtil::getNowInStringFormat (now, "%Y-%m-%d %H:%M:%S");
}


void DateTimeUtil::initTimeZoneObject() {

}

int DateTimeUtil::getUtcOffsetBasedOnTimeZone(std::string timeZoneName) {
        /*
         * this is very slow method, don't call it in a loop, improve it later
         */
        //traceme(timeZoneName);

        boost::local_time::time_zone_ptr tzp = tz.time_zone_from_region (
                timeZoneName.c_str ());
        if (tzp != NULL) {
                boost::posix_time::time_duration timeOffset =
                        tzp->base_utc_offset ();
//			MLOG(StringUtil::toStr(tzp->base_utc_offset()));
                //  MLOG(3)<<" timeZone  : " <<StringUtil::toStr(timeOffset.hours());
                return timeOffset.hours ();
        }
        throw std::logic_error("failed to find timeZone for "+ timeZoneName);
}

TimeType DateTimeUtil::StringToMicro(std::string str) {
        return ConverterUtil::convertTo<TimeType> (str);
}

TimeType DateTimeUtil::MicroToSecond(TimeType micro) {
        return micro / 1000000;
}

TimeType DateTimeUtil::getNowInMicroSecond() { // returns the epoch time in milliseconds
        Time time_t_epoch (boost::gregorian::date (1970, 1, 1));
        Time now = microsec_clock::universal_time ();
        time_duration diff = now - time_t_epoch;
        TimeType x = diff.total_microseconds ();
        return x;

}

TimeType DateTimeUtil::getNowInSecond() { // returns the epoch time in seconds
        Time time_t_epoch (boost::gregorian::date (1970, 1, 1));
        Time now = microsec_clock::universal_time ();
        time_duration diff = now - time_t_epoch;
        TimeType x = diff.total_seconds ();
        return x;
}

TimeType DateTimeUtil::getDaysInSeconds(int days) {
        return days * 24 * 3600;
}

TimeType DateTimeUtil::getNowInMilliSecond() { // returns the epoch time in milliseconds
        Time time_t_epoch (boost::gregorian::date (1970, 1, 1));
        Time now = microsec_clock::universal_time ();
        time_duration diff = now - time_t_epoch;
        TimeType x = diff.total_milliseconds ();

        return x;

}

TimeType DateTimeUtil::getNowHourInMilliSecond() { // returns current hour since epoch time in milliseconds
        Time time_t_epoch (boost::gregorian::date (1970, 1, 1));
        Time now = microsec_clock::universal_time ();
        time_duration diff = now - time_t_epoch;
        TimeType nowInMillis = diff.total_milliseconds ();
        long numberOfHoursPassedEpoch = (nowInMillis / 3600) / 1000;
        TimeType nowHourInMillis = numberOfHoursPassedEpoch * 3600 * 1000;
        return nowHourInMillis;

}

TimeType DateTimeUtil::getNowDayInMilliSecond() { // returns current day since epoch time in milliseconds
        Time time_t_epoch (boost::gregorian::date (1970, 1, 1));
        Time now = microsec_clock::universal_time ();
        time_duration diff = now - time_t_epoch;
        TimeType nowInMillis = diff.total_milliseconds ();
        long numberOfDaysPassedEpoch = (nowInMillis / (3600 * 24)) / 1000;
        TimeType nowDayInMillis = numberOfDaysPassedEpoch * 3600 * 24 * 1000;
        return nowDayInMillis;

}

TimeType DateTimeUtil::getNowMinuteInMilliSecond() { // returns current minute since epoch time in milliseconds
        Time time_t_epoch (boost::gregorian::date (1970, 1, 1));
        Time now = microsec_clock::universal_time ();
        time_duration diff = now - time_t_epoch;
        TimeType nowInMillis = diff.total_milliseconds ();
        long numberOfMinutesPassedEpoch = (nowInMillis / 60) / 1000;
        TimeType nowMinuteInMillis = numberOfMinutesPassedEpoch * 60 * 1000;
        return nowMinuteInMillis;

}

/*
 * returns current time in this format 2001-08-23
 * which determines the partition key of device history (rowkey)
 *
 */
std::string DateTimeUtil::getNowInYYYMMDDFormat() {
        Poco::DateTime now;
        Poco::DateTime dt = now;
        return DateTimeUtil::getNowInStringFormat (dt.timestamp (), "%Y-%m-%d");
}

Poco::DateTime DateTimeUtil::getCurrentDateWithDayPercision() {
        Poco::DateTime now;
        Poco::DateTime nowWithOnlyDay (
                now.year(),
                now.month(),
                now.day(),
                0,
                0,
                0,
                0);
        return nowWithOnlyDay;
}


/*
 * returns current time in this format 2001-08-23
 * which determines the partition key of device history (rowkey)
 * takes a argument as daysAgo
 */
std::string DateTimeUtil::getNDayAgoInStringFormat(int daysAgo) {
        Poco::DateTime now;
        Poco::Timespan span (daysAgo * Poco::Timespan::DAYS);
        Poco::DateTime dt = now + span;
        return DateTimeUtil::getNowInStringFormat (dt.timestamp (), "%Y-%m-%d");
}

/**
 * this retuns the active hour in UTC
 */
int DateTimeUtil::getCurrentHourAsIntegerInUTC() {
        const boost::posix_time::ptime now =
                boost::posix_time::microsec_clock::universal_time ();

        // Get the time offset in current day
        const boost::posix_time::time_duration td = now.time_of_day ();
        int hours = td.hours ();
        MLOG(2) << "getCurrentHourAsIntegerInUTC : Time now in housr is  " << hours;
        return hours;
}

int DateTimeUtil::getMinuteNowAsIntegerInUTC() {
        const boost::posix_time::ptime now =
                boost::posix_time::microsec_clock::universal_time ();

        // Get the time offset in current day
        const boost::posix_time::time_duration td = now.time_of_day ();
        int hours = td.minutes ();
        MLOG(2) << "getMinuteNowAsIntegerInUTC : Time now in minutes is  " << hours;
        return hours;
}

int DateTimeUtil::getSecondNowAsIntegerInUTC() {
        const boost::posix_time::ptime now =
                boost::posix_time::microsec_clock::universal_time ();

        // Get the time offset in current day
        const boost::posix_time::time_duration td = now.time_of_day ();
        int hours = td.seconds ();
        MLOG(2) << "getSecondNowAsIntegerInUTC : Time now in seconds is  " << hours;
        return hours;
}

//TODO : change the name of this method to timestampToStr()
std::string DateTimeUtil::getNowInStringFormat(Timestamp now, const std::string &format) {
//yyyy-mm-dd HH:mm:ss  this is one of acceptable formats by cassandra
//std::string s(DateTimeFormatter::format(dt, "%e %b %Y %H:%M"));

        /**
         *
           %w - abbreviated weekday (Mon, Tue, ...)
           %W - full weekday (Monday, Tuesday, ...)
           %b - abbreviated month (Jan, Feb, ...)
           %B - full month (January, February, ...)
           %d - zero-padded day of month (01 .. 31)
           %e - day of month (1 .. 31)
           %f - space-padded day of month ( 1 .. 31)
           %m - zero-padded month (01 .. 12)
           %n - month (1 .. 12)
           %o - space-padded month ( 1 .. 12)
           %y - year without century (70)
           %Y - year with century (1970)
           %H - hour (00 .. 23)
           %h - hour (00 .. 12)
           %a - am/pm
           %A - AM/PM
           %M - minute (00 .. 59)
           %S - second (00 .. 59)
           %S - seconds and microseconds (equivalent to %S.%F)
           %i - millisecond (000 .. 999)
           %c - centisecond (0 .. 9)
           %F - fractional seconds/microseconds (000000 - 999999)
           %z - time zone differential in ISO 8601 format (Z or +NN.NN)
           %Z - time zone differential in RFC format (GMT or +NNNN)
           %% - percent sign
         */

        std::string dt (DateTimeFormatter::format (now, format));

        if (dt.compare (format) == 0) {
                throwEx("the date format didn't work, format is " + format);
        }
        return dt;
}


// TimeType getNowInSecondsUTCTimeBasedOnNYTimeZone() {
//	 // current date/time based on current system
//	   time_t now = time(0);
//
//	   // convert now to string form
//	   char* dt = ctime(&now);
//
//	   cout << "The local date and time is: " << dt << endl;
//
//	   // convert now to tm struct for UTC
//	   tm *gmtm = gmtime(&now);
//	   dt = asctime(gmtm);
//	   cout << "The UTC date and time is:"<< dt << endl;
//}

//void  getNowInSecondsInUTC() {
//	   time_t now = time(0);
//	   cout << "Number of sec since January 1,1970 in UTC:" << now << endl;
//	   tm *ltm = gmtime_r(&now);
//
//	   // print various components of tm structure.
//	   cout << "Year: "<< 1900 + ltm->tm_year << endl;
//	   cout << "Month: "<< 1 + ltm->tm_mon<< endl;
//	   cout << "Day: "<<  ltm->tm_mday << endl;
//	   cout << "Time: "<< 1 + ltm->tm_hour << ":";
//	   cout << 1 + ltm->tm_min << ":";
//	   cout << 1 + ltm->tm_sec << endl;
//}

//int main(int argc, char **argv) {
//
//  boost::local_time::tz_database tz;
//  tz.load_from_file("/tmp/date_time_zonespec.csv");
//
//  boost::local_time::time_zone_ptr tzp =
//            tz.time_zone_from_region("America/Los_Angeles");
//
//  int year = 2012;
//  boost::posix_time::ptime t1 = tzp->dst_local_start_time(year);
//  boost::posix_time::ptime t2 = tzp->dst_local_end_time(year);
//  std::cout << "DST ran from " << t1 << " to " << t2 << std::endl;
//  std::cout << "DST shortcut " << tzp->dst_zone_abbrev() << std::endl;
//  std::cout << "DST name     " << tzp->dst_zone_name() << std::endl;
//
//}

TimeType DateTimeUtil::getThisHourSinceEpoch() {
        time_t now;

        now = time (NULL);
        //printf("Its %ld seconds since January 1, 1970 00:00:00", (TimeType) now);
        return (TimeType) (now / 3600); //this means n hour has elapsed from epoch

}

TimeType DateTimeUtil::getNowInseconds() {
        time_t now;

        now = time (NULL);
        //printf("Its %ld seconds since January 1, 1970 00:00:00", (TimeType) now);
        return (TimeType) now;

}

TimeType DateTimeUtil::getNowInNanoSecond() { // returns the epoch time in milliseconds
        Time time_t_epoch (boost::gregorian::date (1970, 1, 1));
        Time now = microsec_clock::universal_time ();
        time_duration diff = now - time_t_epoch;
        TimeType x = diff.total_nanoseconds ();

        return x;

}

TimeType DateTimeUtil::getNowInUniversalFormat() { // returns the epoch time in milliseconds
        Time time_t_epoch (boost::gregorian::date (1970, 1, 1));
        Time now = microsec_clock::universal_time ();
        time_duration diff = now - time_t_epoch;
        TimeType x = diff.total_milliseconds ();

        return x;
}

//-----------------------------------------------------------------------------
// Format current time (calculated as an offset in current day) in this form:
//
//     "hh:mm:ss.SSS" (where "SSS" are milliseconds)
//-----------------------------------------------------------------------------
std::string DateTimeUtil::getTimeNowAsString() {
        // Get current time from the clock, using microseconds resolution
        const boost::posix_time::ptime now =
                boost::posix_time::microsec_clock::universal_time ();

        // Get the time offset in current day
        const boost::posix_time::time_duration td = now.time_of_day ();

        //
        // Extract hours, minutes, seconds and milliseconds.
        //
        // Since there is no direct accessor ".milliseconds()",
        // milliseconds are computed _by difference_ between total milliseconds
        // (for which there is an accessor), and the hours/minutes/seconds
        // values previously fetched.
        //

        const long hours = td.hours ();
        const long minutes = td.minutes ();
        const long seconds = td.seconds ();
        const long milliseconds = td.total_milliseconds ()
                                  - ((hours * 3600 + minutes * 60 + seconds) * 1000);

        //
        // Format like this:
        //
        //      hh:mm:ss.SSS
        //
        // e.g. 02:15:40:321
        //
        //      ^          ^
        //      |          |
        //      123456789*12
        //      ---------10-     --> 12 chars + \0 --> 13 chars should suffice
        //
        //
        char buf[40];
        sprintf (buf, "%02ld:%02ld:%02ld.%03ld", hours, minutes, seconds,
                 milliseconds);

        return buf;
}



std::string DateTimeUtil::getWeekdayBasedOnUTC()
{
        namespace date = boost::gregorian;
//    auto today = date::day_clock::local_day();
        auto today = date::day_clock::universal_day();
        auto weekday = today.day_of_week();
//    return weekday.as_short_string();
        return weekday.as_long_string();
}

int DateTimeUtil::getWeekDayNumberBasedOnUTC() {
        std::string longDayName = getWeekdayBasedOnUTC();
        if(StringUtil::equalsIgnoreCase ("saturday", longDayName)) {
                return 0;
        } else if(StringUtil::equalsIgnoreCase ("sunday", longDayName)) {
                return 1;
        } else if(StringUtil::equalsIgnoreCase ("monday", longDayName)) {
                return 2;
        } else if(StringUtil::equalsIgnoreCase ("tuesday", longDayName)) {
                return 3;
        } else if(StringUtil::equalsIgnoreCase ("wednesday", longDayName)) {
                return 4;
        } else if(StringUtil::equalsIgnoreCase ("thursday", longDayName)) {
                return 5;
        } else if(StringUtil::equalsIgnoreCase ("friday", longDayName)) {
                return 6;
        }
        throwEx ("wrong day : " + longDayName);
}

int DateTimeUtil::getHourNumberInWeekBasedOnUserOffset(int userOffset) {
        int weekNumber = getWeekDayNumberBasedOnUserOffset(userOffset);
        if (weekNumber == 7 ) {
                return ((weekNumber - 1 ) * 24 ) + DateTimeUtil::getCurrentHourAsIntegerInUTC ();
        } else {
                return ((weekNumber) * 24 ) + DateTimeUtil::getCurrentHourAsIntegerInUTC ();
        }
}

int DateTimeUtil::getWeekDayNumberBasedOnUserOffset(int userOffset) {
        int userLocalTime = userOffset + DateTimeUtil::getCurrentHourAsIntegerInUTC ();
        int userDayOffset = 0;
        if (userLocalTime >= 24) {
                userDayOffset = 1;
        }
        std::string longDayName = getWeekdayBasedOnUTC();
        if(StringUtil::equalsIgnoreCase ("saturday", longDayName)) {
                return userDayOffset + 0;
        } else if(StringUtil::equalsIgnoreCase ("saturday", longDayName)) {
                return userDayOffset + 1;
        } else if(StringUtil::equalsIgnoreCase ("sunday", longDayName)) {
                return userDayOffset + 2;
        } else if(StringUtil::equalsIgnoreCase ("monday", longDayName)) {
                return userDayOffset + 3;
        } else if(StringUtil::equalsIgnoreCase ("tuesday", longDayName)) {
                return userDayOffset + 4;
        } else if(StringUtil::equalsIgnoreCase ("wednesday", longDayName)) {
                return userDayOffset + 5;
        } else if(StringUtil::equalsIgnoreCase ("thursday", longDayName)) {
                return userDayOffset + 6;
        } else if(StringUtil::equalsIgnoreCase ("friday", longDayName)) {
                if (userDayOffset == 1) {
                        return 0;
                }
                return userDayOffset + 7;
        }
        throwEx ("wrong day : " + longDayName);
}


//DATE
std::string DateTimeUtil::getCurrentDateAsString() {

        /*
           using namespace boost::posix_time;
           using namespace boost::gregorian;

           //get the current time from the clock -- one second resolution
           ptime now = second_clock::universal_time();
           //Get the date part out of the time
           date today = now.date();
         */

        boost::posix_time::ptime now =
                boost::posix_time::second_clock::universal_time ();

        std::string nowAsString (boost::posix_time::to_simple_string (now).c_str ());
        return nowAsString;
}

/*
 *  Demonstrate conversions between a local time and utc
 * Output:
 *
 * UTC <--> New York while DST is NOT active (5 hours)
 * 2001-Dec-31 19:00:00 in New York is 2002-Jan-01 00:00:00 UTC time
 * 2002-Jan-01 00:00:00 UTC is 2001-Dec-31 19:00:00 New York time
 *
 * UTC <--> New York while DST is active (4 hours)
 * 2002-May-31 20:00:00 in New York is 2002-Jun-01 00:00:00 UTC time
 * 2002-Jun-01 00:00:00 UTC is 2002-May-31 20:00:00 New York time
 *
 * UTC <--> Arizona (7 hours)
 * 2002-May-31 17:00:00 in Arizona is 2002-Jun-01 00:00:00 UTC time

 ******************************#include "boost/date_time/posix_time/posix_time.hpp"
 ******************************#include "boost/date_time/local_time_adjustor.hpp"
 ******************************#include "boost/date_time/c_local_time_adjustor.hpp"
 ******************************#include <iostream>

   int
   main()
   {
   using namespace boost::posix_time;
   using namespace boost::gregorian;

   //This local adjustor depends on the machine TZ settings-- highly dangerous!
   typedef boost::date_time::c_local_adjustor<ptime> local_adj;
   ptime t10(date(2002,Jan,1), hours(7));
   ptime t11 = local_adj::utc_to_local(t10);
   std::cout << "UTC <--> Zone base on TZ setting" << std::endl;
   std::cout << to_simple_string(t11) << " in your TZ is "
   << to_simple_string(t10) << " UTC time "
   << std::endl;
   time_duration td = t11 - t10;
   std::cout << "A difference of: "
   << to_simple_string(td) << std::endl;


   //eastern timezone is utc-5
   typedef boost::date_time::local_adjustor<ptime, -5, us_dst> us_eastern;

   ptime t1(date(2001,Dec,31), hours(19)); //5 hours b/f midnight NY time

   std::cout << "\nUTC <--> New York while DST is NOT active (5 hours)"
   << std::endl;
   ptime t2 =  us_eastern::local_to_utc(t1);
   std::cout << to_simple_string(t1) << " in New York is "
   << to_simple_string(t2) << " UTC time "
   << std::endl;

   ptime t3 = us_eastern::utc_to_local(t2);//back should be the same
   std::cout << to_simple_string(t2) << " UTC is "
   << to_simple_string(t3) << " New York time "
   << "\n\n";

   ptime t4(date(2002,May,31), hours(20)); //4 hours b/f midnight NY time
   std::cout << "UTC <--> New York while DST is active (4 hours)" << std::endl;
   ptime t5 = us_eastern::local_to_utc(t4);
   std::cout << to_simple_string(t4) << " in New York is "
   << to_simple_string(t5) << " UTC time "
   << std::endl;

   ptime t6 = us_eastern::utc_to_local(t5);//back should be the same
   std::cout << to_simple_string(t5) << " UTC is "
   << to_simple_string(t6) << " New York time "
   << "\n" << std::endl;


   //Arizona timezone is utc-7 with no dst
   typedef boost::date_time::local_adjustor<ptime, -7, no_dst> us_arizona;

   ptime t7(date(2002,May,31), hours(17));
   std::cout << "UTC <--> Arizona (7 hours)" << std::endl;
   ptime t8 = us_arizona::local_to_utc(t7);
   std::cout << to_simple_string(t7) << " in Arizona is "
   << to_simple_string(t8) << " UTC time "
   << std::endl;

   return 0;
   }
 * /
 */
