/*
 * DoNothingMessageProcessor.h
 *
 *  Created on: Aug 1, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_KAFKA_DONOTHINGMESSAGEPROCESSOR_H_
#define GICAPODS_GICAPODSSERVER_SRC_KAFKA_DONOTHINGMESSAGEPROCESSOR_H_

#include <string>
#include <memory>
#include <vector>
#include "MessageProcessor.h"

class  DoNothingMessageProcessor;


class DoNothingMessageProcessor : public MessageProcessor {

public:

     void process(std::string msg);
	   bool isReadyToProcess();
     virtual ~DoNothingMessageProcessor() ;
     static std::shared_ptr<DoNothingMessageProcessor> getInstance();
};





#endif /* GICAPODS_GICAPODSSERVER_SRC_KAFKA_DONOTHINGMESSAGEPROCESSOR_H_ */
