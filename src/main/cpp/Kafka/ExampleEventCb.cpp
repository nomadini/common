#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <csignal>
#include <cstring>

#include <getopt.h>

/*
 * Typically include path in a real application would be
 * #include <librdkafka/rdkafkacpp.h>
 */
#include "ExampleEventCb.h"
#include <librdkafka/rdkafkacpp.h>

	void ExampleEventCb::event_cb(RdKafka::Event &event) {
		switch (event.type()) {
		case RdKafka::Event::EVENT_ERROR:
			std::cerr << "ERROR (" << RdKafka::err2str(event.err()) << "): "
					<< event.str() << std::endl;
			if (event.err() == RdKafka::ERR__ALL_BROKERS_DOWN) {
				//stop sending messages
				//run = false;

			}
			break;

		case RdKafka::Event::EVENT_STATS:
			std::cerr << "\"STATS\": " << event.str() << std::endl;
			break;

		case RdKafka::Event::EVENT_LOG:
			fprintf(stderr, "LOG-%i-%s: %s\n", event.severity(),
					event.fac().c_str(), event.str().c_str());
			break;

		default:
			std::cerr << "EVENT " << event.type() << " ("
					<< RdKafka::err2str(event.err()) << "): " << event.str()
					<< std::endl;
			break;
		}
	}