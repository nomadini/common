/*
 * ExampleDeliveryReportCb.h
 *
 *  Created on: Jul 31, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_KAFKA_EXAMPLEDELIVERYREPORTCB_H_
#define GICAPODS_GICAPODSSERVER_SRC_KAFKA_EXAMPLEDELIVERYREPORTCB_H_

#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <csignal>
#include <cstring>

#include <getopt.h>
#include <string>
#include <memory>
#include <vector>
/*
 * Typically include path in a real application would be
 * #include <librdkafka/rdkafkacpp.h>
 */
#include <librdkafka/rdkafkacpp.h>
class ExampleDeliveryReportCb: public RdKafka::DeliveryReportCb {
public:
	void dr_cb(RdKafka::Message &message);
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_KAFKA_EXAMPLEDELIVERYREPORTCB_H_ */
