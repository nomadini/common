
#ifndef KafkaSubscriber_H
#define KafkaSubscriber_H


#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <csignal>
#include <cstring>

#include <getopt.h>
#include <string>
#include <memory>
#include <vector>
#include <unordered_map>
#include <librdkafka/rdkafkacpp.h>
#include "MyHashPartitionerCb.h"
#include "AtomicLong.h"
class EntityToModuleStateStats;
#include "MessageProcessor.h"
#include "AtomicLong.h"
#include "AtomicBoolean.h"
#include "ExampleEventCb.h"
#include "PartitionRebalancer.h"

class EntityToModuleStateStats;
class KafkaSubscriber;

//http://docs.confluent.io/2.0.0/clients/librdkafka/classRdKafka_1_1KafkaConsumer.html#details
//this KafkaSubscriber is based on high level api which is for kafka0.9 and above
class KafkaSubscriber {

public:
std::string groupId;
std::shared_ptr<PartitionRebalancer> ex_rebalance_cb;
std::shared_ptr<ExampleEventCb> ex_event_cb;
KafkaSubscriber(const char * host,
                std::string groupId,
                const std::vector<std::string> & vecTopics,
                bool startFromBeginningOfAllPartitions,
                std::shared_ptr<MessageProcessor> messageProcessor);



virtual ~KafkaSubscriber();

void connect();
void consumeMSG();
void stop();

void setConfiguration();
void setPartition();
void setConsumer();
void setTopics();
void msg_consume(RdKafka::Message * message, void * arg);
static void checkResultOfConfig(int resultOfConfig, std::string errstr);


std::shared_ptr<gicapods::AtomicLong> lastOffsetRead;
std::shared_ptr<gicapods::AtomicLong> consumerTimeout;
std::shared_ptr<gicapods::AtomicLong> consumingMsgFailed;
std::shared_ptr<gicapods::AtomicLong> consumerReadMessage;
std::shared_ptr<gicapods::AtomicLong> endOfPartitionReached;
std::shared_ptr<gicapods::AtomicBoolean> scorerHealthFlag;

std::shared_ptr<MessageProcessor> messageProcessor;
EntityToModuleStateStats* entityToModuleStateStats;
void invokeTheProcessor(RdKafka::Message *msg);
void tryToFetchOneMessage();
std::string m_brokers;
std::vector<std::string> m_vecTopics;

RdKafka::Conf * m_pConf;
RdKafka::Conf * m_pTconf;

RdKafka::Topic * m_pTopic;
RdKafka::KafkaConsumer * m_pConsumer;

bool m_isRun;

static void dumpConfigurations(RdKafka::Conf *config, std::string name);
};

#endif
