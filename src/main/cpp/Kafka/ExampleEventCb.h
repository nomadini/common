#ifndef ExampleEventCb_H
#define ExampleEventCb_H

#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <csignal>
#include <cstring>
#include <string>
#include <memory>
#include <vector>
#include <getopt.h>

/*
 * Typically include path in a real application would be
 * #include <librdkafka/rdkafkacpp.h>
 */
#include <librdkafka/rdkafkacpp.h>
class ExampleEventCb: public RdKafka::EventCb {
public:
	void event_cb(RdKafka::Event &event);
};
#endif
