/*
 * MessageProcessor.h
 *
 *  Created on: Aug 1, 2015
 *      Author: mtaabodi
 */

#ifndef MessageProcessor_H
#define MessageProcessor_H


#include <string>
#include <memory>
#include <vector>
class MessageProcessor {

public:
/**
 * this is how you define an interface properly
 */

virtual ~MessageProcessor();
virtual void process(std::string msg) = 0;
virtual bool isReadyToProcess() = 0;
};




#endif /* GICAPODS_GICAPODSSERVER_SRC_KAFKA_MESSAGEPROCESSOR_H_ */
