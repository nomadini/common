#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <csignal>
#include <cstring>

#include <getopt.h>

#include "GUtil.h"

/*
 * Typically include path in a real application would be
 * #include <librdkafka/rdkafkacpp.h>
 */
#include "ExampleDeliveryReportCb.h"

void ExampleDeliveryReportCb::dr_cb(RdKafka::Message &message) {
								if (message.errstr().compare("Success") != 0) {
																MLOG(3)<<"Message error : "<<message.errstr();
								}
}
