
#include "PartitionRebalancer.h"
#include "GUtil.h"


void PartitionRebalancer::part_list_print (const std::vector<RdKafka::TopicPartition*>&partitions){
								for (unsigned int i = 0; i < partitions.size(); i++) {
																LOG(ERROR) << partitions[i]->topic() <<
																								"[" << partitions[i]->partition() << "], ";
								}
								LOG(ERROR) << "\n";
}


PartitionRebalancer::PartitionRebalancer (bool readFromStartOfAllPartitions) {
								this->readFromStartOfAllPartitions = readFromStartOfAllPartitions;
}

void PartitionRebalancer::rebalance_cb (
								RdKafka::KafkaConsumer *consumer,
								RdKafka::ErrorCode err,
								std::vector<RdKafka::TopicPartition*> &topicPartitions) {
								LOG(ERROR) << "RebalanceCb: " << RdKafka::err2str(err) << ": ";
								part_list_print(topicPartitions);
								if (readFromStartOfAllPartitions) {

																for (auto topicPartition : topicPartitions) {
																								//TODO : create  a topic to start_offset mapping that comes from properties
																								// if (mapOfTopicToOffsetToRead.find(topicPartition->topic())) {
																								//  LOG(INFO)<<"setting offset for topic "<< topicPartition->topic();
																								//
																								// }

																								// //set the offset to beginning
																								topicPartition->set_offset(0);
																}
								}
								if (err == RdKafka::ERR__ASSIGN_PARTITIONS) {
																consumer->assign(topicPartitions);
								} else if (err == RdKafka::ERR__REVOKE_PARTITIONS) {
																// Application may commit offsets manually here
																// if auto.commit.enable=false
																consumer->commitSync();
																consumer->unassign();
								} else {
																std::cerr << "Rebalancing error :" << RdKafka::err2str(err) << std::endl;
								}

}
