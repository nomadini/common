



#include "GUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include <boost/exception/all.hpp>
#include "ValueTraceTask.h"
#include "CassandraServiceTypeTraits.h"
#include "CassandraReadService.h"
#include "Feature.h"

#include "EntityToModuleStateStats.h"
#include "DeviceSegmentPair.h"

template <class V>
ValueTraceTask<V>::ValueTraceTask(
        gicapods::ConfigService* configService,
        EntityToModuleStateStats* entityToModuleStateStats)
        : NomadiniTask(entityToModuleStateStats),Object(__FILE__) {
        this->entityToModuleStateStats = entityToModuleStateStats;

        CassTypeTraits = std::make_unique<CassandraServiceTypeTraits<V> >(configService);
        delayToRunTaskInMillis = 5000;//by default
}

template <class V>
void ValueTraceTask<V>::runTask() {
        try {


                NULL_CHECK(value);
                std::shared_ptr<V> readValue = cassandraReadService->readDataOptionalFromDb(value);
                if (readValue != nullptr &&
                    CassTypeTraits->equals(value.get(), readValue.get()) == NOT_EQUAL_AT_ALL) {
                        LOG_EVERY_N(ERROR, 1000) << "value is not written properly for typeName : "
                                                 << CassTypeTraits->getValueType();

                        entityToModuleStateStats->addStateModuleForEntity(
                                "TRACE_FAILED-" + CassTypeTraits->getValueType(),
                                "ValueTraceTask",
                                "ALL",
                                EntityToModuleStateStats::warning
                                );

                } else {
                        entityToModuleStateStats->addStateModuleForEntity(
                                "TRACE_SUCCESS_HAPPENED-" + CassTypeTraits->getValueType(),
                                "ValueTraceTask",
                                "ALL",
                                EntityToModuleStateStats::important
                                );

                }
        } catch (std::exception const &e) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "TRACE_KNOWN_EXCEPTION-" + CassTypeTraits->getValueType(),
                        "ValueTraceTask",
                        "ALL",
                        EntityToModuleStateStats::exception
                        );

                LOG(ERROR)<<"error happening when handling request  "<<boost::diagnostic_information(e);
        } catch (...) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "TRACE_UNKNOWN_EXCEPTION-" + CassTypeTraits->getValueType(),
                        "ValueTraceTask",
                        "ALL",
                        EntityToModuleStateStats::exception
                        );
                LOG(ERROR)<<"unknown error happening when handling request";
        }

}

template <class V>
ValueTraceTask<V>::~ValueTraceTask() {

}

template <class V>
bool ValueTraceTask<V>::isReadyToBeSubmitted() {
        long now = DateTimeUtil::getNowInMilliSecond();
        if ((now - timeCreatedInMillis) >= delayToRunTaskInMillis) {
                return true;
        } else {
                return false;
        }



}
#include "FeatureDeviceHistory.h"
#include "DeviceFeatureHistory.h"
#include "IpToDeviceIdsMap.h"
#include "DeviceIdToIpsMap.h"
#include "PixelDeviceHistory.h"
#include "GicapodsIdToExchangeIdsMap.h"
#include "DeviceSegmentHistory.h"
#include "AdHistory.h"
#include "EventLog.h"
#include "BidEventLog.h"
#include "Wiretap.h"
#include "FeatureToFeatureHistory.h"
#include "ModelScore.h"

#include "SegmentDevices.h"
template class ValueTraceTask<SegmentDevices>;
template class ValueTraceTask<FeatureToFeatureHistory>;
template class ValueTraceTask<EventLog>;
template class ValueTraceTask<ModelScore>;
template class ValueTraceTask<BidEventLog>;
template class ValueTraceTask<GicapodsIdToExchangeIdsMap>;

template class ValueTraceTask<FeatureDeviceHistory>;
template class ValueTraceTask<DeviceFeatureHistory>;
template class ValueTraceTask<IpToDeviceIdsMap>;
template class ValueTraceTask<DeviceIdToIpsMap>;
template class ValueTraceTask<PixelDeviceHistory>;
template class ValueTraceTask<DeviceSegmentHistory>;
template class ValueTraceTask<AdHistory>;
template class ValueTraceTask<Wiretap>;
