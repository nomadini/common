/*
 * ValueBatchUpdateTask.h
 *
 *  Created on: Sep 7, 2015
 *      Author: mtaabodi
 */

#ifndef ValueBatchUpdateTask_H
#define ValueBatchUpdateTask_H




class EntityToModuleStateStats;
class DeviceFeatureHistory;
#include "NomadiniTask.h"

class CassandraManagedType;
template<class V>
class CassandraServiceTypeTraits;

#include "CassandraServiceInterface.h"
#include <vector>
#include <memory>
#include <unordered_map>
#include "Object.h"
class ValueBatchUpdateTask : public NomadiniTask,public Object {
public:

EntityToModuleStateStats* entityToModuleStateStats;

std::shared_ptr<std::unordered_map<std::string, std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > > > > typesToQueueOfWriteBatches;
std::shared_ptr<std::unordered_map<std::string, CassandraServiceInterface*> > mapOfTypesToCassandraServices;

ValueBatchUpdateTask(EntityToModuleStateStats* entityToModuleStateStats);

void writeBatchDataForType(std::string type,
                           std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > >& queueOfWriteBatches);
virtual void runTask();

void pushValueInQueue(std::shared_ptr<CassandraManagedType> value);

virtual ~ValueBatchUpdateTask();
private:

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_SVMSGDWORKER_H_ */
