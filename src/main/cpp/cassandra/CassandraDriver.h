
#ifndef CassandraDriver_h
#define CassandraDriver_h

#include "cassandra.h"

#include "Object.h"
#include "CassandraDriverInterface.h"
namespace gicapods { class ConfigService; }
#include "AtomicLong.h"
class EntityToModuleStateStats;
/*
   uses this cpp driver documentation is here : https://datastax.github.io/cpp-driver/topics/basics/
 */
class CassandraDriver : public CassandraDriverInterface, public Object {

public:

std::string contactPoints;
std::string username;
std::string password;
std::string schema;
int maxConcurrentRequestsToServe;
int numberOfIoWorkerThreads;
int numberOfQueueSizeIO;
int numberOfPendingRequestLowWaterMark;
int numberOfPendingRequestHighWaterMark;
int numberOfCoreConnectionsPerHost;
int numberOfMaxConnectionsPerHost;
int requestTimeout;
int connectionTimeout;
int connectToClusterRequestTimeoutInMillis;

CassCluster* cluster_;
EntityToModuleStateStats* entityToModuleStateStats;

static void _print_error(const char *func, CassFuture *future,
                         const std::string &queryStr,
                         EntityToModuleStateStats* entityToModuleStateStats);
static void _printErrorWithNoExit(
        const char* func,
        CassFuture* future,
        const std::string & queryStr,
        EntityToModuleStateStats* entityToModuleStateStats);

CassandraDriver(gicapods::ConfigService* configService,
                EntityToModuleStateStats* entityToModuleStateStats,
                std::string contactPoints,
                std::string username,
                std::string password,
                std::string schema);

CassandraDriver(gicapods::ConfigService* configService,
                EntityToModuleStateStats* entityToModuleStateStats);

CassCluster* create_cluster();

void closeSessionAndCluster();
void setRequestTimeout(int timeoutInMillis);
int getRequestTimeout();

CassError connect_session(CassSession *session, const CassCluster *cluster);

int startCassandraCluster();

void deleteAllTestEntries();

CassSession* getSession();

virtual ~CassandraDriver();
void deleteAll(std::string tableName);
};

#endif
