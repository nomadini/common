//things we store in cassandra : BrowserOffer
#ifndef CassandraBatchInserter_h
#define CassandraBatchInserter_h



#include "cassandra.h"


namespace gicapods { class ConfigService; }
#include "AtomicLong.h"
class CassandraDriverInterface;
class EntityToModuleStateStats;
#include <tbb/concurrent_queue.h>
#include "NomadiniTask.h"
template<class V>
class CassandraServiceTypeTraits;

#include "CassandraManagedType.h"
template<class V>
class CassandraTracer;

#include "StatisticsUtil.h"
class AsyncThreadPoolService;
class CassandraServiceQueueManager;

class RandomUtil;
#include "Object.h"

template<class V>
class CassandraBatchInserter : public Object {

public:
std::shared_ptr<StatisticsUtil> tracingDecisionRandomizer;
std::shared_ptr<gicapods::AtomicLong> numberOfReadFailures;
std::shared_ptr<gicapods::AtomicLong> numberOfTotalReads;
std::unique_ptr<CassandraServiceTypeTraits<V> > cassandraServiceTypeTraits;
CassandraTracer<V>* cassandraTracer;
EntityToModuleStateStats* entityToModuleStateStats;
gicapods::ConfigService* configService;
std::unique_ptr<CassandraServiceQueueManager> cassandraServiceQueueManager;

AsyncThreadPoolService* asyncThreadPoolService;

CassandraBatchInserter(CassandraTracer<V>* cassandraTracer,
                       EntityToModuleStateStats* entityToModuleStateStats,

                       AsyncThreadPoolService* asyncThreadPoolService,
                       gicapods::ConfigService* configService);

CassError prepare_insert_into_batch(CassSession* session,
                                    const CassPrepared** prepared,
                                    std::string query);

CassError tryInsertIntoPreparedStatement(CassSession* session,
                                         const CassPrepared** prepared,
                                         std::string query);
std::string getName();

void executeBatchWithRetry(const CassBatch* batch,
                           const CassSession* session);

CassError executeBatch(const CassBatch* batch,
                       const CassSession* session,
                       int tryNumber);
void insert_into_batch_with_prepared(
        const CassSession* session,
        const CassPrepared* prepared,
        std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > >& values);

virtual ~CassandraBatchInserter();
};

#endif
