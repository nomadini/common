//things we store in cassandra : BrowserOffer
#ifndef CassandraServiceQueueManager_h
#define CassandraServiceQueueManager_h



#include "cassandra.h"

#include "AtomicBoolean.h"

namespace gicapods { class ConfigService; }
#include "AtomicLong.h"
class CassandraDriverInterface;
class EntityToModuleStateStats;
#include <tbb/concurrent_queue.h>
#include <unordered_map>
#include "NomadiniTask.h"
#include "CassandraManagedType.h"
#include "CassandraServiceInterface.h"
class AsyncThreadPoolService;

#include "Object.h"
class CassandraServiceQueueManager : public Object {

public:
int dequeueAsyncTasksIntervalInSeconds;
std::shared_ptr<gicapods::AtomicBoolean> stopConsumingThread;
std::shared_ptr<gicapods::AtomicBoolean> dequeueThreadIsRunning;
//number of Jobs that are pushed into one task
int numberOfJobsInOneTask;
CassandraDriverInterface* cassandraDriver;

EntityToModuleStateStats* entityToModuleStateStats;

std::shared_ptr<std::unordered_map<std::string, CassandraServiceInterface*> > mapOfTypesToCassandraServices;


std::shared_ptr<tbb::concurrent_queue<std::shared_ptr<CassandraManagedType> > > queueOfWriteBatches;

std::shared_ptr<tbb::concurrent_queue<std::shared_ptr<CassandraManagedType> > > queueOfTraceValues;


AsyncThreadPoolService* asyncThreadPoolService;

void submitTaskToAsyncService();

CassandraServiceQueueManager(CassandraDriverInterface* cassandraDriver,
                             EntityToModuleStateStats* entityToModuleStateStats,

                             AsyncThreadPoolService* asyncThreadPoolService,
                             int dequeueAsyncTasksIntervalInSeconds);

void dequeueTheAsyncTasks();

void pushToWriteBatchQueue(std::shared_ptr<CassandraManagedType> value);

virtual ~CassandraServiceQueueManager();
};

#endif
