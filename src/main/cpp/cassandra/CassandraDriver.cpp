
#include "GUtil.h"

#include "SignalHandler.h"
#include "cassandra.h"
#include "DateTimeUtil.h"

#include "CollectionUtil.h"
#include "StringUtil.h"
#include "CassandraDriverInterface.h"
#include "DateTimeUtil.h"
#include "AtomicLong.h"
#include "Poco/Runnable.h"
#include "ConfigService.h"
#include "ConverterUtil.h"
#include "CassandraDriver.h"
#include "EntityToModuleStateStats.h"


#define NUMBER_OF_SESSIONS 10
#define NUMBER_OF_CLUSTERS 1
#define NUM_IO_WORKER_THREADS 8

CassandraDriver::CassandraDriver(
								gicapods::ConfigService* configService,
								EntityToModuleStateStats* entityToModuleStateStats,
								std::string contactPoints,
								std::string username,
								std::string password,
								std::string schema) :
								CassandraDriver(
																configService,
																entityToModuleStateStats){

								this->contactPoints = contactPoints;
								this->username = username;
								this->password = password;
								this->schema = schema;

								maxConcurrentRequestsToServe = 100;

}

CassandraDriver::CassandraDriver(
								gicapods::ConfigService* configService,
								EntityToModuleStateStats* entityToModuleStateStats) : Object(__FILE__){
								maxConcurrentRequestsToServe = 100;
								this->entityToModuleStateStats = entityToModuleStateStats;
								if (this->contactPoints.empty()) {
																this->contactPoints = configService->get("cassandraContactPoints");
								}

								if (this->username.empty()) {
																this->username = configService->get("cassandraUsername");
								}
								if (this->password.empty()) {
																this->password = configService->get("cassandraPassword");
								}
								if (this->schema.empty()) {
																this->schema = configService->get("cassandraSchema");
								}

								this->numberOfIoWorkerThreads = ConverterUtil::convertTo<int>(configService->get("cassandraNumberOfIoWorkerThreads"));

								this->numberOfQueueSizeIO = ConverterUtil::convertTo<int>(configService->get("cassandraNumberOfQueueSizeIO"));
								this->numberOfPendingRequestLowWaterMark = ConverterUtil::convertTo<int>(configService->get("cassandraNumberOfPendingRequestLowWaterMark"));
								this->numberOfPendingRequestHighWaterMark = ConverterUtil::convertTo<int>(configService->get("cassandraNumberOfPendingRequestHighWaterMark"));
								this->numberOfCoreConnectionsPerHost = ConverterUtil::convertTo<int>(configService->get("cassandraNumberOfCoreConnectionsPerHost"));
								this->numberOfMaxConnectionsPerHost = ConverterUtil::convertTo<int>(configService->get("cassandraNumberOfMaxConnectionsPerHost"));
								this->requestTimeout = ConverterUtil::convertTo<int>(configService->get("cassandraRequestTimeoutInMillis"));
								this->connectionTimeout = ConverterUtil::convertTo<int>(configService->get("cassandraConnectionTimeoutInMillis"));
								this->connectToClusterRequestTimeoutInMillis = ConverterUtil::convertTo<int>(configService->get("cassandraConnectToClusterRequestTimeoutInMillis"));

								LOG(INFO) << "cassandra driver properties : "<<
																" numberOfIoWorkerThreads: "  << this->numberOfIoWorkerThreads<<
																" numberOfQueueSizeIO: "  << this->numberOfQueueSizeIO<<
																" numberOfPendingRequestLowWaterMark: "  << this->numberOfPendingRequestLowWaterMark<<
																" numberOfPendingRequestHighWaterMark: "  << this->numberOfPendingRequestHighWaterMark<<
																" numberOfCoreConnectionsPerHost: "  << this->numberOfCoreConnectionsPerHost<<
																" numberOfMaxConnectionsPerHost: "  << this->numberOfMaxConnectionsPerHost<<
																" requestTimeout: "  << this->requestTimeout<<
																" connectionTimeout: "  << this->connectionTimeout;

								numberOfCuncurrentReuqestsNow = std::make_shared<gicapods::AtomicLong>();

}

CassandraDriver::~CassandraDriver() {
								try {
																closeSessionAndCluster();
								} catch(...) {
																gicapods::Util::showStackTrace();
								}

}

CassSession* CassandraDriver::getSession() {
								if(session == NULL) {
																startCassandraCluster();
								}
								return session;
}


void CassandraDriver::_print_error(
								const char* func,
								CassFuture* future,
								const std::string & queryStr,
								EntityToModuleStateStats* entityToModuleStateStats) {
								const char* message;
								size_t message_length;
								cass_future_error_message(future, &message, &message_length);

								std::string errorMessage(message, message_length);

								entityToModuleStateStats->addStateModuleForEntity(
																"ERROR_IN_CASSANDRA_QUERY",
																"CassandraService",
																EntityToModuleStateStats::all,
																EntityToModuleStateStats::exception);

								LOG_EVERY_N(ERROR, 100)
																<<"cassandra query threw error : "
																<< errorMessage
																<< " function "<< StringUtil::toStr(func) <<  "  query : "<< queryStr;
								//we rather exit than continue this app
								EXIT("");
}

void CassandraDriver::_printErrorWithNoExit(
								const char* func,
								CassFuture* future,
								const std::string & queryStr,
								EntityToModuleStateStats* entityToModuleStateStats) {
								const char* message;
								size_t message_length;
								cass_future_error_message(future, &message, &message_length);

								std::string errorMessage(message, message_length);

								entityToModuleStateStats->addStateModuleForEntity(
																"ERROR_IN_CASSANDRA_QUERY",
																"CassandraService",
																EntityToModuleStateStats::all,
																EntityToModuleStateStats::exception);

								LOG_EVERY_N(ERROR, 1)
																<<"cassandra query threw error : "
																<< errorMessage
																<< " function "<< StringUtil::toStr(func) <<  "  query : "<< queryStr;

}



CassCluster* CassandraDriver::create_cluster() {

								MLOG(3)<< " creating cassandra cluster";

								LOG(INFO)<< " cassandra cluster contact points are "<<contactPoints<<
																" username and password are  "<<username<< " ,  "<<password;

								CassCluster* cluster = cass_cluster_new();
								cass_cluster_set_contact_points(cluster, contactPoints.c_str());
								cass_cluster_set_credentials(cluster, username.c_str(), password.c_str());

								cass_cluster_set_queue_size_io(cluster, numberOfQueueSizeIO);
								cass_cluster_set_pending_requests_low_water_mark(cluster, numberOfPendingRequestLowWaterMark);
								cass_cluster_set_pending_requests_high_water_mark(cluster, numberOfPendingRequestHighWaterMark);
								cass_cluster_set_core_connections_per_host(cluster, numberOfCoreConnectionsPerHost);
								cass_cluster_set_max_connections_per_host(cluster, numberOfMaxConnectionsPerHost);
								cass_cluster_set_request_timeout(cluster, requestTimeout);
								cass_cluster_set_connect_timeout(cluster, connectionTimeout);

								return cluster;
}

void CassandraDriver::setRequestTimeout(int timeoutInMillis) {
								cass_cluster_set_request_timeout(cluster_, requestTimeout);
}

int CassandraDriver::getRequestTimeout() {
								return this->requestTimeout;
}

void CassandraDriver::closeSessionAndCluster() {
								if (session != NULL) {
																LOG(INFO)<<"closing cassandra session";
																CassFuture*  close_future = cass_session_close(session);
																cass_session_free (session);
																cass_future_wait(close_future);
																cass_future_free(close_future);
																if (cluster_ != NULL) {
																								LOG(INFO)<<"closing cassandra cluster";
																								cass_cluster_free(cluster_);
																}
								}
}

CassError CassandraDriver::connect_session(CassSession* session, const CassCluster* cluster) {
								CassError rc = CASS_OK;
								CassFuture* future = cass_session_connect_keyspace(session, cluster, schema.c_str());

								cass_future_wait(future);
								rc = cass_future_error_code(future);
								cass_future_free(future);
								return rc;
}

int CassandraDriver::startCassandraCluster() {

								CassError rc = CASS_OK;

								LOG(INFO)<< " connecting to cassandra cluster.....";

								cluster_ = create_cluster();

								//increasing timeout for connecting
								//we set this high for the case of using valgrind
								cass_cluster_set_request_timeout(cluster_, connectToClusterRequestTimeoutInMillis);
								session = cass_session_new();
								CassError connectionResult;
								for (int i =0; i < 100; i++) {
																//try 100 times in case of faiure
																connectionResult = connect_session(session, cluster_);
																if (connectionResult == CASS_OK) {
																								break;//connected successfuly
																}
																gicapods::Util::sleepViaBoost(_L_, 1);
								}


								if (connectionResult != CASS_OK ) {
																cass_cluster_free(cluster_);
																cass_session_free(session);
																return -1;
								}

								//changing the timeout back to setting
								cass_cluster_set_request_timeout(cluster_, this->requestTimeout);

								return 0;
}

void CassandraDriver::deleteAllTestEntries() {

}

void CassandraDriver::deleteAll(std::string tableName) {
								if(!StringUtil::containsCaseInSensitive(this->schema, "test")) {
																throwEx("cannot truncate non-test schema: " + this->schema);
								}
								std::string str = "truncate " + tableName;
								const char *query = str.c_str ();
								auto statement = cass_statement_new (query, 0);
								auto future = cass_session_execute (getSession (), statement);
								auto rc = cass_future_error_code(future);
								if (rc != CASS_OK) {
																const char* message;
																size_t message_length;
																cass_future_error_message(future, &message, &message_length);
																LOG_EVERY_N(ERROR, 100)<<google::COUNTER<<"th CasssandraDriver : Error: " <<  StringUtil::toStr(message);
								}
								cass_future_free (future);
								cass_statement_free (statement);
}
