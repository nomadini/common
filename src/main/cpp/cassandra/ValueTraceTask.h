/*
 * ValueTraceTask.h
 *
 *  Created on: Sep 7, 2015
 *      Author: mtaabodi
 */

#ifndef ValueTraceTask_H
#define ValueTraceTask_H




class EntityToModuleStateStats;
class DeviceFeatureHistory;
#include "NomadiniTask.h"
#include "CassandraReadService.h"

#include <vector>
#include <memory>

template<class V>
class CassandraServiceTypeTraits;
#include "Object.h"
#include "ConfigService.h"

template<class V>
class ValueTraceTask : public NomadiniTask, public Object {
public:
std::unique_ptr<CassandraServiceTypeTraits<V> > CassTypeTraits;
EntityToModuleStateStats* entityToModuleStateStats;

long delayToRunTaskInMillis;
std::shared_ptr<V> value;
CassandraReadService<V>* cassandraReadService;

ValueTraceTask(
        gicapods::ConfigService* configService,
        EntityToModuleStateStats* entityToModuleStateStats);

virtual void runTask();

virtual bool isReadyToBeSubmitted();

virtual ~ValueTraceTask();

private:

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_GALAXIE_MODELER_SVMSGDWORKER_H_ */
