//things we store in cassandra : BrowserOffer
#ifndef CassandraReadService_h
#define CassandraReadService_h



#include "cassandra.h"


namespace gicapods { class ConfigService; }
#include "AtomicLong.h"
class CassandraDriverInterface;
class EntityToModuleStateStats;
#include <tbb/concurrent_queue.h>
#include "NomadiniTask.h"
template<class V>
class CassandraServiceTypeTraits;

#include "CassandraManagedType.h"
#include "StatisticsUtil.h"
#include "CassandraBatchInserter.h"
class AsyncThreadPoolService;
class CassandraServiceQueueManager;

class RandomUtil;
#include "Object.h"

template<class V>
class CassandraReadService : public Object {

public:
std::shared_ptr<StatisticsUtil> tracingDecisionRandomizer;
std::shared_ptr<gicapods::AtomicLong> numberOfReadFailures;
std::shared_ptr<gicapods::AtomicLong> numberOfTotalReads;
std::unique_ptr<CassandraServiceTypeTraits<V> > cassandraServiceTypeTraits;
CassandraDriverInterface* cassandraDriver;
EntityToModuleStateStats* entityToModuleStateStats;
gicapods::ConfigService* configService;
std::unique_ptr<CassandraServiceQueueManager> cassandraServiceQueueManager;

AsyncThreadPoolService* asyncThreadPoolService;

CassandraReadService(CassandraDriverInterface* cassandraDriver,
                     EntityToModuleStateStats* entityToModuleStateStats,
                     AsyncThreadPoolService* asyncThreadPoolService,
                     gicapods::ConfigService* configService);

std::shared_ptr<V> readDataOptionalFromDb(std::shared_ptr<V> value, int timeoutInMillis = 10000);

std::string getName();

void recordFailure();

void queueForPruning(std::shared_ptr<V> value);
std::shared_ptr<std::vector<std::shared_ptr<V> > > readWithPaging(int pageSize, int limit);

virtual ~CassandraReadService();
};

#endif
