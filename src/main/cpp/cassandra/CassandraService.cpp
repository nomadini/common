
#include "GUtil.h"

#include "SignalHandler.h"
#include "cassandra.h"
#include "DateTimeUtil.h"
#include "CassandraServiceTypeTraits.h"
#include "CollectionUtil.h"
#include "StringUtil.h"
#include "CassandraService.h"
#include "AsyncThreadPoolService.h"

#include "Poco/Runnable.h"

#include "EntityToModuleStateStats.h"
#include "StatisticsUtil.h"
#include "RandomUtil.h"
#include "ValueBatchUpdateTask.h"
#include "ValueTraceTask.h"
#include "AtomicLong.h"
#include "ConfigService.h"
#include "CassandraServiceQueueManager.h"
#include "CassandraManagedType.h"
#include "HttpUtil.h"
#include "JsonArrayUtil.h"
#include <thread>

template <class V>
CassandraService<V>::CassandraService(
								CassandraDriverInterface* cassandraDriver,
								EntityToModuleStateStats* entityToModuleStateStats,

								AsyncThreadPoolService* asyncThreadPoolService,
								gicapods::ConfigService* configService)  : Object(__FILE__) {

								this->cassandraDriver = cassandraDriver;
								this->entityToModuleStateStats =  entityToModuleStateStats;

								this->asyncThreadPoolService =  asyncThreadPoolService;

								cassandraRemoteServerUrl = configService->get("cassandraRemoteServerUrl");
								directDbCallEnabled = configService->getAsBooleanFromString("directDbCallEnabled");
								cassandraServiceTypeTraits = std::make_unique<CassandraServiceTypeTraits<V> > (configService);
								cassandraServiceDirectDb = std::make_unique<CassandraServiceDirectDb<V> >(
																cassandraDriver,
																entityToModuleStateStats,

																asyncThreadPoolService,
																configService);

}
template <class V>
CassandraService<V>::~CassandraService() {

}

template <class V>
void CassandraService<V>::writeBatchDataOverHttp(std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > >& arrayOfValues) {
								std::shared_ptr<V> object = nullptr;
								auto arrayOfSpecificValues =
																std::make_shared<std::vector<std::shared_ptr<V> > > ();

								for(auto valueGeneral : *arrayOfValues) {
																std::shared_ptr<V> value = std::static_pointer_cast<V>(valueGeneral);
																arrayOfSpecificValues->push_back(value);
								}
								std::string url = cassandraRemoteServerUrl+"/writeBatchData/"+ cassandraServiceTypeTraits->getValueType() + "/";
								LOG_EVERY_N(INFO, 1000)<<google::COUNTER<<"th : hitting url : "<<url;
								try {
																HttpUtil::sendPostRequest (
																								url,
																								JsonArrayUtil::convertListToJson<V>(*arrayOfSpecificValues),
																								1000,
																								10);
								} catch(...) {
																entityToModuleStateStats->addStateModuleForEntity(
																								"WRITE_DATA_FAILED" + cassandraServiceTypeTraits->getValueType(),
																								"CassandraService",
																								"ALL",
																								EntityToModuleStateStats::exception);
								}
}

template <class V>
void CassandraService<V>::writeBatchData(std::shared_ptr<std::vector<std::shared_ptr<CassandraManagedType> > >& arrayOfValues) {
								if(directDbCallEnabled) {
																cassandraServiceDirectDb->writeBatchDataToDb(arrayOfValues);
								} else {
																writeBatchDataOverHttp(arrayOfValues);
								}
}

/**
   client code calls this method read data over http
 */
template <class V>
std::shared_ptr<V> CassandraService<V>::readDataOptionalOverHttp(std::shared_ptr<V> value, int timeoutInMillis) {
								std::shared_ptr<V> object = nullptr;
								std::string dataResponse = HttpUtil::sendPostRequest (
																cassandraRemoteServerUrl+"/readDataOptional/"+ cassandraServiceTypeTraits->getValueType() + "/",
																value->toJson(),
																1000,
																1);

								if (!dataResponse.empty()) {
																object = V::fromJson(dataResponse);
								}
								return object;
}

template <class V>
std::shared_ptr<V> CassandraService<V>::readDataOptional(std::shared_ptr<V> value, int timeoutInMillis) {
								if(directDbCallEnabled) {
																return cassandraServiceDirectDb->cassandraReadService->readDataOptionalFromDb(value, timeoutInMillis);
								} else {
																return readDataOptionalOverHttp(value);
								}

}

template <class V>
void CassandraService<V>::pushToWriteBatchQueue(std::shared_ptr<CassandraManagedType> value) {
								value->valueType = cassandraServiceTypeTraits->getValueType();
								assertAndThrow(!StringUtil::equalsIgnoreCase(value->getValueType(), "CassandraManagedType"));
								cassandraServiceQueueManager->pushToWriteBatchQueue(value);
}


template <class V>
std::shared_ptr<std::vector<std::shared_ptr<V> > > CassandraService<V>::readWithPaging(int pageSize, int limit) {
								return cassandraServiceDirectDb->cassandraReadService->readWithPaging(pageSize, limit);
}

#include "AdHistory.h"
#include "FeatureDeviceHistory.h"
#include "DeviceFeatureHistory.h"
#include "IpToDeviceIdsMap.h"
#include "DeviceIdToIpsMap.h"
#include "PixelDeviceHistory.h"
#include "DeviceSegmentHistory.h"
#include "BidEventLog.h"
#include "EventLog.h"
#include "Wiretap.h"
#include "FeatureToFeatureHistory.h"
#include "FeatureToFeatureHistory.h"
#include "ModelScore.h"
#include "SegmentDevices.h"
template class CassandraService<SegmentDevices>;
template class CassandraService<FeatureToFeatureHistory>;
template class CassandraService<AdHistory>;

template class CassandraService<GicapodsIdToExchangeIdsMap>;
template class CassandraService<EventLog>;
template class CassandraService<ModelScore>;
template class CassandraService<BidEventLog>;
template class CassandraService<FeatureDeviceHistory>;
template class CassandraService<DeviceFeatureHistory>;
template class CassandraService<IpToDeviceIdsMap>;
template class CassandraService<DeviceIdToIpsMap>;
template class CassandraService<PixelDeviceHistory>;
template class CassandraService<DeviceSegmentHistory>;
template class CassandraService<Wiretap>;
