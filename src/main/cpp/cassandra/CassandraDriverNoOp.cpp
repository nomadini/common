
#include "GUtil.h"

#include "SignalHandler.h"
#include "cassandra.h"
#include "DateTimeUtil.h"

#include "CollectionUtil.h"
#include "StringUtil.h"
#include "CassandraDriverNoOp.h"
#include "DateTimeUtil.h"
#include "AtomicLong.h"
#include "Poco/Runnable.h"
#include "ConfigService.h"
#include "ConverterUtil.h"
#include "EntityToModuleStateStats.h"

CassandraDriverNoOp::CassandraDriverNoOp(){

								numberOfCuncurrentReuqestsNow = std::make_shared<gicapods::AtomicLong>();
}

CassandraDriverNoOp::~CassandraDriverNoOp() {

}

CassCluster* CassandraDriverNoOp::create_cluster(){
}

void CassandraDriverNoOp::closeSessionAndCluster(){
}
void CassandraDriverNoOp::setRequestTimeout(int timeoutInMillis){
}
int CassandraDriverNoOp::getRequestTimeout(){
								return 0;
}

CassError CassandraDriverNoOp::connect_session(CassSession *session, const CassCluster *cluster){
}


int CassandraDriverNoOp::startCassandraCluster(){
								return 0;
}

void CassandraDriverNoOp::deleteAllTestEntries(){
}

CassSession* CassandraDriverNoOp::getSession(){
								return nullptr;
}

void CassandraDriverNoOp::deleteAll(std::string tableName){
}
