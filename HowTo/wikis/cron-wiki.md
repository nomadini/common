#How to modify the cron jobs?
 open crontab with vim  using command : export VISUAL=vim; crontab -e


#Cron Jobs on R2
#midnight every 3 days
0 0 */3 * * mysqldump -u root -pmahin --all-databases | gzip > /home/workspace/mysqldumps/mysqldb_`date +%F`.sql.gz

#Cron Jobs on R3

*/5 * * * * /usr/local/spark/bin/spark-submit   --class com.dstillery.spark.jobs.ImpressionCompressor   --master local[8] --num-executors 1  /opt/spark/jobs/spark-cassandra-example-1.0-version-2.1.0-hadoop2.7-SNAPSHOT.jar   100

*/20 * * * * /usr/local/spark/bin/spark-submit   --class com.dstillery.spark.jobs.TopDeviceAndIpAndMrgs10SeenProducer   --master local[8] --num-executors 1  /opt/spark/jobs/spark-cassandra-example-1.0-version-2.1.0-hadoop2.7-SNAPSHOT.jar   100

*/20 * * * * /usr/local/spark/bin/spark-submit --class com.dstillery.spark.jobs.TopMGRS100FinderPerIabCategory   --master local[8] --num-executors 1 /opt/spark/jobs/spark-cassandra-example-1.0-version-2.1.0-hadoop2.7-SNAPSHOT.jar   100

*/20 * * * * /usr/local/spark/bin/spark-submit  --class com.dstillery.spark.jobs.PixelStatsPopulator  --master local[8]   /home/workspace/spark-cassandra/target/spark-cassandra-example-1.0-version-2.1.0-hadoop2.7-SNAPSHOT.jar \ 100
