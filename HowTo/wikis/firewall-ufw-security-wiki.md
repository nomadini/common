#How to setup firewall ?
https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-ubuntu-14-04

#How to check ports?
 ufw status  or sudo ufw status verbose

#How to allow a port
sudo ufw allow 6710

# how to allow only an IP to connect to port 22
sudo ufw allow from 192.168.0.4 to any port 22 
