

#What does a Model actually represent?
a Model contains info on how a segment is created. it's like a segment recipe.

#What is an Offer ?
an Offer is a collection of pixels to enables us to bundle pixels together and build models based on them

#What is a Pixel?
a Pixel is a coordinated code and a third party sends us and we know users belong to that category.

In our system, we never record a segment or relate a model to a pixel.Everything is connected to an OFFER.
Offer can have many PIXELs to users can bundle bunch of pixels together.


# How Segments are created ?

Rule 1.
  Every segment has is attached to a model which describes fully how it's created.

For AT segment. we have an AT-CATCH-ALL model entry. which
For NON-AT segment, each model defines what offers or seeds have been used to create the segment

# How to get all segments for a offer?
 we need to find all segments for a pixel when a pixel hits. and record the AT segments for that Offer. each Offer must have at least one corresponding AT_SEGMENT and one offer_segment_table entry.


the model that defines the segments..must have positive offer ids or negative offer ids..from there
we get the pixel ids...that the segment was created based on..


#How does segment and model entities are created for an AT pixel

when a an AT Offer is created..we create a AT_ADV_X_CATCH_ALL model(if not existing for the advertiser)
and we create a AT segment to represent all the devices that qualify for that AT
