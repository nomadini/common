#Redis is install on r3. one of the spark jobs talk to it

#how to install redis
sudo apt-get  install redis-tools  redis-server

#how to access redis shell
redis-cli

#How to make redis accept remote connections
  sudo vi /etc/redis/redis.conf
  and change bind value to 0.0.0.0
  and restart redis -> sudo systemctl restart redis-server.service

#redis Command line
  How to get all keys that maatch pattern ->   keys *
  How to get value ->  get topMGRSForIabCats_last_24hr
  How to remove redis key ->   DEL "new_mango:topMGRSForIabCats_last_24hr"
  How to debug the size of key and field ->  DEBUG OBJECT "new_mango:tag:place-tags:key"
  How to delete all keys --> FLUSHALL

#MISCONF redis to save snapshot is resolved by running
'config set stop-writes-on-bgsave-error no'
in redis-cli
