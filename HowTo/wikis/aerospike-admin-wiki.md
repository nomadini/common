#how to install and run aerospike server

How to run aerospike server :

```
wget -O aerospike.tgz 'http://aerospike.com/download/server/latest/artifact/ubuntu14'
tar -xvf aerospike.tgz && cd aerospike-server-community-3.11.1.1-ubuntu14.04
sudo ./asinstall # this will install the .deb packages
sudo service aerospike start && \
sudo tail -f /var/log/aerospike/aerospike.log | grep cake
# wait for it. "service ready: soon there will be cake!"
```

How to setup up namespaces ?
refer to Aerospike-DDL.md


#How to access shell?
GO to r2  and typ aql
example queries :
SHOW NAMESPACES
SHOW SETS
SHOW BINS
explain select * from rtb.ipsSeenV2 where PK=209.30.40.79

#how to check histogram of ttls in rtb namespace?
asinfo -h 127.0.0.1 -p 3000 -v "hist-dump:ns=rtb;hist=ttl"
