#how to setup spark ?
1. first install scala version 2.11.x
sudo apt-get update && sudo apt-get -f install && sudo apt-get remove scala && sudo apt-get install scala && scala -version

2. install maven
  sudo apt-get install maven

3. cp -r Common/thirdParty/spark-2.1.0-bin-hadoop2.7.tgz /tmp && cd /tmp && tar -xvf spark-2.1.0-bin-hadoop2.7.tgz && mv spark-2.1.0-bin-hadoop2.7  /usr/local/spark

4. add the path and verify spark works
PATH=$PATH:/usr/local/spark/bin --> add this line to your bash and source ~/.bash_profile
verify spark is working using command :  spark-shell

4-1.copy spark env and set the worker params as below, to clean up temp directories
cp /usr/local/spark/conf/spark-env.sh.template /usr/local/spark/conf/spark-env.sh
echo 'SPARK_WORKER_OPTS="-Dspark.worker.cleanup.enabled=true -Dspark.worker.cleanup.interval=120 -Dspark.worker.cleanup.appDataTtl=120"' >>  /usr/local/spark/conf/spark-env.sh

4-2. how to enable logging in sprk:
cp /usr/local/spark/conf/log4j.properties.template /usr/local/spark/conf/log4j.properties
add your logging level here
      log4j.logger.com.dstillery=DEBUG
       vi /usr/local/spark/conf/log4j.properties


5. start the master and worker
how to run cluster : first start master /usr/local/spark/sbin/start-master.sh
//find the address of master from spark logs in /usr/local/spark/logs/
how to run cluster : find the master address from the logs of start-master output
first start one worker /usr/local/spark/sbin/start-slave.sh spark://alpha3.nyc:7077

6. install the spark-cassandra ( use this as a tutorial : https://www.codementor.io/sheena/tutorials/installing-cassandra-spark-linux-debian-ubuntu-14-du107vbhx)

git clone https://github.com/datastax/spark-cassandra-connector.git && cd spark-cassandra-connector && git checkout v2.0.0-M3 && ./sbt/sbt -Dscala-2.11=true assembly

(this version works well with scala 2.11 and cassandra 3.9)
./sbt/sbt -Dscala-2.11=true assembly  (if this line fails because of some jars not found, build your spark-cassandra project to download those jars via maven by adding them to the pom.xml of spark-cassandra project and doing mvn clean install)
cp /home/workspace/spark-cassandra-connector/spark-cassandra-connector/target/full/scala-2.11/spark-cassandra-connector-assembly-2.0.0-M3.jar ~/spark-cassandra-connector-assembly-2.0.0-M3.jar
 spark-shell --jars ~/spark-cassandra-connector-assembly-2.0.0-M3.jar


6.1 SPARK UI on Mac is here : http://10.15.103.7:4040/jobs/
7. build your project and submit it to spark
    How to run apache-cassandra app ? answer : build the jar and submit it to spark
git clone https://mtaabodi@bitbucket.org/mtaabodi/spark-cassandra.git && cd spark-cassandra/ && git checkout feature/version-2.1.0-hadoop2.7 && git pull &&  mvn install

thats how you submit a job to spark :
go to r3 server and run the job
spark-submit \
  --class com.dstillery.spark.jobs.TopMGRS100FinderPerIabCategory \
  --master local[8] \
  /home/workspace/spark-cassandra/target/spark-cassandra-example-1.0-version-2.1.0-hadoop2.7-SNAPSHOT.jar \ 100

spark-submit \
  --class com.dstillery.spark.jobs.PixelStatsPopulator \
  --master local[8] \
  /home/workspace/spark-cassandra/target/spark-cassandra-example-1.0-version-2.1.0-hadoop2.7-SNAPSHOT.jar \ 100


spark sql guide : http://spark.apache.org/docs/latest/sql-programming-guide.html
spark cassandra guide : https://github.com/datastax/spark-cassandra-connector/blob/master/doc/3_selection.md
these are the funcitons you can call on rdd : http://spark.apache.org/docs/latest/programming-guide.html
how to get spark version :  spark-submit --version



How to run apache-cassandra app ? answer : build the jar and submit it to spark
cd workspace/apache-cassandra
mvn clean install

(run these commands in spark shell to query cassandra)

import com.datastax.spark.connector._, org.apache.spark.SparkContext, org.apache.spark.SparkContext._, org.apache.spark.SparkConf
sc.stop()
val conf = new SparkConf(true).set("spark.cassandra.connection.host", "10.136.46.157")
val sc = new SparkContext(conf)
val rdd = sc.cassandraTable("gicapods", "eventlog")
rdd.first
rdd.foreach(println)
val firstRow = rdd.first
firstRow.get[Int]("advertiserid")
firstRow.get[String]("deviceuseragent")

sc.cassandraTable("gicapods", "eventlog")
.select("advertiserid", "campaignid").where("adsize = ?", "300x50").foreach(println)

//sc.cassandraTable("gicapods", "eventlog").keyBy(row => (row.getString("adsize"), row.getString("adsize"))).spanByKey

#this is how you sum the counts  --> good tutorial : https://docs.datastax.com/en/latest-dse/datastax_enterprise/spark/usingSparkContext.html
val table = sc.cassandraTable("gicapods", "eventlog")
val total = table.select("winbidprice").as((c: Double) => c).sum

sc.cassandraTable("gicapods", "eventlog").select("eventid", "sitecategory").where("sitecategory = ?", "[\"IAB3-2\"]").foreach(println)


###If you're running in spark shell, you shouldn't instantiate a HiveContext,
####there's one created automatically called sqlContext (the name is misleading - if you compiled Spark with Hive, it will be a HiveContext). See similar discussion
val hc = new org.apache.spark.sql.hive.HiveContext(sc)
sqlContext.sql("select * from gicapods.adhistory")
