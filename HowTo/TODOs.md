# immediate tasks to do next

##
 * run and monitor modeler and scoring

 * create the crafted and location audience and show device count and show their relatvie probability for them and show them
   in a separate tab in segment modal in mango. also add crosswalk segment in different tab in segment modal

 * add Audience Code to pixels
 * make all spark jobs to call http to save info in influxdb (
 * make model distribution to have ttl for a model and make scoring throw up on it and show model  distribution in )
 * change targetgroup segment filter to work with segment group

 * add clustering for pixels of a client.
 * add time based buckets for feature selection(select a feature for modeling if user has seen it in an hour for example) and add an algo called TimeBucketAlpha that captures this

# Future Stuff
* add Deep Learning capability using Deep Learning 4j (in java)
* add scraping and insights for pixels and segments and geo categories
* add segments that were created based on offer in the edit offer page
* add image classificaiton based on tenser flow for each url that comes as bidding
* add StochasticSGD which you already have as another algo, There are many other classifiers
add them as other algos too
* add Support Vector Regression from shogun , you can get coeffiecients too as another alog :
http://www.shogun-toolbox.org/examples/latest/examples/regression/support_vector_regression.html

# Accomplished Tasks
* added pixel cluster chart to the edit pixel page
* add pixel stats to the UI pixel table. (by saving action event in events cassandra)

* add spark job to count pixel counts and populate mysql table pixel count
* change UI to have segment group and include and exclude, so users can include/exclude segment groups
* add validation for not having a models without seed websites
* run action recorder to record pixel info for building models (action recorder is running. modeler running)
