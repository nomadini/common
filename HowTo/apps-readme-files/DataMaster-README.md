# DataMaster

# How to run All the dataMaster tests
/home/workspace/dataMaster/Tests/Unit/Cmake/DataMasterTests.out

# How to run All the common tests
/home/workspace/Common/src/ObjectModels/Tests/Cmake/CommonTests.out


How to Run DataMaster

start cassandra : cassandra &
start aeroSpike : sudo service aerospike start
cp /var/data/conf/dataMaster-test.properties /var/data/conf/dataMaster.properties

put the dataMaster.properties content from DataMaster/SampleConfig in /var/data/conf/dataMaster-test.properties

nohup /home/workspace/DataMaster/DataMaster.out >/dev/null 2>&1 &



how to disable logging for dataMaster logs
wget http://localhost:9980/log/disableAll
wget http://localhost:9980/log/enableAll
wget http://localhost:9980/log/enableOnly/filter  //enables only the classes with name "filter" in them
wget http://localhost:9980/log/enableWith/pace
wget http://localhost:9980/log/enableOnly/request
wget http://localhost:9980/log/enableOnly/


how to build the tests in dataMaster :
cd /home/workspace/DataMaster/Tests/Unit/Cmake && ./autoBuild


how to build DataMaster :
you can run the ./autoBuild.sh  script in project.
or run the cmake command  : cmake . -G Ninja -DCMAKE_MAKE_PROGRAM=ninja && ninja

how to query for the counts in dataMaster :
mysql -h 127.0.0.1 -u root -p
mahin
use gicapods
select name, sum(count) from counter group by name;

//keep writing tests for the dataMaster!!!!
gtests arugments :


how to run all tests :
run cassandra if its not running : cassandra &
run all tests like this : /home/workspace/DataMaster/Tests/Unit/Cmake/BidderUnitTestsAll.out
/home/workspace/DataMaster/Tests/Unit/Cmake/BidderUnitTestsAll.out --gtest_filter=*passingTgWithRightDeviceIdSegment*
TODO :
use this neural network library to create models
http://leenissen.dk/fann/wp/

Characterisics of a great app :

1. every aspect needs to be monitorable
    1.1 it needs to have great dashboard that shows the states of the system(s)

2. every important feature needs to able to changed,tweaked and managed dynamically ( over http for example)

3. every feature needs to be testable and test-proof.
