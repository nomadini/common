
#How to Run Bider

run this command : `_rbd` or Bidder/Bidder.out from /home/workspace directory
then run all the dependent microservices :
1. You need PropertyServer to be running
2. You need the MaxmindHttpProxy to be running
3. You need the DataMaster to be running
4. You need the AdServer to be running


#How to find top processes by cpu and memory usage
ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%mem | head

#How to rsync over pipeline and copy data  (this is for dstillery job)
rsync -ae 'ssh -A mtaabodi@login.pipelane.net ssh -A' mtaabodi@ala1002.la.pl.pvt:/var/data/geo-data/* .

#make sure you increase the limit of open files on Bidder Server
ulimit -n 90000

#Bidder
dependent micro services :
rm /home/workspace/Common/PropertyServer/src/build/Cmake/PropertyServer.out
rm /home/workspace/Common/MaxmindHttpProxy/src/build/Cmake/MaxmindHttpProxy.out
rm /home/workspace/Common/KafkaHttpProxy/src/build/Cmake/KafkaHttpProxy.out
rm /home/workspace/Common/MetricHealthChecker/src/build/Cmake/MetricHealthChecker.out
rm /home/workspace/Common/PlaceFinderApp/src/build/Cmake/PlaceFinderApp.out

nohup /home/workspace/Common/PropertyServer/src/build/Cmake/PropertyServer.out >/dev/null 2>&1 &
nohup /home/workspace/Common/MaxmindHttpProxy/src/build/Cmake/MaxmindHttpProxy.out >/dev/null 2>&1 &

nohup /home/workspace/Common/KafkaHttpProxy/src/build/Cmake/KafkaHttpProxy.out >/dev/null 2>&1 &
nohup /home/workspace/Common/MetricHealthChecker/src/build/Cmake/MetricHealthChecker.out >/dev/null 2>&1 &

nohup /home/workspace/Common/PlaceFinderApp/src/build/Cmake/PlaceFinderApp.out >/dev/null 2>&1 &
nohup /home/workspace/DataMaster/DataMaster.out >/dev/null 2>&1 &

#this is not a required microservice anymore, but its helpful to monitor important stats
nohup /home/workspace/Common/MetricHealthChecker/src/build/Cmake/MetricHealthChecker.out >/dev/null 2>&1 &

###Dashboard
dashboard is located at : http://67.205.132.35:8141/dashboard/

grafana dashboard : http://67.205.161.25:3100/dashboard/db/bidder

Good C++ tooling blog :
http://nickdesaulniers.github.io/blog/2015/07/23/additional-c-slash-c-plus-plus-tooling/

how to start bidder forwarder and other bidders:

Bidder/Bidder.out --property bidder-forward.properties --port 9980 --appname BidderForwarder
Bidder/Bidder.out --property bidder.properties --port 9041 --appname Bidder1
Bidder/Bidder.out --property bidder.properties --port 9042 --appname Bidder2

select module,count\(\*\) from metric group by module;

#How to debug Bidder
select distinct appname, module , state from metric;

select module, state, sum(value) from metric where  timeOfRecord > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 10 SECOND)  group by module, state; ;


select module, state, sum(value) from metric where  module like '%filter%' and timeOfRecord > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 5 SECOND)  group by module, state ORDER BY sum(value) ASC;

select module , state, sum(value) from metric where appname like 'Bidder' and timeOfRecord > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 30 SECOND) group by module, state ORDER BY sum(value) ASC;

select module, state,  entity, sum(value)  from metric where appname like 'Bidder' and timeOfRecord > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 100 SECOND) group by module, state, entity  ORDER BY sum(value) ASC;

select module, state,  entity, sum(value)  from metric where appname like '%Scorer%' and timeOfRecord > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 100 SECOND) group by module, state, entity  ORDER BY sum(value) ASC;

#get all the metrics of an Bidder
select module, state,  entity, level, sum(value)  from metric where appname like 'Bidder' and timeOfRecord > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 100 SECOND) group by module, state, entity,level  ORDER BY sum(value) ASC;

#get metrics for BidderForwarder
select module, state,  entity, level, sum(value)  from metric where appname like 'BidderForwarder' and timeOfRecord > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 100 SECOND) group by module, state, entity,level  ORDER BY sum(value) ASC;

#get metrics for ExchangeSimulator
select module, state,  entity, level, sum(value)  from metric where appname like 'ExchangeSimulator' and timeOfRecord > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 100 SECOND) group by module, state, entity,level  ORDER BY sum(value) ASC;

#get all the important metrics for an bidder
select module, state,  entity, sum(value)  from metric where level = 'IMPORTANT' and appname like 'Bidder' and timeOfRecord > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 100 SECOND) group by module, state, entity ;

#get all important metrics for all apps
 select appname, module, state,  entity, sum(value)  from metric where level = 'IMPORTANT'  and timeOfRecord > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 600 SECOND) group by module, state, entity , appname ORDER BY appname, module, state, entity;

queries for dashboard :

#this will give us average  of available targetgroups that went through data reload pipeline in last 10 minute
select module , state, AVG(value) from metric where appname like 'Bidder' and state like '%OfAvailableTargetGroups%' and timeOfRecord > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 10 MINUTE)  group by state;

#this will show us BidderMainProcessor details
select module , state, sum(value) from metric where appname like 'Bidder' and module like 'BidderMainProcessor' and timeOfRecord > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 10 MINUTE)  group by state;


#this will show us the ImpressionServHandler details
select module , state, sum(value) from metric where module like 'ImpressionServHandler' and timeOfRecord > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 10 MINUTE)  group by state;

#get impressions in last 10 minutes
select * from impression where created_at  > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 10 MINUTE)  order by created_at DESC ;

#get the last 10 mintues of event log in cassandra


#Bidder --> this will show BidRequestHandler details
 select module , state, sum(value) from metric where appname like 'Bidder' and module like 'BidRequestHandler' and timeOfRecord > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 10 MINUTE)  group by state;

#Bidder --> this will show BidModeControllerService details
 select module , state, sum(value) from metric where appname like 'Bidder' and module like 'BidModeControllerService' and timeOfRecord > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 10 MINUTE)  group by state;

#get the percentage failures for each filter and tg
 select TARGET_GROUP_ID, FILTERNAME, avg(PERC_FAILURES) from TargetGroupFilterCountDbRecord where TIME_REPORTED > DATE_SUB(UTC_TIMESTAMP(), INTERVAL 100 MINUTE) group by TARGET_GROUP_ID, FILTERNAME ORDER BY avg(PERC_FAILURES);


#get the bid and win differences for the pass 100 minutes
 select tg.ID as tgId ,  numberOfBids,  sumOfBidPrices,  numberOfWins,  platformCostSpentByAdServer     from targetgroup tg     left join (       SELECT TARGET_GROUP_ID,               sum(NUM_WINS) as numberOfWins,                sum(PLATFORM_COST_SPENT) as platformCostSpentByAdServer               FROM TgWinningPerformanceMetricDto WHERE TIME_REPORTED >= DATE_SUB(UTC_TIMESTAMP(), INTERVAL 100 MINUTE) AND TIME_REPORTED <= UTC_TIMESTAMP() group by TARGET_GROUP_ID) tgw on tg.ID = tgw.TARGET_GROUP_ID  left join (SELECT TARGET_GROUP_ID, sum(NUM_BIDS) as numberOfBids, sum(SUM_BID_PRICES) as sumOfBidPrices FROM TgBiddingPerformanceMetric WHERE TIME_REPORTED >= DATE_SUB(UTC_TIMESTAMP(), INTERVAL 100 MINUTE) AND  TIME_REPORTED <= UTC_TIMESTAMP() group by TARGET_GROUP_ID)  tgb on tg.ID = tgb.TARGET_GROUP_ID group by tg.ID;

#How to easily commit different projects

cd ActionRecorder/ && git commit -am "fixing" && git push && cd ..

# How to run All the bidder tests
/home/workspace/Bidder/src/test/cpp/Cmake/BidderUnitTests.out

# How to run All the common tests
/home/workspace/Common/src/ObjectModels/Tests/Cmake/CommonTests.out


#How to Run Bidder

sudo ln -s /home/workspace/Common/include/Util/date_time_zonespec.csv /var/data/date_time_zonespec.csv
cassandra &
sudo service aerospike start
mysqld &
./Bidder.out

put the bidder.properties content from Bidder/SampleConfig in /var/data/conf/bidder-test.properties

how to disable logging for bidder logs
wget http://localhost:9980/log/disableAll
wget http://localhost:9980/log/enableAll
wget http://localhost:9980/log/enableOnly/filter  //enables only the classes with name "filter" in them
wget http://localhost:9980/log/enableWith/pace
wget http://localhost:9980/log/enableOnly/request
wget http://localhost:9980/log/enableOnly/


how to build the tests in bidder :
cd /home/workspace/Bidder/Tests/Unit/Cmake && ./autoBuild


how to build Bidder :
you can run the ./autoBuild.sh  script in project.
or run the cmake command  : cmake . -G Ninja -DCMAKE_MAKE_PROGRAM=ninja && ninja

how to query for the counts in bidder :
mysql -h 127.0.0.1 -u root -p
mahin
use gicapods
select name, sum(count) from counter group by name;

//keep writing tests for the bidder!!!!
gtests arugments :

#how to get all tests
./BidderUnitTests.out --gtest_list_tests

#How to run a set of tests
/home/workspace/Bidder/src/test/cpp/Cmake/BidderUnitTests.out --gtest_filter=TargetGroupBudgetCappingFilterModuleTests*
/home/workspace/Bidder/src/test/cpp/Cmake/BidderUnitTests.out --gtest_filter=DeviceIdSegmentModuleTest*

/home/workspace/Bidder/src/test/cpp/Cmake/BidderUnitTests.out

#how to run a single test
/home/workspace/Bidder/src/test/cpp/Cmake/BidderUnitTests.out --gtest_filter=TargetGroupBudgetCappingFilterModuleTests*.filteringCampaignsWithNoRealTimeInfo*

how to run a test in gdb with arguments

gdb ./BidderUnitTests.out
r --gtest_filter=*BlockedCreativeAttributeFilterTests.passingCreativeThatHasNoBlockingAttributes*


how to run all tests :
run cassandra if its not running : cassandra &
run all tests like this : /home/workspace/Bidder/Tests/Unit/Cmake/BidderUnitTestsAll.out
/home/workspace/Bidder/Tests/Unit/Cmake/BidderUnitTestsAll.out --gtest_filter=*passingTgWithRightDeviceIdSegment*
TODO :
use this neural network library to create models
http://leenissen.dk/fann/wp/

Characterisics of a great app :

1. every aspect needs to be monitorable
    1.1 it needs to have great dashboard that shows the states of the system(s)

2. every important feature needs to able to changed,tweaked and managed dynamically ( over http for example)

3. every feature needs to be testable and test-proof.



Heap Profiling using valgrind : http://valgrind.org/docs/manual/ms-manual.html#ms-manual.anexample
massif is faster than memcheck tool
valgrind  --tool=massif AdServer/AdServer.out
ms_print massif.out.21778
valgrind  --tool=massif Bidder/Bidder.out > leakReport.txt 2>&1 &

valgrind leak checker :
valgrind --tool=memcheck --leak-check=full --show-leak-kinds=all AdServer/AdServer.out

#run a process in background and report it to leakReport.txt
nohup valgrind --tool=memcheck --leak-check=full --show-leak-kinds=all AdServer/AdServer.out > leakReport.txt 2>&1 &

nohup valgrind --tool=memcheck --leak-check=full --show-leak-kinds=all Bidder/Bidder.out > leakReport.txt 2>&1 &


valgrind threading checker :
valgrind --tool=helgrind  AdServer/AdServer.out

How to find double free or corruption with valgrind
valgrind --tool=memcheck --track-origins=yes --keep-stacktraces=alloc-and-free Bidder/Bidder.out > leakReport.txt 2>&1




Google Performance tools :
http://goog-perftools.sourceforge.net/

how to find leaks using heap checker in gperftools:
https://gperftools.github.io/gperftools/heap_checker.html
HEAPCHECK=strict Bidder/Bidder.out
HEAPCHECK=normal Bidder/Bidder.out
pprof Bidder/Bidder.out "/tmp/Bidder.out.7596._main_-end.heap" --inuse_objects --lines --heapcheck  --edgefraction=1e-10 --nodefraction=1e-10 --text


https://gperftools.github.io/gperftools/heapprofile.html
how to run heap profiler :  HEAPPROFILE=/tmp/heapprof Bidder/Bidder.out


compare profiles after a while :
pprof --text --base=/tmp/netheap.0001.heap Bidder/Bidder.out /tmp/netheap.0005.heap
 pprof --text --base=./BidderAsyncJobsService_reloadCaches.0001.heap Bidder/Bidder.out ./BidderAsyncJobsService_reloadCaches.0036.heap

how to view data: pprof --text Bidder/Bidder.out /tmp/netheap.0001.heap
create SVG report : pprof -web Bidder/Bidder.out /tmp/netheap.0001.heap



text report explanations for gprof
The first column contains the direct memory use in MB.
The fourth column contains memory use by the procedure and all of its callees.
The second and fifth columns are just percentage representations of the numbers in the first and fourth columns.
The third column is a cumulative sum of the second column (i.e., the kth entry in the third column is the sum of the first k entries in the second column.)


DIFFERENT SPECS
Rubicon Spec: http://kb.rubiconproject.com/index.php/RTB/DSP_Best_Practices_For_DealID
http://kb.rubiconproject.com/index.php/RTB/OpenRTB#PMP_Object
Username: tony@dstillery.com
Password: Cougar99

Google Spec: https://developers.google.com/ad-exchange/rtb/downloads/realtime-bidding-proto

OpenRTB Spec 2.3: https://github.com/google/openrtb

AppNexus Spec (by request) 2.4: https://wiki.appnexus.com/pages/viewpage.action?spaceKey=adnexusdocumentation&title=OpenRTB+2.4+Bidding+Protocol
Username: bidder_prospect_july16
Password: peony


PulsePoint spec: http://docs.pulsepoint.com/display/BUYER/PulsePoint+RTB+API

Regards,
Mahmoud Taabodi


Things to do in future
*)very important, add win rate % metrics , if its too low, for an exchange, we throttle the bids

*) very important,
add specific featuresRequired fields for target groups so that we don't do RON when something is wrong with the system. and data.

*)add creativeType and adType,(as arrays) to make sure that
right creatives are being selected for banner, mobile and video

*)advertiser cost calculation and pacing by adveriserCost

*) conversion recording and optimizing the spend creatives for conversion

*)write a module in bidder to bid higher for the devices that have clicks and conversions in their adInteractions

*) convert lat and lon to mgprs 100 by 100 or 10 by 10 and store it in cassandra and build models based on that

*) put 1 percent of bids in cassandra and add report jobs in spark. to see what your bidding on

*) add bidder face app that calls the bidder, so we can take the bidder down without disrupting exchanges

*) find bad geo data using spark in cassandra and black list those devices (if they have moved more than speed of sound , or if they exist in the same lat lon more than 100 times in a day for a deviceId)

*) write a metricCompressor

*)add the incemental data reload service Capability
*)read the crosswalkedDevices for a device in scoring and write the segments for them too.(with crosswalked description- to differentiate them in reporting)
*)read crosswalkedDevices for a device in bidder and check the crosswalked segments for that device too
*)add IabSubCategory modeling feature

*)implement ip counting based last seen filter in bidder (using aerospike)
*)implement the fraud deviceId Service using spark
*)implement good pattern device id service using spark
*)implement mobile whitelist feature

*)add spark job that
parse deal for openrtb23, continue target filter for deal ,  
add device segment support in cache using aerospike and ad history and ip device deviceIp map


*) add strict UI validations for models
* * )implement the reading of crosswalked devices from cache and scoring them too, when you are scoring a device, and in bidder, read the crosswalked devices and their segments too when reading a segment.

* )have a set of always passing target groups in bidder to be able to test the adserver and pacer functionality

*) add full time and the sample part to the deviceIds so can sample them.

*) add a queue to randomUtil, so that you read the premade random numbers from a queue, it speeds up things faster.

*) add pattern extractor interface in scorer, and find users with different patters, based on the time of the feature history, and geo history and add them to segments...like isNewsJunkie? IsBrowsingNews? isSportJunkie?
isFoodie? isHungryNow? isLookingForClothes? and blah blah, and add these patterns to segments table..

*) add device make and model detection from bid requests based on 51 degrees

*)improve the datarequest handler to return only the modified data, so that we can reload the data every minute...
and send a notifiction on what was reloaded.....using entityStatus

*) make all apps dependent on the metricHealthCheck and if anything is wrong, they have to stop working specially bidding....

*) add logs of event after bid and impression and add them to hive..

send batch to scorer- more models type building - better cut off estimator



add GelocationCache for code in MySqlGeoLocationService
same for this MySqlGeoSegmentListService and MySqlInventoryService

featureDeviceHistory only for segment seeds and other whitlisted domains, and limit per day

DeviceFeatureHistory should have a limit on size per day and if exceeds should have badDeviceLists

ipDevice and DeviceIps should have a limit and if too many  should have bad Ip and bad DeviceLists


add deal discovery feature to UI and backend :
A deal discovered can have,
name, description, available impressions , bid floor, type(first party , third party), publisher name, and etc...

you get it from exchanges and show it in UI and users can select and add a client deal.
