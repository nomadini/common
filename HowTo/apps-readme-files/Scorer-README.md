# Scorer

##Scorer Architecture

ScoringMessageProcessor reads messages from Kafka queue and runs a set of modules.
These are the list of modules :

* scoredInLastNMinutesModule
* deviceFeatureFetcherModule
* calculateScoreModule
* segmentCreatorModule
* mySqlSegmentPersistenceModule
* deviceSegmentPersistenceModule
* segmentToDevicePersistenceModule

##How to run Scorer?

1.first run kafka (read the r notes on google drive)
cd ~/kafka_2.10-0.8.2.1/
nohup bin/kafka-server-start.sh config/server.properties &

2. start cassandra
cassandra &

3. build and run scorer
scons && src/Galaxie/ScorerProject/SvmSgdScorer

checkout the logs in logs/log.txt


Important TODO : 
use Kafka Manager to see topics metadata
https://github.com/yahoo/kafka-manager
