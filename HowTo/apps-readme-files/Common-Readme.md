
#how to create a new app
  1.create the files for it
  2.create the CMakeLists for it and link it and commit it in the remote server
  3.create properties for it and link it(the directory is already linked)
  4.add the entry to allAutoBuild.sh file


Commands for creating symlinks

sudo ln -s /home/workspace/common/Build/CMakeLists-ExchangeSimulator.txt /home/workspace/ExchangeSimulator/CMakeLists.txt

sudo ln -s /home/workspace/common/Config/ /var/data/conf

sudo ln -s /home/workspace/common/Build/CMakeLists-PropertyServer.txt   /home/workspace/common/PropertyServer/src/build/Cmake/CMakeLists.txt

sudo ln -s /home/workspace/common/Build/CMakeLists-MetricHealthChecker.txt   /home/workspace/common/MetricHealthChecker/src/build/Cmake/CMakeLists.txt

sudo ln -s /home/workspace/common/Build/CMakeLists-PlaceFinderApp.txt   /home/workspace/common/PlaceFinderApp/src/build/Cmake/CMakeLists.txt

sudo ln -s /home/workspace/common/Build/CMakeLists-KafkaHttpProxy.txt   /home/workspace/common/KafkaHttpProxy/src/build/Cmake/CMakeLists.txt



sudo ln -s /home/workspace/common/Build/CMakeLists-MaxmindHttpProxy.txt   /home/workspace/common/MaxmindHttpProxy/src/build/Cmake/CMakeLists.txt

sudo ln -s /home/workspace/common/Build/allAutoBuild.sh /home/workspace/common/allAutoBuild.sh
sudo ln -s /home/workspace/common/Build/asyncAllAutoBuild.sh /home/workspace/common/asyncAllAutoBuild.sh
sudo ln -s /home/workspace/common/Build/CMakeLists-Bidder-Unit-Tests.txt /home/workspace/Bidder/Tests/Unit/Cmake/CMakeLists.txt
sudo ln -s /home/workspace/common/Build/CMakeLists-Scorer-Tests.txt /home/workspace/Scorer/Tests/CMakeLists.txt
sudo ln -s /home/workspace/common/Build/CMakeLists-Common-Tests.txt /home/workspace/common/src/test/cpp/Cmake/CMakeLists.txt

sudo ln -s /home/workspace/common/Build/CMakeLists-TargetGroupProject-Tests.txt /home/workspace/targetgroup/src/test/Cmake/CMakeLists.txt

sudo ln -s /home/workspace/common/Build/CMakeLists-ActionRecorder.txt /home/workspace/ActionRecorder/CMakeLists.txt
sudo ln -s /home/workspace/common/Build/CMakeLists-Modeler.txt /home/workspace/Modeler/CMakeLists.txt
sudo ln -s /home/workspace/common/Build/CMakeLists-pixelclusterfinder.txt /home/workspace/pixelclusterfinder/CMakeLists.txt
sudo ln -s /home/workspace/common/Build/CMakeLists-targetgroup.txt /home/workspace/targetgroup/CMakeLists.txt
sudo ln -s /home/workspace/common/Build/CMakeLists-openrtbmodels.txt /home/workspace/openrtbmodels/CMakeLists.txt

sudo ln -s /home/workspace/common/Build/CMakeLists-openrtbmodels-tests.txt /home/workspace/openrtbmodels/OpenRtbModels/src/test/Cmake/CMakeLists.txt

rm /home/workspace/Scorer/CMakeLists.txt && sudo ln -s /home/workspace/common/Build/CMakeLists-Scorer.txt /home/workspace/Scorer/CMakeLists.txt

sudo ln -s /home/workspace/common/Build/CMakeLists-Bidder-Unit-Tests.txt /home/workspace/Bidder/Tests/Unit/Cmake/CMakeLists.txt


sudo ln -s /home/workspace/common/Build/CMakeLists-Bidder.txt /home/workspace/bidder/CMakeLists.txt
sudo ln -s /home/workspace/common/Build/CMakeLists-Scorer.txt /home/workspace/scorer/CMakeLists.txt
sudo ln -s /home/workspace/common/Build/CMakeLists-Modeler.txt /home/workspace/modeler/CMakeLists.txt
sudo ln -s /home/workspace/common/Build/CMakeLists-AdServer.txt /home/workspace/modeler/CMakeLists.txt

sudo ln -s /home/workspace/common/Build/CMakeLists-BidderForwarder.txt /home/workspace/BidderForwarder/CMakeLists.txt
sudo ln -s /home/workspace/common/Build/CMakeLists-Bidder.txt /home/workspace/Bidder/CMakeLists.txt
sudo ln -s /home/workspace/common/Build/CMakeLists-Bidder-Filters.txt
/home/workspace/Bidder/Bidder-1.0/Modules/Filters/Cmake/CMakeLists.txt

sudo ln -s /home/workspace/common/Build/CMakeLists-Bidder-Modules.txt /home/workspace/Bidder/Bidder-1.0/Modules/Cmake/CMakeLists.txt

sudo ln -s /home/workspace/common/Config/ /var/data/conf

#How to easily commit different projects

use this command from your workspace : bash Common/pushAll.sh "this is my commit"

#How to Run Tests ?


##Common Tests
/home/workspace/common/src/test/cpp/Cmake/CommonTests.out
/home/workspace/common/src/test/cpp/Cmake/CommonTests.out  --gtest_filter="*testWritingAndReadingAerospikeRecords*"

/home/workspace/common/src/test/cpp/Cmake/CommonTests.out  --gtest_filter="*testWritingVisitHistoryInMultipleThreads*"
/home/workspace/common/src/test/cpp/Cmake/CommonTests.out  --gtest_filter="*testHistogram*"
/home/workspace/common/src/test/cpp/Cmake/CommonTests.out   --gtest_filter="*testFilteringStructure*"
/home/workspace/common/src/test/cpp/Cmake/CommonTests.out   --gtest_filter="*testReloadingCaches*"

 /home/workspace/targetgroup/src/test/Cmake/TargetGroupTests.out   --gtest_filter="TargetGroupRecencyModelMapCacheServiceTest.testReloadingCaches"

##OpenRtb Tests
/home/workspace/openrtbmodels/OpenRtbModels/src/test/Cmake/MyOpenRtbModelTests.out



#Nomadini JIRA link
https://nodamini.atlassian.net/projects/BT/issues/BT-64?filter=allopenissues
https://nodamini.atlassian.net/browse/MOD-3
https://nodamini.atlassian.net/browse/MAN-40
https://bitbucket.org/nomadini/modeler
https://nodamini.atlassian.net/projects/BT/issues/BT-64?filter=allopenissues



Research this!! and Fb allocator for small objects
As I mention here, I've seen Intel TBB's custom STL allocator significantly improve performance of a multithreaded app simply by changing a single

std::vector<T>
to
std::vector<T,tbb::scalable_allocator<T> >
(this is a quick and convenient way of switching the allocator to use TBB's nifty thread-private heaps; see page 7 in this document)
