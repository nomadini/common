# Modeler

## How to compile and run this project ?

cmake . -G Ninja -DCMAKE_MAKE_PROGRAM=ninja && ninja

##How to run the FST library demo tests

go to this directory :

## How to build and run tests

go to this folder and run these commands.
cd /home/workspace/modeler/Modeler-1.0/Tests
./ModelerTests.out --gtest_filter="*runDemo31*"
./ModelerTests.out --gtest_filter="*runDemo30*"
./ModelerTests.out  --gtest_filter="*createARFFFileFromCassandraData*"
./ModelerTests.out --gtest_filter="*runDemo31WithOurData*"

make sure that Cassandra is running :  this is how you start cassandra server ==>  cassandra &
make sure that mysqld server is running : this is how you start mysql server -->   mysqld

go to this directory : ~/workspace/modeler/Modeler-1.0/Tests using command :  cd ~/workspace/modeler/Modeler-1.0/Tests
run this command to build all the tests :  cmake . -G Ninja -DCMAKE_MAKE_PROGRAM=ninja && ninja
and run a specific tests using this command :
./ModelerTests.out --gtest_filter=*ModelerFunctionalTests_creatingOneModel.sendSeedModelRequestAndVerifyModelWasBuilt*