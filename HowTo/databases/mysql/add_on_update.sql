#tables who need to be moved to a different schema...because they produce so much data
delete from impression;
delete from metric;
delete from TargetGroupFilterCountDbRecord;

TgBiddingPerformanceMetric
 insert into  recency_model values (1, 2, 'test1', 'test1-descr',3.45, '{"featureScores":[{"score":12.020000457763672,"name":"abc.com"},{"score":13.039999961853028,"name":"bbc.com"}]}', NOW(), NOW());
insert into advertiser_recency_model_mapvalues (1, 2, 1, 'active', NOW(), NOW());
insert into targetgroup_recency_model_map(id, targetgroup_id, recency_model_id, status) values (1, 12, 1, 'active');
