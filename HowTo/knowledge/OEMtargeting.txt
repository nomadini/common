What is it?
How does it work?
How can I see it in action?
When was this developed/updated?
How do I configure it?
What is it?
Targeting devices based on device manufacturer (aka device make), device model and device specific model (such as Samsung Galaxy S7 SM-G930V)
How does it work?
There are 4 major components of this feature - DeviceMakeModelPopulatorModule, FilterBySpendDeviceTargetCommand, Android Detection Service (aka DeviceMakeModelLookupService) and Apple Detection Service.
DeviceMakeModelPopulatorModule reads device specific model id from segment meta data, and loads device make model info from cache / MySQL. It populates this information into OpportunityContext.UserAgentModel and OpportunityContext.DeviceSpecificModel
If a device specific model is in segment meta data is not found and it's not an Apple / desktop device, then it invokes Android detection service to look up device specific model id by raw user agent string. Android detection service also sends device specific model id to UDS.
FilterBySpendDeviceTargetCommand filters out devices by comparing device make, device model and device specific model.
Apple device model is detected by matching screen sizes and device independent pixel ratios with known values for Apple mobile devices. When a creative is served (except VAST video), a JavaScript snippet is executed in the creative to gather screen information, and it calls our Apple detection endpoint in heaters with a set of query parameters. In the endpoint, Apple detection service writes looks up device specific model id from cache / MySQL and sends it to UDS.
How can I see it in action?
By querying fracture tables and cfp tables, we can see the deviceMakeId, deviceMakeName, deviceSpecificModelId and deviceSpecificName populated by the feature.
By querying segment meta data (Checkout MetaDataIndex class) from UDS, one can verify deviceSpecificModelId written to UDS.
There are 2 nodes under adserv in JMX, they measure the performance of DeviceMakeModelPopulatorModule and FiftyOneDegreesDeviceMakeModelLookupService.
There are apple_device_detection_events and apple_device_detection_exceptions nodes under heater in JMX that contain counters for Apple device detection events and exceptions.
There are apple_device_detection_events and apple_device_detection_exceptions measurements in influx adserv database.
There are DaoDeviceSpecificModelStore and DaoDeviceSpecificModelRemoteStore entries on OrbservUtil/CacheStats page.
There are events in the event log of type 22 (DEVICE_DETECTION).
