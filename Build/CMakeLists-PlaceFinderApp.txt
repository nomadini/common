cmake_minimum_required(VERSION 3.0)
project(Scorer)

SET (CMAKE_CXX_COMPILER             "/usr/bin/clang++")
SET (CMAKE_CXX_FLAGS " -std=c++1z -Wl -rdynamic -w  -g  -Wno-write-strings")
SET(CMAKE_LIBRARY_OUTPUT_DIRECTORY "/var/data/out/library") #for shared libraries
SET(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "/var/data/out/library") #for static libraries
set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE "${CMAKE_COMMAND} -E time") #prints the time of compilation

# Configure CCache if available
find_program(CCACHE_FOUND ccache)
if(CCACHE_FOUND)
        set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
        set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK ccache)
endif(CCACHE_FOUND)

include(/home/workspace/common/build/conanbuildinfo.cmake)
conan_basic_setup()

include_directories(
        "/usr/local/include/"
        "/usr/include"
        "/home/workspace/common/src/main/cpp"
        "/home/workspace/common/src/main/cpp/Util"
        "/home/workspace/common/src/main/cpp/concurrency"
        "/home/workspace/common/src/main/cpp/Util/JsonUtil"
        "/home/workspace/common/src/main/cpp/GeneralController"
"/home/workspace/common/src/main/cpp/GeneralController/DataRequest"
        "/home/workspace/common/src/main/cpp/ObjectModels"
        "/home/workspace/common/src/main/cpp/ObjectModels/common-modules"
        "/home/workspace/common/src/main/test/cpp/Helper"
        "/home/workspace/common/src/main/cpp/ObjectModels/campaign" "/home/workspace/common/src/main/cpp/ObjectModels/campaign/campaign-offer"
	"/home/workspace/common/src/main/cpp/ObjectModels/deal"
      "/home/workspace/openrtbmodels/OpenRtbModels"
      "/home/workspace/openrtbmodels/src/main/cplusplus/2_3/"
"/home/workspace/openrtbmodels/src/main/cplusplus/2_3/request-response"
"/home/workspace/openrtbmodels/src/main/cplusplus/2_3/common-models"
      "/home/workspace/common/src/main/cpp/ObjectModels/creative"
  "/home/workspace/common/src/main/cpp/ObjectModels/net"
        "/home/workspace/common/src/main/cpp/ObjectModels/modeling"
"/home/workspace/common/src/main/cpp/ObjectModels/modelScore"
"/home/workspace/common/src/main/cpp/ObjectModels/modeling/predictive"
"/home/workspace/common/src/main/cpp/ObjectModels/modeling/recencyModel"
"/home/workspace/common/src/main/cpp/ObjectModels/modeling/devicefeature"
"/home/workspace/common/src/main/cpp/ObjectModels/modeling/devicesegment"
"/home/workspace/common/src/main/cpp/ObjectModels/modeling/featuredevice"
        "/home/workspace/common/src/main/cpp/ObjectModels/OfferPixel"   "/home/workspace/common/src/main/cpp/ObjectModels/OfferPixel/map"
        "/home/workspace/openrtbmodels/OpenRtbModels"
        "/home/workspace/common/src/main/cpp/ObjectModels/segment"
"/home/workspace/common/src/main/cpp/ObjectModels/segment/offersegment"
"/home/workspace/common/src/main/cpp/ObjectModels/segment/segmentgroup"
"/home/workspace/common/src/main/cpp/ObjectModels/segment/segmentgroup_segment_map"
	    "/home/workspace/common/src/main/cpp/ObjectModels/threadPool"
        "/home/workspace/common/src/main/cpp/ObjectModels/targetgroup"
        "/home/workspace/targetgroup/src/main/cpp"
"/home/workspace/targetgroup/src/main/cpp/bidding-performance-metric"
"/home/workspace/targetgroup/src/main/cpp/bidding-performance-metric/http-request-model"
"/home/workspace/targetgroup/src/main/cpp/bidding-performance-metric/mysql"
"/home/workspace/targetgroup/src/main/cpp/bidding-performance-metric/models"
        "/home/workspace/targetgroup/src/main/cpp/winning-performance-metric"
        "/home/workspace/targetgroup/src/main/cpp/targetgroup_maps"
        "/home/workspace/targetgroup/src/main/cpp/tgStats"
        "/home/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgBwlist"
        "/home/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgCreative"
"/home/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgMgrsSegment"
        "/home/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgDaypart"

        "/home/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgGeolocation"
        "/home/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgGeosegment"
        "/home/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgGeosegmentlist"
        "/home/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgInventory"
        "/home/workspace/common/src/main/cpp/ObjectModels/segment"
"/home/workspace/common/src/main/cpp/ObjectModels/segment/offersegment"
"/home/workspace/common/src/main/cpp/ObjectModels/segment/segmentgroup"
"/home/workspace/common/src/main/cpp/ObjectModels/segment/segmentgroup_segment_map"
	    "/home/workspace/common/src/main/cpp/ObjectModels/threadPool"

"/home/workspace/targetgroup/src/main/cpp/targetgroup_maps/tgRecencyModel"
        "/home/workspace/common/src/main/cpp/ObjectModels/targetgroup-delivery-info"
        "/home/workspace/common/src/main/cpp/ObjectModels/targetgroup-delivery-info/hourly"
        "/home/workspace/common/src/main/cpp/ObjectModels/targetgroup-delivery-info/daily"
        "/home/workspace/common/src/main/cpp/ObjectModels"
        "/home/workspace/common/src/main/cpp/ObjectModels/Pacing"
        "/home/workspace/common/src/main/cpp/ObjectModels/Metric"
"/home/workspace/common/src/main/cpp/ObjectModels/validation-services"
        "/home/workspace/common/src/main/cpp/config"
        "/home/workspace/common/src/main/cpp/Kafka"
        "/home/workspace/common/src/main/cpp/cassandra"
        "/home/workspace/common/src/main/cpp/aerospike"
        "/home/workspace/common/src/main/cpp/recency" "/home/workspace/common/src/main/cpp/ObjectModels/device"
        "/home/workspace/common/src/main/cpp/mysql"
        "/home/workspace/common/src/main/cpp/mysql/targetgroup_maps"
        "/home/workspace/common/src/main/cpp/GeneralController"
"/home/workspace/common/src/main/cpp/GeneralController/DataRequest"
        "/home/workspace/common/src/main/cpp/Util/Caching"
        "/home/workspace/common/src/main/cpp/ObjectModels/adhistory"
"/home/workspace/common/src/main/cpp/ObjectModels/wiretap"

"/home/workspace/common/src/main/cpp/ObjectModels/client"
"/home/workspace/common/src/main/cpp/ObjectModels/advertiser"
"/home/workspace/common/src/main/cpp/ObjectModels/clusterer"
"/home/workspace/common/src/main/cpp/ObjectModels/clusterer"
"/home/workspace/common/src/main/cpp/ObjectModels/bad-device"
        "/home/workspace/common/src/main/cpp/ObjectModels/adserver"
        "/home/workspace/common/src/main/cpp/ObjectModels/bwlist"
"/home/workspace/common/src/main/cpp/ObjectModels/whitelistedBiddingDomains"
"/home/workspace/common/src/main/cpp/ObjectModels/whitelistedModelingDomains"

        "/home/workspace/common/src/main/cpp/ObjectModels/counter"
        "/home/workspace/common/src/main/cpp/ObjectModels/eventlog"
        "/home/workspace/common/src/main/cpp/ObjectModels/geo"
         "/home/workspace/common/src/main/cpp/ObjectModels/geo/places"
        "/home/workspace/common/src/main/cpp/ObjectModels/inventory"
        "/home/workspace/common/src/main/cpp/ObjectModels/http"
        "/home/workspace/common/src/main/cpp/ObjectModels/ipdeviceidmap"
        "/home/workspace/common/src/main/cpp/ObjectModels/GicapodsIdToExchangeIdsMap"
        "/home/workspace/common/src/main/cpp/ObjectModels/cache"
"/home/workspace/common/src/main/cpp/ObjectModels/cache-container"
"/home/workspace/common/src/main/cpp/ObjectModels/cache-container"
"/home/workspace/common/src/main/cpp/ObjectModels/cache-realtime"

        "/home/workspace/common/src/main/cpp/ObjectModels/Metric"
"/home/workspace/common/src/main/cpp/ObjectModels/validation-services"
        "/home/workspace/common/src/main/cpp/ObjectModels/common-modules"


        "/home/workspace/common/PlaceFinderApp/src/main/cpp/"
        )


        FILE(GLOB SOURCE_FILES_FOR_LIB
                "/home/workspace/common/PlaceFinderApp/src/main/cpp/*.cpp"
                )

        link_directories(
                "/usr/local/lib"
                "/usr/lib/x86_64-linux-gnu/"
                "/home/workspace/common/"
                "/usr/lib")


FILE(GLOB SOURCE_FILES
  "/home/workspace/common/PlaceFinderApp/src/main/cpp/*.cpp"
  "/home/workspace/common/PlaceFinderApp/src/main/cpp/app/*.cpp"
)
link_directories(
        "/usr/local/lib"
          "/var/data/out/library"
        "/usr/lib/x86_64-linux-gnu/"
        "/home/workspace/common/"
        "/usr/lib")

add_executable(PlaceFinderApp.out ${SOURCE_FILES})
TARGET_LINK_LIBRARIES(PlaceFinderApp.out LINK_PUBLIC
"segment"
"tgBwlist"
"tgMgrsSegment" "tgCreative"
"tgDaypart" "targetGroup"

"tgGeolocation"
"tgGeosegment"
"tgGeosegmentlist"
"tgInventory" "tgRecencyModel"
"tgRealtime"
"MyRecency"
"GeneralControllers" "NomadiniModelScoreLib" "MyCassandraLib"
"MyCampaignLib"
"MyCreativeLib"
"MyCacheContainer"
"CommonModulesLib"
"MyConfigLib"
"MyGeneralUtilLib"
"MyModelingLib"
"MyOfferPixelLib"
"MyMetricsLib"
"MyValidationServicesLib"

 "MyBadDeviceLib"
"MyCacheRealTimeLib" "Geographic"

"MyDealLib"
"segment"

"MyGeneralObjectModelsLib"
"MyEventLogLib"
"MyGeoLib"
"MyAdHistory" "wiretap"
"MyBwListLib" "MyWhitelistedBiddingDomains" "MywhitelistedModelingDomains"
"MyClientLib"
"MyAdvertiserLib" "MyClustererLib"
 "MyBadDeviceLib"
"MyCacheRealTimeLib" "Geographic"

"MyDealLib"
"tgBwlist"
"tgMgrsSegment" "tgCreative"
"MyDeviceLib"
"MyModelingLib"
"targetGroup"
"MyCreativeLib" "MyCacheContainer" "CommonModulesLib"
"MyInventoryLib"
"MyHttpLib"
"MyDeviceLib"
"MyKafkaLib"
"MyAerospike"
"aerospike"
"ev"
"dl"
"crypto"

"MySqlLib"
"NomadiniModelScoreLib" "MyCassandraLib"
"GicapodsIdToExchangeIdsMapLib"
"MyIpToDeviceIdsMapLib"
"MyConcurrencyLib" "MyNetLib" "MyDeviceLib"
"glog"
"gtest"
"gmock"
"tbb"

"unwind"
"maxminddb"
"shogun"
"nomadiniThreadPool"
"pthread"
"boost_random"
"boost_system"
"boost_thread"
"boost_regex"
"boost_program_options"
"PocoNet"
"PocoFoundation" "PocoNetSSL"

"PocoUtil"
"boost_filesystem"
"boost_date_time"
"mysqlcppconn"
"cassandra_static" "cassandra" "uv"
"curl" "ssl"
"crypto"
"rdkafka++"
"rdkafka")

#cmake . -G Ninja -DCMAKE_MAKE_PROGRAM=ninja && ninja

#        "Scorer-1.0/Tests/*.cpp"
##        "Scorer-1.0/Tests/TestCases/*.cpp"
##        "Scorer-1.0/Tests/TestCases/TestUtil/*.cpp"
#        "Scorer-1.0/LoadTester/*.cpp"
