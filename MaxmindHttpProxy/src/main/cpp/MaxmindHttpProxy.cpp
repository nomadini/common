#include "GUtil.h"
#include "BeanFactory.h"

#include "SignalHandler.h"
#include "Poco/Util/HelpFormatter.h"
#include "MaxmindHttpProxy.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/HTTPServer.h"
#include "Poco/ThreadPool.h"
#include "ConverterUtil.h"
#include <thread>
#include <boost/exception/all.hpp>
#include "EntityToModuleStateStats.h"
#include "DateTimeUtil.h"
#include "FileUtil.h"
#include "ConfigService.h"
#include "MaxmindHttpProxyRequestHandlerFactory.h"
#include "EntityToModuleStateStatsPersistenceService.h"
#include "PocoHttpServer.h"


#include <new>
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>


MaxmindHttpProxy::MaxmindHttpProxy() {

}


MaxmindHttpProxy::~MaxmindHttpProxy() {
}

void MaxmindHttpProxy::init() {
        httpServer = PocoHttpServer::createHttpServer(configService,
                                                      maxmindHttpProxyRequestHandlerFactory);
}

std::string MaxmindHttpProxy::getName() {
        return "MaxmindHttpProxy";
}
void MaxmindHttpProxy::runEveryMinute() {
        while(true) {
                PocoHttpServer::printServerInfo(httpServer);

                gicapods::Util::sleepViaBoost (_L_, 60);
        }
}
int MaxmindHttpProxy::main(const std::vector<std::string> &args) {

        this->entityToModuleStateStatsPersistenceService->startThread();
        std::thread runEveryMinuteThread (&MaxmindHttpProxy::runEveryMinute, this);
        runEveryMinuteThread.detach ();



        // start the HTTPServer
        httpServer->start ();
        // wait for CTRL-C or kill

        waitForTerminationRequest ();
        // Stop the HTTPServer
        httpServer->stop ();
        LOG(INFO)<<"server is shutting down......";
        //sleeping for 3 seconds for threads to stop
        gicapods::Util::sleepViaBoost(_L_, 3);

        LOG(INFO)<<"exiting program.";
        return Application::EXIT_OK;
}
