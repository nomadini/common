
#include "MaxmindHttpProxyRequestHandler.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>

#include "StringUtil.h"
#include "ConverterUtil.h"
#include "GUtil.h"
#include "HttpUtil.h"
#include "StatisticsUtil.h"
#include "JsonUtil.h"
#include "ExceptionUtil.h"
#include "EntityToModuleStateStats.h"

MaxmindHttpProxyRequestHandler::MaxmindHttpProxyRequestHandler(
        EntityToModuleStateStats* entityToModuleStateStats) {
        this->entityToModuleStateStats = entityToModuleStateStats;


}

void MaxmindHttpProxyRequestHandler::handleRequest(Poco::Net::HTTPServerRequest &request,
                                                   Poco::Net::HTTPServerResponse &response) {
        try {

                entityToModuleStateStats->addStateModuleForEntity ("numberOfBidsRecieved",
                                                                   "MaxmindHttpProxyRequestHandler",
                                                                   "ALL");
                auto requestInJson = HttpUtil::getRequestBody (request);
                auto doc = parseJsonSafely(requestInJson);
                std::string deviceIp = JsonUtil::GetStringSafely(*doc,"deviceIp");

                std::string deviceCity;
                std::string deviceCountry;
                std::string deviceState;
                std::string userTimeZone;
                MMDB_lookup_result_s result;
                bool resultOfReadOperation = maxMindService->readIpInfo(StringUtil::toStr(deviceIp), result);
                if (resultOfReadOperation) {

                        deviceCity = StringUtil::replaceStringCaseInsensitive(
                                maxMindService->getCity(result), "'", "");
                        deviceCountry = StringUtil::replaceStringCaseInsensitive(maxMindService->getCountryCode (result), "'", "");
                        userTimeZone = StringUtil::replaceStringCaseInsensitive(maxMindService->getTimeZone (result), "'", "");
                        deviceState = StringUtil::replaceStringCaseInsensitive(maxMindService->getStateName (result), "'", "");

                } else {
                        entityToModuleStateStats->addStateModuleForEntity ("WARNING:maxMindService_FAILED_TO_GET_IP_INFO",
                                                                           "OpportunityContextBuilderOpenRtb2_3",
                                                                           "ALL");
                }

                std::string finalResponseContent;


                auto docResponse = JsonUtil::createADcoument (rapidjson::kObjectType);
                RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

                JsonUtil::addMemberToValue_FromPair(docResponse.get(), "deviceIp", deviceIp, value);
                JsonUtil::addMemberToValue_FromPair(docResponse.get(), "deviceCountry", deviceCountry, value);
                JsonUtil::addMemberToValue_FromPair(docResponse.get(), "deviceCity", deviceCity, value);
                JsonUtil::addMemberToValue_FromPair(docResponse.get(), "deviceState", deviceState, value);
                JsonUtil::addMemberToValue_FromPair(docResponse.get(), "userTimeZone", userTimeZone, value);
                finalResponseContent = JsonUtil::valueToString (value);

                std::ostream &ostr = response.send ();
                ostr << finalResponseContent;

                MLOG_EVERY_N(INFO, 1000)<< google::COUNTER<<"th finalResponseContent : "<< finalResponseContent;


        } catch (std::exception const &e) {
                entityToModuleStateStats->addStateModuleForEntity ("Unknown Exception ",
                                                                   "MaxmindHttpProxyRequestHandler",
                                                                   "ALL");
        } catch (...) {
                entityToModuleStateStats->addStateModuleForEntity ("Unknown Exception ",
                                                                   "MaxmindHttpProxyRequestHandler",
                                                                   "ALL");
        }

        HttpUtil::setEmptyResponse(response);

}

MaxmindHttpProxyRequestHandler::~MaxmindHttpProxyRequestHandler() {
}
