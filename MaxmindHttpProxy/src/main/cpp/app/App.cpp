#include "GUtil.h"
#include "MaxmindHttpProxyApp.h"
#include <memory>
#include <string>


int main(int argc, char* argv[]) {
        auto maxmindHttpProxyApp = std::make_shared<MaxmindHttpProxyApp>();

        maxmindHttpProxyApp->startTheApp(argc, argv);
}
