#ifndef PropertyServer_H_
#define PropertyServer_H_

#include <memory>
#include <string>
#include "PropertyServerRequestHandlerFactory.h"
#include "Poco/Util/Application.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/ServerApplication.h"
#include "Object.h"
#include "AtomicBoolean.h"
class EntityToModuleStateStatsPersistenceService;
class DataReloadService;
namespace gicapods { class ConfigService; }

#include <boost/thread.hpp>
#include <thread>
class ComparatorService;
class TgFilterMeasuresService;
class BeanFactory;
class ModuleContainer;
class PropertyServer;



class PropertyServer : public std::enable_shared_from_this<PropertyServer>
        , public Poco::Util::ServerApplication
{

public:
gicapods::ConfigService* configService;
EntityToModuleStateStats* entityToModuleStateStats;
EntityToModuleStateStatsPersistenceService* entityToModuleStateStatsPersistenceService;
std::unique_ptr<BeanFactory> beanFactory;
PropertyServerRequestHandlerFactory* propertyServerRequestHandlerFactory;


PropertyServer();
virtual ~PropertyServer();

void initFactory();

std::string getName();

int main(const std::vector<std::string> &args);

private:
bool _helpRequested;
};

#endif
