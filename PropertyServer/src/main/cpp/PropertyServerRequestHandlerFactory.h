#ifndef PropertyServerRequestHandlerFactory_H
#define PropertyServerRequestHandlerFactory_H

#include <memory>
#include <string>
#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerRequest.h"
#include <unordered_map>
class OpportunityContext;
class BidRequestHandlerPipelineProcessor;
class EntityToModuleStateStats;
class TargetGroup;
class BidRequestHandler;
class CommonRequestHandlerFactory;
namespace gicapods {
class ConfigService;
class BootableConfigService;
}
class BidModeControllerService;
class TgBiddingPerformanceMetricInBiddingPeriodService;
#include "AtomicLong.h"
#include "AtomicBoolean.h"
#include "AtomicDouble.h"
#include "Poco/Logger.h"
#include "Object.h"
class PropertyServerRequestHandlerFactory;



class PropertyServerRequestHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory, public Object {

public:
EntityToModuleStateStats* entityToModuleStateStats;
gicapods::ConfigService* configService;
gicapods::BootableConfigService* bootableConfigService;

PropertyServerRequestHandlerFactory();
void init();
Poco::Net::HTTPRequestHandler *createRequestHandler(const Poco::Net::HTTPServerRequest &request);

};

#endif
