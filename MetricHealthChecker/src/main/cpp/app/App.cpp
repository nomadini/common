#include "GUtil.h"
#include "MetricHealthCheckerApp.h"
#include <memory>
#include <string>


int main(int argc, char* argv[]) {
        auto metricHealthCheckerApp = std::make_shared<MetricHealthCheckerApp>();

        metricHealthCheckerApp->startTheApp(argc, argv);
}
