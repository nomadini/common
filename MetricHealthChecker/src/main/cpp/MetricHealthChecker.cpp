#include "GUtil.h"
#include "BeanFactory.h"

#include "SignalHandler.h"
#include "Poco/Util/HelpFormatter.h"
#include "MetricHealthChecker.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/HTTPServer.h"
#include "Poco/ThreadPool.h"
#include "ConverterUtil.h"
#include <boost/exception/all.hpp>
#include "EntityToModuleStateStats.h"
#include "DateTimeUtil.h"
#include "FileUtil.h"
#include "ConfigService.h"
#include "EntityToModuleStateStatsPersistenceService.h"


#include <new>
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>


MetricHealthChecker::MetricHealthChecker(unsigned short port) {
        this->port = port;
}

MetricHealthChecker::~MetricHealthChecker() {
}

std::string MetricHealthChecker::getName() {
        return "MetricHealthChecker";
}

int MetricHealthChecker::main(const std::vector<std::string> &args) {

}
