apt-get  -y update && apt-get install  -f -y  build-essential --fix-missing
apt-get  -y update && apt-get install  -f -y  vim --fix-missing
apt-get  -y update && apt-get install  -f -y  cmake --fix-missing
apt-get  -y update && apt-get install  -f -y  curl --fix-missing
apt-get  -y update && apt-get install  -f -y   libcurl4-openssl-dev --fix-missing
apt-get  -y update && apt-get install  -f -y  software-properties-common --fix-missing
apt-get  -y update && apt-get install  -f -y  wget --fix-missing
apt-get  -y update && install -f -y wget curl
apt-get  -y update && apt-get install  -f -y  autotools-dev --fix-missing
apt-get  -y update && apt-get install  -f -y  ninja-build --fix-missing
apt-get  -y update && apt-get install  -f -y  automake --fix-missing

apt-get -y install libmysqlcppconn-dev --fix-missing
apt-get  -y update && apt-get install  -f -y   g++ --fix-missing
apt-get  -y update && apt-get install  -f -y   libunwind-dev --fix-missing
apt-get  -y update && apt-get install  -f -y  gdb --fix-missing

apt-get  -y update && apt-get install  -f -y  llvm-7 --fix-missing
apt-get  -y update && apt-get install  -f -y  binutils-gold --fix-missing
apt-get  -y update && apt-get install  -f -y  clang --fix-missing
apt-get  -y update && apt-get install  -f -y  ccache --fix-missing

apt-get  -y update && apt-get install  -f -y  git --fix-missing
apt-get  -y update && apt-get install  -f -y  libsnappy-dev --fix-missing
apt-get  -y update && apt-get install  -f -y   libgflags-dev libgoogle-glog0v5 --fix-missing
apt-get  -y update && apt-get install  -f -y   libgoogle-glog0v5 --fix-missing
apt-get  -y update && apt-get install  -f -y  python3-pip --fix-missing
apt-get  -y update && apt-get install  -f -y  libgoogle-glog-dev --fix-missing

apt-get  -y update && apt-get install  -f -y   libssl-dev --fix-missing
apt-get  -y update && apt-get install  -f -y   libuv1-dev --fix-missing
apt-get  -y update && apt-get install  -f -y   python3-setuptools --fix-missing
apt-get  -y update && apt-get install  -f -y   libshogun16 --fix-missing
apt-get  -y update && apt-get install  -f -y   libshogun-dev --fix-missing
pip3 install conan

apt-get  -y update && apt-get install  -f -y  openjdk-8-jdk --fix-missing
apt-get  -y update &&  apt-get install  -f -y   maven --fix-missing

apt-get update && apt-get install libgtest-dev && apt-get install clang libc++-dev && cd /tmp/ && git clone -b $(curl -L https://grpc.io/release) https://github.com/grpc/grpc && cd grpc && git submodule update --init && make && make install


# make sure gold is being used  (This is very important for faster linking)
mv /usr/bin/ld /usr/bin/ld-real && ln -s /usr/bin/gold /usr/bin/ld

cd /tmp/ && wget https://github.com/edenhill/librdkafka/archive/v1.2.2.tar.gz && tar -xvf v1.2.2.tar.gz && cd librdkafka-1.2.2/ && ./configure && make &&  make install && cd src-cpp/ && make && make install
cp -r /home/workspace/common/thirdParty/GeographicLib-1.46.tar.gz /tmp && cd /tmp && tar -xvf GeographicLib-1.46.tar.gz && cd GeographicLib-1.46 && mkdir BUILD && cd BUILD && cmake ..  && cmake . && make && make test && make install
apt-get install libtool &&  cp -r /home/workspace/common/thirdParty/libmaxminddb-1.2.1.tar.gz /tmp && cd /tmp && tar -xvf libmaxminddb-1.2.1.tar.gz && cd /tmp/libmaxminddb-1.2.1 && ./configure && make && make check && make install && ldconfig
# install cassandra driver
cd /tmp/ && wget https://github.com/datastax/cpp-driver/archive/2.14.0.tar.gz && tar -xvf 2.14.0.tar.gz && cd /tmp/cpp-driver-2.14.0 && cmake . && make && make install && cp -r /usr/local/lib/x86_64-linux-gnu/libcassandra* /usr/local/lib/
cp /home/workspace/common/thirdParty/yaml-cpp-0.5.3.tar.gz /tmp && cd /tmp && tar -xvf yaml-cpp-0.5.3.tar.gz && cd yaml-cpp-yaml-cpp-0.5.3/ && cmake . && make && make install


cd /home/workspace/targetgroup && sudo ln -s /home/workspace/common/Build/CMakeLists-targetgroup.txt CMakeLists.txt

pip3 install conan
conan profile update settings.compiler.libcxx=libstdc++11 default

mkdir -p /home/workspace
mkdir -p /home/workspace/common
cd /home/workspace

#make sure you add  clang to conan default like below
#     [settings]
#     os=Linux
#     os_build=Linux
#     arch=x86_64
#     arch_build=x86_64
#     compiler=Clang
#     compiler.version=7
#     compiler.libcxx=libstdc++11
#     build_type=Release
#     [options]
#     [build_requires]
#     [env]
#     CC=/usr/bin/clang
#     CXX=/usr/bin/clang++

cd /home/workspace/common
 mkdir -p build && cd build
 conan install .. -s compiler=clang -s compiler.libcxx=libstdc++11 -s compiler.version=6.0 --build missing

cd /home/workspace/common
 cmake . -G Ninja -DCMAKE_MAKE_PROGRAM=ninja && ninja
