#!/bin/bash
# declare an array called array and define 3 vales
#run this script on linux like this :   bash ./pullAll.sh
array=(ExchangeSimulator
DataMaster
Common
AdServer
Bidder
BidderForwarder
ActionRecorder
Scorer
Modeler
targetgroup
openrtbmodels
pixelclusterfinder)
for i in "${array[@]}"
do
    echo $i
    cd /home/workspace/$i
    git clean -f && git reset --hard && git pull
done
