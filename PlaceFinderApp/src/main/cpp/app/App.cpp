#include "GUtil.h"
#include "PlaceFinderApp.h"
#include <memory>
#include <string>


int main(int argc, char* argv[]) {
        auto placeFinderApp = std::make_shared<PlaceFinderApp>();

        placeFinderApp->startTheApp(argc, argv);
}
