
#include "StringUtil.h"
#include "TempUtil.h"

#include "GUtil.h"
#include "UnknownRequestHandler.h"
#include "ConverterUtil.h"
#include <boost/foreach.hpp>
#include "ConfigService.h"
#include "TargetGroup.h"
#include "PlaceFinderRequestHandler.h"
#include "PlaceFinderRequestHandlerFactory.h"
#include "NetworkUtil.h"
#include "HttpUtil.h"
PlaceFinderRequestHandlerFactory::PlaceFinderRequestHandlerFactory() : Object(__FILE__) {

}

void PlaceFinderRequestHandlerFactory::init() {
}


Poco::Net::HTTPRequestHandler
*PlaceFinderRequestHandlerFactory::createRequestHandler(const Poco::Net::HTTPServerRequest &request) {

        try {


                MLOG(10) << "PlaceFinderRequestHandlerFactory : request uri to process : " << request.getURI ();

                if (StringUtil::containsCaseInSensitive (request.getURI (), "/maxmind-data-fetch/")) {
                        auto placeFinderRequestHandler = new PlaceFinderRequestHandler (entityToModuleStateStats);
                        placeFinderRequestHandler->placeFinderModule = placeFinderModule;
                        placeFinderRequestHandler->logResponseOneInEveryXRequests = logResponseOneInEveryXRequests;
                        return placeFinderRequestHandler;
                }

        } catch (...) {
                LOG(ERROR) <<"unknow exception happened";
        }
        return new UnknownRequestHandler (entityToModuleStateStats);
}
