#ifndef PlaceFinder_H_
#define PlaceFinder_H_

#include <memory>
#include <string>
#include "PlaceFinderRequestHandlerFactory.h"
#include "Poco/Util/Application.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/ServerApplication.h"
#include "Object.h"
#include "AtomicBoolean.h"

#include "Poco/Net/HTTPServer.h"
class EntityToModuleStateStatsPersistenceService;
class DataReloadService;
namespace gicapods { class ConfigService; }

#include <boost/thread.hpp>
#include <thread>
class ComparatorService;
class TgFilterMeasuresService;
class BeanFactory;
class ModuleContainer;
class PlaceFinder;



class PlaceFinder : public std::enable_shared_from_this<PlaceFinder>
        , public Poco::Util::ServerApplication
{

public:

std::shared_ptr<Poco::Net::HTTPServer> httpServer;
gicapods::ConfigService* configService;
EntityToModuleStateStats* entityToModuleStateStats;
EntityToModuleStateStatsPersistenceService* entityToModuleStateStatsPersistenceService;
std::unique_ptr<BeanFactory> beanFactory;
PlaceFinderRequestHandlerFactory* placeFinderRequestHandlerFactory;

PlaceFinder();
virtual ~PlaceFinder();

void initFactory();
std::string getName();

int main(const std::vector<std::string> &args);

private:
bool _helpRequested;
};

#endif
