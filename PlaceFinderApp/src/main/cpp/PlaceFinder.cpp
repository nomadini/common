#include "GUtil.h"
#include "BeanFactory.h"

#include "SignalHandler.h"
#include "Poco/Util/HelpFormatter.h"
#include "PlaceFinder.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/HTTPServer.h"
#include "Poco/ThreadPool.h"
#include "ConverterUtil.h"
#include <boost/exception/all.hpp>
#include "EntityToModuleStateStats.h"
#include "DateTimeUtil.h"
#include "FileUtil.h"
#include "ConfigService.h"
#include "PlaceFinderRequestHandlerFactory.h"
#include "EntityToModuleStateStatsPersistenceService.h"
#include "PocoHttpServer.h"


#include <new>
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>


PlaceFinder::PlaceFinder() {

}

PlaceFinder::~PlaceFinder() {
}

void PlaceFinder::initFactory() {

}

std::string PlaceFinder::getName() {
        return "PlaceFinder";
}

int PlaceFinder::main(const std::vector<std::string> &args) {
        this->entityToModuleStateStatsPersistenceService->startThread();

        httpServer = PocoHttpServer::createHttpServer(configService, placeFinderRequestHandlerFactory);

        // start the HTTPServer
        httpServer->start ();
        // wait for CTRL-C or kill

        waitForTerminationRequest ();
        // Stop the HTTPServer
        httpServer->stop ();
        LOG(INFO)<<"server is shutting down......";
        //sleeping for 3 seconds for threads to stop
        gicapods::Util::sleepViaBoost(_L_, 3);

        LOG(INFO)<<"exiting program.";
        return Application::EXIT_OK;
}
