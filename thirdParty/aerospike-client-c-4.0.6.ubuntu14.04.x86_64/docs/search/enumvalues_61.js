var searchData=
[
  ['aerospike_5ferr',['AEROSPIKE_ERR',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78afa70454d71a3dd1e26c6c040fbc6c0fc',1,'as_status.h']]],
  ['aerospike_5ferr_5fasync_5fconnection',['AEROSPIKE_ERR_ASYNC_CONNECTION',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78ae38bc4dcc387395505664f43ffb6c5f3',1,'as_status.h']]],
  ['aerospike_5ferr_5fbatch_5fdisabled',['AEROSPIKE_ERR_BATCH_DISABLED',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78aa7ea48f1093f7709288f17737989da97',1,'as_status.h']]],
  ['aerospike_5ferr_5fbatch_5fmax_5frequests_5fexceeded',['AEROSPIKE_ERR_BATCH_MAX_REQUESTS_EXCEEDED',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78ab8e1673b2463f155e1015bc2e60278b9',1,'as_status.h']]],
  ['aerospike_5ferr_5fbatch_5fqueues_5ffull',['AEROSPIKE_ERR_BATCH_QUEUES_FULL',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a0db13c2f3dfdf01d620b35e0ff228cd5',1,'as_status.h']]],
  ['aerospike_5ferr_5fbin_5fexists',['AEROSPIKE_ERR_BIN_EXISTS',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a39ffdc3c9b06a2cf7a3e69a88d83bbcd',1,'as_status.h']]],
  ['aerospike_5ferr_5fbin_5fincompatible_5ftype',['AEROSPIKE_ERR_BIN_INCOMPATIBLE_TYPE',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a1965a855632931447773368a1f377088',1,'as_status.h']]],
  ['aerospike_5ferr_5fbin_5fname',['AEROSPIKE_ERR_BIN_NAME',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a5c7267050f66b0a49a71510ae27ee105',1,'as_status.h']]],
  ['aerospike_5ferr_5fbin_5fnot_5ffound',['AEROSPIKE_ERR_BIN_NOT_FOUND',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a0fd339aebc0dda11c9dd3747105a3743',1,'as_status.h']]],
  ['aerospike_5ferr_5fclient',['AEROSPIKE_ERR_CLIENT',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a3f77381acd433b0d4851b3d56a3fb11c',1,'as_status.h']]],
  ['aerospike_5ferr_5fclient_5fabort',['AEROSPIKE_ERR_CLIENT_ABORT',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a1c94d4674e403e1b6f74d1c5acd42181',1,'as_status.h']]],
  ['aerospike_5ferr_5fcluster',['AEROSPIKE_ERR_CLUSTER',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a3b2b4fd09a1772ed08ca7566a67bdccb',1,'as_status.h']]],
  ['aerospike_5ferr_5fcluster_5fchange',['AEROSPIKE_ERR_CLUSTER_CHANGE',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78af951e292245e5ebeaae12e4ffc43d388',1,'as_status.h']]],
  ['aerospike_5ferr_5fdevice_5foverload',['AEROSPIKE_ERR_DEVICE_OVERLOAD',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a2b7e650c598a0c077ae5333c657f08b7',1,'as_status.h']]],
  ['aerospike_5ferr_5ffail_5fforbidden',['AEROSPIKE_ERR_FAIL_FORBIDDEN',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a989dc5e38b566fe8ffcd41ffe59773bb',1,'as_status.h']]],
  ['aerospike_5ferr_5fgeo_5finvalid_5fgeojson',['AEROSPIKE_ERR_GEO_INVALID_GEOJSON',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a835f7f3d092888cda3dceb682c11e097',1,'as_status.h']]],
  ['aerospike_5ferr_5findex',['AEROSPIKE_ERR_INDEX',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a5ceecc5994951207733afac4c4fc200f',1,'as_status.h']]],
  ['aerospike_5ferr_5findex_5ffound',['AEROSPIKE_ERR_INDEX_FOUND',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a200b7bb08fff30b562d23e8e1828dd63',1,'as_status.h']]],
  ['aerospike_5ferr_5findex_5fmaxcount',['AEROSPIKE_ERR_INDEX_MAXCOUNT',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78ae688a67596b905bd6f9a223565b048c8',1,'as_status.h']]],
  ['aerospike_5ferr_5findex_5fname_5fmaxlen',['AEROSPIKE_ERR_INDEX_NAME_MAXLEN',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a7b3997598ab33d8b9112a665376a7bd7',1,'as_status.h']]],
  ['aerospike_5ferr_5findex_5fnot_5ffound',['AEROSPIKE_ERR_INDEX_NOT_FOUND',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a462667b833e8143df36a34d536db73e6',1,'as_status.h']]],
  ['aerospike_5ferr_5findex_5fnot_5freadable',['AEROSPIKE_ERR_INDEX_NOT_READABLE',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78afb76207e602aafb877fffb3294753568',1,'as_status.h']]],
  ['aerospike_5ferr_5findex_5foom',['AEROSPIKE_ERR_INDEX_OOM',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78adb9832aa8e281dd15458cf5ca7af5f79',1,'as_status.h']]],
  ['aerospike_5ferr_5finvalid_5fhost',['AEROSPIKE_ERR_INVALID_HOST',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78abf2dba7a2c7ee2b188c2a023e2702906',1,'as_status.h']]],
  ['aerospike_5ferr_5finvalid_5fnode',['AEROSPIKE_ERR_INVALID_NODE',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78ab82fc98735a8d777ec5e6cc22f2423d8',1,'as_status.h']]],
  ['aerospike_5ferr_5flarge_5fitem_5fnot_5ffound',['AEROSPIKE_ERR_LARGE_ITEM_NOT_FOUND',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78aea23e60937b3ebe084e7966c64a834d8',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fbin_5falready_5fexists',['AEROSPIKE_ERR_LDT_BIN_ALREADY_EXISTS',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a76502bd19cf5f6463ff9cf0d26f2739d',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fbin_5fdamaged',['AEROSPIKE_ERR_LDT_BIN_DAMAGED',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a251dbe104e7d8d65662be86cd860c595',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fbin_5fdoes_5fnot_5fexist',['AEROSPIKE_ERR_LDT_BIN_DOES_NOT_EXIST',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78aea09955933209f58cd37359da255f39e',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fbin_5fname_5fnot_5fstring',['AEROSPIKE_ERR_LDT_BIN_NAME_NOT_STRING',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78ab11109c6baddc13b4d859a80c29b2d5f',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fbin_5fname_5ftoo_5flong',['AEROSPIKE_ERR_LDT_BIN_NAME_TOO_LONG',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78af243deeade377f4e55138f133d3c672d',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fdelete',['AEROSPIKE_ERR_LDT_DELETE',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a84713d0e3b4f294abc89b1625859421c',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5ffilter_5ffunction_5fbad',['AEROSPIKE_ERR_LDT_FILTER_FUNCTION_BAD',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a2c2b50c8be00fb185b0e413176aaae19',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5ffilter_5ffunction_5fnot_5ffound',['AEROSPIKE_ERR_LDT_FILTER_FUNCTION_NOT_FOUND',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a3386b9ff6078bdc76f57d35f876a53d1',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5finput_5fparm',['AEROSPIKE_ERR_LDT_INPUT_PARM',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78ab87a754c758a220a65afba7424fd298b',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5finsert',['AEROSPIKE_ERR_LDT_INSERT',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a66dcace7b97c533b463d374f2c1ad836',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5finternal',['AEROSPIKE_ERR_LDT_INTERNAL',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78abd79f42106d780a887f36cdbf8b14f1c',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fkey_5ffunction_5fbad',['AEROSPIKE_ERR_LDT_KEY_FUNCTION_BAD',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78aef649a1a7602ef1313d4e36f9820054b',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fkey_5ffunction_5fnot_5ffound',['AEROSPIKE_ERR_LDT_KEY_FUNCTION_NOT_FOUND',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78aa1514f2028c5364776965cbf1d7e1f86',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fnot_5ffound',['AEROSPIKE_ERR_LDT_NOT_FOUND',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a96ab3cc7d93d75837ab1475cb6c64d40',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fnull_5fbin_5fname',['AEROSPIKE_ERR_LDT_NULL_BIN_NAME',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a5c534bc71286100bec58028f6173d6ce',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fsearch',['AEROSPIKE_ERR_LDT_SEARCH',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a7fd431358384ea489f83575f45144224',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fsub_5frec_5fnot_5ffound',['AEROSPIKE_ERR_LDT_SUB_REC_NOT_FOUND',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a7400da608eff98d7801e06aa2f567c8a',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fsubrec_5fclose',['AEROSPIKE_ERR_LDT_SUBREC_CLOSE',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a4b95444e8119361cc9e9e192fbe69c13',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fsubrec_5fcreate',['AEROSPIKE_ERR_LDT_SUBREC_CREATE',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a9e63c02565d22679a8cecdee5d8a89c4',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fsubrec_5fdamaged',['AEROSPIKE_ERR_LDT_SUBREC_DAMAGED',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78aa6a5b5185ba7cfb2890f84fdabe6ee60',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fsubrec_5fdelete',['AEROSPIKE_ERR_LDT_SUBREC_DELETE',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a4b1e576917f8e6637f6c336b0a581f05',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fsubrec_5fopen',['AEROSPIKE_ERR_LDT_SUBREC_OPEN',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a3de6a7c15f08879f54ab6357d3090e5f',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fsubrec_5fpool_5fdamaged',['AEROSPIKE_ERR_LDT_SUBREC_POOL_DAMAGED',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78ab39ada17ae80b69970dcfd95a5efdc33',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fsubrec_5fupdate',['AEROSPIKE_ERR_LDT_SUBREC_UPDATE',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a210611f3c3a8181ba99fa57896a868ce',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5ftoo_5fmany_5fopen_5fsubrecs',['AEROSPIKE_ERR_LDT_TOO_MANY_OPEN_SUBRECS',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78af4176e3a1fab15fac3bc096779c4410a',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5ftop_5frec_5fnot_5ffound',['AEROSPIKE_ERR_LDT_TOP_REC_NOT_FOUND',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78aa1e706e31d9d725de00ffa591e16fdc0',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5ftoprec_5fcreate',['AEROSPIKE_ERR_LDT_TOPREC_CREATE',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a4c19ed8a3e0883082af10b5579d3edfb',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5ftoprec_5fupdate',['AEROSPIKE_ERR_LDT_TOPREC_UPDATE',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78afa78bde5fc7e8b03aef46f870b6eb39a',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5ftrans_5ffunction_5fbad',['AEROSPIKE_ERR_LDT_TRANS_FUNCTION_BAD',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78abff1b90f65499c1f50b8b0332ec9e155',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5ftrans_5ffunction_5fnot_5ffound',['AEROSPIKE_ERR_LDT_TRANS_FUNCTION_NOT_FOUND',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a3afde1962005844c9e028c54dae4c678',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5ftype_5fmismatch',['AEROSPIKE_ERR_LDT_TYPE_MISMATCH',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a173e525fb109eda244488e11d82b4f8b',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5funique_5fkey',['AEROSPIKE_ERR_LDT_UNIQUE_KEY',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a2d9879dd5683c2ccba13836783c07703',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5funtrans_5ffunction_5fbad',['AEROSPIKE_ERR_LDT_UNTRANS_FUNCTION_BAD',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78ae98990ca799de502f50576a5565befa7',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5funtrans_5ffunction_5fnot_5ffound',['AEROSPIKE_ERR_LDT_UNTRANS_FUNCTION_NOT_FOUND',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a3794bcee5ffc2036e76e49157ec4da21',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fuser_5fmodule_5fbad',['AEROSPIKE_ERR_LDT_USER_MODULE_BAD',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a292c829c892935a9fc285bb45c03875d',1,'as_status.h']]],
  ['aerospike_5ferr_5fldt_5fuser_5fmodule_5fnot_5ffound',['AEROSPIKE_ERR_LDT_USER_MODULE_NOT_FOUND',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78aa92579c415ab04e5222de6bd87d041c5',1,'as_status.h']]],
  ['aerospike_5ferr_5flua_5ffile_5fnot_5ffound',['AEROSPIKE_ERR_LUA_FILE_NOT_FOUND',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a2e7416909f69a9b374543d414ce1b2f5',1,'as_status.h']]],
  ['aerospike_5ferr_5fnamespace_5fnot_5ffound',['AEROSPIKE_ERR_NAMESPACE_NOT_FOUND',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78ae19792287736977a9c95f806a8369246',1,'as_status.h']]],
  ['aerospike_5ferr_5fno_5fmore_5fconnections',['AEROSPIKE_ERR_NO_MORE_CONNECTIONS',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78af471d7743d55c338c5a57799c80fa080',1,'as_status.h']]],
  ['aerospike_5ferr_5fno_5fxdr',['AEROSPIKE_ERR_NO_XDR',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a437e7f8d08d9050f8e4ded6156298608',1,'as_status.h']]],
  ['aerospike_5ferr_5fparam',['AEROSPIKE_ERR_PARAM',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a86452b633f01b9d5af94545f708c1fbd',1,'as_status.h']]],
  ['aerospike_5ferr_5fquery',['AEROSPIKE_ERR_QUERY',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a6378b709626c1714fc9f377d09b0f849',1,'as_status.h']]],
  ['aerospike_5ferr_5fquery_5faborted',['AEROSPIKE_ERR_QUERY_ABORTED',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78ad13781efeac4ef344444dfdbc2a5c82c',1,'as_status.h']]],
  ['aerospike_5ferr_5fquery_5fqueue_5ffull',['AEROSPIKE_ERR_QUERY_QUEUE_FULL',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78af1705b9331a4e393be480c3f1cfcb2e4',1,'as_status.h']]],
  ['aerospike_5ferr_5fquery_5ftimeout',['AEROSPIKE_ERR_QUERY_TIMEOUT',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78ac76cc0f3b1965d933aefd47f35961a5c',1,'as_status.h']]],
  ['aerospike_5ferr_5frecord_5fbusy',['AEROSPIKE_ERR_RECORD_BUSY',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78aa98ed6f666c5cb695badc52f02784e62',1,'as_status.h']]],
  ['aerospike_5ferr_5frecord_5fexists',['AEROSPIKE_ERR_RECORD_EXISTS',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78afb4f582d169e62a00b4dc2c13b3ea57a',1,'as_status.h']]],
  ['aerospike_5ferr_5frecord_5fgeneration',['AEROSPIKE_ERR_RECORD_GENERATION',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a2ba0f93c8bb23627c571848b86a02473',1,'as_status.h']]],
  ['aerospike_5ferr_5frecord_5fkey_5fmismatch',['AEROSPIKE_ERR_RECORD_KEY_MISMATCH',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a4a121a2e138821859032834d71ab1474',1,'as_status.h']]],
  ['aerospike_5ferr_5frecord_5fnot_5ffound',['AEROSPIKE_ERR_RECORD_NOT_FOUND',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a8b7b440d354ec93fb4d44904dcd0fe69',1,'as_status.h']]],
  ['aerospike_5ferr_5frecord_5ftoo_5fbig',['AEROSPIKE_ERR_RECORD_TOO_BIG',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a3130ccd83c0abf9af7c5854816721bd3',1,'as_status.h']]],
  ['aerospike_5ferr_5frequest_5finvalid',['AEROSPIKE_ERR_REQUEST_INVALID',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a8fd7a0b672fd7533fc4e0e4a6474020e',1,'as_status.h']]],
  ['aerospike_5ferr_5fscan_5faborted',['AEROSPIKE_ERR_SCAN_ABORTED',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78aa7dd26b00709fdb45ee9ad9b440fa27b',1,'as_status.h']]],
  ['aerospike_5ferr_5fserver',['AEROSPIKE_ERR_SERVER',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a76803c6b319145788514bd081d7d0619',1,'as_status.h']]],
  ['aerospike_5ferr_5fserver_5ffull',['AEROSPIKE_ERR_SERVER_FULL',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a29f2ad806aea9a88f4f38a6aac1de7fa',1,'as_status.h']]],
  ['aerospike_5ferr_5ftimeout',['AEROSPIKE_ERR_TIMEOUT',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a3cabae0bac0edc455e0fd40337f02267',1,'as_status.h']]],
  ['aerospike_5ferr_5fudf',['AEROSPIKE_ERR_UDF',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a1282cf27ce30e35500ed6cdee19b7040',1,'as_status.h']]],
  ['aerospike_5ferr_5fudf_5fnot_5ffound',['AEROSPIKE_ERR_UDF_NOT_FOUND',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a054214ddb36a0736838670f149985734',1,'as_status.h']]],
  ['aerospike_5ferr_5funsupported_5ffeature',['AEROSPIKE_ERR_UNSUPPORTED_FEATURE',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78af38d5a7cfdc47c4048a3fe93f91c03f8',1,'as_status.h']]],
  ['aerospike_5fexpired_5fpassword',['AEROSPIKE_EXPIRED_PASSWORD',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a2d188b04a2a3138d068f780cab7a5bd6',1,'as_status.h']]],
  ['aerospike_5fforbidden_5fpassword',['AEROSPIKE_FORBIDDEN_PASSWORD',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a60ea25102e1b7797ab4e3a8b37b2df75',1,'as_status.h']]],
  ['aerospike_5fillegal_5fstate',['AEROSPIKE_ILLEGAL_STATE',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a481d89e4c681f17ed9f0b57eb5a60323',1,'as_status.h']]],
  ['aerospike_5finvalid_5fcommand',['AEROSPIKE_INVALID_COMMAND',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a6cda36cc8c89c6d97e9571f919f0faf2',1,'as_status.h']]],
  ['aerospike_5finvalid_5fcredential',['AEROSPIKE_INVALID_CREDENTIAL',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a12e13d9cc85fbcc0ee98bc0405b6a890',1,'as_status.h']]],
  ['aerospike_5finvalid_5ffield',['AEROSPIKE_INVALID_FIELD',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a575cde0beaed58ae4759f57bb44ffd7a',1,'as_status.h']]],
  ['aerospike_5finvalid_5fpassword',['AEROSPIKE_INVALID_PASSWORD',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a95bb36d73b371cb0a3392cbade52c6fb',1,'as_status.h']]],
  ['aerospike_5finvalid_5fprivilege',['AEROSPIKE_INVALID_PRIVILEGE',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78ab7e0a6f9a65431ebf8fa111ff0e9555c',1,'as_status.h']]],
  ['aerospike_5finvalid_5frole',['AEROSPIKE_INVALID_ROLE',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a0b39b44e4ceb0876d438a821dd1c01c3',1,'as_status.h']]],
  ['aerospike_5finvalid_5fuser',['AEROSPIKE_INVALID_USER',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78abf12586bd9eee3f2261c3d9cc3c29983',1,'as_status.h']]],
  ['aerospike_5fno_5fmore_5frecords',['AEROSPIKE_NO_MORE_RECORDS',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a57a13d971330cbb700b632716c8a0e64',1,'as_status.h']]],
  ['aerospike_5fnot_5fauthenticated',['AEROSPIKE_NOT_AUTHENTICATED',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a9b51636e8f9c8217dd38296878f376b6',1,'as_status.h']]],
  ['aerospike_5fok',['AEROSPIKE_OK',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a5dfaf5e30524f7ce1e2c8bc68530ed7a',1,'as_status.h']]],
  ['aerospike_5fquery_5fend',['AEROSPIKE_QUERY_END',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a3821aa3dfd72292fe3de74be4176c5cf',1,'as_status.h']]],
  ['aerospike_5frole_5falready_5fexists',['AEROSPIKE_ROLE_ALREADY_EXISTS',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a99d2336859a3f257088b04ffad95721f',1,'as_status.h']]],
  ['aerospike_5frole_5fviolation',['AEROSPIKE_ROLE_VIOLATION',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78ad55a9117d71f4d59d044d930717b918a',1,'as_status.h']]],
  ['aerospike_5fsecurity_5fnot_5fenabled',['AEROSPIKE_SECURITY_NOT_ENABLED',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a13f2f2154a7903cf576ec440bb3d94f8',1,'as_status.h']]],
  ['aerospike_5fsecurity_5fnot_5fsupported',['AEROSPIKE_SECURITY_NOT_SUPPORTED',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78aa158fb449bf26725ce11181d12e2d004',1,'as_status.h']]],
  ['aerospike_5fsecurity_5fscheme_5fnot_5fsupported',['AEROSPIKE_SECURITY_SCHEME_NOT_SUPPORTED',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a30716d86a9a2a58632d15038d5bec795',1,'as_status.h']]],
  ['aerospike_5fuser_5falready_5fexists',['AEROSPIKE_USER_ALREADY_EXISTS',['../dc/d42/as__status_8h.html#a3ad74d875d02385ccddd9ef171839a78a54afdd2951279a9bab6ff04c3acc3007',1,'as_status.h']]],
  ['as_5farraylist_5ferr_5falloc',['AS_ARRAYLIST_ERR_ALLOC',['../d9/d63/as__arraylist_8h.html#ad6e3dc10edafffc49fce3136716e30d5ac7a25189e1698c722ac8851fca4b3b15',1,'as_arraylist.h']]],
  ['as_5farraylist_5ferr_5findex',['AS_ARRAYLIST_ERR_INDEX',['../d9/d63/as__arraylist_8h.html#ad6e3dc10edafffc49fce3136716e30d5a6ccb82418e6718a001df79cf8dd4a2c0',1,'as_arraylist.h']]],
  ['as_5farraylist_5ferr_5fmax',['AS_ARRAYLIST_ERR_MAX',['../d9/d63/as__arraylist_8h.html#ad6e3dc10edafffc49fce3136716e30d5ac8438b4672cad6f16dab77f3e2bdf494',1,'as_arraylist.h']]],
  ['as_5farraylist_5fok',['AS_ARRAYLIST_OK',['../d9/d63/as__arraylist_8h.html#ad6e3dc10edafffc49fce3136716e30d5a589e4ab6a0c629fc4ff1d1f0af0ae4e1',1,'as_arraylist.h']]],
  ['as_5fbytes_5fblob',['AS_BYTES_BLOB',['../d0/dd4/as__bytes_8h.html#a0cf2a6a1f39668f606b19711b3a98bf3a0fa2ec9ad79adb5a98d35d81df850801',1,'as_bytes.h']]],
  ['as_5fbytes_5fcsharp',['AS_BYTES_CSHARP',['../d0/dd4/as__bytes_8h.html#a0cf2a6a1f39668f606b19711b3a98bf3a9c64b84c08b08726a680e7cc1d8b21f9',1,'as_bytes.h']]],
  ['as_5fbytes_5fdouble',['AS_BYTES_DOUBLE',['../d0/dd4/as__bytes_8h.html#a0cf2a6a1f39668f606b19711b3a98bf3abc57b21c7f760493d4be55fd30dbc277',1,'as_bytes.h']]],
  ['as_5fbytes_5ferlang',['AS_BYTES_ERLANG',['../d0/dd4/as__bytes_8h.html#a0cf2a6a1f39668f606b19711b3a98bf3a47c0b0a56e3f0547e9489f82147436ab',1,'as_bytes.h']]],
  ['as_5fbytes_5fgeojson',['AS_BYTES_GEOJSON',['../d0/dd4/as__bytes_8h.html#a0cf2a6a1f39668f606b19711b3a98bf3a68f27c219084c443170c74f3d08f236a',1,'as_bytes.h']]],
  ['as_5fbytes_5finteger',['AS_BYTES_INTEGER',['../d0/dd4/as__bytes_8h.html#a0cf2a6a1f39668f606b19711b3a98bf3a5f9f33da149f6b156a3100cf67db1936',1,'as_bytes.h']]],
  ['as_5fbytes_5fjava',['AS_BYTES_JAVA',['../d0/dd4/as__bytes_8h.html#a0cf2a6a1f39668f606b19711b3a98bf3a24e69314bb507bfef3ae8b0f7a512fee',1,'as_bytes.h']]],
  ['as_5fbytes_5fldt',['AS_BYTES_LDT',['../d0/dd4/as__bytes_8h.html#a0cf2a6a1f39668f606b19711b3a98bf3af1c5fa9c84c53bf62c77c546357ed10b',1,'as_bytes.h']]],
  ['as_5fbytes_5flist',['AS_BYTES_LIST',['../d0/dd4/as__bytes_8h.html#a0cf2a6a1f39668f606b19711b3a98bf3a153010b18135670276067dd35be13fe8',1,'as_bytes.h']]],
  ['as_5fbytes_5fmap',['AS_BYTES_MAP',['../d0/dd4/as__bytes_8h.html#a0cf2a6a1f39668f606b19711b3a98bf3ace3fa3cce49f342bfcb14af6746fbdb9',1,'as_bytes.h']]],
  ['as_5fbytes_5fphp',['AS_BYTES_PHP',['../d0/dd4/as__bytes_8h.html#a0cf2a6a1f39668f606b19711b3a98bf3ad8651857771ca322e59698bd5650dc1e',1,'as_bytes.h']]],
  ['as_5fbytes_5fpython',['AS_BYTES_PYTHON',['../d0/dd4/as__bytes_8h.html#a0cf2a6a1f39668f606b19711b3a98bf3ab825ee0b1fb2cb05582fa7d52f9d46fa',1,'as_bytes.h']]],
  ['as_5fbytes_5fruby',['AS_BYTES_RUBY',['../d0/dd4/as__bytes_8h.html#a0cf2a6a1f39668f606b19711b3a98bf3a36ab1ed89a9c6c10dba9c0a4c56fc4aa',1,'as_bytes.h']]],
  ['as_5fbytes_5fstring',['AS_BYTES_STRING',['../d0/dd4/as__bytes_8h.html#a0cf2a6a1f39668f606b19711b3a98bf3aced633c092db99e07d40384045321206',1,'as_bytes.h']]],
  ['as_5fbytes_5ftype_5fmax',['AS_BYTES_TYPE_MAX',['../d0/dd4/as__bytes_8h.html#a0cf2a6a1f39668f606b19711b3a98bf3af4140542d6b5bae834a1ecf1775a1a98',1,'as_bytes.h']]],
  ['as_5fbytes_5fundef',['AS_BYTES_UNDEF',['../d0/dd4/as__bytes_8h.html#a0cf2a6a1f39668f606b19711b3a98bf3a838ad5b636327b5ded281397383fd09b',1,'as_bytes.h']]],
  ['as_5fconnection_5ffrom_5fpool',['AS_CONNECTION_FROM_POOL',['../d5/d13/as__event__internal_8h.html#a0a117b64e0e77ea6f1240220dc656d2fad1c199efb8e4f4c3ebfcce0acf2d0d22',1,'as_event_internal.h']]],
  ['as_5fconnection_5fnew',['AS_CONNECTION_NEW',['../d5/d13/as__event__internal_8h.html#a0a117b64e0e77ea6f1240220dc656d2facd62a505654d8a8b4d5ecc7ab88e49ca',1,'as_event_internal.h']]],
  ['as_5fconnection_5ftoo_5fmany',['AS_CONNECTION_TOO_MANY',['../d5/d13/as__event__internal_8h.html#a0a117b64e0e77ea6f1240220dc656d2fa6a4be2c1853ea73418c178531f2bc889',1,'as_event_internal.h']]],
  ['as_5findex_5fgeo2dsphere',['AS_INDEX_GEO2DSPHERE',['../d7/d06/aerospike__index_8h.html#a0770157c63c5e0551112b2df5cbe2889a3942653110579f993cfcb2ac6f77d866',1,'aerospike_index.h']]],
  ['as_5findex_5fnumeric',['AS_INDEX_NUMERIC',['../d7/d06/aerospike__index_8h.html#a0770157c63c5e0551112b2df5cbe2889a2b267d91e0ae97b93bfdaf2491530f3c',1,'aerospike_index.h']]],
  ['as_5findex_5fstring',['AS_INDEX_STRING',['../d7/d06/aerospike__index_8h.html#a0770157c63c5e0551112b2df5cbe2889ae6537740ac5c705ef23de2f49c0bfb74',1,'aerospike_index.h']]],
  ['as_5findex_5ftype_5fdefault',['AS_INDEX_TYPE_DEFAULT',['../d5/db9/group__index__operations.html#ggaafc49cfd82122ec1e5113c4bfe426731a4d045aadd92832ca52d12288371c09be',1,'aerospike_index.h']]],
  ['as_5findex_5ftype_5flist',['AS_INDEX_TYPE_LIST',['../d5/db9/group__index__operations.html#ggaafc49cfd82122ec1e5113c4bfe426731a2cee5755aeded73d60124dd83c552bbb',1,'aerospike_index.h']]],
  ['as_5findex_5ftype_5fmapkeys',['AS_INDEX_TYPE_MAPKEYS',['../d5/db9/group__index__operations.html#ggaafc49cfd82122ec1e5113c4bfe426731a9a31819234e167d85702ac35743a7eac',1,'aerospike_index.h']]],
  ['as_5findex_5ftype_5fmapvalues',['AS_INDEX_TYPE_MAPVALUES',['../d5/db9/group__index__operations.html#ggaafc49cfd82122ec1e5113c4bfe426731aa001a4840ebf5626d9b6623720c56b91',1,'aerospike_index.h']]],
  ['as_5fjob_5fstatus_5fcompleted',['AS_JOB_STATUS_COMPLETED',['../db/d10/as__job_8h.html#a41be6412b953ed88808b1dc466e01779add423bfbe2b87225245d73dc9b06d792',1,'as_job.h']]],
  ['as_5fjob_5fstatus_5finprogress',['AS_JOB_STATUS_INPROGRESS',['../db/d10/as__job_8h.html#a41be6412b953ed88808b1dc466e01779a5657f0424f3afe1aa210ac6e6e0a1679',1,'as_job.h']]],
  ['as_5fjob_5fstatus_5fundef',['AS_JOB_STATUS_UNDEF',['../db/d10/as__job_8h.html#a41be6412b953ed88808b1dc466e01779a7657561501279621efe1cc9e5bd867b1',1,'as_job.h']]],
  ['as_5fldt_5fllist',['AS_LDT_LLIST',['../dd/de7/as__ldt_8h.html#a41f81b402e62910c3489569be86df97baa2a1ef128d163a3eda7a0ec73cd79576',1,'as_ldt.h']]],
  ['as_5fldt_5flmap',['AS_LDT_LMAP',['../dd/de7/as__ldt_8h.html#a41f81b402e62910c3489569be86df97ba3f1359fb15aa46a4b342e5f064bf8a75',1,'as_ldt.h']]],
  ['as_5fldt_5flset',['AS_LDT_LSET',['../dd/de7/as__ldt_8h.html#a41f81b402e62910c3489569be86df97badb1e9e5010dc6153f49f7b2b39a558c7',1,'as_ldt.h']]],
  ['as_5fldt_5flstack',['AS_LDT_LSTACK',['../dd/de7/as__ldt_8h.html#a41f81b402e62910c3489569be86df97baa66b6cecb5bc2a34d25a2f6cfaf2ed3d',1,'as_ldt.h']]],
  ['as_5flog_5flevel_5fdebug',['AS_LOG_LEVEL_DEBUG',['../df/d9b/as__log_8h.html#acb520b5e7bf0d294ffd50ac705efe591adc1b156fe30e486885116ece010b9ab2',1,'as_log.h']]],
  ['as_5flog_5flevel_5ferror',['AS_LOG_LEVEL_ERROR',['../df/d9b/as__log_8h.html#acb520b5e7bf0d294ffd50ac705efe591aa48a53f095c250eeab4984157dfc5652',1,'as_log.h']]],
  ['as_5flog_5flevel_5finfo',['AS_LOG_LEVEL_INFO',['../df/d9b/as__log_8h.html#acb520b5e7bf0d294ffd50ac705efe591a3dda4b509c4580136292996cef5e6ccf',1,'as_log.h']]],
  ['as_5flog_5flevel_5ftrace',['AS_LOG_LEVEL_TRACE',['../df/d9b/as__log_8h.html#acb520b5e7bf0d294ffd50ac705efe591a7d7bda6ad404b202b99b2e9c679108b9',1,'as_log.h']]],
  ['as_5flog_5flevel_5fwarn',['AS_LOG_LEVEL_WARN',['../df/d9b/as__log_8h.html#acb520b5e7bf0d294ffd50ac705efe591aa59f1d9c30d71535e8b09ac4a76b55d2',1,'as_log.h']]],
  ['as_5fmap_5fcreate_5fonly',['AS_MAP_CREATE_ONLY',['../d7/d94/as__map__operations_8h.html#accb22c1797f0c395d2489ecf6f7d5f1a',1,'as_map_operations.h']]],
  ['as_5fmap_5fkey_5fordered',['AS_MAP_KEY_ORDERED',['../d7/d94/as__map__operations_8h.html#a39f3c43df819f64fbde9cd07916d13e7',1,'as_map_operations.h']]],
  ['as_5fmap_5fkey_5fvalue_5fordered',['AS_MAP_KEY_VALUE_ORDERED',['../d7/d94/as__map__operations_8h.html#a7115843e3a8e39355b635ab0d9c3fb31',1,'as_map_operations.h']]],
  ['as_5fmap_5freturn_5fcount',['AS_MAP_RETURN_COUNT',['../d7/d94/as__map__operations_8h.html#afe5a04ba1430df19a1ed11bbc7a00dff',1,'as_map_operations.h']]],
  ['as_5fmap_5freturn_5findex',['AS_MAP_RETURN_INDEX',['../d7/d94/as__map__operations_8h.html#aea2ff2c5b4cd27a028ae715807604942',1,'as_map_operations.h']]],
  ['as_5fmap_5freturn_5fkey',['AS_MAP_RETURN_KEY',['../d7/d94/as__map__operations_8h.html#a2ae38422e236c123e3f1a8205eb5456b',1,'as_map_operations.h']]],
  ['as_5fmap_5freturn_5fkey_5fvalue',['AS_MAP_RETURN_KEY_VALUE',['../d7/d94/as__map__operations_8h.html#a36b3381d0d5df2185a8455930305eab2',1,'as_map_operations.h']]],
  ['as_5fmap_5freturn_5fnone',['AS_MAP_RETURN_NONE',['../d7/d94/as__map__operations_8h.html#a4417c3c8803257c1e3a15b9a434f3924',1,'as_map_operations.h']]],
  ['as_5fmap_5freturn_5frank',['AS_MAP_RETURN_RANK',['../d7/d94/as__map__operations_8h.html#ade537b06bfac12606b94ccc46c077e1e',1,'as_map_operations.h']]],
  ['as_5fmap_5freturn_5freverse_5findex',['AS_MAP_RETURN_REVERSE_INDEX',['../d7/d94/as__map__operations_8h.html#abae481d52fac201637410c1fcb7ad954',1,'as_map_operations.h']]],
  ['as_5fmap_5freturn_5freverse_5frank',['AS_MAP_RETURN_REVERSE_RANK',['../d7/d94/as__map__operations_8h.html#afea818cbf6a649535dca5d6b0ff5719b',1,'as_map_operations.h']]],
  ['as_5fmap_5freturn_5fvalue',['AS_MAP_RETURN_VALUE',['../d7/d94/as__map__operations_8h.html#a763a8199aa836d0653734f844e529992',1,'as_map_operations.h']]],
  ['as_5fmap_5funordered',['AS_MAP_UNORDERED',['../d7/d94/as__map__operations_8h.html#adb07277b5170e6cca5b93b032b2d86a5',1,'as_map_operations.h']]],
  ['as_5fmap_5fupdate',['AS_MAP_UPDATE',['../d7/d94/as__map__operations_8h.html#a05848ae1f54d05ddc0a9ce34ca33c61f',1,'as_map_operations.h']]],
  ['as_5fmap_5fupdate_5fonly',['AS_MAP_UPDATE_ONLY',['../d7/d94/as__map__operations_8h.html#ac904d7c0a6202ecdffe1b396684d41f8',1,'as_map_operations.h']]],
  ['as_5foperator_5fappend',['AS_OPERATOR_APPEND',['../d2/d6a/as__operations_8h.html#ad4155e0f960c2caeec11d878d497d861a02503da1a4dcd5d04ba673912cac01a0',1,'as_operations.h']]],
  ['as_5foperator_5fcdt_5fmodify',['AS_OPERATOR_CDT_MODIFY',['../d2/d6a/as__operations_8h.html#ad4155e0f960c2caeec11d878d497d861ac3094e2783c0606ecf6ac096ace5d12b',1,'as_operations.h']]],
  ['as_5foperator_5fcdt_5fread',['AS_OPERATOR_CDT_READ',['../d2/d6a/as__operations_8h.html#ad4155e0f960c2caeec11d878d497d861ada8bb53721ab2236a8ef2edb7d8a9bd8',1,'as_operations.h']]],
  ['as_5foperator_5fincr',['AS_OPERATOR_INCR',['../d2/d6a/as__operations_8h.html#ad4155e0f960c2caeec11d878d497d861afd55f0678b4c98536948439d169ef53f',1,'as_operations.h']]],
  ['as_5foperator_5fmap_5fmodify',['AS_OPERATOR_MAP_MODIFY',['../d2/d6a/as__operations_8h.html#ad4155e0f960c2caeec11d878d497d861af52884cdd836d42e1b362ed0a22424ca',1,'as_operations.h']]],
  ['as_5foperator_5fmap_5fread',['AS_OPERATOR_MAP_READ',['../d2/d6a/as__operations_8h.html#ad4155e0f960c2caeec11d878d497d861a20a605ffbc5408eb7c9e2aa8672e4b12',1,'as_operations.h']]],
  ['as_5foperator_5fprepend',['AS_OPERATOR_PREPEND',['../d2/d6a/as__operations_8h.html#ad4155e0f960c2caeec11d878d497d861a00ea0c85dcbfc9a0aa9aed962852c5e0',1,'as_operations.h']]],
  ['as_5foperator_5fread',['AS_OPERATOR_READ',['../d2/d6a/as__operations_8h.html#ad4155e0f960c2caeec11d878d497d861a5ffee29fbfb711c81de43be7002e8324',1,'as_operations.h']]],
  ['as_5foperator_5ftouch',['AS_OPERATOR_TOUCH',['../d2/d6a/as__operations_8h.html#ad4155e0f960c2caeec11d878d497d861a74a1bb7534427f62f9484285c4b3dbad',1,'as_operations.h']]],
  ['as_5foperator_5fwrite',['AS_OPERATOR_WRITE',['../d2/d6a/as__operations_8h.html#ad4155e0f960c2caeec11d878d497d861a6bdddcb60c45df667df06bc3f4dfd302',1,'as_operations.h']]],
  ['as_5forder_5fascending',['AS_ORDER_ASCENDING',['../d7/d76/as__query_8h.html#afb12435966b8655c5837ca90e275afdaaeef2c3b804083bf13ab4ceecf8da0c42',1,'as_query.h']]],
  ['as_5forder_5fdescending',['AS_ORDER_DESCENDING',['../d7/d76/as__query_8h.html#afb12435966b8655c5837ca90e275afdaa0fbe02510cf61883411c825fe815adf3',1,'as_query.h']]],
  ['as_5fpolicy_5fcommit_5flevel_5fall',['AS_POLICY_COMMIT_LEVEL_ALL',['../db/d65/group__client__policies.html#gga17faf52aeb845998e14ba0f3745e8f23a66072efd608201b0ea2f53b8fef602dd',1,'as_policy.h']]],
  ['as_5fpolicy_5fcommit_5flevel_5fmaster',['AS_POLICY_COMMIT_LEVEL_MASTER',['../db/d65/group__client__policies.html#gga17faf52aeb845998e14ba0f3745e8f23a4d77c84d499db5c408f079b773f4d3ec',1,'as_policy.h']]],
  ['as_5fpolicy_5fconsistency_5flevel_5fall',['AS_POLICY_CONSISTENCY_LEVEL_ALL',['../db/d65/group__client__policies.html#gga34dbe8d01c941be845145af643f9b5aba85c89551f70558d9d171688a38680a7d',1,'as_policy.h']]],
  ['as_5fpolicy_5fconsistency_5flevel_5fone',['AS_POLICY_CONSISTENCY_LEVEL_ONE',['../db/d65/group__client__policies.html#gga34dbe8d01c941be845145af643f9b5aba8ede72cf20b66de5a0ee3e4a3eafb87d',1,'as_policy.h']]],
  ['as_5fpolicy_5fexists_5fcreate',['AS_POLICY_EXISTS_CREATE',['../db/d65/group__client__policies.html#gga50b94613bcf416c9c2691c9831b89238ad16b0877851078392639872bf535d08b',1,'as_policy.h']]],
  ['as_5fpolicy_5fexists_5fcreate_5for_5freplace',['AS_POLICY_EXISTS_CREATE_OR_REPLACE',['../db/d65/group__client__policies.html#gga50b94613bcf416c9c2691c9831b89238ad6c9ed38aba0a961289fda5191c34eba',1,'as_policy.h']]],
  ['as_5fpolicy_5fexists_5fignore',['AS_POLICY_EXISTS_IGNORE',['../db/d65/group__client__policies.html#gga50b94613bcf416c9c2691c9831b89238a8da20a452160e9907cdbd8fad66d9fb4',1,'as_policy.h']]],
  ['as_5fpolicy_5fexists_5freplace',['AS_POLICY_EXISTS_REPLACE',['../db/d65/group__client__policies.html#gga50b94613bcf416c9c2691c9831b89238aca97ed3bc33c200114f01077a541fadd',1,'as_policy.h']]],
  ['as_5fpolicy_5fexists_5fupdate',['AS_POLICY_EXISTS_UPDATE',['../db/d65/group__client__policies.html#gga50b94613bcf416c9c2691c9831b89238a41cc6beea7d09bc53bf1b591130373bc',1,'as_policy.h']]],
  ['as_5fpolicy_5fgen_5feq',['AS_POLICY_GEN_EQ',['../db/d65/group__client__policies.html#gga38c1a40903e463e5d0af0141e8c64061ab82f65bef2623f650205736b7106af46',1,'as_policy.h']]],
  ['as_5fpolicy_5fgen_5fgt',['AS_POLICY_GEN_GT',['../db/d65/group__client__policies.html#gga38c1a40903e463e5d0af0141e8c64061ad76b2711b5722893fc70a34a9ff06ea6',1,'as_policy.h']]],
  ['as_5fpolicy_5fgen_5fignore',['AS_POLICY_GEN_IGNORE',['../db/d65/group__client__policies.html#gga38c1a40903e463e5d0af0141e8c64061abb948de450ad629d1673fc4895f15b4f',1,'as_policy.h']]],
  ['as_5fpolicy_5fkey_5fdigest',['AS_POLICY_KEY_DIGEST',['../db/d65/group__client__policies.html#ggaa9c8a79b2ab9d3812876c3ec5d1d50ecac47b8f04c89bf1394b5cee872fb96138',1,'as_policy.h']]],
  ['as_5fpolicy_5fkey_5fsend',['AS_POLICY_KEY_SEND',['../db/d65/group__client__policies.html#ggaa9c8a79b2ab9d3812876c3ec5d1d50ecaababc37bcffeb6ac44a51336a28e3ef1',1,'as_policy.h']]],
  ['as_5fpolicy_5freplica_5fany',['AS_POLICY_REPLICA_ANY',['../db/d65/group__client__policies.html#ggabce1fb468ee9cbfe54b7ab834cec79aba4c20e72dbdf8cd54d933c75bfd4d78d6',1,'as_policy.h']]],
  ['as_5fpolicy_5freplica_5fmaster',['AS_POLICY_REPLICA_MASTER',['../db/d65/group__client__policies.html#ggabce1fb468ee9cbfe54b7ab834cec79aba550010179f47fa409fe61cb81d5b82fe',1,'as_policy.h']]],
  ['as_5fpolicy_5fretry_5fnone',['AS_POLICY_RETRY_NONE',['../db/d65/group__client__policies.html#ggaa9730980a8b0eda8ab936a48009a6718ac0d899018f322988652a1436630b198b',1,'as_policy.h']]],
  ['as_5fpolicy_5fretry_5fonce',['AS_POLICY_RETRY_ONCE',['../db/d65/group__client__policies.html#ggaa9730980a8b0eda8ab936a48009a6718aa28b736fa9d721de6ebdb7e5995ede52',1,'as_policy.h']]],
  ['as_5fpredicate_5fequal',['AS_PREDICATE_EQUAL',['../d7/d76/as__query_8h.html#a5730b4f5c2058b9172de31d9375fbe55a6ef5e591c69a35066e00f28ad46ca172',1,'as_query.h']]],
  ['as_5fpredicate_5frange',['AS_PREDICATE_RANGE',['../d7/d76/as__query_8h.html#a5730b4f5c2058b9172de31d9375fbe55a6b87d1c341a2c3e9419eadf5aa09ef67',1,'as_query.h']]],
  ['as_5fprivilege_5fdata_5fadmin',['AS_PRIVILEGE_DATA_ADMIN',['../dd/d3f/as__admin_8h.html#a3abfbabd6287af263860154d044b44b3ac30cd476e82e90287587a3fc287da166',1,'as_admin.h']]],
  ['as_5fprivilege_5fread',['AS_PRIVILEGE_READ',['../dd/d3f/as__admin_8h.html#a3abfbabd6287af263860154d044b44b3a2b5a0adef4f81772663bcca86ac9e59a',1,'as_admin.h']]],
  ['as_5fprivilege_5fread_5fwrite',['AS_PRIVILEGE_READ_WRITE',['../dd/d3f/as__admin_8h.html#a3abfbabd6287af263860154d044b44b3acdd77740e704d7e56b995c8812150a36',1,'as_admin.h']]],
  ['as_5fprivilege_5fread_5fwrite_5fudf',['AS_PRIVILEGE_READ_WRITE_UDF',['../dd/d3f/as__admin_8h.html#a3abfbabd6287af263860154d044b44b3aa9362775801abec2913a93d34060918d',1,'as_admin.h']]],
  ['as_5fprivilege_5fsys_5fadmin',['AS_PRIVILEGE_SYS_ADMIN',['../dd/d3f/as__admin_8h.html#a3abfbabd6287af263860154d044b44b3ae654146f9bd1f5a82d6f4207b4bd9ae4',1,'as_admin.h']]],
  ['as_5fprivilege_5fuser_5fadmin',['AS_PRIVILEGE_USER_ADMIN',['../dd/d3f/as__admin_8h.html#a3abfbabd6287af263860154d044b44b3ae1f60956c1d707beb49588dd62dbfd73',1,'as_admin.h']]],
  ['as_5fscan_5fpriority_5fauto',['AS_SCAN_PRIORITY_AUTO',['../db/d31/as__scan_8h.html#af69df789f7a7a0585d455d1cb7a974d3a822442e69f22ddcaf23324427eb9ebc6',1,'as_scan.h']]],
  ['as_5fscan_5fpriority_5fhigh',['AS_SCAN_PRIORITY_HIGH',['../db/d31/as__scan_8h.html#af69df789f7a7a0585d455d1cb7a974d3a940424bc1958bdcfed9212135cdcbd8e',1,'as_scan.h']]],
  ['as_5fscan_5fpriority_5flow',['AS_SCAN_PRIORITY_LOW',['../db/d31/as__scan_8h.html#af69df789f7a7a0585d455d1cb7a974d3ae9ffba1b0d670943154dbd8454f81e1d',1,'as_scan.h']]],
  ['as_5fscan_5fpriority_5fmedium',['AS_SCAN_PRIORITY_MEDIUM',['../db/d31/as__scan_8h.html#af69df789f7a7a0585d455d1cb7a974d3ae78061140bf5a63367a7793f6d12b864',1,'as_scan.h']]],
  ['as_5fscan_5fstatus_5faborted',['AS_SCAN_STATUS_ABORTED',['../db/d31/as__scan_8h.html#a4a227b0991afa03c0ad00f4721f8dc3ead221dc37f21ef5a2616f32ee54edbdd3',1,'as_scan.h']]],
  ['as_5fscan_5fstatus_5fcompleted',['AS_SCAN_STATUS_COMPLETED',['../db/d31/as__scan_8h.html#a4a227b0991afa03c0ad00f4721f8dc3eae3ff34df1aaec688a105a4b9387bea8d',1,'as_scan.h']]],
  ['as_5fscan_5fstatus_5finprogress',['AS_SCAN_STATUS_INPROGRESS',['../db/d31/as__scan_8h.html#a4a227b0991afa03c0ad00f4721f8dc3eaa54e2916123733c1bb3769dda394f075',1,'as_scan.h']]],
  ['as_5fscan_5fstatus_5fundef',['AS_SCAN_STATUS_UNDEF',['../db/d31/as__scan_8h.html#a4a227b0991afa03c0ad00f4721f8dc3ea55a5d3619bbd07b62f778aa074806230',1,'as_scan.h']]],
  ['as_5fstream_5ferr',['AS_STREAM_ERR',['../dc/d15/as__stream_8h.html#a1d8f505a0614e7efa9f217fa4404bfc8ab8e78d885a09d4a58c3d2071a7331a09',1,'as_stream.h']]],
  ['as_5fstream_5fok',['AS_STREAM_OK',['../dc/d15/as__stream_8h.html#a1d8f505a0614e7efa9f217fa4404bfc8ac3e43c78131a1a2832f98ec81fef099c',1,'as_stream.h']]],
  ['as_5fudf_5ftype_5flua',['AS_UDF_TYPE_LUA',['../d3/d26/as__udf_8h.html#a9a6aa360bfb4382ef8a0783af2effbdbad9909e40af5159b02ef8720073575231',1,'as_udf.h']]]
];
