#!/usr/bin/env bash
################################################################################

PYMODULE=bcrypt

PACKAGE=python-bcrypt

################################################################################

if [ $EUID -ne 0 ]; then
	echo "This script requires root or sudo privileges."
	exit 1
fi

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

python <<EOF
try:
	import ${PYMODULE}
	import sys
	sys.exit(0)
except Exception as e:
	import sys
	sys.exit(1)
EOF
has_pymodule=$?

if [ $has_pymodule -eq 0 ]; then
	exit 0
fi

if [ -f /etc/os-release ]; then
	. /etc/os-release
fi

distro_id=${ID,,}
distro_version=${VERSION_ID}

case "$distro_id:$distro_version" in
	'debian:8' | 'ubuntu:'* )
		echo Installing ${PACKAGE}
		apt-get install -y ${PACKAGE}
		;;
	* )
		echo Installing python-dev
		apt-get install -y python-dev

		echo Installing python-setuptools
		apt-get install -y python-setuptools

		echo Installing pip
		easy_install pip

		echo Installing py-bcrypt
		pip install py-bcrypt
		;;
esac