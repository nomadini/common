#include "GUtil.h"
#include "StringUtil.h"
#include "KafkaHttpProxyApp.h"
#include "ConfigService.h"
#include "TempUtil.h"
#include "ConverterUtil.h"
#include "BeanFactory.h"
#include "ProgramOptionParser.h"
#include "KafkaHttpProxy.h"
#include "KafkaProducer.h"
#include <thread>


void KafkaHttpProxyApp::processArguments(
        int argc,
        char* argv[],
        std::string& propertyFileName,
        unsigned short& port,
        std::string& appName,
        std::string& logDirectory,
        std::string& appVersion) {


        //The default_value will be used when the option is not specified at all. The implicit_value will be used when the option is specific without a value. If a value is specified, it will override the default and implicit.
        namespace po = boost::program_options;
        boost::program_options::options_description desc("Allowed options");
        desc.add_options()
        // First parameter describes option name/short name
        // The second is parameter to option
        // The third is description
                ("help,h", "print usage message")
                ("appname,a",
                po::value(&appName)->default_value("KafkaHttpProxy"), "app name")
                ("property,p", boost::program_options::value(&propertyFileName)->default_value("kafkaHttpProxy.properties"), "kafkaHttpProxyApp property file name")
                ("port,t", boost::program_options::value(&port)->default_value(1), "kafkaHttpProxyApp port")
                ("version,v", boost::program_options::value(&appVersion)->default_value("SNAPSHOT"), "version")
                ("log_dir,l", boost::program_options::value(&logDirectory)->default_value("/var/log/"), "kafkaHttpProxyApp log directory location");

        boost::program_options::variables_map vm;
        boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
                std::cout << desc << "\n";
                return;
        }


        std::cout << "property = " << vm["property"].as<std::string>() << "\n";
        std::cout << "port = " << vm["port"].as<unsigned short>() << "\n";
        std::cout << "appname = " << vm["appname"].as<std::string>() << "\n";

        //THE REAL ASSIGNMENT HAPPENS HERE !!
        //THE REAL ASSIGNMENT HAPPENS HERE !!
        //THE REAL ASSIGNMENT HAPPENS HERE !!
        appName = vm["appname"].as<std::string>();
        appVersion = vm["version"].as<std::string>();
        propertyFileName = vm["property"].as<std::string>();
        port = vm["port"].as<unsigned short>();
}

void KafkaHttpProxyApp::startTheApp(int argc, char* argv[]) {


        std::string appName = "";
        std::string appVersion = "";
        std::string logDirectory = "";
        std::string propertyFileName = "kafkaHttpProxy.properties";
        unsigned short kafkaHttpProxyAppPort = 1;

        processArguments(argc,
                         argv,
                         propertyFileName,
                         kafkaHttpProxyAppPort,
                         appName,
                         logDirectory,
                         appVersion);


        TempUtil::configureLogging (appName, argv);
        LOG(ERROR) << appName <<" starting with properties "<< propertyFileName <<
                std::endl<<" original arguments : argc "<< argc << " argv : " <<  *argv;


        auto beanFactory = std::make_unique<BeanFactory>(appVersion);
        beanFactory->propertyFileName = propertyFileName;
        beanFactory->commonPropertyFileName = "common.properties";
        beanFactory->appName = appName;
        beanFactory->initializeModules();


        auto kafkaHttpProxyRequestHandlerFactory =
                std::make_shared<KafkaHttpProxyRequestHandlerFactory>();
        kafkaHttpProxyRequestHandlerFactory->kafkaProducer = beanFactory->kafkaProducer.get();
        kafkaHttpProxyRequestHandlerFactory->configService = beanFactory->configService.get();
        kafkaHttpProxyRequestHandlerFactory->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();

        auto kafkaHttpProxy = std::make_shared<KafkaHttpProxy>();
        kafkaHttpProxy->configService = beanFactory->configService.get();
        kafkaHttpProxy->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        kafkaHttpProxy->entityToModuleStateStatsPersistenceService = beanFactory->entityToModuleStateStatsPersistenceService.get();
        kafkaHttpProxy->kafkaHttpProxyRequestHandlerFactory = kafkaHttpProxyRequestHandlerFactory.get();

        kafkaHttpProxy->run(argc, argv);
}
