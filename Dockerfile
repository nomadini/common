FROM ubuntu:18.04
# this will fix the source problems for apt-get when running with docker
# Replace archive.ubuntu.com with a more trusted mirror say us.archive.ubuntu.com
RUN sed -i'' 's/archive\.ubuntu\.com/us\.archive\.ubuntu\.com/' /etc/apt/sources.list
RUN dpkg --configure -a
RUN apt-get clean
RUN apt-get update
RUN apt-get upgrade

COPY . /home/workspace/common
#we always return 0 as status code to make run not abort the operation

CMD ["/bin/bash && cd /home/workspace/common"]

#CMD ["echo "Host bitbucket.org\nStrictHostKeyChecking no" >> ~/.ssh/config"]
#CMD ["ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts"]

EXPOSE 6379
